(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/forms'), require('@angular/common'), require('angular-font-awesome'), require('ngx-mydatepicker')) :
    typeof define === 'function' && define.amd ? define('@raisedigital/ng-dynamic-forms', ['exports', '@angular/core', '@angular/forms', '@angular/common', 'angular-font-awesome', 'ngx-mydatepicker'], factory) :
    (factory((global.raisedigital = global.raisedigital || {}, global.raisedigital['ng-dynamic-forms'] = {}),global.ng.core,global.ng.forms,global.ng.common,global.angularFontAwesome,global.ngxMydatepicker));
}(this, (function (exports,core,forms,common,angularFontAwesome,ngxMydatepicker) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (b.hasOwnProperty(p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try {
                step(generator.next(value));
            }
            catch (e) {
                reject(e);
            } }
            function rejected(value) { try {
                step(generator["throw"](value));
            }
            catch (e) {
                reject(e);
            } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }
    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function () { if (t[0] & 1)
                throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f)
                throw new TypeError("Generator is already executing.");
            while (_)
                try {
                    if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                        return t;
                    if (y = 0, t)
                        op = [op[0] & 2, t.value];
                    switch (op[0]) {
                        case 0:
                        case 1:
                            t = op;
                            break;
                        case 4:
                            _.label++;
                            return { value: op[1], done: false };
                        case 5:
                            _.label++;
                            y = op[1];
                            op = [0];
                            continue;
                        case 7:
                            op = _.ops.pop();
                            _.trys.pop();
                            continue;
                        default:
                            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                _ = 0;
                                continue;
                            }
                            if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                _.label = op[1];
                                break;
                            }
                            if (op[0] === 6 && _.label < t[1]) {
                                _.label = t[1];
                                t = op;
                                break;
                            }
                            if (t && _.label < t[2]) {
                                _.label = t[2];
                                _.ops.push(op);
                                break;
                            }
                            if (t[2])
                                _.ops.pop();
                            _.trys.pop();
                            continue;
                    }
                    op = body.call(thisArg, _);
                }
                catch (e) {
                    op = [6, e];
                    y = 0;
                }
                finally {
                    f = t = 0;
                }
            if (op[0] & 5)
                throw op[1];
            return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var FieldSet = /** @class */ (function () {
        function FieldSet(set) {
            this.set = set;
        }
        /**
         * @param {?} form
         * @return {?}
         */
        FieldSet.prototype.setForm = /**
         * @param {?} form
         * @return {?}
         */
            function (form) {
                this.set.forEach(function (field) {
                    field.setForm(form);
                });
            };
        /**
         * @return {?}
         */
        FieldSet.prototype.updateValues = /**
         * @return {?}
         */
            function () {
                this.set.forEach(function (field) {
                    field.deriveValue();
                });
            };
        Object.defineProperty(FieldSet.prototype, "fields", {
            get: /**
             * @return {?}
             */ function () {
                return this.set;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} item
         * @return {?}
         */
        FieldSet.prototype.push = /**
         * @param {?} item
         * @return {?}
         */
            function (item) {
                this.set.push(item);
            };
        /**
         * @param {?} item
         * @param {?} before
         * @param {?} attr
         * @param {?=} field
         * @return {?}
         */
        FieldSet.prototype.insertBefore = /**
         * @param {?} item
         * @param {?} before
         * @param {?} attr
         * @param {?=} field
         * @return {?}
         */
            function (item, before, attr, field) {
                if (field === void 0) {
                    field = null;
                }
                /** @type {?} */
                var parent = null;
                if (field === null) {
                    /** @type {?} */
                    var search = this.fieldWithParent(before, attr);
                    parent = search.parent;
                    field = search.field;
                }
                if (parent === null) {
                    this.set.splice(this.set.indexOf(field), 0, item);
                }
                else {
                    parent.fields.insertBefore(item, before, attr, field);
                }
            };
        /**
         * @param {?} item
         * @param {?} before
         * @param {?=} field
         * @return {?}
         */
        FieldSet.prototype.insertBeforeName = /**
         * @param {?} item
         * @param {?} before
         * @param {?=} field
         * @return {?}
         */
            function (item, before, field) {
                if (field === void 0) {
                    field = null;
                }
                return this.insertBefore(item, before, 'name', field);
            };
        /**
         * @param {?} item
         * @param {?} before
         * @param {?=} field
         * @return {?}
         */
        FieldSet.prototype.insertBeforeId = /**
         * @param {?} item
         * @param {?} before
         * @param {?=} field
         * @return {?}
         */
            function (item, before, field) {
                if (field === void 0) {
                    field = null;
                }
                return this.insertBefore(item, before, 'id', field);
            };
        /**
         * @param {?} item
         * @param {?} after
         * @param {?} attr
         * @param {?=} field
         * @return {?}
         */
        FieldSet.prototype.insertAfter = /**
         * @param {?} item
         * @param {?} after
         * @param {?} attr
         * @param {?=} field
         * @return {?}
         */
            function (item, after, attr, field) {
                if (field === void 0) {
                    field = null;
                }
                /** @type {?} */
                var parent = null;
                if (field === null) {
                    /** @type {?} */
                    var search = this.fieldWithParent(after, attr);
                    parent = search.parent;
                    field = search.field;
                }
                if (parent === null) {
                    this.set.splice(this.set.indexOf(field) + 1, 0, item);
                }
                else {
                    parent.fields.insertAfter(item, after, attr, field);
                }
            };
        /**
         * @param {?} item
         * @param {?} after
         * @param {?} _attr
         * @param {?=} field
         * @return {?}
         */
        FieldSet.prototype.insertAfterName = /**
         * @param {?} item
         * @param {?} after
         * @param {?} _attr
         * @param {?=} field
         * @return {?}
         */
            function (item, after, _attr, field) {
                if (field === void 0) {
                    field = null;
                }
                this.insertAfter(item, after, 'name', field);
            };
        /**
         * @param {?} item
         * @param {?} after
         * @param {?} _attr
         * @param {?=} field
         * @return {?}
         */
        FieldSet.prototype.insertAfterId = /**
         * @param {?} item
         * @param {?} after
         * @param {?} _attr
         * @param {?=} field
         * @return {?}
         */
            function (item, after, _attr, field) {
                if (field === void 0) {
                    field = null;
                }
                this.insertAfter(item, after, 'id', field);
            };
        /**
         * @param {?} key
         * @param {?=} field
         * @param {?=} attr
         * @return {?}
         */
        FieldSet.prototype.remove = /**
         * @param {?} key
         * @param {?=} field
         * @param {?=} attr
         * @return {?}
         */
            function (key, field, attr) {
                if (field === void 0) {
                    field = null;
                }
                /** @type {?} */
                var parent = null;
                if (field === null) {
                    /** @type {?} */
                    var search = this.fieldWithParent(key, attr);
                    parent = search.parent;
                    field = search.field;
                }
                if (parent === null) {
                    this.set.splice(this.set.indexOf(field), 1);
                }
                else {
                    parent.fields.remove(key, field, attr);
                }
            };
        /**
         * @param {?} name
         * @param {?=} field
         * @return {?}
         */
        FieldSet.prototype.removeByName = /**
         * @param {?} name
         * @param {?=} field
         * @return {?}
         */
            function (name, field) {
                if (field === void 0) {
                    field = null;
                }
                this.remove(name, field, 'name');
            };
        /**
         * @param {?} id
         * @param {?=} field
         * @return {?}
         */
        FieldSet.prototype.removeById = /**
         * @param {?} id
         * @param {?=} field
         * @return {?}
         */
            function (id, field) {
                if (field === void 0) {
                    field = null;
                }
                this.remove(id, field, 'id');
            };
        /**
         * @param {?} key
         * @param {?} attr
         * @return {?}
         */
        FieldSet.prototype.field = /**
         * @param {?} key
         * @param {?} attr
         * @return {?}
         */
            function (key, attr) {
                return this.fieldWithParent(key, attr).field;
            };
        /**
         * @param {?} name
         * @return {?}
         */
        FieldSet.prototype.fieldByName = /**
         * @param {?} name
         * @return {?}
         */
            function (name) {
                return this.field(name, 'name');
            };
        /**
         * @param {?} id
         * @return {?}
         */
        FieldSet.prototype.fieldById = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                return this.field(id, 'id');
            };
        /**
         * @param {?} key
         * @param {?} attr
         * @return {?}
         */
        FieldSet.prototype.fieldWithParent = /**
         * @param {?} key
         * @param {?} attr
         * @return {?}
         */
            function (key, attr) {
                /** @type {?} */
                var field = null;
                /** @type {?} */
                var parent = null;
                /** @type {?} */
                var search = this.set.filter(function (child) { return child[attr] === key; });
                if (search.length > 0) {
                    field = search[0];
                }
                else {
                    /** @type {?} */
                    var parents = this.set.filter(function (child) { return child.hasFields === true; });
                    for (var i = 0; i < parents.length; i++) {
                        /** @type {?} */
                        var check = parents[i].fields.fieldWithParent(key, attr);
                        if (check.field != null) {
                            field = check.field;
                            parent = parents[i];
                            break;
                        }
                    }
                }
                return { field: field, parent: parent };
            };
        /**
         * @param {?} name
         * @return {?}
         */
        FieldSet.prototype.fieldByNameWithParent = /**
         * @param {?} name
         * @return {?}
         */
            function (name) {
                return this.fieldWithParent(name, 'name');
            };
        /**
         * @param {?} id
         * @return {?}
         */
        FieldSet.prototype.fieldByIdWithParent = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                return this.fieldWithParent(id, 'id');
            };
        /**
         * @return {?}
         */
        FieldSet.prototype.resetValidation = /**
         * @return {?}
         */
            function () {
                this.fields.forEach(function (field) {
                    field.resetValidation();
                });
            };
        return FieldSet;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var FormBase = /** @class */ (function () {
        function FormBase(options) {
            if (options === void 0) {
                options = {};
            }
            this.isForm = false;
            this.hasData = false;
            this.hasFields = false;
            this.hasLabel = true;
            this.isAction = false;
            this.name = options.name || '';
            this.id = this.name;
            this.classes = options.classes || [];
            this.fields = new FieldSet(options.fields || []);
            this.readOnly = options.readOnly || false;
            this.change = options.change || null;
            this.blur = options.blur || null;
        }
        /**
         * @return {?}
         */
        FormBase.prototype.getForm = /**
         * @return {?}
         */
            function () {
                return this.form;
            };
        /**
         * @param {?} form
         * @return {?}
         */
        FormBase.prototype.setForm = /**
         * @param {?} form
         * @return {?}
         */
            function (form) {
                this.form = form;
                if (form.prefix) {
                    this.id = "" + form.prefix + this.name;
                }
                if (!this.isForm) {
                    this.fields.setForm(form);
                }
            };
        /**
         * @return {?}
         */
        FormBase.prototype.deriveValue = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2 /*return*/, Promise.resolve(false)];
                    });
                });
            };
        Object.defineProperty(FormBase.prototype, "control", {
            get: /**
             * @return {?}
             */ function () {
                return null;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} value
         * @return {?}
         */
        FormBase.prototype.onChange = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                /** @type {?} */
                var propagate = true;
                if (this.change != null) {
                    propagate = this.change(this, value);
                }
                if (propagate) {
                    this.form.fieldChange(this.name, value);
                }
            };
        /**
         * @param {?} value
         * @return {?}
         */
        FormBase.prototype.onBlur = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                if (this.blur != null) {
                    this.blur(this, value);
                }
            };
        /**
         * @return {?}
         */
        FormBase.prototype.onClick = /**
         * @return {?}
         */
            function () {
            };
        /**
         * @param {?} message
         * @return {?}
         */
        FormBase.prototype.setMessage = /**
         * @param {?} message
         * @return {?}
         */
            function (message) {
                this.message = message;
            };
        /**
         * @param {?} result
         * @return {?}
         */
        FormBase.prototype.loadValidation = /**
         * @param {?} result
         * @return {?}
         */
            function (result) {
                this.resetValidation();
                if (result.messages != null && result.messages.length > 0) {
                    this.setMessage(result.messages[0]);
                }
                for (var key in result.fields) {
                    if (result.fields.hasOwnProperty(key)) {
                        /** @type {?} */
                        var field = this.fields.fieldByName(key);
                        if (field != null) {
                            field.loadValidation(result.fields[key]);
                        }
                    }
                }
            };
        /**
         * @return {?}
         */
        FormBase.prototype.resetValidation = /**
         * @return {?}
         */
            function () {
                this.setMessage(null);
                if (this.fields) {
                    this.fields.resetValidation();
                }
            };
        return FormBase;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    /**
     * @template T
     */
    var /**
     * @template T
     */ Field = /** @class */ (function (_super) {
        __extends(Field, _super);
        function Field(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.hasData = true;
            _this.value = options.value;
            _this.label = options.label || '';
            _this.classes.push('ndf-field');
            _this.placeholder = options.placeholder || '';
            if (_this.placeholder.length > 0) {
                _this.classes.push('ndf-placeholder');
            }
            _this.derive = options.derive || null;
            return _this;
        }
        /**
         * @param {?} form
         * @return {?}
         */
        Field.prototype.setForm = /**
         * @param {?} form
         * @return {?}
         */
            function (form) {
                _super.prototype.setForm.call(this, form);
                this.deriveValue();
            };
        /**
         * @param {?} value
         * @return {?}
         */
        Field.prototype.onChange = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.value = value;
                _super.prototype.onChange.call(this, this.prepareValue(value));
            };
        /**
         * @return {?}
         */
        Field.prototype.deriveValue = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var _this = this;
                    return __generator(this, function (_a) {
                        return [2 /*return*/, _super.prototype.deriveValue.call(this).then(function (derived) {
                                if (!derived) {
                                    if (_this.derive != null) {
                                        return _this.derive(_this);
                                    }
                                    else if (_this.form.obj.hasOwnProperty(_this.name)) {
                                        _this.value = _this.form.obj[_this.name];
                                        return Promise.resolve(true);
                                    }
                                }
                                return Promise.resolve(false);
                            })];
                    });
                });
            };
        /**
         * @param {?} value
         * @return {?}
         */
        Field.prototype.prepareValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                if (value == null) {
                    return '';
                }
                else {
                    return value;
                }
            };
        return Field;
    }(FormBase));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var CheckboxField = /** @class */ (function (_super) {
        __extends(CheckboxField, _super);
        function CheckboxField(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.type = 'checkbox';
            _this.hasLabel = false;
            _this.classes.push('ndf-checkbox');
            return _this;
        }
        return CheckboxField;
    }(Field));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    /**
     * @template T
     */
    var /**
     * @template T
     */ OptionSetField = /** @class */ (function (_super) {
        __extends(OptionSetField, _super);
        function OptionSetField(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.type = 'optionset';
            _this.optionSet = [];
            _this.options = [];
            _this.classes.push('ndf-optionset');
            _this.optionSet = options['options'] || [];
            Promise.resolve(_this.optionSet).then(function (optionList) {
                _this.setOptions(optionList);
            });
            return _this;
        }
        /**
         * @param {?} options
         * @return {?}
         */
        OptionSetField.prototype.setOptions = /**
         * @param {?} options
         * @return {?}
         */
            function (options) {
                this.options = options;
            };
        return OptionSetField;
    }(Field));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    /**
     * @template T
     */
    var /**
     * @template T
     */ CheckboxSetField = /** @class */ (function (_super) {
        __extends(CheckboxSetField, _super);
        function CheckboxSetField(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.type = 'checkboxset';
            _this.classes.push('ndf-checkboxset');
            return _this;
        }
        /**
         * @param {?} value
         * @return {?}
         */
        CheckboxSetField.prototype.onChange = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                /** @type {?} */
                var obj = this.form.obj[this.name];
                if (this.isChecked(value)) {
                    obj.splice(obj.indexOf(value), 1);
                }
                else {
                    obj.push(value);
                }
            };
        /**
         * @param {?} value
         * @return {?}
         */
        CheckboxSetField.prototype.isChecked = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                /** @type {?} */
                var selected = this.form.obj[this.name];
                if (selected != null) {
                    return selected.filter(function (o) { return o === value; }).length > 0;
                }
                return false;
            };
        return CheckboxSetField;
    }(OptionSetField));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var ContentField = /** @class */ (function (_super) {
        __extends(ContentField, _super);
        function ContentField(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.type = 'content';
            _this.content = options['content'] || '';
            _this.classes.push('ndf-field');
            _this.classes.push('ndf-content');
            return _this;
        }
        return ContentField;
    }(FormBase));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var DateField = /** @class */ (function (_super) {
        __extends(DateField, _super);
        function DateField(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.type = 'date';
            _this.classes.push('ndf-date');
            return _this;
        }
        /**
         * @param {?} value
         * @return {?}
         */
        DateField.prototype.onChange = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                _super.prototype.onChange.call(this, (value.jsdate == null) ? null : new Date(value.jsdate));
            };
        return DateField;
    }(Field));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var DropdownField = /** @class */ (function (_super) {
        __extends(DropdownField, _super);
        function DropdownField(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.type = 'dropdown';
            _this.classes.push('ndf-dropdown');
            _this.loaded = options['emptyText'] || '';
            _this.emptyText = options['loadingText'] || '';
            return _this;
        }
        /**
         * @param {?} options
         * @return {?}
         */
        DropdownField.prototype.setOptions = /**
         * @param {?} options
         * @return {?}
         */
            function (options) {
                _super.prototype.setOptions.call(this, options);
                this.emptyText = this.loaded;
            };
        return DropdownField;
    }(OptionSetField));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var FieldGroup = /** @class */ (function (_super) {
        __extends(FieldGroup, _super);
        function FieldGroup(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.hasFields = true;
            _this.type = 'group';
            return _this;
        }
        /**
         * @return {?}
         */
        FieldGroup.prototype.deriveValue = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var _this = this;
                    return __generator(this, function (_a) {
                        return [2 /*return*/, _super.prototype.deriveValue.call(this).then(function (derived) {
                                _this.fields.updateValues();
                                return Promise.resolve(derived);
                            })];
                    });
                });
            };
        return FieldGroup;
    }(FormBase));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var FormAction = /** @class */ (function (_super) {
        __extends(FormAction, _super);
        function FormAction(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.isAction = true;
            _this.type = 'action';
            _this.title = options['title'] || null;
            _this.icon = options['icon'] || null;
            _this.actionType = options['actionType'] || 'submit';
            _this.click = options['click'] || null;
            _this.classes.push('ndf-action');
            return _this;
        }
        /**
         * @param {?} text
         * @return {?}
         */
        FormAction.prototype.showLoading = /**
         * @param {?} text
         * @return {?}
         */
            function (text) {
                this.loading = text;
            };
        /**
         * @return {?}
         */
        FormAction.prototype.clearLoading = /**
         * @return {?}
         */
            function () {
                this.loading = null;
            };
        /**
         * @return {?}
         */
        FormAction.prototype.onClick = /**
         * @return {?}
         */
            function () {
                if (this.click != null) {
                    this.click(this);
                }
            };
        return FormAction;
    }(FormBase));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var HeaderField = /** @class */ (function (_super) {
        __extends(HeaderField, _super);
        function HeaderField(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.type = 'header';
            _this.title = options['title'] || '';
            _this.classes.push('ndf-field');
            _this.classes.push('ndf-header');
            return _this;
        }
        return HeaderField;
    }(FormBase));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var MultiChildField = /** @class */ (function (_super) {
        __extends(MultiChildField, _super);
        function MultiChildField(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.child = false;
            _this.counter = 0;
            _this.subForm = options['subForm'] || null;
            _this.newObj = options['newObj'] || null;
            _this.addButtonText = options['addButtonText'] || 'Add New Line';
            _this.removeButtonText = options['removeButtonText'] || 'Remove Line';
            _this.child = options['child'] || false;
            _this.classes.push('ndf-multichild');
            if (options['title']) {
                _this.fields.push(new HeaderField({
                    title: options['title']
                }));
                _this.fields.push(new FieldGroup({
                    name: _this.name + "_controls",
                    fields: [
                        new FormAction({
                            title: _this.addButtonText,
                            icon: 'plus',
                            click: function () {
                                if (_this.subForm !== null && _this.newObj !== null) {
                                    /** @type {?} */
                                    var obj = _this.newObj();
                                    _this.list.push(obj);
                                    _this.addChild(obj);
                                }
                            },
                            classes: ['ndf-add']
                        })
                    ],
                    classes: ['ndf-full-bordered', 'ndf-multichild-footer']
                }));
            }
            return _this;
        }
        Object.defineProperty(MultiChildField.prototype, "list", {
            get: /**
             * @return {?}
             */ function () {
                if (this.form.obj[this.name] == null) {
                    this.form.obj[this.name] = [];
                }
                return (( /** @type {?} */(this.form.obj[this.name])));
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} form
         * @return {?}
         */
        MultiChildField.prototype.setForm = /**
         * @param {?} form
         * @return {?}
         */
            function (form) {
                var _this = this;
                _super.prototype.setForm.call(this, form);
                if (this.subForm !== null) {
                    this.list.forEach(function (obj) {
                        _this.addChild(obj);
                    });
                }
            };
        /**
         * @param {?} result
         * @return {?}
         */
        MultiChildField.prototype.loadValidation = /**
         * @param {?} result
         * @return {?}
         */
            function (result) {
                this.resetValidation();
                for (var key in result.fields) {
                    if (result.fields.hasOwnProperty(key)) {
                        /** @type {?} */
                        var child = this.form.prefix + "line_" + key;
                        /** @type {?} */
                        var field = this.fields.fieldByName(child);
                        if (field != null) {
                            field.loadValidation(result.fields[key]);
                        }
                    }
                }
            };
        /**
         * @param {?} obj
         * @return {?}
         */
        MultiChildField.prototype.addChild = /**
         * @param {?} obj
         * @return {?}
         */
            function (obj) {
                var _this = this;
                /** @type {?} */
                var group = this.form.prefix + "line_" + this.counter + "_group";
                this.fields.insertBeforeId(new FieldGroup({
                    name: group,
                    fields: [
                        this.subForm(obj, this.form.prefix + "line_" + this.counter),
                        new FormAction({
                            title: this.removeButtonText,
                            icon: 'trash',
                            click: function () {
                                _this.fields.removeById(group);
                                _this.list.splice(_this.list.indexOf(obj), 1);
                            },
                            classes: ['ndf-remove']
                        })
                    ],
                    classes: ['ndf-full-bordered', 'ndf-multichild-row', (this.child) ? 'ndf-child' : 'ndf-parent']
                }), this.name + "_controls");
                this.counter++;
            };
        return MultiChildField;
    }(FieldGroup));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var TextField = /** @class */ (function (_super) {
        __extends(TextField, _super);
        function TextField(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.type = 'text';
            _this.value = options['value'] || '';
            _this.classes.push('ndf-text');
            _this.inputType = options['inputType'] || '';
            return _this;
        }
        return TextField;
    }(Field));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var NumberField = /** @class */ (function (_super) {
        __extends(NumberField, _super);
        function NumberField(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.value = options['value'] || 0;
            _this.precision = (options['precision'] != null) ? options['precision'] : 2;
            return _this;
        }
        /**
         * @return {?}
         */
        NumberField.prototype.deriveValue = /**
         * @return {?}
         */
            function () {
                return __awaiter(this, void 0, void 0, function () {
                    var _this = this;
                    return __generator(this, function (_a) {
                        return [2 /*return*/, _super.prototype.deriveValue.call(this).then(function (derived) {
                                if (!derived) {
                                    _this.value = _this.prepareValue(_this.value);
                                    return Promise.resolve(true);
                                }
                                return Promise.resolve(false);
                            })];
                    });
                });
            };
        /**
         * @return {?}
         */
        NumberField.prototype.onBlur = /**
         * @return {?}
         */
            function () {
                this.value = this.prepareValue(this.value);
            };
        /**
         * @param {?} value
         * @return {?}
         */
        NumberField.prototype.prepareValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                /** @type {?} */
                var prepared;
                if (typeof value === 'number') {
                    prepared = ( /** @type {?} */(value));
                }
                else if (typeof value === 'undefined') {
                    prepared = 0;
                }
                else {
                    prepared = parseFloat(value.toString().replace(/[^0-9.]/g, ''));
                }
                return prepared.toFixed(this.precision);
            };
        return NumberField;
    }(TextField));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var TextareaField = /** @class */ (function (_super) {
        __extends(TextareaField, _super);
        function TextareaField(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.type = 'textarea';
            _this.value = options['value'] || '';
            _this.classes.push('ndf-textarea');
            return _this;
        }
        return TextareaField;
    }(Field));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var FormService = /** @class */ (function () {
        function FormService() {
            this.formGroup = new forms.FormGroup({});
        }
        Object.defineProperty(FormService.prototype, "group", {
            get: /**
             * @return {?}
             */ function () {
                return this.formGroup;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} field
         * @return {?}
         */
        FormService.prototype.registerField = /**
         * @param {?} field
         * @return {?}
         */
            function (field) {
                if (!this.group.contains(field.id)) {
                    this.group.addControl(field.id, this.controlFromField(field));
                }
            };
        /**
         * @param {?} fields
         * @return {?}
         */
        FormService.prototype.registerFields = /**
         * @param {?} fields
         * @return {?}
         */
            function (fields) {
                var _this = this;
                fields.forEach(function (field) {
                    _this.registerField(field);
                });
            };
        /**
         * @param {?} field
         * @return {?}
         */
        FormService.prototype.deregisterField = /**
         * @param {?} field
         * @return {?}
         */
            function (field) {
                this.group.removeControl(field.id);
            };
        /**
         * @param {?} fields
         * @return {?}
         */
        FormService.prototype.deregisterFields = /**
         * @param {?} fields
         * @return {?}
         */
            function (fields) {
                var _this = this;
                fields.forEach(function (field) {
                    _this.deregisterField(field);
                });
            };
        /**
         * @param {?} field
         * @return {?}
         */
        FormService.prototype.controlFromField = /**
         * @param {?} field
         * @return {?}
         */
            function (field) {
                return new forms.FormControl(field.value || '');
            };
        FormService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        FormService.ctorParameters = function () { return []; };
        return FormService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var Form = /** @class */ (function (_super) {
        __extends(Form, _super);
        function Form(options) {
            if (options === void 0) {
                options = {};
            }
            var _this = _super.call(this, options) || this;
            _this.hasFields = true;
            _this.isForm = true;
            _this.obj = options['obj'] || {};
            _this.submit = options['submit'] || null;
            _this.fields.setForm(_this);
            return _this;
        }
        Object.defineProperty(Form.prototype, "prefix", {
            get: /**
             * @return {?}
             */ function () {
                if (this.form) {
                    return "" + this.form.prefix + this.name + "_";
                }
                else if (this.name) {
                    return this.name + "_";
                }
                else {
                    return '';
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} obj
         * @return {?}
         */
        Form.prototype.update = /**
         * @param {?} obj
         * @return {?}
         */
            function (obj) {
                this.obj = obj;
                this.fields.updateValues();
            };
        /**
         * @param {?} key
         * @param {?} value
         * @return {?}
         */
        Form.prototype.fieldChange = /**
         * @param {?} key
         * @param {?} value
         * @return {?}
         */
            function (key, value) {
                this.obj[key] = value;
            };
        /**
         * @return {?}
         */
        Form.prototype.onSubmit = /**
         * @return {?}
         */
            function () {
                if (this.submit !== null) {
                    this.submit(this);
                }
            };
        return Form;
    }(FormBase));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var FormComponent = /** @class */ (function () {
        function FormComponent(formService) {
            this.formService = formService;
        }
        /**
         * @return {?}
         */
        FormComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        Object.defineProperty(FormComponent.prototype, "group", {
            get: /**
             * @return {?}
             */ function () {
                return this.formService.group;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        FormComponent.prototype.onSubmit = /**
         * @return {?}
         */
            function () {
                this.form.onSubmit();
            };
        FormComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-form',
                        template: "<form (ngSubmit)=\"onSubmit()\" [ngClass]=\"form.classes\" [formGroup]=\"group\">\n    <p *ngIf=\"form.message\" class=\"ndf-error\">{{ form.message }}</p>\n    <ndf-form-item *ngFor=\"let item of form.fields.fields\" [item]=\"item\"></ndf-form-item>\n</form>\n",
                        providers: [FormService]
                    }] }
        ];
        /** @nocollapse */
        FormComponent.ctorParameters = function () {
            return [
                { type: FormService }
            ];
        };
        FormComponent.propDecorators = {
            form: [{ type: core.Input }]
        };
        return FormComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var FormItemComponent = /** @class */ (function () {
        function FormItemComponent(formService) {
            this.formService = formService;
        }
        /**
         * @return {?}
         */
        FormItemComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                if (this.item.hasData) {
                    this.formService.registerField(( /** @type {?} */(this.item)));
                }
            };
        /**
         * @return {?}
         */
        FormItemComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                if (this.item.hasData) {
                    this.formService.deregisterField(( /** @type {?} */(this.item)));
                }
            };
        Object.defineProperty(FormItemComponent.prototype, "group", {
            get: /**
             * @return {?}
             */ function () {
                return this.formService.group;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} value
         * @return {?}
         */
        FormItemComponent.prototype.onChange = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.item.onChange(value);
            };
        /**
         * @param {?} value
         * @return {?}
         */
        FormItemComponent.prototype.onBlur = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.item.onBlur(value);
            };
        /**
         * @return {?}
         */
        FormItemComponent.prototype.onClick = /**
         * @return {?}
         */
            function () {
                this.item.onClick();
            };
        FormItemComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-form-item',
                        template: "<div [ngSwitch]=\"item.type\">\n    <ndf-checkbox-field *ngSwitchCase=\"'checkbox'\" [item]=\"item\"></ndf-checkbox-field>\n    <ndf-checkbox-set-field *ngSwitchCase=\"'checkboxset'\" [item]=\"item\"></ndf-checkbox-set-field>\n    <ndf-content-field *ngSwitchCase=\"'content'\" [item]=\"item\"></ndf-content-field>\n    <ndf-date-field *ngSwitchCase=\"'date'\" [item]=\"item\"></ndf-date-field>\n    <ndf-dropdown-field *ngSwitchCase=\"'dropdown'\" [item]=\"item\"></ndf-dropdown-field>\n    <ndf-field-group *ngSwitchCase=\"'group'\" [item]=\"item\"></ndf-field-group>\n    <ndf-field-group *ngSwitchCase=\"'multichild'\" [item]=\"item\"></ndf-field-group>\n    <ndf-form-action *ngSwitchCase=\"'action'\" [item]=\"item\"></ndf-form-action>\n    <ndf-header-field *ngSwitchCase=\"'header'\" [item]=\"item\"></ndf-header-field>\n    <ndf-option-set-field *ngSwitchCase=\"'optionset'\" [item]=\"item\"></ndf-option-set-field>\n    <ndf-text-field *ngSwitchCase=\"'text'\" [item]=\"item\"></ndf-text-field>\n    <ndf-textarea-field *ngSwitchCase=\"'textarea'\" [item]=\"item\"></ndf-textarea-field>\n</div>\n",
                        providers: [FormService]
                    }] }
        ];
        /** @nocollapse */
        FormItemComponent.ctorParameters = function () {
            return [
                { type: FormService }
            ];
        };
        FormItemComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return FormItemComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var CheckboxFieldComponent = /** @class */ (function (_super) {
        __extends(CheckboxFieldComponent, _super);
        function CheckboxFieldComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CheckboxFieldComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-checkbox-field',
                        template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label>\n        <div class=\"box\"><fa *ngIf=\"item.value\" name=\"check\" size=\"lt\"></fa></div>\n        <input type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.value\" (ngModelChange)=\"onChange($event)\" />\n        {{item.label}}\n    </label>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{item.message}}</p>\n</div>\n",
                        providers: [FormService]
                    }] }
        ];
        CheckboxFieldComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return CheckboxFieldComponent;
    }(FormItemComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var CheckboxSetFieldComponent = /** @class */ (function (_super) {
        __extends(CheckboxSetFieldComponent, _super);
        function CheckboxSetFieldComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CheckboxSetFieldComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-checkbox-set-field',
                        template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <ul>\n        <li *ngFor=\"let option of item.options\">\n            <label>\n                <div class=\"box\"><fa *ngIf=\"item.isChecked(option.key)\" name=\"check\" size=\"lt\"></fa></div>\n                <input type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.isChecked(option.key)\" [value]=\"option.key\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange(option.key)\" />\n                {{ option.value }}\n            </label>\n        </li>\n    </ul>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                        providers: [FormService]
                    }] }
        ];
        CheckboxSetFieldComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return CheckboxSetFieldComponent;
    }(FormItemComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var ContentFieldComponent = /** @class */ (function (_super) {
        __extends(ContentFieldComponent, _super);
        function ContentFieldComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        ContentFieldComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-content-field',
                        template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <p>{{ item.content }}</p>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                        providers: [FormService]
                    }] }
        ];
        ContentFieldComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return ContentFieldComponent;
    }(FormItemComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var DateFieldComponent = /** @class */ (function (_super) {
        __extends(DateFieldComponent, _super);
        function DateFieldComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        DateFieldComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-date-field',
                        template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <div class=\"ndf-date-picker\">\n        <input ngx-mydatepicker [name]=\"item.id\" [value]=\"item.value | date:'dd/MM/yyyy'\" [readonly]=\"item.readOnly\" (dateChanged)=\"onChange($event)\" [options]=\"{ dateFormat: 'dd/mm/yyyy', selectorHeight: 'auto' }\" #dp=\"ngx-mydatepicker\" />\n        <button *ngIf=\"item.value != null\" type=\"button\" class=\"ndf-date-clear\" (click)=\"dp.clearDate()\">\n            <fa name=\"times\" size=\"lt\"></fa>\n        </button>\n        <button type=\"button\" class=\"ndf-date-open\" (click)=\"dp.toggleCalendar()\">\n            <fa name=\"calendar-o\" size=\"lt\"></fa>\n        </button>\n    </div>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                        providers: [FormService]
                    }] }
        ];
        DateFieldComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return DateFieldComponent;
    }(FormItemComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var DropdownFieldComponent = /** @class */ (function (_super) {
        __extends(DropdownFieldComponent, _super);
        function DropdownFieldComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        DropdownFieldComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-dropdown-field',
                        template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <select [id]=\"item.id\" [formControlName]=\"item.id\" [value]=\"item.value ? item.value : ''\" (ngModelChange)=\"onChange($event)\">\n        <option *ngIf=\"item.emptyText && item.options.length < 1\" value=\"\">{{ item.emptyText }}</option>\n        <option *ngIf=\"item.placeholder && item.options.length > 0\" value=\"\">{{ item.placeholder }}</option>\n        <option *ngFor=\"let option of item.options\" [value]=\"option.key\" [disabled]=\"option.disabled\">{{ option.value }}</option>\n    </select>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                        providers: [FormService]
                    }] }
        ];
        DropdownFieldComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return DropdownFieldComponent;
    }(FormItemComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var FieldGroupComponent = /** @class */ (function (_super) {
        __extends(FieldGroupComponent, _super);
        function FieldGroupComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        FieldGroupComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-field-group',
                        template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <ndf-form-item *ngFor=\"let child of item.fields.fields\" [item]=\"child\"></ndf-form-item>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                        providers: [FormService]
                    }] }
        ];
        FieldGroupComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return FieldGroupComponent;
    }(FormItemComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var FormActionComponent = /** @class */ (function (_super) {
        __extends(FormActionComponent, _super);
        function FormActionComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Object.defineProperty(FormActionComponent.prototype, "label", {
            get: /**
             * @return {?}
             */ function () {
                return this.item.loading ? this.item.loading : this.item.title;
            },
            enumerable: true,
            configurable: true
        });
        FormActionComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-form-action',
                        template: "<button [attr.disabled]=\"item.loading != null ? '' : null\" [ngClass]=\"item.classes\" [type]=\"item.actionType\" (click)=\"onClick()\" [title]=\"item.title\">\n    <fa *ngIf=\"item.loading\" name=\"spinner\" size=\"lt\" animation=\"spin\"></fa><fa *ngIf=\"!item.loading && item.icon\" [name]=\"item.icon\" size=\"lt\"></fa> {{ label }}\n</button>\n",
                        providers: [FormService]
                    }] }
        ];
        FormActionComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return FormActionComponent;
    }(FormItemComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var HeaderFieldComponent = /** @class */ (function (_super) {
        __extends(HeaderFieldComponent, _super);
        function HeaderFieldComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        HeaderFieldComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-header-field',
                        template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <h3>{{ item.title }}</h3>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                        providers: [FormService]
                    }] }
        ];
        HeaderFieldComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return HeaderFieldComponent;
    }(FormItemComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var OptionSetFieldComponent = /** @class */ (function (_super) {
        __extends(OptionSetFieldComponent, _super);
        function OptionSetFieldComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        OptionSetFieldComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-option-set-field',
                        template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <ul>\n        <li *ngFor=\"let option of item.options\">\n            <label>\n                <div class=\"box\"><div *ngIf=\"item.value == option.key\"></div></div>\n                <input type=\"radio\" [formControlName]=\"item.id\" [checked]=\"item.value == option.key\" [value]=\"option.key\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" />\n                {{ option.value }}\n            </label>\n        </li>\n    </ul>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                        providers: [FormService]
                    }] }
        ];
        OptionSetFieldComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return OptionSetFieldComponent;
    }(FormItemComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var TextFieldComponent = /** @class */ (function (_super) {
        __extends(TextFieldComponent, _super);
        function TextFieldComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        TextFieldComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-text-field',
                        template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <label *ngIf=\"!item.hasLabel\">\n        <div *ngIf=\"item.type == 'checkbox'\" class=\"box\"><fa *ngIf=\"item.value\" name=\"check\" size=\"lt\"></fa></div>\n        <input *ngIf=\"item.type == 'checkbox'\" type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.value\" (ngModelChange)=\"onChange($event)\" />\n        {{ item.label }}\n    </label>\n    <input [formControlName]=\"item.id\" [id]=\"item.id\" [type]=\"item.inputType\" [placeholder]=\"item.placeholder\" [value]=\"item.value\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" (blur)=\"onBlur($event)\" />\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                        providers: [FormService]
                    }] }
        ];
        TextFieldComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return TextFieldComponent;
    }(FormItemComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var TextareaFieldComponent = /** @class */ (function (_super) {
        __extends(TextareaFieldComponent, _super);
        function TextareaFieldComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        TextareaFieldComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ndf-textarea-field',
                        template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <textarea [formControlName]=\"item.id\" [id]=\"item.id\" [value]=\"item.value\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" (blur)=\"onBlur($event)\"></textarea>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                        providers: [FormService]
                    }] }
        ];
        TextareaFieldComponent.propDecorators = {
            item: [{ type: core.Input }]
        };
        return TextareaFieldComponent;
    }(FormItemComponent));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var NgDynamicFormsModule = /** @class */ (function () {
        function NgDynamicFormsModule() {
        }
        /**
         * @return {?}
         */
        NgDynamicFormsModule.forRoot = /**
         * @return {?}
         */
            function () {
                return {
                    ngModule: NgDynamicFormsModule,
                    providers: [FormService]
                };
            };
        NgDynamicFormsModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            common.CommonModule,
                            forms.FormsModule,
                            forms.ReactiveFormsModule,
                            angularFontAwesome.AngularFontAwesomeModule,
                            ngxMydatepicker.NgxMyDatePickerModule.forRoot()
                        ],
                        declarations: [
                            FormComponent,
                            FormItemComponent,
                            CheckboxFieldComponent,
                            CheckboxSetFieldComponent,
                            ContentFieldComponent,
                            DateFieldComponent,
                            DropdownFieldComponent,
                            FieldGroupComponent,
                            FormActionComponent,
                            HeaderFieldComponent,
                            OptionSetFieldComponent,
                            TextFieldComponent,
                            TextareaFieldComponent
                        ],
                        exports: [
                            FormComponent,
                            FormItemComponent,
                            CheckboxFieldComponent,
                            CheckboxSetFieldComponent,
                            ContentFieldComponent,
                            DateFieldComponent,
                            DropdownFieldComponent,
                            FieldGroupComponent,
                            FormActionComponent,
                            HeaderFieldComponent,
                            OptionSetFieldComponent,
                            TextFieldComponent,
                            TextareaFieldComponent
                        ]
                    },] }
        ];
        return NgDynamicFormsModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    exports.CheckboxField = CheckboxField;
    exports.CheckboxSetField = CheckboxSetField;
    exports.ContentField = ContentField;
    exports.DateField = DateField;
    exports.DropdownField = DropdownField;
    exports.FieldGroup = FieldGroup;
    exports.Field = Field;
    exports.FormAction = FormAction;
    exports.HeaderField = HeaderField;
    exports.MultiChildField = MultiChildField;
    exports.NumberField = NumberField;
    exports.OptionSetField = OptionSetField;
    exports.TextField = TextField;
    exports.TextareaField = TextareaField;
    exports.FormService = FormService;
    exports.FieldSet = FieldSet;
    exports.FormBase = FormBase;
    exports.Form = Form;
    exports.NgDynamicFormsModule = NgDynamicFormsModule;
    exports.ɵc = CheckboxFieldComponent;
    exports.ɵd = CheckboxSetFieldComponent;
    exports.ɵe = ContentFieldComponent;
    exports.ɵf = DateFieldComponent;
    exports.ɵg = DropdownFieldComponent;
    exports.ɵh = FieldGroupComponent;
    exports.ɵi = FormActionComponent;
    exports.ɵb = FormItemComponent;
    exports.ɵa = FormComponent;
    exports.ɵj = HeaderFieldComponent;
    exports.ɵk = OptionSetFieldComponent;
    exports.ɵl = TextFieldComponent;
    exports.ɵm = TextareaFieldComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFpc2VkaWdpdGFsLW5nLWR5bmFtaWMtZm9ybXMudW1kLmpzLm1hcCIsInNvdXJjZXMiOlsibm9kZV9tb2R1bGVzL3RzbGliL3RzbGliLmVzNi5qcyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZC1zZXQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZm9ybS1iYXNlLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2ZpZWxkcy9maWVsZC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvY2hlY2tib3gtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL29wdGlvbi1zZXQtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2NoZWNrYm94LXNldC1maWVsZC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvY29udGVudC1maWVsZC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvZGF0ZS1maWVsZC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvZHJvcGRvd24tZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2ZpZWxkLWdyb3VwLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2ZpZWxkcy9mb3JtLWFjdGlvbi50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvaGVhZGVyLWZpZWxkLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2ZpZWxkcy9tdWx0aS1jaGlsZC1maWVsZC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvdGV4dC1maWVsZC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvbnVtYmVyLWZpZWxkLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2ZpZWxkcy90ZXh0YXJlYS1maWVsZC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZm9ybS50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2Zvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2NvbXBvbmVudHMvZm9ybS1pdGVtLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2NoZWNrYm94LWZpZWxkLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2NoZWNrYm94LXNldC1maWVsZC5jb21wb25lbnQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy9jb250ZW50LWZpZWxkLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2RhdGUtZmllbGQuY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2NvbXBvbmVudHMvZHJvcGRvd24tZmllbGQuY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2NvbXBvbmVudHMvZmllbGQtZ3JvdXAuY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2NvbXBvbmVudHMvZm9ybS1hY3Rpb24uY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2NvbXBvbmVudHMvaGVhZGVyLWZpZWxkLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL29wdGlvbi1zZXQtZmllbGQuY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2NvbXBvbmVudHMvdGV4dC1maWVsZC5jb21wb25lbnQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy90ZXh0YXJlYS1maWVsZC5jb21wb25lbnQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvbmctZHluYW1pYy1mb3Jtcy5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiLyohICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbkNvcHlyaWdodCAoYykgTWljcm9zb2Z0IENvcnBvcmF0aW9uLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2VcclxudGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGVcclxuTGljZW5zZSBhdCBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblRISVMgQ09ERSBJUyBQUk9WSURFRCBPTiBBTiAqQVMgSVMqIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcclxuS0lORCwgRUlUSEVSIEVYUFJFU1MgT1IgSU1QTElFRCwgSU5DTFVESU5HIFdJVEhPVVQgTElNSVRBVElPTiBBTlkgSU1QTElFRFxyXG5XQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgVElUTEUsIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLFxyXG5NRVJDSEFOVEFCTElUWSBPUiBOT04tSU5GUklOR0VNRU5ULlxyXG5cclxuU2VlIHRoZSBBcGFjaGUgVmVyc2lvbiAyLjAgTGljZW5zZSBmb3Igc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zXHJcbmFuZCBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiogKi9cclxuLyogZ2xvYmFsIFJlZmxlY3QsIFByb21pc2UgKi9cclxuXHJcbnZhciBleHRlbmRTdGF0aWNzID0gZnVuY3Rpb24oZCwgYikge1xyXG4gICAgZXh0ZW5kU3RhdGljcyA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiB8fFxyXG4gICAgICAgICh7IF9fcHJvdG9fXzogW10gfSBpbnN0YW5jZW9mIEFycmF5ICYmIGZ1bmN0aW9uIChkLCBiKSB7IGQuX19wcm90b19fID0gYjsgfSkgfHxcclxuICAgICAgICBmdW5jdGlvbiAoZCwgYikgeyBmb3IgKHZhciBwIGluIGIpIGlmIChiLmhhc093blByb3BlcnR5KHApKSBkW3BdID0gYltwXTsgfTtcclxuICAgIHJldHVybiBleHRlbmRTdGF0aWNzKGQsIGIpO1xyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZXh0ZW5kcyhkLCBiKSB7XHJcbiAgICBleHRlbmRTdGF0aWNzKGQsIGIpO1xyXG4gICAgZnVuY3Rpb24gX18oKSB7IHRoaXMuY29uc3RydWN0b3IgPSBkOyB9XHJcbiAgICBkLnByb3RvdHlwZSA9IGIgPT09IG51bGwgPyBPYmplY3QuY3JlYXRlKGIpIDogKF9fLnByb3RvdHlwZSA9IGIucHJvdG90eXBlLCBuZXcgX18oKSk7XHJcbn1cclxuXHJcbmV4cG9ydCB2YXIgX19hc3NpZ24gPSBmdW5jdGlvbigpIHtcclxuICAgIF9fYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiBfX2Fzc2lnbih0KSB7XHJcbiAgICAgICAgZm9yICh2YXIgcywgaSA9IDEsIG4gPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XHJcbiAgICAgICAgICAgIHMgPSBhcmd1bWVudHNbaV07XHJcbiAgICAgICAgICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSkgdFtwXSA9IHNbcF07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIF9fYXNzaWduLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3Jlc3QocywgZSkge1xyXG4gICAgdmFyIHQgPSB7fTtcclxuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxyXG4gICAgICAgIHRbcF0gPSBzW3BdO1xyXG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxyXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIGlmIChlLmluZGV4T2YocFtpXSkgPCAwKVxyXG4gICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcclxuICAgIHJldHVybiB0O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYykge1xyXG4gICAgdmFyIGMgPSBhcmd1bWVudHMubGVuZ3RoLCByID0gYyA8IDMgPyB0YXJnZXQgOiBkZXNjID09PSBudWxsID8gZGVzYyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IodGFyZ2V0LCBrZXkpIDogZGVzYywgZDtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5kZWNvcmF0ZSA9PT0gXCJmdW5jdGlvblwiKSByID0gUmVmbGVjdC5kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYyk7XHJcbiAgICBlbHNlIGZvciAodmFyIGkgPSBkZWNvcmF0b3JzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSBpZiAoZCA9IGRlY29yYXRvcnNbaV0pIHIgPSAoYyA8IDMgPyBkKHIpIDogYyA+IDMgPyBkKHRhcmdldCwga2V5LCByKSA6IGQodGFyZ2V0LCBrZXkpKSB8fCByO1xyXG4gICAgcmV0dXJuIGMgPiAzICYmIHIgJiYgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCByKSwgcjtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fcGFyYW0ocGFyYW1JbmRleCwgZGVjb3JhdG9yKSB7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKHRhcmdldCwga2V5KSB7IGRlY29yYXRvcih0YXJnZXQsIGtleSwgcGFyYW1JbmRleCk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fbWV0YWRhdGEobWV0YWRhdGFLZXksIG1ldGFkYXRhVmFsdWUpIHtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5tZXRhZGF0YSA9PT0gXCJmdW5jdGlvblwiKSByZXR1cm4gUmVmbGVjdC5tZXRhZGF0YShtZXRhZGF0YUtleSwgbWV0YWRhdGFWYWx1ZSk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2F3YWl0ZXIodGhpc0FyZywgX2FyZ3VtZW50cywgUCwgZ2VuZXJhdG9yKSB7XHJcbiAgICByZXR1cm4gbmV3IChQIHx8IChQID0gUHJvbWlzZSkpKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcclxuICAgICAgICBmdW5jdGlvbiBmdWxmaWxsZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3IubmV4dCh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XHJcbiAgICAgICAgZnVuY3Rpb24gcmVqZWN0ZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3JbXCJ0aHJvd1wiXSh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XHJcbiAgICAgICAgZnVuY3Rpb24gc3RlcChyZXN1bHQpIHsgcmVzdWx0LmRvbmUgPyByZXNvbHZlKHJlc3VsdC52YWx1ZSkgOiBuZXcgUChmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHJlc3VsdC52YWx1ZSk7IH0pLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCk7IH1cclxuICAgICAgICBzdGVwKChnZW5lcmF0b3IgPSBnZW5lcmF0b3IuYXBwbHkodGhpc0FyZywgX2FyZ3VtZW50cyB8fCBbXSkpLm5leHQoKSk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZ2VuZXJhdG9yKHRoaXNBcmcsIGJvZHkpIHtcclxuICAgIHZhciBfID0geyBsYWJlbDogMCwgc2VudDogZnVuY3Rpb24oKSB7IGlmICh0WzBdICYgMSkgdGhyb3cgdFsxXTsgcmV0dXJuIHRbMV07IH0sIHRyeXM6IFtdLCBvcHM6IFtdIH0sIGYsIHksIHQsIGc7XHJcbiAgICByZXR1cm4gZyA9IHsgbmV4dDogdmVyYigwKSwgXCJ0aHJvd1wiOiB2ZXJiKDEpLCBcInJldHVyblwiOiB2ZXJiKDIpIH0sIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiAoZ1tTeW1ib2wuaXRlcmF0b3JdID0gZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KSwgZztcclxuICAgIGZ1bmN0aW9uIHZlcmIobikgeyByZXR1cm4gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIHN0ZXAoW24sIHZdKTsgfTsgfVxyXG4gICAgZnVuY3Rpb24gc3RlcChvcCkge1xyXG4gICAgICAgIGlmIChmKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiR2VuZXJhdG9yIGlzIGFscmVhZHkgZXhlY3V0aW5nLlwiKTtcclxuICAgICAgICB3aGlsZSAoXykgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKGYgPSAxLCB5ICYmICh0ID0gb3BbMF0gJiAyID8geVtcInJldHVyblwiXSA6IG9wWzBdID8geVtcInRocm93XCJdIHx8ICgodCA9IHlbXCJyZXR1cm5cIl0pICYmIHQuY2FsbCh5KSwgMCkgOiB5Lm5leHQpICYmICEodCA9IHQuY2FsbCh5LCBvcFsxXSkpLmRvbmUpIHJldHVybiB0O1xyXG4gICAgICAgICAgICBpZiAoeSA9IDAsIHQpIG9wID0gW29wWzBdICYgMiwgdC52YWx1ZV07XHJcbiAgICAgICAgICAgIHN3aXRjaCAob3BbMF0pIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDogY2FzZSAxOiB0ID0gb3A7IGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcclxuICAgICAgICAgICAgICAgIGNhc2UgNTogXy5sYWJlbCsrOyB5ID0gb3BbMV07IG9wID0gWzBdOyBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgNzogb3AgPSBfLm9wcy5wb3AoKTsgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEodCA9IF8udHJ5cywgdCA9IHQubGVuZ3RoID4gMCAmJiB0W3QubGVuZ3RoIC0gMV0pICYmIChvcFswXSA9PT0gNiB8fCBvcFswXSA9PT0gMikpIHsgXyA9IDA7IGNvbnRpbnVlOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSAzICYmICghdCB8fCAob3BbMV0gPiB0WzBdICYmIG9wWzFdIDwgdFszXSkpKSB7IF8ubGFiZWwgPSBvcFsxXTsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAodCAmJiBfLmxhYmVsIDwgdFsyXSkgeyBfLmxhYmVsID0gdFsyXTsgXy5vcHMucHVzaChvcCk7IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRbMl0pIF8ub3BzLnBvcCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgb3AgPSBib2R5LmNhbGwodGhpc0FyZywgXyk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxyXG4gICAgICAgIGlmIChvcFswXSAmIDUpIHRocm93IG9wWzFdOyByZXR1cm4geyB2YWx1ZTogb3BbMF0gPyBvcFsxXSA6IHZvaWQgMCwgZG9uZTogdHJ1ZSB9O1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19leHBvcnRTdGFyKG0sIGV4cG9ydHMpIHtcclxuICAgIGZvciAodmFyIHAgaW4gbSkgaWYgKCFleHBvcnRzLmhhc093blByb3BlcnR5KHApKSBleHBvcnRzW3BdID0gbVtwXTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fdmFsdWVzKG8pIHtcclxuICAgIHZhciBtID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9bU3ltYm9sLml0ZXJhdG9yXSwgaSA9IDA7XHJcbiAgICBpZiAobSkgcmV0dXJuIG0uY2FsbChvKTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgbmV4dDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBpZiAobyAmJiBpID49IG8ubGVuZ3RoKSBvID0gdm9pZCAwO1xyXG4gICAgICAgICAgICByZXR1cm4geyB2YWx1ZTogbyAmJiBvW2krK10sIGRvbmU6ICFvIH07XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fcmVhZChvLCBuKSB7XHJcbiAgICB2YXIgbSA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvW1N5bWJvbC5pdGVyYXRvcl07XHJcbiAgICBpZiAoIW0pIHJldHVybiBvO1xyXG4gICAgdmFyIGkgPSBtLmNhbGwobyksIHIsIGFyID0gW10sIGU7XHJcbiAgICB0cnkge1xyXG4gICAgICAgIHdoaWxlICgobiA9PT0gdm9pZCAwIHx8IG4tLSA+IDApICYmICEociA9IGkubmV4dCgpKS5kb25lKSBhci5wdXNoKHIudmFsdWUpO1xyXG4gICAgfVxyXG4gICAgY2F0Y2ggKGVycm9yKSB7IGUgPSB7IGVycm9yOiBlcnJvciB9OyB9XHJcbiAgICBmaW5hbGx5IHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBpZiAociAmJiAhci5kb25lICYmIChtID0gaVtcInJldHVyblwiXSkpIG0uY2FsbChpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZmluYWxseSB7IGlmIChlKSB0aHJvdyBlLmVycm9yOyB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gYXI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3NwcmVhZCgpIHtcclxuICAgIGZvciAodmFyIGFyID0gW10sIGkgPSAwOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKVxyXG4gICAgICAgIGFyID0gYXIuY29uY2F0KF9fcmVhZChhcmd1bWVudHNbaV0pKTtcclxuICAgIHJldHVybiBhcjtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXdhaXQodikge1xyXG4gICAgcmV0dXJuIHRoaXMgaW5zdGFuY2VvZiBfX2F3YWl0ID8gKHRoaXMudiA9IHYsIHRoaXMpIDogbmV3IF9fYXdhaXQodik7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jR2VuZXJhdG9yKHRoaXNBcmcsIF9hcmd1bWVudHMsIGdlbmVyYXRvcikge1xyXG4gICAgaWYgKCFTeW1ib2wuYXN5bmNJdGVyYXRvcikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN5bWJvbC5hc3luY0l0ZXJhdG9yIGlzIG5vdCBkZWZpbmVkLlwiKTtcclxuICAgIHZhciBnID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pLCBpLCBxID0gW107XHJcbiAgICByZXR1cm4gaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIpLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXM7IH0sIGk7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgaWYgKGdbbl0pIGlbbl0gPSBmdW5jdGlvbiAodikgeyByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKGEsIGIpIHsgcS5wdXNoKFtuLCB2LCBhLCBiXSkgPiAxIHx8IHJlc3VtZShuLCB2KTsgfSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHJlc3VtZShuLCB2KSB7IHRyeSB7IHN0ZXAoZ1tuXSh2KSk7IH0gY2F0Y2ggKGUpIHsgc2V0dGxlKHFbMF1bM10sIGUpOyB9IH1cclxuICAgIGZ1bmN0aW9uIHN0ZXAocikgeyByLnZhbHVlIGluc3RhbmNlb2YgX19hd2FpdCA/IFByb21pc2UucmVzb2x2ZShyLnZhbHVlLnYpLnRoZW4oZnVsZmlsbCwgcmVqZWN0KSA6IHNldHRsZShxWzBdWzJdLCByKTsgfVxyXG4gICAgZnVuY3Rpb24gZnVsZmlsbCh2YWx1ZSkgeyByZXN1bWUoXCJuZXh0XCIsIHZhbHVlKTsgfVxyXG4gICAgZnVuY3Rpb24gcmVqZWN0KHZhbHVlKSB7IHJlc3VtZShcInRocm93XCIsIHZhbHVlKTsgfVxyXG4gICAgZnVuY3Rpb24gc2V0dGxlKGYsIHYpIHsgaWYgKGYodiksIHEuc2hpZnQoKSwgcS5sZW5ndGgpIHJlc3VtZShxWzBdWzBdLCBxWzBdWzFdKTsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hc3luY0RlbGVnYXRvcihvKSB7XHJcbiAgICB2YXIgaSwgcDtcclxuICAgIHJldHVybiBpID0ge30sIHZlcmIoXCJuZXh0XCIpLCB2ZXJiKFwidGhyb3dcIiwgZnVuY3Rpb24gKGUpIHsgdGhyb3cgZTsgfSksIHZlcmIoXCJyZXR1cm5cIiksIGlbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXM7IH0sIGk7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4sIGYpIHsgaVtuXSA9IG9bbl0gPyBmdW5jdGlvbiAodikgeyByZXR1cm4gKHAgPSAhcCkgPyB7IHZhbHVlOiBfX2F3YWl0KG9bbl0odikpLCBkb25lOiBuID09PSBcInJldHVyblwiIH0gOiBmID8gZih2KSA6IHY7IH0gOiBmOyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jVmFsdWVzKG8pIHtcclxuICAgIGlmICghU3ltYm9sLmFzeW5jSXRlcmF0b3IpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJTeW1ib2wuYXN5bmNJdGVyYXRvciBpcyBub3QgZGVmaW5lZC5cIik7XHJcbiAgICB2YXIgbSA9IG9bU3ltYm9sLmFzeW5jSXRlcmF0b3JdLCBpO1xyXG4gICAgcmV0dXJuIG0gPyBtLmNhbGwobykgOiAobyA9IHR5cGVvZiBfX3ZhbHVlcyA9PT0gXCJmdW5jdGlvblwiID8gX192YWx1ZXMobykgOiBvW1N5bWJvbC5pdGVyYXRvcl0oKSwgaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIpLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXM7IH0sIGkpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IGlbbl0gPSBvW25dICYmIGZ1bmN0aW9uICh2KSB7IHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7IHYgPSBvW25dKHYpLCBzZXR0bGUocmVzb2x2ZSwgcmVqZWN0LCB2LmRvbmUsIHYudmFsdWUpOyB9KTsgfTsgfVxyXG4gICAgZnVuY3Rpb24gc2V0dGxlKHJlc29sdmUsIHJlamVjdCwgZCwgdikgeyBQcm9taXNlLnJlc29sdmUodikudGhlbihmdW5jdGlvbih2KSB7IHJlc29sdmUoeyB2YWx1ZTogdiwgZG9uZTogZCB9KTsgfSwgcmVqZWN0KTsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19tYWtlVGVtcGxhdGVPYmplY3QoY29va2VkLCByYXcpIHtcclxuICAgIGlmIChPYmplY3QuZGVmaW5lUHJvcGVydHkpIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KGNvb2tlZCwgXCJyYXdcIiwgeyB2YWx1ZTogcmF3IH0pOyB9IGVsc2UgeyBjb29rZWQucmF3ID0gcmF3OyB9XHJcbiAgICByZXR1cm4gY29va2VkO1xyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9faW1wb3J0U3Rhcihtb2QpIHtcclxuICAgIGlmIChtb2QgJiYgbW9kLl9fZXNNb2R1bGUpIHJldHVybiBtb2Q7XHJcbiAgICB2YXIgcmVzdWx0ID0ge307XHJcbiAgICBpZiAobW9kICE9IG51bGwpIGZvciAodmFyIGsgaW4gbW9kKSBpZiAoT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobW9kLCBrKSkgcmVzdWx0W2tdID0gbW9kW2tdO1xyXG4gICAgcmVzdWx0LmRlZmF1bHQgPSBtb2Q7XHJcbiAgICByZXR1cm4gcmVzdWx0O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19pbXBvcnREZWZhdWx0KG1vZCkge1xyXG4gICAgcmV0dXJuIChtb2QgJiYgbW9kLl9fZXNNb2R1bGUpID8gbW9kIDogeyBkZWZhdWx0OiBtb2QgfTtcclxufVxyXG4iLCJpbXBvcnQgeyBGb3JtQmFzZSB9IGZyb20gJy4vZm9ybS1iYXNlJztcbmltcG9ydCB7IEZvcm0gfSBmcm9tICcuL2Zvcm0nO1xuXG5cbmV4cG9ydCBjbGFzcyBGaWVsZFNldCB7XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgc2V0OiBGb3JtQmFzZVtdKSB7IH1cblxuICBzZXRGb3JtKGZvcm06IEZvcm0pIHtcbiAgICB0aGlzLnNldC5mb3JFYWNoKChmaWVsZCkgPT4ge1xuICAgICAgZmllbGQuc2V0Rm9ybShmb3JtKTtcbiAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZVZhbHVlcygpOiB2b2lkIHtcbiAgICB0aGlzLnNldC5mb3JFYWNoKChmaWVsZCkgPT4ge1xuICAgICAgZmllbGQuZGVyaXZlVmFsdWUoKTtcbiAgICB9KTtcbiAgfVxuXG4gIGdldCBmaWVsZHMoKSB7XG4gICAgcmV0dXJuIHRoaXMuc2V0O1xuICB9XG5cbiAgcHVzaChpdGVtOiBGb3JtQmFzZSk6IHZvaWQge1xuICAgIHRoaXMuc2V0LnB1c2goaXRlbSk7XG4gIH1cblxuICBpbnNlcnRCZWZvcmUoaXRlbTogRm9ybUJhc2UsIGJlZm9yZTogc3RyaW5nLCBhdHRyOiBzdHJpbmcsIGZpZWxkOiBGb3JtQmFzZSA9IG51bGwpOiB2b2lkIHtcbiAgICBsZXQgcGFyZW50OiBGb3JtQmFzZSA9IG51bGw7XG4gICAgaWYgKGZpZWxkID09PSBudWxsKSB7XG4gICAgICBjb25zdCBzZWFyY2g6IHsgZmllbGQ6IEZvcm1CYXNlLCBwYXJlbnQ6IEZvcm1CYXNlIH0gPSB0aGlzLmZpZWxkV2l0aFBhcmVudChiZWZvcmUsIGF0dHIpO1xuICAgICAgcGFyZW50ID0gc2VhcmNoLnBhcmVudDtcbiAgICAgIGZpZWxkID0gc2VhcmNoLmZpZWxkO1xuICAgIH1cbiAgICBpZiAocGFyZW50ID09PSBudWxsKSB7XG4gICAgICB0aGlzLnNldC5zcGxpY2UodGhpcy5zZXQuaW5kZXhPZihmaWVsZCksIDAsIGl0ZW0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBwYXJlbnQuZmllbGRzLmluc2VydEJlZm9yZShpdGVtLCBiZWZvcmUsIGF0dHIsIGZpZWxkKTtcbiAgICB9XG4gIH1cblxuICBpbnNlcnRCZWZvcmVOYW1lKGl0ZW06IEZvcm1CYXNlLCBiZWZvcmU6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIHJldHVybiB0aGlzLmluc2VydEJlZm9yZShpdGVtLCBiZWZvcmUsICduYW1lJywgZmllbGQpO1xuICB9XG5cbiAgaW5zZXJ0QmVmb3JlSWQoaXRlbTogRm9ybUJhc2UsIGJlZm9yZTogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKTogdm9pZCB7XG4gICAgcmV0dXJuIHRoaXMuaW5zZXJ0QmVmb3JlKGl0ZW0sIGJlZm9yZSwgJ2lkJywgZmllbGQpO1xuICB9XG5cbiAgaW5zZXJ0QWZ0ZXIoaXRlbTogRm9ybUJhc2UsIGFmdGVyOiBzdHJpbmcsIGF0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIGxldCBwYXJlbnQ6IEZvcm1CYXNlID0gbnVsbDtcbiAgICBpZiAoZmllbGQgPT09IG51bGwpIHtcbiAgICAgIGNvbnN0IHNlYXJjaDogeyBmaWVsZDogRm9ybUJhc2UsIHBhcmVudDogRm9ybUJhc2UgfSA9IHRoaXMuZmllbGRXaXRoUGFyZW50KGFmdGVyLCBhdHRyKTtcbiAgICAgIHBhcmVudCA9IHNlYXJjaC5wYXJlbnQ7XG4gICAgICBmaWVsZCA9IHNlYXJjaC5maWVsZDtcbiAgICB9XG4gICAgaWYgKHBhcmVudCA9PT0gbnVsbCkge1xuICAgICAgdGhpcy5zZXQuc3BsaWNlKHRoaXMuc2V0LmluZGV4T2YoZmllbGQpICsgMSwgMCwgaXRlbSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHBhcmVudC5maWVsZHMuaW5zZXJ0QWZ0ZXIoaXRlbSwgYWZ0ZXIsIGF0dHIsIGZpZWxkKTtcbiAgICB9XG4gIH1cblxuICBpbnNlcnRBZnRlck5hbWUoaXRlbTogRm9ybUJhc2UsIGFmdGVyOiBzdHJpbmcsIF9hdHRyOiBzdHJpbmcsIGZpZWxkOiBGb3JtQmFzZSA9IG51bGwpOiB2b2lkIHtcbiAgICB0aGlzLmluc2VydEFmdGVyKGl0ZW0sIGFmdGVyLCAnbmFtZScsIGZpZWxkKTtcbiAgfVxuXG4gIGluc2VydEFmdGVySWQoaXRlbTogRm9ybUJhc2UsIGFmdGVyOiBzdHJpbmcsIF9hdHRyOiBzdHJpbmcsIGZpZWxkOiBGb3JtQmFzZSA9IG51bGwpOiB2b2lkIHtcbiAgICB0aGlzLmluc2VydEFmdGVyKGl0ZW0sIGFmdGVyLCAnaWQnLCBmaWVsZCk7XG4gIH1cblxuICByZW1vdmUoa2V5OiBzdHJpbmcsIGZpZWxkOiBGb3JtQmFzZSA9IG51bGwsIGF0dHI6IHN0cmluZykge1xuICAgIGxldCBwYXJlbnQ6IEZvcm1CYXNlID0gbnVsbDtcbiAgICBpZiAoZmllbGQgPT09IG51bGwpIHtcbiAgICAgIGNvbnN0IHNlYXJjaDogeyBmaWVsZDogRm9ybUJhc2UsIHBhcmVudDogRm9ybUJhc2UgfSA9IHRoaXMuZmllbGRXaXRoUGFyZW50KGtleSwgYXR0cik7XG4gICAgICBwYXJlbnQgPSBzZWFyY2gucGFyZW50O1xuICAgICAgZmllbGQgPSBzZWFyY2guZmllbGQ7XG4gICAgfVxuICAgIGlmIChwYXJlbnQgPT09IG51bGwpIHtcbiAgICAgIHRoaXMuc2V0LnNwbGljZSh0aGlzLnNldC5pbmRleE9mKGZpZWxkKSwgMSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHBhcmVudC5maWVsZHMucmVtb3ZlKGtleSwgZmllbGQsIGF0dHIpO1xuICAgIH1cbiAgfVxuXG4gIHJlbW92ZUJ5TmFtZShuYW1lOiBzdHJpbmcsIGZpZWxkOiBGb3JtQmFzZSA9IG51bGwpIHtcbiAgICB0aGlzLnJlbW92ZShuYW1lLCBmaWVsZCwgJ25hbWUnKTtcbiAgfVxuXG4gIHJlbW92ZUJ5SWQoaWQ6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCkge1xuICAgIHRoaXMucmVtb3ZlKGlkLCBmaWVsZCwgJ2lkJyk7XG4gIH1cblxuICBmaWVsZChrZXk6IHN0cmluZywgYXR0cjogc3RyaW5nKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZFdpdGhQYXJlbnQoa2V5LCBhdHRyKS5maWVsZDtcbiAgfVxuXG4gIGZpZWxkQnlOYW1lKG5hbWU6IHN0cmluZyk6IGFueSB7XG4gICAgcmV0dXJuIHRoaXMuZmllbGQobmFtZSwgJ25hbWUnKTtcbiAgfVxuXG4gIGZpZWxkQnlJZChpZDogc3RyaW5nKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZChpZCwgJ2lkJyk7XG4gIH1cblxuICBmaWVsZFdpdGhQYXJlbnQoa2V5OiBzdHJpbmcsIGF0dHI6IHN0cmluZyk6IHsgZmllbGQ6IGFueSwgcGFyZW50OiBhbnkgfSB7XG4gICAgbGV0IGZpZWxkOiBhbnkgPSBudWxsO1xuICAgIGxldCBwYXJlbnQ6IGFueSA9IG51bGw7XG4gICAgY29uc3Qgc2VhcmNoOiBhbnlbXSA9IHRoaXMuc2V0LmZpbHRlcihjaGlsZCA9PiBjaGlsZFthdHRyXSA9PT0ga2V5KTtcbiAgICBpZiAoc2VhcmNoLmxlbmd0aCA+IDApIHtcbiAgICAgIGZpZWxkID0gc2VhcmNoWzBdO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zdCBwYXJlbnRzOiBhbnlbXSA9IHRoaXMuc2V0LmZpbHRlcihjaGlsZCA9PiBjaGlsZC5oYXNGaWVsZHMgPT09IHRydWUpO1xuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwYXJlbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGNvbnN0IGNoZWNrOiB7IGZpZWxkOiBhbnksIHBhcmVudDogYW55IH0gPSBwYXJlbnRzW2ldLmZpZWxkcy5maWVsZFdpdGhQYXJlbnQoa2V5LCBhdHRyKTtcbiAgICAgICAgaWYgKGNoZWNrLmZpZWxkICE9IG51bGwpIHtcbiAgICAgICAgICBmaWVsZCA9IGNoZWNrLmZpZWxkO1xuICAgICAgICAgIHBhcmVudCA9IHBhcmVudHNbaV07XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHsgZmllbGQ6IGZpZWxkLCBwYXJlbnQ6IHBhcmVudCB9O1xuICB9XG5cbiAgZmllbGRCeU5hbWVXaXRoUGFyZW50KG5hbWU6IHN0cmluZyk6IHsgZmllbGQ6IGFueSwgcGFyZW50OiBhbnkgfSB7XG4gICAgcmV0dXJuIHRoaXMuZmllbGRXaXRoUGFyZW50KG5hbWUsICduYW1lJyk7XG4gIH1cblxuICBmaWVsZEJ5SWRXaXRoUGFyZW50KGlkOiBzdHJpbmcpOiB7IGZpZWxkOiBhbnksIHBhcmVudDogYW55IH0ge1xuICAgIHJldHVybiB0aGlzLmZpZWxkV2l0aFBhcmVudChpZCwgJ2lkJyk7XG4gIH1cblxuICByZXNldFZhbGlkYXRpb24oKTogdm9pZCB7XG4gICAgdGhpcy5maWVsZHMuZm9yRWFjaChmaWVsZCA9PiB7XG4gICAgICBmaWVsZC5yZXNldFZhbGlkYXRpb24oKTtcbiAgICB9KTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgRm9ybSB9IGZyb20gJy4vZm9ybSc7XG5pbXBvcnQgeyBGaWVsZFNldCB9IGZyb20gJy4vZmllbGQtc2V0JztcblxuaW1wb3J0IHsgVmFsaWRhdGlvblJlc3VsdCB9IGZyb20gJy4vdmFsaWRhdGlvbi1yZXN1bHQuaW50ZXJmYWNlJztcblxuaW1wb3J0IHsgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbmV4cG9ydCBjbGFzcyBGb3JtQmFzZSB7XG4gIG5hbWU6IHN0cmluZztcbiAgaWQ6IHN0cmluZztcbiAgdHlwZTogc3RyaW5nO1xuICBjbGFzc2VzOiBzdHJpbmdbXTtcbiAgZmllbGRzOiBGaWVsZFNldDtcbiAgbWVzc2FnZTogc3RyaW5nO1xuICByZWFkT25seTogYm9vbGVhbjtcbiAgY2hhbmdlOiAoZmllbGQ6IEZvcm1CYXNlLCB2YWx1ZTogYW55KSA9PiBib29sZWFuO1xuICBibHVyOiAoZmllbGQ6IEZvcm1CYXNlLCB2YWx1ZTogYW55KSA9PiB2b2lkO1xuICBwcm90ZWN0ZWQgZm9ybTogRm9ybTtcblxuICBpc0Zvcm0gPSBmYWxzZTtcbiAgaGFzRGF0YSA9IGZhbHNlO1xuICBoYXNGaWVsZHMgPSBmYWxzZTtcbiAgaGFzTGFiZWwgPSB0cnVlO1xuICBpc0FjdGlvbiA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHtcbiAgICBuYW1lPzogc3RyaW5nLFxuICAgIGNsYXNzZXM/OiBzdHJpbmdbXSxcbiAgICBmaWVsZHM/OiBGb3JtQmFzZVtdLFxuICAgIHJlYWRPbmx5PzogYm9vbGVhbixcbiAgICBjaGFuZ2U/OiAoZmllbGQ6IEZvcm1CYXNlLCB2YWx1ZTogYW55KSA9PiBib29sZWFuLFxuICAgIGJsdXI/OiAoZmllbGQ6IEZvcm1CYXNlLCB2YWx1ZTogYW55KSA9PiB2b2lkXG4gIH0gPSB7fSkge1xuICAgIHRoaXMubmFtZSA9IG9wdGlvbnMubmFtZSB8fCAnJztcbiAgICB0aGlzLmlkID0gdGhpcy5uYW1lO1xuICAgIHRoaXMuY2xhc3NlcyA9IG9wdGlvbnMuY2xhc3NlcyB8fCBbXTtcbiAgICB0aGlzLmZpZWxkcyA9IG5ldyBGaWVsZFNldChvcHRpb25zLmZpZWxkcyB8fCBbXSk7XG4gICAgdGhpcy5yZWFkT25seSA9IG9wdGlvbnMucmVhZE9ubHkgfHwgZmFsc2U7XG4gICAgdGhpcy5jaGFuZ2UgPSBvcHRpb25zLmNoYW5nZSB8fCBudWxsO1xuICAgIHRoaXMuYmx1ciA9IG9wdGlvbnMuYmx1ciB8fCBudWxsO1xuICB9XG5cbiAgZ2V0Rm9ybSgpOiBGb3JtIHtcbiAgICByZXR1cm4gdGhpcy5mb3JtO1xuICB9XG5cbiAgc2V0Rm9ybShmb3JtOiBGb3JtKTogdm9pZCB7XG4gICAgdGhpcy5mb3JtID0gZm9ybTtcbiAgICBpZiAoZm9ybS5wcmVmaXgpIHtcbiAgICAgIHRoaXMuaWQgPSBgJHtmb3JtLnByZWZpeH0ke3RoaXMubmFtZX1gO1xuICAgIH1cbiAgICBpZiAoIXRoaXMuaXNGb3JtKSB7XG4gICAgICB0aGlzLmZpZWxkcy5zZXRGb3JtKGZvcm0pO1xuICAgIH1cbiAgfVxuXG4gIGFzeW5jIGRlcml2ZVZhbHVlKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZmFsc2UpO1xuICB9XG5cbiAgZ2V0IGNvbnRyb2woKTogRm9ybUNvbnRyb2wge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgb25DaGFuZ2UodmFsdWUpIHtcbiAgICBsZXQgcHJvcGFnYXRlID0gdHJ1ZTtcbiAgICBpZiAodGhpcy5jaGFuZ2UgIT0gbnVsbCkge1xuICAgICAgcHJvcGFnYXRlID0gdGhpcy5jaGFuZ2UodGhpcywgdmFsdWUpO1xuICAgIH1cbiAgICBpZiAocHJvcGFnYXRlKSB7XG4gICAgICB0aGlzLmZvcm0uZmllbGRDaGFuZ2UodGhpcy5uYW1lLCB2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgb25CbHVyKHZhbHVlKSB7XG4gICAgaWYgKHRoaXMuYmx1ciAhPSBudWxsKSB7XG4gICAgICB0aGlzLmJsdXIodGhpcywgdmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIG9uQ2xpY2soKSB7XG5cbiAgfVxuXG4gIHNldE1lc3NhZ2UobWVzc2FnZTogc3RyaW5nKTogdm9pZCB7XG4gICAgdGhpcy5tZXNzYWdlID0gbWVzc2FnZTtcbiAgfVxuXG4gIGxvYWRWYWxpZGF0aW9uKHJlc3VsdDogVmFsaWRhdGlvblJlc3VsdCk6IHZvaWQge1xuICAgIHRoaXMucmVzZXRWYWxpZGF0aW9uKCk7XG4gICAgaWYgKHJlc3VsdC5tZXNzYWdlcyAhPSBudWxsICYmIHJlc3VsdC5tZXNzYWdlcy5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLnNldE1lc3NhZ2UocmVzdWx0Lm1lc3NhZ2VzWzBdKTtcbiAgICB9XG4gICAgZm9yIChjb25zdCBrZXkgaW4gcmVzdWx0LmZpZWxkcykge1xuICAgICAgaWYgKHJlc3VsdC5maWVsZHMuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICBjb25zdCBmaWVsZDogRm9ybUJhc2UgPSB0aGlzLmZpZWxkcy5maWVsZEJ5TmFtZShrZXkpO1xuICAgICAgICBpZiAoZmllbGQgIT0gbnVsbCkge1xuICAgICAgICAgIGZpZWxkLmxvYWRWYWxpZGF0aW9uKHJlc3VsdC5maWVsZHNba2V5XSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXNldFZhbGlkYXRpb24oKSB7XG4gICAgdGhpcy5zZXRNZXNzYWdlKG51bGwpO1xuICAgIGlmICh0aGlzLmZpZWxkcykge1xuICAgICAgdGhpcy5maWVsZHMucmVzZXRWYWxpZGF0aW9uKCk7XG4gICAgfVxuICB9XG59XG4iLCJpbXBvcnQgeyBGb3JtIH0gZnJvbSAnLi4vZm9ybSc7XG5pbXBvcnQgeyBGb3JtQmFzZSB9IGZyb20gJy4uL2Zvcm0tYmFzZSc7XG5cblxuZXhwb3J0IGNsYXNzIEZpZWxkPFQ+IGV4dGVuZHMgRm9ybUJhc2Uge1xuICBpZDogc3RyaW5nO1xuICBoYXNEYXRhID0gdHJ1ZTtcbiAgdmFsdWU6IFQ7XG4gIGxhYmVsOiBzdHJpbmc7XG4gIHBsYWNlaG9sZGVyOiBzdHJpbmc7XG4gIGRlcml2ZTogKGZpZWxkOiBGaWVsZDxUPikgPT4gUHJvbWlzZTxib29sZWFuPjtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7XG4gICAgdmFsdWU/OiBULFxuICAgIGxhYmVsPzogc3RyaW5nLFxuICAgIGNsYXNzZXM/OiBzdHJpbmdbXSxcbiAgICBwbGFjZWhvbGRlcj86IHN0cmluZyxcbiAgICBkZXJpdmU/OiAoZmllbGQ6IEZpZWxkPFQ+KSA9PiBQcm9taXNlPGJvb2xlYW4+XG4gIH0gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMudmFsdWUgPSBvcHRpb25zLnZhbHVlO1xuICAgIHRoaXMubGFiZWwgPSBvcHRpb25zLmxhYmVsIHx8ICcnO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtZmllbGQnKTtcbiAgICB0aGlzLnBsYWNlaG9sZGVyID0gb3B0aW9ucy5wbGFjZWhvbGRlciB8fCAnJztcbiAgICBpZiAodGhpcy5wbGFjZWhvbGRlci5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLXBsYWNlaG9sZGVyJyk7XG4gICAgfVxuICAgIHRoaXMuZGVyaXZlID0gb3B0aW9ucy5kZXJpdmUgfHwgbnVsbDtcbiAgfVxuXG4gIHNldEZvcm0oZm9ybTogRm9ybSk6IHZvaWQge1xuICAgIHN1cGVyLnNldEZvcm0oZm9ybSk7XG4gICAgdGhpcy5kZXJpdmVWYWx1ZSgpO1xuICB9XG5cbiAgb25DaGFuZ2UodmFsdWUpIHtcbiAgICB0aGlzLnZhbHVlID0gdmFsdWU7XG4gICAgc3VwZXIub25DaGFuZ2UodGhpcy5wcmVwYXJlVmFsdWUodmFsdWUpKTtcbiAgfVxuXG4gIGFzeW5jIGRlcml2ZVZhbHVlKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgIHJldHVybiBzdXBlci5kZXJpdmVWYWx1ZSgpLnRoZW4oZGVyaXZlZCA9PiB7XG4gICAgICBpZiAoIWRlcml2ZWQpIHtcbiAgICAgICAgaWYgKHRoaXMuZGVyaXZlICE9IG51bGwpIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5kZXJpdmUodGhpcyk7XG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5mb3JtLm9iai5oYXNPd25Qcm9wZXJ0eSh0aGlzLm5hbWUpKSB7XG4gICAgICAgICAgdGhpcy52YWx1ZSA9IHRoaXMuZm9ybS5vYmpbdGhpcy5uYW1lXTtcbiAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHRydWUpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcbiAgICB9KTtcbiAgfVxuXG4gIHByZXBhcmVWYWx1ZSh2YWx1ZTogYW55KTogYW55IHtcbiAgICBpZiAodmFsdWUgPT0gbnVsbCkge1xuICAgICAgcmV0dXJuICcnO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gdmFsdWU7XG4gICAgfVxuICB9XG59XG4iLCJpbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4vZmllbGQnO1xuXG5cbmV4cG9ydCBjbGFzcyBDaGVja2JveEZpZWxkIGV4dGVuZHMgRmllbGQ8Ym9vbGVhbj4ge1xuICB0eXBlID0gJ2NoZWNrYm94JztcbiAgaGFzTGFiZWwgPSBmYWxzZTtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1jaGVja2JveCcpO1xuICB9XG59XG4iLCJpbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4vZmllbGQnO1xuXG5leHBvcnQgY2xhc3MgT3B0aW9uU2V0RmllbGQ8VD4gZXh0ZW5kcyBGaWVsZDxUPiB7XG4gIHR5cGUgPSAnb3B0aW9uc2V0JztcbiAgb3B0aW9uU2V0OiB7IGtleTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nLCBkaXNhYmxlZD86IGJvb2xlYW4gfVtdID0gW107XG4gIG9wdGlvbnM6IHsga2V5OiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcsIGRpc2FibGVkPzogYm9vbGVhbiB9W10gPSBbXTtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1vcHRpb25zZXQnKTtcbiAgICB0aGlzLm9wdGlvblNldCA9IG9wdGlvbnNbJ29wdGlvbnMnXSB8fCBbXTtcbiAgICBQcm9taXNlLnJlc29sdmUodGhpcy5vcHRpb25TZXQpLnRoZW4ob3B0aW9uTGlzdCA9PiB7XG4gICAgICB0aGlzLnNldE9wdGlvbnMob3B0aW9uTGlzdCk7XG4gICAgfSk7XG4gIH1cblxuICBzZXRPcHRpb25zKG9wdGlvbnM6IEFycmF5PHsga2V5OiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcsIGRpc2FibGVkPzogYm9vbGVhbiB9Pikge1xuICAgIHRoaXMub3B0aW9ucyA9IG9wdGlvbnM7XG4gIH1cbn1cbiIsImltcG9ydCB7IE9wdGlvblNldEZpZWxkIH0gZnJvbSAnLi9vcHRpb24tc2V0LWZpZWxkJztcblxuXG5leHBvcnQgY2xhc3MgQ2hlY2tib3hTZXRGaWVsZDxUPiBleHRlbmRzIE9wdGlvblNldEZpZWxkPFQ+IHtcbiAgdHlwZSA9ICdjaGVja2JveHNldCc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtY2hlY2tib3hzZXQnKTtcbiAgfVxuXG4gIG9uQ2hhbmdlKHZhbHVlKSB7XG4gICAgY29uc3Qgb2JqID0gdGhpcy5mb3JtLm9ialt0aGlzLm5hbWVdO1xuICAgIGlmICh0aGlzLmlzQ2hlY2tlZCh2YWx1ZSkpIHtcbiAgICAgIG9iai5zcGxpY2Uob2JqLmluZGV4T2YodmFsdWUpLCAxKTtcbiAgICB9IGVsc2Uge1xuICAgICAgb2JqLnB1c2godmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIGlzQ2hlY2tlZCh2YWx1ZSk6IGJvb2xlYW4ge1xuICAgIGNvbnN0IHNlbGVjdGVkID0gdGhpcy5mb3JtLm9ialt0aGlzLm5hbWVdO1xuICAgIGlmIChzZWxlY3RlZCAhPSBudWxsKSB7XG4gICAgICByZXR1cm4gc2VsZWN0ZWQuZmlsdGVyKG8gPT4gbyA9PT0gdmFsdWUpLmxlbmd0aCA+IDA7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgRm9ybUJhc2UgfSBmcm9tICcuLi9mb3JtLWJhc2UnO1xuXG5leHBvcnQgY2xhc3MgQ29udGVudEZpZWxkIGV4dGVuZHMgRm9ybUJhc2Uge1xuICB0eXBlID0gJ2NvbnRlbnQnO1xuICBjb250ZW50OiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMuY29udGVudCA9IG9wdGlvbnNbJ2NvbnRlbnQnXSB8fCAnJztcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWZpZWxkJyk7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1jb250ZW50Jyk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi9maWVsZCc7XG5cbmltcG9ydCB7IElNeURhdGVNb2RlbCB9IGZyb20gJ25neC1teWRhdGVwaWNrZXInO1xuXG5leHBvcnQgY2xhc3MgRGF0ZUZpZWxkIGV4dGVuZHMgRmllbGQ8RGF0ZT4ge1xuICB0eXBlID0gJ2RhdGUnO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWRhdGUnKTtcbiAgfVxuXG4gIG9uQ2hhbmdlKHZhbHVlOiBJTXlEYXRlTW9kZWwpIHtcbiAgICBzdXBlci5vbkNoYW5nZSgodmFsdWUuanNkYXRlID09IG51bGwpID8gbnVsbCA6IG5ldyBEYXRlKHZhbHVlLmpzZGF0ZSkpO1xuICB9XG59XG4iLCJpbXBvcnQgeyBPcHRpb25TZXRGaWVsZCB9IGZyb20gJy4vb3B0aW9uLXNldC1maWVsZCc7XG5cbmV4cG9ydCBjbGFzcyBEcm9wZG93bkZpZWxkIGV4dGVuZHMgT3B0aW9uU2V0RmllbGQ8c3RyaW5nPiB7XG4gIHR5cGUgPSAnZHJvcGRvd24nO1xuICBlbXB0eVRleHQ6IHN0cmluZztcbiAgbG9hZGVkOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtZHJvcGRvd24nKTtcbiAgICB0aGlzLmxvYWRlZCA9IG9wdGlvbnNbJ2VtcHR5VGV4dCddIHx8ICcnO1xuICAgIHRoaXMuZW1wdHlUZXh0ID0gb3B0aW9uc1snbG9hZGluZ1RleHQnXSB8fCAnJztcbiAgfVxuXG4gIHNldE9wdGlvbnMob3B0aW9uczogQXJyYXk8eyBrZXk6IHN0cmluZywgdmFsdWU6IHN0cmluZywgZGlzYWJsZWQ/OiBib29sZWFuIH0+KSB7XG4gICAgc3VwZXIuc2V0T3B0aW9ucyhvcHRpb25zKTtcbiAgICB0aGlzLmVtcHR5VGV4dCA9IHRoaXMubG9hZGVkO1xuICB9XG59XG4iLCJpbXBvcnQgeyBGb3JtQmFzZSB9IGZyb20gJy4uL2Zvcm0tYmFzZSc7XG5cbmV4cG9ydCBjbGFzcyBGaWVsZEdyb3VwIGV4dGVuZHMgRm9ybUJhc2Uge1xuICBoYXNGaWVsZHMgPSB0cnVlO1xuICB0eXBlID0gJ2dyb3VwJztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gIH1cblxuICBhc3luYyBkZXJpdmVWYWx1ZSgpOiBQcm9taXNlPGJvb2xlYW4+IHtcbiAgICByZXR1cm4gc3VwZXIuZGVyaXZlVmFsdWUoKS50aGVuKGRlcml2ZWQgPT4ge1xuICAgICAgdGhpcy5maWVsZHMudXBkYXRlVmFsdWVzKCk7XG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGRlcml2ZWQpO1xuICAgIH0pO1xuICB9XG59XG4iLCJpbXBvcnQgeyBGb3JtQmFzZSB9IGZyb20gJy4uL2Zvcm0tYmFzZSc7XG5cbmV4cG9ydCBjbGFzcyBGb3JtQWN0aW9uIGV4dGVuZHMgRm9ybUJhc2Uge1xuICBpc0FjdGlvbiA9IHRydWU7XG4gIHR5cGUgPSAnYWN0aW9uJztcbiAgdGl0bGU6IHN0cmluZztcbiAgaWNvbjogc3RyaW5nO1xuICBhY3Rpb25UeXBlOiBzdHJpbmc7XG4gIGxvYWRpbmc6IHN0cmluZztcbiAgY2xpY2s6IChhY3Rpb246IEZvcm1BY3Rpb24pID0+IHZvaWQ7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMudGl0bGUgPSBvcHRpb25zWyd0aXRsZSddIHx8IG51bGw7XG4gICAgdGhpcy5pY29uID0gb3B0aW9uc1snaWNvbiddIHx8IG51bGw7XG4gICAgdGhpcy5hY3Rpb25UeXBlID0gb3B0aW9uc1snYWN0aW9uVHlwZSddIHx8ICdzdWJtaXQnO1xuICAgIHRoaXMuY2xpY2sgPSBvcHRpb25zWydjbGljayddIHx8IG51bGw7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1hY3Rpb24nKTtcbiAgfVxuXG4gIHNob3dMb2FkaW5nKHRleHQ6IHN0cmluZykge1xuICAgIHRoaXMubG9hZGluZyA9IHRleHQ7XG4gIH1cblxuICBjbGVhckxvYWRpbmcoKSB7XG4gICAgdGhpcy5sb2FkaW5nID0gbnVsbDtcbiAgfVxuXG4gIG9uQ2xpY2soKSB7XG4gICAgaWYgKHRoaXMuY2xpY2sgIT0gbnVsbCkge1xuICAgICAgdGhpcy5jbGljayh0aGlzKTtcbiAgICB9XG4gIH1cbn1cbiIsImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuZXhwb3J0IGNsYXNzIEhlYWRlckZpZWxkIGV4dGVuZHMgRm9ybUJhc2Uge1xuICB0eXBlID0gJ2hlYWRlcic7XG4gIHRpdGxlOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMudGl0bGUgPSBvcHRpb25zWyd0aXRsZSddIHx8ICcnO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtZmllbGQnKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWhlYWRlcicpO1xuICB9XG59XG4iLCJpbXBvcnQgeyBGaWVsZEdyb3VwIH0gZnJvbSAnLi9maWVsZC1ncm91cCc7XG5pbXBvcnQgeyBIZWFkZXJGaWVsZCB9IGZyb20gJy4vaGVhZGVyLWZpZWxkJztcbmltcG9ydCB7IEZvcm1BY3Rpb24gfSBmcm9tICcuL2Zvcm0tYWN0aW9uJztcbmltcG9ydCB7IEZvcm0gfSBmcm9tICcuLi9mb3JtJztcbmltcG9ydCB7IFZhbGlkYXRpb25SZXN1bHQgfSBmcm9tICcuLi92YWxpZGF0aW9uLXJlc3VsdC5pbnRlcmZhY2UnO1xuXG5leHBvcnQgY2xhc3MgTXVsdGlDaGlsZEZpZWxkIGV4dGVuZHMgRmllbGRHcm91cCB7XG4gIHR5cGU6ICdtdWx0aWNoaWxkJztcbiAgc3ViRm9ybTogKG9iajogYW55LCBwcmVmaXg6IHN0cmluZykgPT4gRm9ybTtcbiAgbmV3T2JqOiAoKSA9PiBhbnk7XG4gIGFkZEJ1dHRvblRleHQ6IHN0cmluZztcbiAgcmVtb3ZlQnV0dG9uVGV4dDogc3RyaW5nO1xuICBjaGlsZCA9IGZhbHNlO1xuICBwcml2YXRlIGNvdW50ZXIgPSAwO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLnN1YkZvcm0gPSBvcHRpb25zWydzdWJGb3JtJ10gfHwgbnVsbDtcbiAgICB0aGlzLm5ld09iaiA9IG9wdGlvbnNbJ25ld09iaiddIHx8IG51bGw7XG4gICAgdGhpcy5hZGRCdXR0b25UZXh0ID0gb3B0aW9uc1snYWRkQnV0dG9uVGV4dCddIHx8ICdBZGQgTmV3IExpbmUnO1xuICAgIHRoaXMucmVtb3ZlQnV0dG9uVGV4dCA9IG9wdGlvbnNbJ3JlbW92ZUJ1dHRvblRleHQnXSB8fCAnUmVtb3ZlIExpbmUnO1xuICAgIHRoaXMuY2hpbGQgPSBvcHRpb25zWydjaGlsZCddIHx8IGZhbHNlO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtbXVsdGljaGlsZCcpO1xuICAgIGlmIChvcHRpb25zWyd0aXRsZSddKSB7XG4gICAgICB0aGlzLmZpZWxkcy5wdXNoKG5ldyBIZWFkZXJGaWVsZCh7XG4gICAgICAgIHRpdGxlOiBvcHRpb25zWyd0aXRsZSddXG4gICAgICB9KSk7XG4gICAgICB0aGlzLmZpZWxkcy5wdXNoKG5ldyBGaWVsZEdyb3VwKHtcbiAgICAgICAgbmFtZTogYCR7dGhpcy5uYW1lfV9jb250cm9sc2AsXG4gICAgICAgIGZpZWxkczogW1xuICAgICAgICAgIG5ldyBGb3JtQWN0aW9uKHtcbiAgICAgICAgICAgIHRpdGxlOiB0aGlzLmFkZEJ1dHRvblRleHQsXG4gICAgICAgICAgICBpY29uOiAncGx1cycsXG4gICAgICAgICAgICBjbGljazogKCkgPT4ge1xuICAgICAgICAgICAgICBpZiAodGhpcy5zdWJGb3JtICE9PSBudWxsICYmIHRoaXMubmV3T2JqICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgY29uc3Qgb2JqOiBhbnkgPSB0aGlzLm5ld09iaigpO1xuICAgICAgICAgICAgICAgIHRoaXMubGlzdC5wdXNoKG9iaik7XG4gICAgICAgICAgICAgICAgdGhpcy5hZGRDaGlsZChvYmopO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgY2xhc3NlcyA6IFsnbmRmLWFkZCddXG4gICAgICAgICAgfSlcbiAgICAgICAgXSxcbiAgICAgICAgY2xhc3NlczogWyduZGYtZnVsbC1ib3JkZXJlZCcsICduZGYtbXVsdGljaGlsZC1mb290ZXInXVxuICAgICAgfSkpO1xuICAgIH1cbiAgfVxuXG4gIGdldCBsaXN0KCk6IEFycmF5PGFueT4ge1xuICAgIGlmICh0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV0gPT0gbnVsbCkge1xuICAgICAgdGhpcy5mb3JtLm9ialt0aGlzLm5hbWVdID0gW107XG4gICAgfVxuICAgIHJldHVybiAodGhpcy5mb3JtLm9ialt0aGlzLm5hbWVdIGFzIEFycmF5PGFueT4pO1xuICB9XG5cbiAgc2V0Rm9ybShmb3JtOiBGb3JtKTogdm9pZCB7XG4gICAgc3VwZXIuc2V0Rm9ybShmb3JtKTtcbiAgICBpZiAodGhpcy5zdWJGb3JtICE9PSBudWxsKSB7XG4gICAgICB0aGlzLmxpc3QuZm9yRWFjaCgob2JqKSA9PiB7XG4gICAgICAgIHRoaXMuYWRkQ2hpbGQob2JqKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIGxvYWRWYWxpZGF0aW9uKHJlc3VsdDogVmFsaWRhdGlvblJlc3VsdCk6IHZvaWQge1xuICAgIHRoaXMucmVzZXRWYWxpZGF0aW9uKCk7XG4gICAgZm9yIChjb25zdCBrZXkgaW4gcmVzdWx0LmZpZWxkcykge1xuICAgICAgaWYgKHJlc3VsdC5maWVsZHMuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICBjb25zdCBjaGlsZCA9IGAke3RoaXMuZm9ybS5wcmVmaXh9bGluZV8ke2tleX1gO1xuICAgICAgICBjb25zdCBmaWVsZCA9IHRoaXMuZmllbGRzLmZpZWxkQnlOYW1lKGNoaWxkKTtcbiAgICAgICAgaWYgKGZpZWxkICE9IG51bGwpIHtcbiAgICAgICAgICBmaWVsZC5sb2FkVmFsaWRhdGlvbihyZXN1bHQuZmllbGRzW2tleV0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBhZGRDaGlsZChvYmo6IGFueSk6IHZvaWQge1xuICAgIGNvbnN0IGdyb3VwID0gYCR7dGhpcy5mb3JtLnByZWZpeH1saW5lXyR7dGhpcy5jb3VudGVyfV9ncm91cGA7XG4gICAgdGhpcy5maWVsZHMuaW5zZXJ0QmVmb3JlSWQoXG4gICAgICBuZXcgRmllbGRHcm91cCh7XG4gICAgICAgIG5hbWU6IGdyb3VwLFxuICAgICAgICBmaWVsZHM6IFtcbiAgICAgICAgICB0aGlzLnN1YkZvcm0ob2JqLCBgJHt0aGlzLmZvcm0ucHJlZml4fWxpbmVfJHt0aGlzLmNvdW50ZXJ9YCksXG4gICAgICAgICAgbmV3IEZvcm1BY3Rpb24oe1xuICAgICAgICAgICAgdGl0bGU6IHRoaXMucmVtb3ZlQnV0dG9uVGV4dCxcbiAgICAgICAgICAgIGljb246ICd0cmFzaCcsXG4gICAgICAgICAgICBjbGljazogKCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLmZpZWxkcy5yZW1vdmVCeUlkKGdyb3VwKTtcbiAgICAgICAgICAgICAgdGhpcy5saXN0LnNwbGljZSh0aGlzLmxpc3QuaW5kZXhPZihvYmopLCAxKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjbGFzc2VzIDogWyduZGYtcmVtb3ZlJ11cbiAgICAgICAgICB9KVxuICAgICAgICBdLFxuICAgICAgICBjbGFzc2VzOiBbJ25kZi1mdWxsLWJvcmRlcmVkJywgJ25kZi1tdWx0aWNoaWxkLXJvdycsICh0aGlzLmNoaWxkKSA/ICduZGYtY2hpbGQnIDogJ25kZi1wYXJlbnQnXVxuICAgICAgfSksIGAke3RoaXMubmFtZX1fY29udHJvbHNgXG4gICAgKTtcbiAgICB0aGlzLmNvdW50ZXIrKztcbiAgfVxufVxuIiwiaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuL2ZpZWxkJztcblxuZXhwb3J0IGNsYXNzIFRleHRGaWVsZCBleHRlbmRzIEZpZWxkPHN0cmluZz4ge1xuICB0eXBlID0gJ3RleHQnO1xuICBpbnB1dFR5cGU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy52YWx1ZSA9IG9wdGlvbnNbJ3ZhbHVlJ10gfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi10ZXh0Jyk7XG4gICAgdGhpcy5pbnB1dFR5cGUgPSBvcHRpb25zWydpbnB1dFR5cGUnXSB8fCAnJztcbiAgfVxufVxuIiwiaW1wb3J0IHsgVGV4dEZpZWxkIH0gZnJvbSAnLi90ZXh0LWZpZWxkJztcblxuXG5leHBvcnQgY2xhc3MgTnVtYmVyRmllbGQgZXh0ZW5kcyBUZXh0RmllbGQge1xuICBwcmVjaXNpb246IG51bWJlcjtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy52YWx1ZSA9IG9wdGlvbnNbJ3ZhbHVlJ10gfHwgMDtcbiAgICB0aGlzLnByZWNpc2lvbiA9IChvcHRpb25zWydwcmVjaXNpb24nXSAhPSBudWxsKSA/IG9wdGlvbnNbJ3ByZWNpc2lvbiddIDogMjtcbiAgfVxuXG4gIGFzeW5jIGRlcml2ZVZhbHVlKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgIHJldHVybiBzdXBlci5kZXJpdmVWYWx1ZSgpLnRoZW4oZGVyaXZlZCA9PiB7XG4gICAgICBpZiAoIWRlcml2ZWQpIHtcbiAgICAgICAgdGhpcy52YWx1ZSA9IHRoaXMucHJlcGFyZVZhbHVlKHRoaXMudmFsdWUpO1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHRydWUpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShmYWxzZSk7XG4gICAgfSk7XG4gIH1cblxuICBvbkJsdXIoKTogdm9pZCB7XG4gICAgdGhpcy52YWx1ZSA9IHRoaXMucHJlcGFyZVZhbHVlKHRoaXMudmFsdWUpO1xuICB9XG5cbiAgcHJlcGFyZVZhbHVlKHZhbHVlOiBhbnkpOiBhbnkge1xuICAgIGxldCBwcmVwYXJlZDogbnVtYmVyO1xuICAgIGlmICh0eXBlb2YgdmFsdWUgPT09ICdudW1iZXInKSB7XG4gICAgICBwcmVwYXJlZCA9IHZhbHVlIGFzIG51bWJlcjtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHByZXBhcmVkID0gMDtcbiAgICB9IGVsc2Uge1xuICAgICAgcHJlcGFyZWQgPSBwYXJzZUZsb2F0KHZhbHVlLnRvU3RyaW5nKCkucmVwbGFjZSgvW14wLTkuXS9nLCAnJykpO1xuICAgIH1cbiAgICByZXR1cm4gcHJlcGFyZWQudG9GaXhlZCh0aGlzLnByZWNpc2lvbik7XG4gIH1cbn1cbiIsImltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi9maWVsZCc7XG5cbmV4cG9ydCBjbGFzcyBUZXh0YXJlYUZpZWxkIGV4dGVuZHMgRmllbGQ8c3RyaW5nPiB7XG4gIHR5cGUgPSAndGV4dGFyZWEnO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLnZhbHVlID0gb3B0aW9uc1sndmFsdWUnXSB8fCAnJztcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLXRleHRhcmVhJyk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Db250cm9sLCBGb3JtR3JvdXAgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbmltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL2ZpZWxkJztcblxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRm9ybVNlcnZpY2Uge1xuICBwcml2YXRlIGZvcm1Hcm91cDogRm9ybUdyb3VwO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuZm9ybUdyb3VwID0gbmV3IEZvcm1Hcm91cCh7fSk7XG4gIH1cblxuICBnZXQgZ3JvdXAoKTogRm9ybUdyb3VwIHtcbiAgICByZXR1cm4gdGhpcy5mb3JtR3JvdXA7XG4gIH1cblxuICByZWdpc3RlckZpZWxkKGZpZWxkOiBGaWVsZDxhbnk+KSB7XG4gICAgaWYgKCF0aGlzLmdyb3VwLmNvbnRhaW5zKGZpZWxkLmlkKSkge1xuICAgICAgdGhpcy5ncm91cC5hZGRDb250cm9sKGZpZWxkLmlkLCB0aGlzLmNvbnRyb2xGcm9tRmllbGQoZmllbGQpKTtcbiAgICB9XG4gIH1cblxuICByZWdpc3RlckZpZWxkcyhmaWVsZHM6IEZpZWxkPGFueT5bXSkge1xuICAgIGZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcbiAgICAgIHRoaXMucmVnaXN0ZXJGaWVsZChmaWVsZCk7XG4gICAgfSk7XG4gIH1cblxuICBkZXJlZ2lzdGVyRmllbGQoZmllbGQ6IEZpZWxkPGFueT4pIHtcbiAgICB0aGlzLmdyb3VwLnJlbW92ZUNvbnRyb2woZmllbGQuaWQpO1xuICB9XG5cbiAgZGVyZWdpc3RlckZpZWxkcyhmaWVsZHM6IEZpZWxkPGFueT5bXSkge1xuICAgIGZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcbiAgICAgIHRoaXMuZGVyZWdpc3RlckZpZWxkKGZpZWxkKTtcbiAgICB9KTtcbiAgfVxuXG4gIGNvbnRyb2xGcm9tRmllbGQoZmllbGQ6IEZpZWxkPGFueT4pOiBGb3JtQ29udHJvbCB7XG4gICAgcmV0dXJuIG5ldyBGb3JtQ29udHJvbChmaWVsZC52YWx1ZSB8fCAnJyk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi9mb3JtLWJhc2UnO1xuXG5cbmV4cG9ydCBjbGFzcyBGb3JtIGV4dGVuZHMgRm9ybUJhc2Uge1xuICBvYmo6IGFueTtcbiAgc3VibWl0OiAoZm9ybTogRm9ybSkgPT4gdm9pZDtcbiAgaGFzRmllbGRzID0gdHJ1ZTtcbiAgaXNGb3JtID0gdHJ1ZTtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7XG4gICAgbmFtZT86IHN0cmluZyxcbiAgICBmaWVsZHM/OiBGb3JtQmFzZVtdLFxuICAgIG9iaj86IGFueSxcbiAgICBzdWJtaXQ/OiAoZm9ybTogRm9ybSkgPT4gdm9pZDtcbiAgICBjbGFzc2VzPzogc3RyaW5nW11cbiAgfSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5vYmogPSBvcHRpb25zWydvYmonXSB8fCB7fTtcbiAgICB0aGlzLnN1Ym1pdCA9IG9wdGlvbnNbJ3N1Ym1pdCddIHx8IG51bGw7XG4gICAgdGhpcy5maWVsZHMuc2V0Rm9ybSh0aGlzKTtcbiAgfVxuXG4gIGdldCBwcmVmaXgoKTogc3RyaW5nIHtcbiAgICBpZiAodGhpcy5mb3JtKSB7XG4gICAgICByZXR1cm4gYCR7dGhpcy5mb3JtLnByZWZpeH0ke3RoaXMubmFtZX1fYDtcbiAgICB9IGVsc2UgaWYgKHRoaXMubmFtZSkge1xuICAgICAgcmV0dXJuIGAke3RoaXMubmFtZX1fYDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuICcnO1xuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZShvYmo6IGFueSkge1xuICAgIHRoaXMub2JqID0gb2JqO1xuICAgIHRoaXMuZmllbGRzLnVwZGF0ZVZhbHVlcygpO1xuICB9XG5cbiAgZmllbGRDaGFuZ2Uoa2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICB0aGlzLm9ialtrZXldID0gdmFsdWU7XG4gIH1cblxuICBvblN1Ym1pdCgpIHtcbiAgICBpZiAodGhpcy5zdWJtaXQgIT09IG51bGwpIHtcbiAgICAgIHRoaXMuc3VibWl0KHRoaXMpO1xuICAgIH1cbiAgfVxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbmltcG9ydCB7IEZvcm0gfSBmcm9tICcuLi9mb3JtJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtZm9ybScsXG4gIHRlbXBsYXRlVXJsOiAnLi9mb3JtLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIGZvcm06IEZvcm07XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBmb3JtU2VydmljZTogRm9ybVNlcnZpY2UpIHt9XG5cbiAgbmdPbkluaXQoKSB7XG5cbiAgfVxuXG4gIGdldCBncm91cCgpOiBGb3JtR3JvdXAge1xuICAgIHJldHVybiB0aGlzLmZvcm1TZXJ2aWNlLmdyb3VwO1xuICB9XG5cbiAgb25TdWJtaXQoKSB7XG4gICAgdGhpcy5mb3JtLm9uU3VibWl0KCk7XG4gIH1cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbmltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcbmltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL2ZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWZvcm0taXRlbScsXG4gIHRlbXBsYXRlVXJsOiAnLi9mb3JtLWl0ZW0uY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBGb3JtSXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgaXRlbTogRm9ybUJhc2U7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBmb3JtU2VydmljZTogRm9ybVNlcnZpY2UpIHt9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgaWYgKHRoaXMuaXRlbS5oYXNEYXRhKSB7XG4gICAgICB0aGlzLmZvcm1TZXJ2aWNlLnJlZ2lzdGVyRmllbGQodGhpcy5pdGVtIGFzIEZpZWxkPGFueT4pO1xuICAgIH1cbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICAgIGlmICh0aGlzLml0ZW0uaGFzRGF0YSkge1xuICAgICAgdGhpcy5mb3JtU2VydmljZS5kZXJlZ2lzdGVyRmllbGQodGhpcy5pdGVtIGFzIEZpZWxkPGFueT4pO1xuICAgIH1cbiAgfVxuXG4gIGdldCBncm91cCgpOiBGb3JtR3JvdXAge1xuICAgIHJldHVybiB0aGlzLmZvcm1TZXJ2aWNlLmdyb3VwO1xuICB9XG5cbiAgb25DaGFuZ2UodmFsdWUpIHtcbiAgICB0aGlzLml0ZW0ub25DaGFuZ2UodmFsdWUpO1xuICB9XG5cbiAgb25CbHVyKHZhbHVlKSB7XG4gICAgdGhpcy5pdGVtLm9uQmx1cih2YWx1ZSk7XG4gIH1cblxuICBvbkNsaWNrKCkge1xuICAgIHRoaXMuaXRlbS5vbkNsaWNrKCk7XG4gIH1cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2hlY2tib3hGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9jaGVja2JveC1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1jaGVja2JveC1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9jaGVja2JveC1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIENoZWNrYm94RmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IENoZWNrYm94RmllbGQ7XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IENoZWNrYm94U2V0RmllbGQgfSBmcm9tICcuLi9maWVsZHMvY2hlY2tib3gtc2V0LWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWNoZWNrYm94LXNldC1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9jaGVja2JveC1zZXQtZmllbGQuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBDaGVja2JveFNldEZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBDaGVja2JveFNldEZpZWxkPGFueT47XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRlbnRGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9jb250ZW50LWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWNvbnRlbnQtZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vY29udGVudC1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIENvbnRlbnRGaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogQ29udGVudEZpZWxkO1xufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEYXRlRmllbGQgfSBmcm9tICcuLi9maWVsZHMvZGF0ZS1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1kYXRlLWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2RhdGUtZmllbGQuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBEYXRlRmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IERhdGVGaWVsZDtcbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRHJvcGRvd25GaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9kcm9wZG93bi1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1kcm9wZG93bi1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9kcm9wZG93bi1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIERyb3Bkb3duRmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IERyb3Bkb3duRmllbGQ7XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IEZpZWxkR3JvdXAgfSBmcm9tICcuLi9maWVsZHMvZmllbGQtZ3JvdXAnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtZmllbGQtZ3JvdXAnLFxuICB0ZW1wbGF0ZVVybDogJy4vZmllbGQtZ3JvdXAuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBGaWVsZEdyb3VwQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBGaWVsZEdyb3VwO1xufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGb3JtQWN0aW9uIH0gZnJvbSAnLi4vZmllbGRzL2Zvcm0tYWN0aW9uJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWZvcm0tYWN0aW9uJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Zvcm0tYWN0aW9uLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgRm9ybUFjdGlvbkNvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogRm9ybUFjdGlvbjtcblxuICBnZXQgbGFiZWwoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5pdGVtLmxvYWRpbmcgPyB0aGlzLml0ZW0ubG9hZGluZyA6IHRoaXMuaXRlbS50aXRsZTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBIZWFkZXJGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9oZWFkZXItZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtaGVhZGVyLWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2hlYWRlci1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIEhlYWRlckZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBIZWFkZXJGaWVsZDtcbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgT3B0aW9uU2V0RmllbGQgfSBmcm9tICcuLi9maWVsZHMvb3B0aW9uLXNldC1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1vcHRpb24tc2V0LWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL29wdGlvbi1zZXQtZmllbGQuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBPcHRpb25TZXRGaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogT3B0aW9uU2V0RmllbGQ8YW55Pjtcbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGV4dEZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL3RleHQtZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtdGV4dC1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi90ZXh0LWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgVGV4dEZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBUZXh0RmllbGQ7XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IFRleHRhcmVhRmllbGQgfSBmcm9tICcuLi9maWVsZHMvdGV4dGFyZWEtZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtdGV4dGFyZWEtZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vdGV4dGFyZWEtZmllbGQuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBUZXh0YXJlYUZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBUZXh0YXJlYUZpZWxkO1xufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgQW5ndWxhckZvbnRBd2Vzb21lTW9kdWxlIH0gZnJvbSAnYW5ndWxhci1mb250LWF3ZXNvbWUnO1xuaW1wb3J0IHsgTmd4TXlEYXRlUGlja2VyTW9kdWxlIH0gZnJvbSAnbmd4LW15ZGF0ZXBpY2tlcic7XG5cbmltcG9ydCB7IEZvcm1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5cbmltcG9ydCB7IENoZWNrYm94RmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2hlY2tib3gtZmllbGQuY29tcG9uZW50JztcbmltcG9ydCB7IENoZWNrYm94U2V0RmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2hlY2tib3gtc2V0LWZpZWxkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb250ZW50RmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29udGVudC1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRGF0ZUZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2RhdGUtZmllbGQuY29tcG9uZW50JztcbmltcG9ydCB7IERyb3Bkb3duRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZHJvcGRvd24tZmllbGQuY29tcG9uZW50JztcbmltcG9ydCB7IEZpZWxkR3JvdXBDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmllbGQtZ3JvdXAuY29tcG9uZW50JztcbmltcG9ydCB7IEZvcm1BY3Rpb25Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybS1hY3Rpb24uY29tcG9uZW50JztcbmltcG9ydCB7IEhlYWRlckZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2hlYWRlci1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgT3B0aW9uU2V0RmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvb3B0aW9uLXNldC1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGV4dEZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3RleHQtZmllbGQuY29tcG9uZW50JztcbmltcG9ydCB7IFRleHRhcmVhRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdGV4dGFyZWEtZmllbGQuY29tcG9uZW50JztcblxuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcbiAgICBBbmd1bGFyRm9udEF3ZXNvbWVNb2R1bGUsXG4gICAgTmd4TXlEYXRlUGlja2VyTW9kdWxlLmZvclJvb3QoKVxuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBGb3JtQ29tcG9uZW50LFxuICAgIEZvcm1JdGVtQ29tcG9uZW50LFxuXG4gICAgQ2hlY2tib3hGaWVsZENvbXBvbmVudCxcbiAgICBDaGVja2JveFNldEZpZWxkQ29tcG9uZW50LFxuICAgIENvbnRlbnRGaWVsZENvbXBvbmVudCxcbiAgICBEYXRlRmllbGRDb21wb25lbnQsXG4gICAgRHJvcGRvd25GaWVsZENvbXBvbmVudCxcbiAgICBGaWVsZEdyb3VwQ29tcG9uZW50LFxuICAgIEZvcm1BY3Rpb25Db21wb25lbnQsXG4gICAgSGVhZGVyRmllbGRDb21wb25lbnQsXG4gICAgT3B0aW9uU2V0RmllbGRDb21wb25lbnQsXG4gICAgVGV4dEZpZWxkQ29tcG9uZW50LFxuICAgIFRleHRhcmVhRmllbGRDb21wb25lbnRcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIEZvcm1Db21wb25lbnQsXG4gICAgRm9ybUl0ZW1Db21wb25lbnQsXG5cbiAgICBDaGVja2JveEZpZWxkQ29tcG9uZW50LFxuICAgIENoZWNrYm94U2V0RmllbGRDb21wb25lbnQsXG4gICAgQ29udGVudEZpZWxkQ29tcG9uZW50LFxuICAgIERhdGVGaWVsZENvbXBvbmVudCxcbiAgICBEcm9wZG93bkZpZWxkQ29tcG9uZW50LFxuICAgIEZpZWxkR3JvdXBDb21wb25lbnQsXG4gICAgRm9ybUFjdGlvbkNvbXBvbmVudCxcbiAgICBIZWFkZXJGaWVsZENvbXBvbmVudCxcbiAgICBPcHRpb25TZXRGaWVsZENvbXBvbmVudCxcbiAgICBUZXh0RmllbGRDb21wb25lbnQsXG4gICAgVGV4dGFyZWFGaWVsZENvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIE5nRHluYW1pY0Zvcm1zTW9kdWxlIHtcbiAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5nTW9kdWxlOiBOZ0R5bmFtaWNGb3Jtc01vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG4gICAgfTtcbiAgfVxufVxuIl0sIm5hbWVzIjpbInRzbGliXzEuX19leHRlbmRzIiwiRm9ybUdyb3VwIiwiRm9ybUNvbnRyb2wiLCJJbmplY3RhYmxlIiwiQ29tcG9uZW50IiwiSW5wdXQiLCJOZ01vZHVsZSIsIkNvbW1vbk1vZHVsZSIsIkZvcm1zTW9kdWxlIiwiUmVhY3RpdmVGb3Jtc01vZHVsZSIsIkFuZ3VsYXJGb250QXdlc29tZU1vZHVsZSIsIk5neE15RGF0ZVBpY2tlck1vZHVsZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQUE7Ozs7Ozs7Ozs7Ozs7O0lBY0E7SUFFQSxJQUFJLGFBQWEsR0FBRyxVQUFTLENBQUMsRUFBRSxDQUFDO1FBQzdCLGFBQWEsR0FBRyxNQUFNLENBQUMsY0FBYzthQUNoQyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsWUFBWSxLQUFLLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUM1RSxVQUFVLENBQUMsRUFBRSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUFFLElBQUksQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0JBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDL0UsT0FBTyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQy9CLENBQUMsQ0FBQztBQUVGLGFBQWdCLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxQixhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3BCLFNBQVMsRUFBRSxLQUFLLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLEVBQUU7UUFDdkMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxDQUFDLEtBQUssSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN6RixDQUFDO0FBRUQsYUFvQ2dCLFNBQVMsQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUMsRUFBRSxTQUFTO1FBQ3ZELE9BQU8sS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLE9BQU8sQ0FBQyxFQUFFLFVBQVUsT0FBTyxFQUFFLE1BQU07WUFDckQsU0FBUyxTQUFTLENBQUMsS0FBSyxJQUFJLElBQUk7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzthQUFFO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQUUsRUFBRTtZQUMzRixTQUFTLFFBQVEsQ0FBQyxLQUFLLElBQUksSUFBSTtnQkFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7YUFBRTtZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUFFLEVBQUU7WUFDOUYsU0FBUyxJQUFJLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxVQUFVLE9BQU8sSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUMsRUFBRTtZQUMvSSxJQUFJLENBQUMsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsVUFBVSxJQUFJLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7U0FDekUsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztBQUVELGFBQWdCLFdBQVcsQ0FBQyxPQUFPLEVBQUUsSUFBSTtRQUNyQyxJQUFJLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLGNBQWEsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztnQkFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDakgsT0FBTyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLE9BQU8sTUFBTSxLQUFLLFVBQVUsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLGNBQWEsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3pKLFNBQVMsSUFBSSxDQUFDLENBQUMsSUFBSSxPQUFPLFVBQVUsQ0FBQyxJQUFJLE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUU7UUFDbEUsU0FBUyxJQUFJLENBQUMsRUFBRTtZQUNaLElBQUksQ0FBQztnQkFBRSxNQUFNLElBQUksU0FBUyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7WUFDOUQsT0FBTyxDQUFDO2dCQUFFLElBQUk7b0JBQ1YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJO3dCQUFFLE9BQU8sQ0FBQyxDQUFDO29CQUM3SixJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQzt3QkFBRSxFQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUNULEtBQUssQ0FBQyxDQUFDO3dCQUFDLEtBQUssQ0FBQzs0QkFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDOzRCQUFDLE1BQU07d0JBQzlCLEtBQUssQ0FBQzs0QkFBRSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7NEJBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxDQUFDO3dCQUN4RCxLQUFLLENBQUM7NEJBQUUsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDOzRCQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQUMsU0FBUzt3QkFDakQsS0FBSyxDQUFDOzRCQUFFLEVBQUUsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDOzRCQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7NEJBQUMsU0FBUzt3QkFDakQ7NEJBQ0ksSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dDQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7Z0NBQUMsU0FBUzs2QkFBRTs0QkFDNUcsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0NBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQUMsTUFBTTs2QkFBRTs0QkFDdEYsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dDQUFFLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7Z0NBQUMsTUFBTTs2QkFBRTs0QkFDckUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0NBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0NBQUMsTUFBTTs2QkFBRTs0QkFDbkUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7NEJBQ3RCLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7NEJBQUMsU0FBUztxQkFDOUI7b0JBQ0QsRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUM5QjtnQkFBQyxPQUFPLENBQUMsRUFBRTtvQkFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFBRTt3QkFBUztvQkFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFBRTtZQUMxRCxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO2dCQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQztTQUNwRjtJQUNMLENBQUM7Ozs7OztBQ2hHRDtRQUNFLGtCQUFvQixHQUFlO1lBQWYsUUFBRyxHQUFILEdBQUcsQ0FBWTtTQUFLOzs7OztRQUV4QywwQkFBTzs7OztZQUFQLFVBQVEsSUFBVTtnQkFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsVUFBQyxLQUFLO29CQUNyQixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNyQixDQUFDLENBQUM7YUFDSjs7OztRQUVELCtCQUFZOzs7WUFBWjtnQkFDRSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQUs7b0JBQ3JCLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDckIsQ0FBQyxDQUFDO2FBQ0o7UUFFRCxzQkFBSSw0QkFBTTs7O2dCQUFWO2dCQUNFLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQzthQUNqQjs7O1dBQUE7Ozs7O1FBRUQsdUJBQUk7Ozs7WUFBSixVQUFLLElBQWM7Z0JBQ2pCLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3JCOzs7Ozs7OztRQUVELCtCQUFZOzs7Ozs7O1lBQVosVUFBYSxJQUFjLEVBQUUsTUFBYyxFQUFFLElBQVksRUFBRSxLQUFzQjtnQkFBdEIsc0JBQUE7b0JBQUEsWUFBc0I7OztvQkFDM0UsTUFBTSxHQUFhLElBQUk7Z0JBQzNCLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTs7d0JBQ1osTUFBTSxHQUEwQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUM7b0JBQ3hGLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO29CQUN2QixLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztpQkFDdEI7Z0JBQ0QsSUFBSSxNQUFNLEtBQUssSUFBSSxFQUFFO29CQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7aUJBQ25EO3FCQUFNO29CQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUN2RDthQUNGOzs7Ozs7O1FBRUQsbUNBQWdCOzs7Ozs7WUFBaEIsVUFBaUIsSUFBYyxFQUFFLE1BQWMsRUFBRSxLQUFzQjtnQkFBdEIsc0JBQUE7b0JBQUEsWUFBc0I7O2dCQUNyRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDdkQ7Ozs7Ozs7UUFFRCxpQ0FBYzs7Ozs7O1lBQWQsVUFBZSxJQUFjLEVBQUUsTUFBYyxFQUFFLEtBQXNCO2dCQUF0QixzQkFBQTtvQkFBQSxZQUFzQjs7Z0JBQ25FLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQzthQUNyRDs7Ozs7Ozs7UUFFRCw4QkFBVzs7Ozs7OztZQUFYLFVBQVksSUFBYyxFQUFFLEtBQWEsRUFBRSxJQUFZLEVBQUUsS0FBc0I7Z0JBQXRCLHNCQUFBO29CQUFBLFlBQXNCOzs7b0JBQ3pFLE1BQU0sR0FBYSxJQUFJO2dCQUMzQixJQUFJLEtBQUssS0FBSyxJQUFJLEVBQUU7O3dCQUNaLE1BQU0sR0FBMEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO29CQUN2RixNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztvQkFDdkIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7aUJBQ3RCO2dCQUNELElBQUksTUFBTSxLQUFLLElBQUksRUFBRTtvQkFDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDdkQ7cUJBQU07b0JBQ0wsTUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7aUJBQ3JEO2FBQ0Y7Ozs7Ozs7O1FBRUQsa0NBQWU7Ozs7Ozs7WUFBZixVQUFnQixJQUFjLEVBQUUsS0FBYSxFQUFFLEtBQWEsRUFBRSxLQUFzQjtnQkFBdEIsc0JBQUE7b0JBQUEsWUFBc0I7O2dCQUNsRixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQzlDOzs7Ozs7OztRQUVELGdDQUFhOzs7Ozs7O1lBQWIsVUFBYyxJQUFjLEVBQUUsS0FBYSxFQUFFLEtBQWEsRUFBRSxLQUFzQjtnQkFBdEIsc0JBQUE7b0JBQUEsWUFBc0I7O2dCQUNoRixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQzVDOzs7Ozs7O1FBRUQseUJBQU07Ozs7OztZQUFOLFVBQU8sR0FBVyxFQUFFLEtBQXNCLEVBQUUsSUFBWTtnQkFBcEMsc0JBQUE7b0JBQUEsWUFBc0I7OztvQkFDcEMsTUFBTSxHQUFhLElBQUk7Z0JBQzNCLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTs7d0JBQ1osTUFBTSxHQUEwQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUM7b0JBQ3JGLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO29CQUN2QixLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztpQkFDdEI7Z0JBQ0QsSUFBSSxNQUFNLEtBQUssSUFBSSxFQUFFO29CQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztpQkFDN0M7cUJBQU07b0JBQ0wsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDeEM7YUFDRjs7Ozs7O1FBRUQsK0JBQVk7Ozs7O1lBQVosVUFBYSxJQUFZLEVBQUUsS0FBc0I7Z0JBQXRCLHNCQUFBO29CQUFBLFlBQXNCOztnQkFDL0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ2xDOzs7Ozs7UUFFRCw2QkFBVTs7Ozs7WUFBVixVQUFXLEVBQVUsRUFBRSxLQUFzQjtnQkFBdEIsc0JBQUE7b0JBQUEsWUFBc0I7O2dCQUMzQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDOUI7Ozs7OztRQUVELHdCQUFLOzs7OztZQUFMLFVBQU0sR0FBVyxFQUFFLElBQVk7Z0JBQzdCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDO2FBQzlDOzs7OztRQUVELDhCQUFXOzs7O1lBQVgsVUFBWSxJQUFZO2dCQUN0QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ2pDOzs7OztRQUVELDRCQUFTOzs7O1lBQVQsVUFBVSxFQUFVO2dCQUNsQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQzdCOzs7Ozs7UUFFRCxrQ0FBZTs7Ozs7WUFBZixVQUFnQixHQUFXLEVBQUUsSUFBWTs7b0JBQ25DLEtBQUssR0FBUSxJQUFJOztvQkFDakIsTUFBTSxHQUFRLElBQUk7O29CQUNoQixNQUFNLEdBQVUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFBLENBQUM7Z0JBQ25FLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3JCLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ25CO3FCQUFNOzt3QkFDQyxPQUFPLEdBQVUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLENBQUMsU0FBUyxLQUFLLElBQUksR0FBQSxDQUFDO29CQUN6RSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7NEJBQ2pDLEtBQUssR0FBZ0MsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQzt3QkFDdkYsSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLElBQUksRUFBRTs0QkFDdkIsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7NEJBQ3BCLE1BQU0sR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3BCLE1BQU07eUJBQ1A7cUJBQ0Y7aUJBQ0Y7Z0JBQ0QsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDO2FBQ3pDOzs7OztRQUVELHdDQUFxQjs7OztZQUFyQixVQUFzQixJQUFZO2dCQUNoQyxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQzNDOzs7OztRQUVELHNDQUFtQjs7OztZQUFuQixVQUFvQixFQUFVO2dCQUM1QixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ3ZDOzs7O1FBRUQsa0NBQWU7OztZQUFmO2dCQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztvQkFDdkIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO2lCQUN6QixDQUFDLENBQUM7YUFDSjtRQUNILGVBQUM7SUFBRCxDQUFDOzs7Ozs7O1FDakhDLGtCQUFZLE9BT047WUFQTSx3QkFBQTtnQkFBQSxZQU9OOztZQWJOLFdBQU0sR0FBRyxLQUFLLENBQUM7WUFDZixZQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ2hCLGNBQVMsR0FBRyxLQUFLLENBQUM7WUFDbEIsYUFBUSxHQUFHLElBQUksQ0FBQztZQUNoQixhQUFRLEdBQUcsS0FBSyxDQUFDO1lBVWYsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztZQUMvQixJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztZQUNyQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksRUFBRSxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQztZQUMxQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUM7U0FDbEM7Ozs7UUFFRCwwQkFBTzs7O1lBQVA7Z0JBQ0UsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO2FBQ2xCOzs7OztRQUVELDBCQUFPOzs7O1lBQVAsVUFBUSxJQUFVO2dCQUNoQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDakIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO29CQUNmLElBQUksQ0FBQyxFQUFFLEdBQUcsS0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFNLENBQUM7aUJBQ3hDO2dCQUNELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO29CQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDM0I7YUFDRjs7OztRQUVLLDhCQUFXOzs7WUFBakI7Ozt3QkFDRSxzQkFBTyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFDOzs7YUFDL0I7UUFFRCxzQkFBSSw2QkFBTzs7O2dCQUFYO2dCQUNFLE9BQU8sSUFBSSxDQUFDO2FBQ2I7OztXQUFBOzs7OztRQUVELDJCQUFROzs7O1lBQVIsVUFBUyxLQUFLOztvQkFDUixTQUFTLEdBQUcsSUFBSTtnQkFDcEIsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksRUFBRTtvQkFDdkIsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUN0QztnQkFDRCxJQUFJLFNBQVMsRUFBRTtvQkFDYixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUN6QzthQUNGOzs7OztRQUVELHlCQUFNOzs7O1lBQU4sVUFBTyxLQUFLO2dCQUNWLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUN4QjthQUNGOzs7O1FBRUQsMEJBQU87OztZQUFQO2FBRUM7Ozs7O1FBRUQsNkJBQVU7Ozs7WUFBVixVQUFXLE9BQWU7Z0JBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO2FBQ3hCOzs7OztRQUVELGlDQUFjOzs7O1lBQWQsVUFBZSxNQUF3QjtnQkFDckMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUN2QixJQUFJLE1BQU0sQ0FBQyxRQUFRLElBQUksSUFBSSxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDekQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3JDO2dCQUNELEtBQUssSUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTtvQkFDL0IsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTs7NEJBQy9CLEtBQUssR0FBYSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUM7d0JBQ3BELElBQUksS0FBSyxJQUFJLElBQUksRUFBRTs0QkFDakIsS0FBSyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7eUJBQzFDO3FCQUNGO2lCQUNGO2FBQ0Y7Ozs7UUFFRCxrQ0FBZTs7O1lBQWY7Z0JBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdEIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO29CQUNmLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLENBQUM7aUJBQy9CO2FBQ0Y7UUFDSCxlQUFDO0lBQUQsQ0FBQzs7Ozs7Ozs7O0FDekdEOzs7UUFBOEJBLHlCQUFRO1FBUXBDLGVBQVksT0FNTjtZQU5NLHdCQUFBO2dCQUFBLFlBTU47O1lBTk4sWUFPRSxrQkFBTSxPQUFPLENBQUMsU0FTZjtZQXRCRCxhQUFPLEdBQUcsSUFBSSxDQUFDO1lBY2IsS0FBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO1lBQzNCLEtBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7WUFDakMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDL0IsS0FBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsV0FBVyxJQUFJLEVBQUUsQ0FBQztZQUM3QyxJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDL0IsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzthQUN0QztZQUNELEtBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUM7O1NBQ3RDOzs7OztRQUVELHVCQUFPOzs7O1lBQVAsVUFBUSxJQUFVO2dCQUNoQixpQkFBTSxPQUFPLFlBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUNwQjs7Ozs7UUFFRCx3QkFBUTs7OztZQUFSLFVBQVMsS0FBSztnQkFDWixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztnQkFDbkIsaUJBQU0sUUFBUSxZQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzthQUMxQzs7OztRQUVLLDJCQUFXOzs7WUFBakI7Ozs7d0JBQ0Usc0JBQU8saUJBQU0sV0FBVyxXQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztnQ0FDckMsSUFBSSxDQUFDLE9BQU8sRUFBRTtvQ0FDWixJQUFJLEtBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxFQUFFO3dDQUN2QixPQUFPLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLENBQUM7cUNBQzFCO3lDQUFNLElBQUksS0FBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTt3Q0FDbEQsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0NBQ3RDLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztxQ0FDOUI7aUNBQ0Y7Z0NBQ0QsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDOzZCQUMvQixDQUFDLEVBQUM7OzthQUNKOzs7OztRQUVELDRCQUFZOzs7O1lBQVosVUFBYSxLQUFVO2dCQUNyQixJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7b0JBQ2pCLE9BQU8sRUFBRSxDQUFDO2lCQUNYO3FCQUFNO29CQUNMLE9BQU8sS0FBSyxDQUFDO2lCQUNkO2FBQ0Y7UUFDSCxZQUFDO0lBQUQsQ0F6REEsQ0FBOEIsUUFBUTs7Ozs7OztRQ0RIQSxpQ0FBYztRQUkvQyx1QkFBWSxPQUFnQjtZQUFoQix3QkFBQTtnQkFBQSxZQUFnQjs7WUFBNUIsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FFZjtZQU5ELFVBQUksR0FBRyxVQUFVLENBQUM7WUFDbEIsY0FBUSxHQUFHLEtBQUssQ0FBQztZQUlmLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDOztTQUNuQztRQUNILG9CQUFDO0lBQUQsQ0FSQSxDQUFtQyxLQUFLOzs7Ozs7Ozs7QUNEeEM7OztRQUF1Q0Esa0NBQVE7UUFLN0Msd0JBQVksT0FBZ0I7WUFBaEIsd0JBQUE7Z0JBQUEsWUFBZ0I7O1lBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBTWY7WUFYRCxVQUFJLEdBQUcsV0FBVyxDQUFDO1lBQ25CLGVBQVMsR0FBeUQsRUFBRSxDQUFDO1lBQ3JFLGFBQU8sR0FBeUQsRUFBRSxDQUFDO1lBSWpFLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ25DLEtBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxVQUFVO2dCQUM3QyxLQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQzdCLENBQUMsQ0FBQzs7U0FDSjs7Ozs7UUFFRCxtQ0FBVTs7OztZQUFWLFVBQVcsT0FBa0U7Z0JBQzNFLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO2FBQ3hCO1FBQ0gscUJBQUM7SUFBRCxDQWpCQSxDQUF1QyxLQUFLOzs7Ozs7Ozs7QUNDNUM7OztRQUF5Q0Esb0NBQWlCO1FBR3hELDBCQUFZLE9BQWdCO1lBQWhCLHdCQUFBO2dCQUFBLFlBQWdCOztZQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUVmO1lBTEQsVUFBSSxHQUFHLGFBQWEsQ0FBQztZQUluQixLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDOztTQUN0Qzs7Ozs7UUFFRCxtQ0FBUTs7OztZQUFSLFVBQVMsS0FBSzs7b0JBQ04sR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3BDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDekIsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUNuQztxQkFBTTtvQkFDTCxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNqQjthQUNGOzs7OztRQUVELG9DQUFTOzs7O1lBQVQsVUFBVSxLQUFLOztvQkFDUCxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDekMsSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFO29CQUNwQixPQUFPLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLEtBQUssS0FBSyxHQUFBLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2lCQUNyRDtnQkFDRCxPQUFPLEtBQUssQ0FBQzthQUNkO1FBQ0gsdUJBQUM7SUFBRCxDQXhCQSxDQUF5QyxjQUFjOzs7Ozs7O1FDRHJCQSxnQ0FBUTtRQUl4QyxzQkFBWSxPQUFnQjtZQUFoQix3QkFBQTtnQkFBQSxZQUFnQjs7WUFBNUIsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FJZjtZQVJELFVBQUksR0FBRyxTQUFTLENBQUM7WUFLZixLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDeEMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDL0IsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7O1NBQ2xDO1FBQ0gsbUJBQUM7SUFBRCxDQVZBLENBQWtDLFFBQVE7Ozs7Ozs7UUNFWEEsNkJBQVc7UUFHeEMsbUJBQVksT0FBZ0I7WUFBaEIsd0JBQUE7Z0JBQUEsWUFBZ0I7O1lBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBRWY7WUFMRCxVQUFJLEdBQUcsTUFBTSxDQUFDO1lBSVosS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7O1NBQy9COzs7OztRQUVELDRCQUFROzs7O1lBQVIsVUFBUyxLQUFtQjtnQkFDMUIsaUJBQU0sUUFBUSxZQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxJQUFJLElBQUksSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2FBQ3hFO1FBQ0gsZ0JBQUM7SUFBRCxDQVhBLENBQStCLEtBQUs7Ozs7Ozs7UUNGREEsaUNBQXNCO1FBS3ZELHVCQUFZLE9BQWdCO1lBQWhCLHdCQUFBO2dCQUFBLFlBQWdCOztZQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUlmO1lBVEQsVUFBSSxHQUFHLFVBQVUsQ0FBQztZQU1oQixLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNsQyxLQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDekMsS0FBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDOztTQUMvQzs7Ozs7UUFFRCxrQ0FBVTs7OztZQUFWLFVBQVcsT0FBa0U7Z0JBQzNFLGlCQUFNLFVBQVUsWUFBQyxPQUFPLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO2FBQzlCO1FBQ0gsb0JBQUM7SUFBRCxDQWhCQSxDQUFtQyxjQUFjOzs7Ozs7O1FDQWpCQSw4QkFBUTtRQUl0QyxvQkFBWSxPQUFnQjtZQUFoQix3QkFBQTtnQkFBQSxZQUFnQjs7WUFBNUIsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FDZjtZQUxELGVBQVMsR0FBRyxJQUFJLENBQUM7WUFDakIsVUFBSSxHQUFHLE9BQU8sQ0FBQzs7U0FJZDs7OztRQUVLLGdDQUFXOzs7WUFBakI7Ozs7d0JBQ0Usc0JBQU8saUJBQU0sV0FBVyxXQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztnQ0FDckMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQ0FDM0IsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDOzZCQUNqQyxDQUFDLEVBQUM7OzthQUNKO1FBQ0gsaUJBQUM7SUFBRCxDQWRBLENBQWdDLFFBQVE7Ozs7Ozs7UUNBUkEsOEJBQVE7UUFTdEMsb0JBQVksT0FBZ0I7WUFBaEIsd0JBQUE7Z0JBQUEsWUFBZ0I7O1lBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBTWY7WUFmRCxjQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLFVBQUksR0FBRyxRQUFRLENBQUM7WUFTZCxLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUM7WUFDdEMsS0FBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDO1lBQ3BDLEtBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLFFBQVEsQ0FBQztZQUNwRCxLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUM7WUFDdEMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7O1NBQ2pDOzs7OztRQUVELGdDQUFXOzs7O1lBQVgsVUFBWSxJQUFZO2dCQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzthQUNyQjs7OztRQUVELGlDQUFZOzs7WUFBWjtnQkFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzthQUNyQjs7OztRQUVELDRCQUFPOzs7WUFBUDtnQkFDRSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxFQUFFO29CQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNsQjthQUNGO1FBQ0gsaUJBQUM7SUFBRCxDQS9CQSxDQUFnQyxRQUFROzs7Ozs7O1FDQVBBLCtCQUFRO1FBSXZDLHFCQUFZLE9BQWdCO1lBQWhCLHdCQUFBO2dCQUFBLFlBQWdCOztZQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUlmO1lBUkQsVUFBSSxHQUFHLFFBQVEsQ0FBQztZQUtkLEtBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNwQyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUMvQixLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzs7U0FDakM7UUFDSCxrQkFBQztJQUFELENBVkEsQ0FBaUMsUUFBUTs7Ozs7OztRQ0lKQSxtQ0FBVTtRQVM3Qyx5QkFBWSxPQUFnQjtZQUFoQix3QkFBQTtnQkFBQSxZQUFnQjs7WUFBNUIsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0E4QmY7WUFsQ0QsV0FBSyxHQUFHLEtBQUssQ0FBQztZQUNOLGFBQU8sR0FBRyxDQUFDLENBQUM7WUFJbEIsS0FBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxDQUFDO1lBQzFDLEtBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQztZQUN4QyxLQUFJLENBQUMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxlQUFlLENBQUMsSUFBSSxjQUFjLENBQUM7WUFDaEUsS0FBSSxDQUFDLGdCQUFnQixHQUFHLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLGFBQWEsQ0FBQztZQUNyRSxLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxLQUFLLENBQUM7WUFDdkMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNwQyxJQUFJLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDcEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxXQUFXLENBQUM7b0JBQy9CLEtBQUssRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDO2lCQUN4QixDQUFDLENBQUMsQ0FBQztnQkFDSixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLFVBQVUsQ0FBQztvQkFDOUIsSUFBSSxFQUFLLEtBQUksQ0FBQyxJQUFJLGNBQVc7b0JBQzdCLE1BQU0sRUFBRTt3QkFDTixJQUFJLFVBQVUsQ0FBQzs0QkFDYixLQUFLLEVBQUUsS0FBSSxDQUFDLGFBQWE7NEJBQ3pCLElBQUksRUFBRSxNQUFNOzRCQUNaLEtBQUssRUFBRTtnQ0FDTCxJQUFJLEtBQUksQ0FBQyxPQUFPLEtBQUssSUFBSSxJQUFJLEtBQUksQ0FBQyxNQUFNLEtBQUssSUFBSSxFQUFFOzt3Q0FDM0MsR0FBRyxHQUFRLEtBQUksQ0FBQyxNQUFNLEVBQUU7b0NBQzlCLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29DQUNwQixLQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lDQUNwQjs2QkFDRjs0QkFDRCxPQUFPLEVBQUcsQ0FBQyxTQUFTLENBQUM7eUJBQ3RCLENBQUM7cUJBQ0g7b0JBQ0QsT0FBTyxFQUFFLENBQUMsbUJBQW1CLEVBQUUsdUJBQXVCLENBQUM7aUJBQ3hELENBQUMsQ0FBQyxDQUFDO2FBQ0w7O1NBQ0Y7UUFFRCxzQkFBSSxpQ0FBSTs7O2dCQUFSO2dCQUNFLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRTtvQkFDcEMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztpQkFDL0I7Z0JBQ0QsMkJBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFnQjthQUNqRDs7O1dBQUE7Ozs7O1FBRUQsaUNBQU87Ozs7WUFBUCxVQUFRLElBQVU7Z0JBQWxCLGlCQU9DO2dCQU5DLGlCQUFNLE9BQU8sWUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLElBQUksRUFBRTtvQkFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFHO3dCQUNwQixLQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3FCQUNwQixDQUFDLENBQUM7aUJBQ0o7YUFDRjs7Ozs7UUFFRCx3Q0FBYzs7OztZQUFkLFVBQWUsTUFBd0I7Z0JBQ3JDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDdkIsS0FBSyxJQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFFO29CQUMvQixJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFOzs0QkFDL0IsS0FBSyxHQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxhQUFRLEdBQUs7OzRCQUN4QyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO3dCQUM1QyxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7NEJBQ2pCLEtBQUssQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3lCQUMxQztxQkFDRjtpQkFDRjthQUNGOzs7OztRQUVPLGtDQUFROzs7O1lBQWhCLFVBQWlCLEdBQVE7Z0JBQXpCLGlCQXFCQzs7b0JBcEJPLEtBQUssR0FBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sYUFBUSxJQUFJLENBQUMsT0FBTyxXQUFRO2dCQUM3RCxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FDeEIsSUFBSSxVQUFVLENBQUM7b0JBQ2IsSUFBSSxFQUFFLEtBQUs7b0JBQ1gsTUFBTSxFQUFFO3dCQUNOLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxhQUFRLElBQUksQ0FBQyxPQUFTLENBQUM7d0JBQzVELElBQUksVUFBVSxDQUFDOzRCQUNiLEtBQUssRUFBRSxJQUFJLENBQUMsZ0JBQWdCOzRCQUM1QixJQUFJLEVBQUUsT0FBTzs0QkFDYixLQUFLLEVBQUU7Z0NBQ0wsS0FBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7Z0NBQzlCLEtBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDOzZCQUM3Qzs0QkFDRCxPQUFPLEVBQUcsQ0FBQyxZQUFZLENBQUM7eUJBQ3pCLENBQUM7cUJBQ0g7b0JBQ0QsT0FBTyxFQUFFLENBQUMsbUJBQW1CLEVBQUUsb0JBQW9CLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLFdBQVcsR0FBRyxZQUFZLENBQUM7aUJBQ2hHLENBQUMsRUFBSyxJQUFJLENBQUMsSUFBSSxjQUFXLENBQzVCLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ2hCO1FBQ0gsc0JBQUM7SUFBRCxDQTdGQSxDQUFxQyxVQUFVOzs7Ozs7O1FDSmhCQSw2QkFBYTtRQUkxQyxtQkFBWSxPQUFnQjtZQUFoQix3QkFBQTtnQkFBQSxZQUFnQjs7WUFBNUIsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FJZjtZQVJELFVBQUksR0FBRyxNQUFNLENBQUM7WUFLWixLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDcEMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUIsS0FBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDOztTQUM3QztRQUNILGdCQUFDO0lBQUQsQ0FWQSxDQUErQixLQUFLOzs7Ozs7O1FDQ0hBLCtCQUFTO1FBR3hDLHFCQUFZLE9BQWdCO1lBQWhCLHdCQUFBO2dCQUFBLFlBQWdCOztZQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUdmO1lBRkMsS0FBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ25DLEtBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksSUFBSSxJQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7O1NBQzVFOzs7O1FBRUssaUNBQVc7OztZQUFqQjs7Ozt3QkFDRSxzQkFBTyxpQkFBTSxXQUFXLFdBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO2dDQUNyQyxJQUFJLENBQUMsT0FBTyxFQUFFO29DQUNaLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0NBQzNDLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztpQ0FDOUI7Z0NBQ0QsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDOzZCQUMvQixDQUFDLEVBQUM7OzthQUNKOzs7O1FBRUQsNEJBQU07OztZQUFOO2dCQUNFLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDNUM7Ozs7O1FBRUQsa0NBQVk7Ozs7WUFBWixVQUFhLEtBQVU7O29CQUNqQixRQUFnQjtnQkFDcEIsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7b0JBQzdCLFFBQVEsc0JBQUcsS0FBSyxFQUFVLENBQUM7aUJBQzVCO3FCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssV0FBVyxFQUFFO29CQUN2QyxRQUFRLEdBQUcsQ0FBQyxDQUFDO2lCQUNkO3FCQUFNO29CQUNMLFFBQVEsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztpQkFDakU7Z0JBQ0QsT0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUN6QztRQUNILGtCQUFDO0lBQUQsQ0FsQ0EsQ0FBaUMsU0FBUzs7Ozs7OztRQ0RQQSxpQ0FBYTtRQUc5Qyx1QkFBWSxPQUFnQjtZQUFoQix3QkFBQTtnQkFBQSxZQUFnQjs7WUFBNUIsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FHZjtZQU5ELFVBQUksR0FBRyxVQUFVLENBQUM7WUFJaEIsS0FBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3BDLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDOztTQUNuQztRQUNILG9CQUFDO0lBQUQsQ0FSQSxDQUFtQyxLQUFLOzs7Ozs7QUNGeEM7UUFVRTtZQUNFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSUMsZUFBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ3BDO1FBRUQsc0JBQUksOEJBQUs7OztnQkFBVDtnQkFDRSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7YUFDdkI7OztXQUFBOzs7OztRQUVELG1DQUFhOzs7O1lBQWIsVUFBYyxLQUFpQjtnQkFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsRUFBRTtvQkFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztpQkFDL0Q7YUFDRjs7Ozs7UUFFRCxvQ0FBYzs7OztZQUFkLFVBQWUsTUFBb0I7Z0JBQW5DLGlCQUlDO2dCQUhDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO29CQUNsQixLQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUMzQixDQUFDLENBQUM7YUFDSjs7Ozs7UUFFRCxxQ0FBZTs7OztZQUFmLFVBQWdCLEtBQWlCO2dCQUMvQixJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDcEM7Ozs7O1FBRUQsc0NBQWdCOzs7O1lBQWhCLFVBQWlCLE1BQW9CO2dCQUFyQyxpQkFJQztnQkFIQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztvQkFDbEIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDN0IsQ0FBQyxDQUFDO2FBQ0o7Ozs7O1FBRUQsc0NBQWdCOzs7O1lBQWhCLFVBQWlCLEtBQWlCO2dCQUNoQyxPQUFPLElBQUlDLGlCQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQzthQUMzQzs7b0JBcENGQyxlQUFVOzs7O1FBcUNYLGtCQUFDO0tBckNEOzs7Ozs7O1FDSDBCSCx3QkFBUTtRQU1oQyxjQUFZLE9BTU47WUFOTSx3QkFBQTtnQkFBQSxZQU1OOztZQU5OLFlBT0Usa0JBQU0sT0FBTyxDQUFDLFNBSWY7WUFkRCxlQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ2pCLFlBQU0sR0FBRyxJQUFJLENBQUM7WUFVWixLQUFJLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDaEMsS0FBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDO1lBQ3hDLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyxDQUFDOztTQUMzQjtRQUVELHNCQUFJLHdCQUFNOzs7Z0JBQVY7Z0JBQ0UsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO29CQUNiLE9BQU8sS0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxNQUFHLENBQUM7aUJBQzNDO3FCQUFNLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDcEIsT0FBVSxJQUFJLENBQUMsSUFBSSxNQUFHLENBQUM7aUJBQ3hCO3FCQUFNO29CQUNMLE9BQU8sRUFBRSxDQUFDO2lCQUNYO2FBQ0Y7OztXQUFBOzs7OztRQUVELHFCQUFNOzs7O1lBQU4sVUFBTyxHQUFRO2dCQUNiLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO2dCQUNmLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7YUFDNUI7Ozs7OztRQUVELDBCQUFXOzs7OztZQUFYLFVBQVksR0FBVyxFQUFFLEtBQVU7Z0JBQ2pDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxDQUFDO2FBQ3ZCOzs7O1FBRUQsdUJBQVE7OztZQUFSO2dCQUNFLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxJQUFJLEVBQUU7b0JBQ3hCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ25CO2FBQ0Y7UUFDSCxXQUFDO0lBQUQsQ0EzQ0EsQ0FBMEIsUUFBUTs7Ozs7O0FDSGxDO1FBZUUsdUJBQW9CLFdBQXdCO1lBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1NBQUk7Ozs7UUFFaEQsZ0NBQVE7OztZQUFSO2FBRUM7UUFFRCxzQkFBSSxnQ0FBSzs7O2dCQUFUO2dCQUNFLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7YUFDL0I7OztXQUFBOzs7O1FBRUQsZ0NBQVE7OztZQUFSO2dCQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdEI7O29CQXBCRkksY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxVQUFVO3dCQUNwQixpUkFBb0M7d0JBQ3BDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtxQkFDM0I7Ozs7O3dCQVBRLFdBQVc7Ozs7MkJBU2pCQyxVQUFLOztRQWVSLG9CQUFDO0tBckJEOzs7Ozs7QUNQQTtRQWVFLDJCQUFvQixXQUF3QjtZQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtTQUFJOzs7O1FBRWhELG9DQUFROzs7WUFBUjtnQkFDRSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUNyQixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsb0JBQUMsSUFBSSxDQUFDLElBQUksR0FBZSxDQUFDO2lCQUN6RDthQUNGOzs7O1FBRUQsdUNBQVc7OztZQUFYO2dCQUNFLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxvQkFBQyxJQUFJLENBQUMsSUFBSSxHQUFlLENBQUM7aUJBQzNEO2FBQ0Y7UUFFRCxzQkFBSSxvQ0FBSzs7O2dCQUFUO2dCQUNFLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7YUFDL0I7OztXQUFBOzs7OztRQUVELG9DQUFROzs7O1lBQVIsVUFBUyxLQUFLO2dCQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzNCOzs7OztRQUVELGtDQUFNOzs7O1lBQU4sVUFBTyxLQUFLO2dCQUNWLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3pCOzs7O1FBRUQsbUNBQU87OztZQUFQO2dCQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDckI7O29CQXBDRkQsY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxlQUFlO3dCQUN6Qiw4bENBQXlDO3dCQUN6QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7cUJBQzNCOzs7Ozt3QkFOUSxXQUFXOzs7OzJCQVFqQkMsVUFBSzs7UUErQlIsd0JBQUM7S0FyQ0Q7Ozs7Ozs7UUNJNENMLDBDQUFpQjtRQUw3RDs7U0FPQzs7b0JBUEFJLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsb0JBQW9CO3dCQUM5QixvYUFBOEM7d0JBQzlDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtxQkFDM0I7OzsyQkFFRUMsVUFBSzs7UUFDUiw2QkFBQztLQUFBLENBRjJDLGlCQUFpQjs7Ozs7OztRQ0FkTCw2Q0FBaUI7UUFMaEU7O1NBT0M7O29CQVBBSSxjQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLHdCQUF3Qjt3QkFDbEMsb3VCQUFrRDt3QkFDbEQsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO3FCQUMzQjs7OzJCQUVFQyxVQUFLOztRQUNSLGdDQUFDO0tBQUEsQ0FGOEMsaUJBQWlCOzs7Ozs7O1FDQXJCTCx5Q0FBaUI7UUFMNUQ7O1NBT0M7O29CQVBBSSxjQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjt3QkFDN0IscUxBQTZDO3dCQUM3QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7cUJBQzNCOzs7MkJBRUVDLFVBQUs7O1FBQ1IsNEJBQUM7S0FBQSxDQUYwQyxpQkFBaUI7Ozs7Ozs7UUNBcEJMLHNDQUFpQjtRQUx6RDs7U0FPQzs7b0JBUEFJLGNBQVMsU0FBQzt3QkFDVCxRQUFRLEVBQUUsZ0JBQWdCO3dCQUMxQixtNEJBQTBDO3dCQUMxQyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7cUJBQzNCOzs7MkJBRUVDLFVBQUs7O1FBQ1IseUJBQUM7S0FBQSxDQUZ1QyxpQkFBaUI7Ozs7Ozs7UUNBYkwsMENBQWlCO1FBTDdEOztTQU9DOztvQkFQQUksY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxvQkFBb0I7d0JBQzlCLDJ2QkFBOEM7d0JBQzlDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtxQkFDM0I7OzsyQkFFRUMsVUFBSzs7UUFDUiw2QkFBQztLQUFBLENBRjJDLGlCQUFpQjs7Ozs7OztRQ0FwQkwsdUNBQWlCO1FBTDFEOztTQU9DOztvQkFQQUksY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxpQkFBaUI7d0JBQzNCLHVQQUEyQzt3QkFDM0MsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO3FCQUMzQjs7OzJCQUVFQyxVQUFLOztRQUNSLDBCQUFDO0tBQUEsQ0FGd0MsaUJBQWlCOzs7Ozs7O1FDQWpCTCx1Q0FBaUI7UUFMMUQ7O1NBV0M7UUFIQyxzQkFBSSxzQ0FBSzs7O2dCQUFUO2dCQUNFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7YUFDaEU7OztXQUFBOztvQkFWRkksY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxpQkFBaUI7d0JBQzNCLDBXQUEyQzt3QkFDM0MsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO3FCQUMzQjs7OzJCQUVFQyxVQUFLOztRQUtSLDBCQUFDO0tBQUEsQ0FOd0MsaUJBQWlCOzs7Ozs7O1FDQWhCTCx3Q0FBaUI7UUFMM0Q7O1NBT0M7O29CQVBBSSxjQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjt3QkFDNUIscUxBQTRDO3dCQUM1QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7cUJBQzNCOzs7MkJBRUVDLFVBQUs7O1FBQ1IsMkJBQUM7S0FBQSxDQUZ5QyxpQkFBaUI7Ozs7Ozs7UUNBZEwsMkNBQWlCO1FBTDlEOztTQU9DOztvQkFQQUksY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxzQkFBc0I7d0JBQ2hDLGdzQkFBZ0Q7d0JBQ2hELFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtxQkFDM0I7OzsyQkFFRUMsVUFBSzs7UUFDUiw4QkFBQztLQUFBLENBRjRDLGlCQUFpQjs7Ozs7OztRQ0F0Qkwsc0NBQWlCO1FBTHpEOztTQU9DOztvQkFQQUksY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7d0JBQzFCLHExQkFBMEM7d0JBQzFDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtxQkFDM0I7OzsyQkFFRUMsVUFBSzs7UUFDUix5QkFBQztLQUFBLENBRnVDLGlCQUFpQjs7Ozs7OztRQ0FiTCwwQ0FBaUI7UUFMN0Q7O1NBT0M7O29CQVBBSSxjQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjt3QkFDOUIscWJBQThDO3dCQUM5QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7cUJBQzNCOzs7MkJBRUVDLFVBQUs7O1FBQ1IsNkJBQUM7S0FBQSxDQUYyQyxpQkFBaUI7Ozs7OztBQ1g3RDtRQXdCQTtTQWdEQzs7OztRQU5RLDRCQUFPOzs7WUFBZDtnQkFDRSxPQUFPO29CQUNMLFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtpQkFDM0IsQ0FBQzthQUNIOztvQkEvQ0ZDLGFBQVEsU0FBQzt3QkFDUixPQUFPLEVBQUU7NEJBQ1BDLG1CQUFZOzRCQUNaQyxpQkFBVzs0QkFDWEMseUJBQW1COzRCQUNuQkMsMkNBQXdCOzRCQUN4QkMscUNBQXFCLENBQUMsT0FBTyxFQUFFO3lCQUNoQzt3QkFDRCxZQUFZLEVBQUU7NEJBQ1osYUFBYTs0QkFDYixpQkFBaUI7NEJBRWpCLHNCQUFzQjs0QkFDdEIseUJBQXlCOzRCQUN6QixxQkFBcUI7NEJBQ3JCLGtCQUFrQjs0QkFDbEIsc0JBQXNCOzRCQUN0QixtQkFBbUI7NEJBQ25CLG1CQUFtQjs0QkFDbkIsb0JBQW9COzRCQUNwQix1QkFBdUI7NEJBQ3ZCLGtCQUFrQjs0QkFDbEIsc0JBQXNCO3lCQUN2Qjt3QkFDRCxPQUFPLEVBQUU7NEJBQ1AsYUFBYTs0QkFDYixpQkFBaUI7NEJBRWpCLHNCQUFzQjs0QkFDdEIseUJBQXlCOzRCQUN6QixxQkFBcUI7NEJBQ3JCLGtCQUFrQjs0QkFDbEIsc0JBQXNCOzRCQUN0QixtQkFBbUI7NEJBQ25CLG1CQUFtQjs0QkFDbkIsb0JBQW9COzRCQUNwQix1QkFBdUI7NEJBQ3ZCLGtCQUFrQjs0QkFDbEIsc0JBQXNCO3lCQUN2QjtxQkFDRjs7UUFRRCwyQkFBQztLQWhERDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=