import { __awaiter } from 'tslib';
import { Injectable, Component, Input, NgModule } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class FieldSet {
    /**
     * @param {?} set
     */
    constructor(set) {
        this.set = set;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    setForm(form) {
        this.set.forEach((field) => {
            field.setForm(form);
        });
    }
    /**
     * @return {?}
     */
    updateValues() {
        this.set.forEach((field) => {
            field.deriveValue();
        });
    }
    /**
     * @return {?}
     */
    get fields() {
        return this.set;
    }
    /**
     * @param {?} item
     * @return {?}
     */
    push(item) {
        this.set.push(item);
    }
    /**
     * @param {?} item
     * @param {?} before
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    insertBefore(item, before, attr, field = null) {
        /** @type {?} */
        let parent = null;
        if (field === null) {
            /** @type {?} */
            const search = this.fieldWithParent(before, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field), 0, item);
        }
        else {
            parent.fields.insertBefore(item, before, attr, field);
        }
    }
    /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    insertBeforeName(item, before, field = null) {
        return this.insertBefore(item, before, 'name', field);
    }
    /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    insertBeforeId(item, before, field = null) {
        return this.insertBefore(item, before, 'id', field);
    }
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    insertAfter(item, after, attr, field = null) {
        /** @type {?} */
        let parent = null;
        if (field === null) {
            /** @type {?} */
            const search = this.fieldWithParent(after, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field) + 1, 0, item);
        }
        else {
            parent.fields.insertAfter(item, after, attr, field);
        }
    }
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    insertAfterName(item, after, _attr, field = null) {
        this.insertAfter(item, after, 'name', field);
    }
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    insertAfterId(item, after, _attr, field = null) {
        this.insertAfter(item, after, 'id', field);
    }
    /**
     * @param {?} key
     * @param {?=} field
     * @param {?=} attr
     * @return {?}
     */
    remove(key, field = null, attr) {
        /** @type {?} */
        let parent = null;
        if (field === null) {
            /** @type {?} */
            const search = this.fieldWithParent(key, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field), 1);
        }
        else {
            parent.fields.remove(key, field, attr);
        }
    }
    /**
     * @param {?} name
     * @param {?=} field
     * @return {?}
     */
    removeByName(name, field = null) {
        this.remove(name, field, 'name');
    }
    /**
     * @param {?} id
     * @param {?=} field
     * @return {?}
     */
    removeById(id, field = null) {
        this.remove(id, field, 'id');
    }
    /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    field(key, attr) {
        return this.fieldWithParent(key, attr).field;
    }
    /**
     * @param {?} name
     * @return {?}
     */
    fieldByName(name) {
        return this.field(name, 'name');
    }
    /**
     * @param {?} id
     * @return {?}
     */
    fieldById(id) {
        return this.field(id, 'id');
    }
    /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    fieldWithParent(key, attr) {
        /** @type {?} */
        let field = null;
        /** @type {?} */
        let parent = null;
        /** @type {?} */
        const search = this.set.filter(child => child[attr] === key);
        if (search.length > 0) {
            field = search[0];
        }
        else {
            /** @type {?} */
            const parents = this.set.filter(child => child.hasFields === true);
            for (let i = 0; i < parents.length; i++) {
                /** @type {?} */
                const check = parents[i].fields.fieldWithParent(key, attr);
                if (check.field != null) {
                    field = check.field;
                    parent = parents[i];
                    break;
                }
            }
        }
        return { field: field, parent: parent };
    }
    /**
     * @param {?} name
     * @return {?}
     */
    fieldByNameWithParent(name) {
        return this.fieldWithParent(name, 'name');
    }
    /**
     * @param {?} id
     * @return {?}
     */
    fieldByIdWithParent(id) {
        return this.fieldWithParent(id, 'id');
    }
    /**
     * @return {?}
     */
    resetValidation() {
        this.fields.forEach(field => {
            field.resetValidation();
        });
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        this.isForm = false;
        this.hasData = false;
        this.hasFields = false;
        this.hasLabel = true;
        this.isAction = false;
        this.name = options.name || '';
        this.id = this.name;
        this.classes = options.classes || [];
        this.fields = new FieldSet(options.fields || []);
        this.readOnly = options.readOnly || false;
        this.change = options.change || null;
        this.blur = options.blur || null;
    }
    /**
     * @return {?}
     */
    getForm() {
        return this.form;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    setForm(form) {
        this.form = form;
        if (form.prefix) {
            this.id = `${form.prefix}${this.name}`;
        }
        if (!this.isForm) {
            this.fields.setForm(form);
        }
    }
    /**
     * @return {?}
     */
    deriveValue() {
        return __awaiter(this, void 0, void 0, function* () {
            return Promise.resolve(false);
        });
    }
    /**
     * @return {?}
     */
    get control() {
        return null;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onChange(value) {
        /** @type {?} */
        let propagate = true;
        if (this.change != null) {
            propagate = this.change(this, value);
        }
        if (propagate) {
            this.form.fieldChange(this.name, value);
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onBlur(value) {
        if (this.blur != null) {
            this.blur(this, value);
        }
    }
    /**
     * @return {?}
     */
    onClick() {
    }
    /**
     * @param {?} message
     * @return {?}
     */
    setMessage(message) {
        this.message = message;
    }
    /**
     * @param {?} result
     * @return {?}
     */
    loadValidation(result) {
        this.resetValidation();
        if (result.messages != null && result.messages.length > 0) {
            this.setMessage(result.messages[0]);
        }
        for (const key in result.fields) {
            if (result.fields.hasOwnProperty(key)) {
                /** @type {?} */
                const field = this.fields.fieldByName(key);
                if (field != null) {
                    field.loadValidation(result.fields[key]);
                }
            }
        }
    }
    /**
     * @return {?}
     */
    resetValidation() {
        this.setMessage(null);
        if (this.fields) {
            this.fields.resetValidation();
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @template T
 */
class Field extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.hasData = true;
        this.value = options.value;
        this.label = options.label || '';
        this.classes.push('ndf-field');
        this.placeholder = options.placeholder || '';
        if (this.placeholder.length > 0) {
            this.classes.push('ndf-placeholder');
        }
        this.derive = options.derive || null;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    setForm(form) {
        super.setForm(form);
        this.deriveValue();
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onChange(value) {
        this.value = value;
        super.onChange(this.prepareValue(value));
    }
    /**
     * @return {?}
     */
    deriveValue() {
        const _super = name => super[name];
        return __awaiter(this, void 0, void 0, function* () {
            return _super("deriveValue").call(this).then(derived => {
                if (!derived) {
                    if (this.derive != null) {
                        return this.derive(this);
                    }
                    else if (this.form.obj.hasOwnProperty(this.name)) {
                        this.value = this.form.obj[this.name];
                        return Promise.resolve(true);
                    }
                }
                return Promise.resolve(false);
            });
        });
    }
    /**
     * @param {?} value
     * @return {?}
     */
    prepareValue(value) {
        if (value == null) {
            return '';
        }
        else {
            return value;
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class CheckboxField extends Field {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'checkbox';
        this.hasLabel = false;
        this.classes.push('ndf-checkbox');
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @template T
 */
class OptionSetField extends Field {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'optionset';
        this.optionSet = [];
        this.options = [];
        this.classes.push('ndf-optionset');
        this.optionSet = options['options'] || [];
        Promise.resolve(this.optionSet).then(optionList => {
            this.setOptions(optionList);
        });
    }
    /**
     * @param {?} options
     * @return {?}
     */
    setOptions(options) {
        this.options = options;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @template T
 */
class CheckboxSetField extends OptionSetField {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'checkboxset';
        this.classes.push('ndf-checkboxset');
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onChange(value) {
        /** @type {?} */
        const obj = this.form.obj[this.name];
        if (this.isChecked(value)) {
            obj.splice(obj.indexOf(value), 1);
        }
        else {
            obj.push(value);
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    isChecked(value) {
        /** @type {?} */
        const selected = this.form.obj[this.name];
        if (selected != null) {
            return selected.filter(o => o === value).length > 0;
        }
        return false;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class ContentField extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'content';
        this.content = options['content'] || '';
        this.classes.push('ndf-field');
        this.classes.push('ndf-content');
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class DateField extends Field {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'date';
        this.classes.push('ndf-date');
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onChange(value) {
        super.onChange((value.jsdate == null) ? null : new Date(value.jsdate));
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class DropdownField extends OptionSetField {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'dropdown';
        this.classes.push('ndf-dropdown');
        this.loaded = options['emptyText'] || '';
        this.emptyText = options['loadingText'] || '';
    }
    /**
     * @param {?} options
     * @return {?}
     */
    setOptions(options) {
        super.setOptions(options);
        this.emptyText = this.loaded;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class FieldGroup extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.hasFields = true;
        this.type = 'group';
    }
    /**
     * @return {?}
     */
    deriveValue() {
        const _super = name => super[name];
        return __awaiter(this, void 0, void 0, function* () {
            return _super("deriveValue").call(this).then(derived => {
                this.fields.updateValues();
                return Promise.resolve(derived);
            });
        });
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class FormAction extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.isAction = true;
        this.type = 'action';
        this.title = options['title'] || null;
        this.icon = options['icon'] || null;
        this.actionType = options['actionType'] || 'submit';
        this.click = options['click'] || null;
        this.classes.push('ndf-action');
    }
    /**
     * @param {?} text
     * @return {?}
     */
    showLoading(text) {
        this.loading = text;
    }
    /**
     * @return {?}
     */
    clearLoading() {
        this.loading = null;
    }
    /**
     * @return {?}
     */
    onClick() {
        if (this.click != null) {
            this.click(this);
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class HeaderField extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'header';
        this.title = options['title'] || '';
        this.classes.push('ndf-field');
        this.classes.push('ndf-header');
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class MultiChildField extends FieldGroup {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.child = false;
        this.counter = 0;
        this.subForm = options['subForm'] || null;
        this.newObj = options['newObj'] || null;
        this.addButtonText = options['addButtonText'] || 'Add New Line';
        this.removeButtonText = options['removeButtonText'] || 'Remove Line';
        this.child = options['child'] || false;
        this.classes.push('ndf-multichild');
        if (options['title']) {
            this.fields.push(new HeaderField({
                title: options['title']
            }));
            this.fields.push(new FieldGroup({
                name: `${this.name}_controls`,
                fields: [
                    new FormAction({
                        title: this.addButtonText,
                        icon: 'plus',
                        click: () => {
                            if (this.subForm !== null && this.newObj !== null) {
                                /** @type {?} */
                                const obj = this.newObj();
                                this.list.push(obj);
                                this.addChild(obj);
                            }
                        },
                        classes: ['ndf-add']
                    })
                ],
                classes: ['ndf-full-bordered', 'ndf-multichild-footer']
            }));
        }
    }
    /**
     * @return {?}
     */
    get list() {
        if (this.form.obj[this.name] == null) {
            this.form.obj[this.name] = [];
        }
        return ((/** @type {?} */ (this.form.obj[this.name])));
    }
    /**
     * @param {?} form
     * @return {?}
     */
    setForm(form) {
        super.setForm(form);
        if (this.subForm !== null) {
            this.list.forEach((obj) => {
                this.addChild(obj);
            });
        }
    }
    /**
     * @param {?} result
     * @return {?}
     */
    loadValidation(result) {
        this.resetValidation();
        for (const key in result.fields) {
            if (result.fields.hasOwnProperty(key)) {
                /** @type {?} */
                const child = `${this.form.prefix}line_${key}`;
                /** @type {?} */
                const field = this.fields.fieldByName(child);
                if (field != null) {
                    field.loadValidation(result.fields[key]);
                }
            }
        }
    }
    /**
     * @param {?} obj
     * @return {?}
     */
    addChild(obj) {
        /** @type {?} */
        const group = `${this.form.prefix}line_${this.counter}_group`;
        this.fields.insertBeforeId(new FieldGroup({
            name: group,
            fields: [
                this.subForm(obj, `${this.form.prefix}line_${this.counter}`),
                new FormAction({
                    title: this.removeButtonText,
                    icon: 'trash',
                    click: () => {
                        this.fields.removeById(group);
                        this.list.splice(this.list.indexOf(obj), 1);
                    },
                    classes: ['ndf-remove']
                })
            ],
            classes: ['ndf-full-bordered', 'ndf-multichild-row', (this.child) ? 'ndf-child' : 'ndf-parent']
        }), `${this.name}_controls`);
        this.counter++;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class TextField extends Field {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'text';
        this.value = options['value'] || '';
        this.classes.push('ndf-text');
        this.inputType = options['inputType'] || '';
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class NumberField extends TextField {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.value = options['value'] || 0;
        this.precision = (options['precision'] != null) ? options['precision'] : 2;
    }
    /**
     * @return {?}
     */
    deriveValue() {
        const _super = name => super[name];
        return __awaiter(this, void 0, void 0, function* () {
            return _super("deriveValue").call(this).then(derived => {
                if (!derived) {
                    this.value = this.prepareValue(this.value);
                    return Promise.resolve(true);
                }
                return Promise.resolve(false);
            });
        });
    }
    /**
     * @return {?}
     */
    onBlur() {
        this.value = this.prepareValue(this.value);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    prepareValue(value) {
        /** @type {?} */
        let prepared;
        if (typeof value === 'number') {
            prepared = (/** @type {?} */ (value));
        }
        else if (typeof value === 'undefined') {
            prepared = 0;
        }
        else {
            prepared = parseFloat(value.toString().replace(/[^0-9.]/g, ''));
        }
        return prepared.toFixed(this.precision);
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class TextareaField extends Field {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'textarea';
        this.value = options['value'] || '';
        this.classes.push('ndf-textarea');
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class FormService {
    constructor() {
        this.formGroup = new FormGroup({});
    }
    /**
     * @return {?}
     */
    get group() {
        return this.formGroup;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    registerField(field) {
        if (!this.group.contains(field.id)) {
            this.group.addControl(field.id, this.controlFromField(field));
        }
    }
    /**
     * @param {?} fields
     * @return {?}
     */
    registerFields(fields) {
        fields.forEach(field => {
            this.registerField(field);
        });
    }
    /**
     * @param {?} field
     * @return {?}
     */
    deregisterField(field) {
        this.group.removeControl(field.id);
    }
    /**
     * @param {?} fields
     * @return {?}
     */
    deregisterFields(fields) {
        fields.forEach(field => {
            this.deregisterField(field);
        });
    }
    /**
     * @param {?} field
     * @return {?}
     */
    controlFromField(field) {
        return new FormControl(field.value || '');
    }
}
FormService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
FormService.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class Form extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.hasFields = true;
        this.isForm = true;
        this.obj = options['obj'] || {};
        this.submit = options['submit'] || null;
        this.fields.setForm(this);
    }
    /**
     * @return {?}
     */
    get prefix() {
        if (this.form) {
            return `${this.form.prefix}${this.name}_`;
        }
        else if (this.name) {
            return `${this.name}_`;
        }
        else {
            return '';
        }
    }
    /**
     * @param {?} obj
     * @return {?}
     */
    update(obj) {
        this.obj = obj;
        this.fields.updateValues();
    }
    /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    fieldChange(key, value) {
        this.obj[key] = value;
    }
    /**
     * @return {?}
     */
    onSubmit() {
        if (this.submit !== null) {
            this.submit(this);
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class FormComponent {
    /**
     * @param {?} formService
     */
    constructor(formService) {
        this.formService = formService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    get group() {
        return this.formService.group;
    }
    /**
     * @return {?}
     */
    onSubmit() {
        this.form.onSubmit();
    }
}
FormComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-form',
                template: "<form (ngSubmit)=\"onSubmit()\" [ngClass]=\"form.classes\" [formGroup]=\"group\">\n    <p *ngIf=\"form.message\" class=\"ndf-error\">{{ form.message }}</p>\n    <ndf-form-item *ngFor=\"let item of form.fields.fields\" [item]=\"item\"></ndf-form-item>\n</form>\n",
                providers: [FormService]
            }] }
];
/** @nocollapse */
FormComponent.ctorParameters = () => [
    { type: FormService }
];
FormComponent.propDecorators = {
    form: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class FormItemComponent {
    /**
     * @param {?} formService
     */
    constructor(formService) {
        this.formService = formService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.item.hasData) {
            this.formService.registerField((/** @type {?} */ (this.item)));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.item.hasData) {
            this.formService.deregisterField((/** @type {?} */ (this.item)));
        }
    }
    /**
     * @return {?}
     */
    get group() {
        return this.formService.group;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onChange(value) {
        this.item.onChange(value);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onBlur(value) {
        this.item.onBlur(value);
    }
    /**
     * @return {?}
     */
    onClick() {
        this.item.onClick();
    }
}
FormItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-form-item',
                template: "<div [ngSwitch]=\"item.type\">\n    <ndf-checkbox-field *ngSwitchCase=\"'checkbox'\" [item]=\"item\"></ndf-checkbox-field>\n    <ndf-checkbox-set-field *ngSwitchCase=\"'checkboxset'\" [item]=\"item\"></ndf-checkbox-set-field>\n    <ndf-content-field *ngSwitchCase=\"'content'\" [item]=\"item\"></ndf-content-field>\n    <ndf-date-field *ngSwitchCase=\"'date'\" [item]=\"item\"></ndf-date-field>\n    <ndf-dropdown-field *ngSwitchCase=\"'dropdown'\" [item]=\"item\"></ndf-dropdown-field>\n    <ndf-field-group *ngSwitchCase=\"'group'\" [item]=\"item\"></ndf-field-group>\n    <ndf-field-group *ngSwitchCase=\"'multichild'\" [item]=\"item\"></ndf-field-group>\n    <ndf-form-action *ngSwitchCase=\"'action'\" [item]=\"item\"></ndf-form-action>\n    <ndf-header-field *ngSwitchCase=\"'header'\" [item]=\"item\"></ndf-header-field>\n    <ndf-option-set-field *ngSwitchCase=\"'optionset'\" [item]=\"item\"></ndf-option-set-field>\n    <ndf-text-field *ngSwitchCase=\"'text'\" [item]=\"item\"></ndf-text-field>\n    <ndf-textarea-field *ngSwitchCase=\"'textarea'\" [item]=\"item\"></ndf-textarea-field>\n</div>\n",
                providers: [FormService]
            }] }
];
/** @nocollapse */
FormItemComponent.ctorParameters = () => [
    { type: FormService }
];
FormItemComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class CheckboxFieldComponent extends FormItemComponent {
}
CheckboxFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-checkbox-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label>\n        <div class=\"box\"><fa *ngIf=\"item.value\" name=\"check\" size=\"lt\"></fa></div>\n        <input type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.value\" (ngModelChange)=\"onChange($event)\" />\n        {{item.label}}\n    </label>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{item.message}}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
CheckboxFieldComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class CheckboxSetFieldComponent extends FormItemComponent {
}
CheckboxSetFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-checkbox-set-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <ul>\n        <li *ngFor=\"let option of item.options\">\n            <label>\n                <div class=\"box\"><fa *ngIf=\"item.isChecked(option.key)\" name=\"check\" size=\"lt\"></fa></div>\n                <input type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.isChecked(option.key)\" [value]=\"option.key\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange(option.key)\" />\n                {{ option.value }}\n            </label>\n        </li>\n    </ul>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
CheckboxSetFieldComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class ContentFieldComponent extends FormItemComponent {
}
ContentFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-content-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <p>{{ item.content }}</p>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
ContentFieldComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class DateFieldComponent extends FormItemComponent {
}
DateFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-date-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <div class=\"ndf-date-picker\">\n        <input ngx-mydatepicker [name]=\"item.id\" [value]=\"item.value | date:'dd/MM/yyyy'\" [readonly]=\"item.readOnly\" (dateChanged)=\"onChange($event)\" [options]=\"{ dateFormat: 'dd/mm/yyyy', selectorHeight: 'auto' }\" #dp=\"ngx-mydatepicker\" />\n        <button *ngIf=\"item.value != null\" type=\"button\" class=\"ndf-date-clear\" (click)=\"dp.clearDate()\">\n            <fa name=\"times\" size=\"lt\"></fa>\n        </button>\n        <button type=\"button\" class=\"ndf-date-open\" (click)=\"dp.toggleCalendar()\">\n            <fa name=\"calendar-o\" size=\"lt\"></fa>\n        </button>\n    </div>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
DateFieldComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class DropdownFieldComponent extends FormItemComponent {
}
DropdownFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-dropdown-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <select [id]=\"item.id\" [formControlName]=\"item.id\" [value]=\"item.value ? item.value : ''\" (ngModelChange)=\"onChange($event)\">\n        <option *ngIf=\"item.emptyText && item.options.length < 1\" value=\"\">{{ item.emptyText }}</option>\n        <option *ngIf=\"item.placeholder && item.options.length > 0\" value=\"\">{{ item.placeholder }}</option>\n        <option *ngFor=\"let option of item.options\" [value]=\"option.key\" [disabled]=\"option.disabled\">{{ option.value }}</option>\n    </select>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
DropdownFieldComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class FieldGroupComponent extends FormItemComponent {
}
FieldGroupComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-field-group',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <ndf-form-item *ngFor=\"let child of item.fields.fields\" [item]=\"child\"></ndf-form-item>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
FieldGroupComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class FormActionComponent extends FormItemComponent {
    /**
     * @return {?}
     */
    get label() {
        return this.item.loading ? this.item.loading : this.item.title;
    }
}
FormActionComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-form-action',
                template: "<button [attr.disabled]=\"item.loading != null ? '' : null\" [ngClass]=\"item.classes\" [type]=\"item.actionType\" (click)=\"onClick()\" [title]=\"item.title\">\n    <fa *ngIf=\"item.loading\" name=\"spinner\" size=\"lt\" animation=\"spin\"></fa><fa *ngIf=\"!item.loading && item.icon\" [name]=\"item.icon\" size=\"lt\"></fa> {{ label }}\n</button>\n",
                providers: [FormService]
            }] }
];
FormActionComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class HeaderFieldComponent extends FormItemComponent {
}
HeaderFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-header-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <h3>{{ item.title }}</h3>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
HeaderFieldComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class OptionSetFieldComponent extends FormItemComponent {
}
OptionSetFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-option-set-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <ul>\n        <li *ngFor=\"let option of item.options\">\n            <label>\n                <div class=\"box\"><div *ngIf=\"item.value == option.key\"></div></div>\n                <input type=\"radio\" [formControlName]=\"item.id\" [checked]=\"item.value == option.key\" [value]=\"option.key\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" />\n                {{ option.value }}\n            </label>\n        </li>\n    </ul>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
OptionSetFieldComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class TextFieldComponent extends FormItemComponent {
}
TextFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-text-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <label *ngIf=\"!item.hasLabel\">\n        <div *ngIf=\"item.type == 'checkbox'\" class=\"box\"><fa *ngIf=\"item.value\" name=\"check\" size=\"lt\"></fa></div>\n        <input *ngIf=\"item.type == 'checkbox'\" type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.value\" (ngModelChange)=\"onChange($event)\" />\n        {{ item.label }}\n    </label>\n    <input [formControlName]=\"item.id\" [id]=\"item.id\" [type]=\"item.inputType\" [placeholder]=\"item.placeholder\" [value]=\"item.value\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" (blur)=\"onBlur($event)\" />\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
TextFieldComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class TextareaFieldComponent extends FormItemComponent {
}
TextareaFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-textarea-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <textarea [formControlName]=\"item.id\" [id]=\"item.id\" [value]=\"item.value\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" (blur)=\"onBlur($event)\"></textarea>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
TextareaFieldComponent.propDecorators = {
    item: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class NgDynamicFormsModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return {
            ngModule: NgDynamicFormsModule,
            providers: [FormService]
        };
    }
}
NgDynamicFormsModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    AngularFontAwesomeModule,
                    NgxMyDatePickerModule.forRoot()
                ],
                declarations: [
                    FormComponent,
                    FormItemComponent,
                    CheckboxFieldComponent,
                    CheckboxSetFieldComponent,
                    ContentFieldComponent,
                    DateFieldComponent,
                    DropdownFieldComponent,
                    FieldGroupComponent,
                    FormActionComponent,
                    HeaderFieldComponent,
                    OptionSetFieldComponent,
                    TextFieldComponent,
                    TextareaFieldComponent
                ],
                exports: [
                    FormComponent,
                    FormItemComponent,
                    CheckboxFieldComponent,
                    CheckboxSetFieldComponent,
                    ContentFieldComponent,
                    DateFieldComponent,
                    DropdownFieldComponent,
                    FieldGroupComponent,
                    FormActionComponent,
                    HeaderFieldComponent,
                    OptionSetFieldComponent,
                    TextFieldComponent,
                    TextareaFieldComponent
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { CheckboxField, CheckboxSetField, ContentField, DateField, DropdownField, FieldGroup, Field, FormAction, HeaderField, MultiChildField, NumberField, OptionSetField, TextField, TextareaField, FormService, FieldSet, FormBase, Form, NgDynamicFormsModule, CheckboxFieldComponent as ɵc, CheckboxSetFieldComponent as ɵd, ContentFieldComponent as ɵe, DateFieldComponent as ɵf, DropdownFieldComponent as ɵg, FieldGroupComponent as ɵh, FormActionComponent as ɵi, FormItemComponent as ɵb, FormComponent as ɵa, HeaderFieldComponent as ɵj, OptionSetFieldComponent as ɵk, TextFieldComponent as ɵl, TextareaFieldComponent as ɵm };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFpc2VkaWdpdGFsLW5nLWR5bmFtaWMtZm9ybXMuanMubWFwIiwic291cmNlcyI6WyJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGQtc2V0LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2Zvcm0tYmFzZS50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2NoZWNrYm94LWZpZWxkLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2ZpZWxkcy9vcHRpb24tc2V0LWZpZWxkLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2ZpZWxkcy9jaGVja2JveC1zZXQtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2NvbnRlbnQtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2RhdGUtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2Ryb3Bkb3duLWZpZWxkLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2ZpZWxkcy9maWVsZC1ncm91cC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvZm9ybS1hY3Rpb24udHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2hlYWRlci1maWVsZC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvbXVsdGktY2hpbGQtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL3RleHQtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL251bWJlci1maWVsZC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvdGV4dGFyZWEtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvc2VydmljZXMvZm9ybS5zZXJ2aWNlLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2Zvcm0udHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy9mb3JtLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2Zvcm0taXRlbS5jb21wb25lbnQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy9jaGVja2JveC1maWVsZC5jb21wb25lbnQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy9jaGVja2JveC1zZXQtZmllbGQuY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2NvbXBvbmVudHMvY29udGVudC1maWVsZC5jb21wb25lbnQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy9kYXRlLWZpZWxkLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2Ryb3Bkb3duLWZpZWxkLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2ZpZWxkLWdyb3VwLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2Zvcm0tYWN0aW9uLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2hlYWRlci1maWVsZC5jb21wb25lbnQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy9vcHRpb24tc2V0LWZpZWxkLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL3RleHQtZmllbGQuY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2NvbXBvbmVudHMvdGV4dGFyZWEtZmllbGQuY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL25nLWR5bmFtaWMtZm9ybXMubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi9mb3JtLWJhc2UnO1xuaW1wb3J0IHsgRm9ybSB9IGZyb20gJy4vZm9ybSc7XG5cblxuZXhwb3J0IGNsYXNzIEZpZWxkU2V0IHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzZXQ6IEZvcm1CYXNlW10pIHsgfVxuXG4gIHNldEZvcm0oZm9ybTogRm9ybSkge1xuICAgIHRoaXMuc2V0LmZvckVhY2goKGZpZWxkKSA9PiB7XG4gICAgICBmaWVsZC5zZXRGb3JtKGZvcm0pO1xuICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlVmFsdWVzKCk6IHZvaWQge1xuICAgIHRoaXMuc2V0LmZvckVhY2goKGZpZWxkKSA9PiB7XG4gICAgICBmaWVsZC5kZXJpdmVWYWx1ZSgpO1xuICAgIH0pO1xuICB9XG5cbiAgZ2V0IGZpZWxkcygpIHtcbiAgICByZXR1cm4gdGhpcy5zZXQ7XG4gIH1cblxuICBwdXNoKGl0ZW06IEZvcm1CYXNlKTogdm9pZCB7XG4gICAgdGhpcy5zZXQucHVzaChpdGVtKTtcbiAgfVxuXG4gIGluc2VydEJlZm9yZShpdGVtOiBGb3JtQmFzZSwgYmVmb3JlOiBzdHJpbmcsIGF0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIGxldCBwYXJlbnQ6IEZvcm1CYXNlID0gbnVsbDtcbiAgICBpZiAoZmllbGQgPT09IG51bGwpIHtcbiAgICAgIGNvbnN0IHNlYXJjaDogeyBmaWVsZDogRm9ybUJhc2UsIHBhcmVudDogRm9ybUJhc2UgfSA9IHRoaXMuZmllbGRXaXRoUGFyZW50KGJlZm9yZSwgYXR0cik7XG4gICAgICBwYXJlbnQgPSBzZWFyY2gucGFyZW50O1xuICAgICAgZmllbGQgPSBzZWFyY2guZmllbGQ7XG4gICAgfVxuICAgIGlmIChwYXJlbnQgPT09IG51bGwpIHtcbiAgICAgIHRoaXMuc2V0LnNwbGljZSh0aGlzLnNldC5pbmRleE9mKGZpZWxkKSwgMCwgaXRlbSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHBhcmVudC5maWVsZHMuaW5zZXJ0QmVmb3JlKGl0ZW0sIGJlZm9yZSwgYXR0ciwgZmllbGQpO1xuICAgIH1cbiAgfVxuXG4gIGluc2VydEJlZm9yZU5hbWUoaXRlbTogRm9ybUJhc2UsIGJlZm9yZTogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKTogdm9pZCB7XG4gICAgcmV0dXJuIHRoaXMuaW5zZXJ0QmVmb3JlKGl0ZW0sIGJlZm9yZSwgJ25hbWUnLCBmaWVsZCk7XG4gIH1cblxuICBpbnNlcnRCZWZvcmVJZChpdGVtOiBGb3JtQmFzZSwgYmVmb3JlOiBzdHJpbmcsIGZpZWxkOiBGb3JtQmFzZSA9IG51bGwpOiB2b2lkIHtcbiAgICByZXR1cm4gdGhpcy5pbnNlcnRCZWZvcmUoaXRlbSwgYmVmb3JlLCAnaWQnLCBmaWVsZCk7XG4gIH1cblxuICBpbnNlcnRBZnRlcihpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgYXR0cjogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKTogdm9pZCB7XG4gICAgbGV0IHBhcmVudDogRm9ybUJhc2UgPSBudWxsO1xuICAgIGlmIChmaWVsZCA9PT0gbnVsbCkge1xuICAgICAgY29uc3Qgc2VhcmNoOiB7IGZpZWxkOiBGb3JtQmFzZSwgcGFyZW50OiBGb3JtQmFzZSB9ID0gdGhpcy5maWVsZFdpdGhQYXJlbnQoYWZ0ZXIsIGF0dHIpO1xuICAgICAgcGFyZW50ID0gc2VhcmNoLnBhcmVudDtcbiAgICAgIGZpZWxkID0gc2VhcmNoLmZpZWxkO1xuICAgIH1cbiAgICBpZiAocGFyZW50ID09PSBudWxsKSB7XG4gICAgICB0aGlzLnNldC5zcGxpY2UodGhpcy5zZXQuaW5kZXhPZihmaWVsZCkgKyAxLCAwLCBpdGVtKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGFyZW50LmZpZWxkcy5pbnNlcnRBZnRlcihpdGVtLCBhZnRlciwgYXR0ciwgZmllbGQpO1xuICAgIH1cbiAgfVxuXG4gIGluc2VydEFmdGVyTmFtZShpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgX2F0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIHRoaXMuaW5zZXJ0QWZ0ZXIoaXRlbSwgYWZ0ZXIsICduYW1lJywgZmllbGQpO1xuICB9XG5cbiAgaW5zZXJ0QWZ0ZXJJZChpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgX2F0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIHRoaXMuaW5zZXJ0QWZ0ZXIoaXRlbSwgYWZ0ZXIsICdpZCcsIGZpZWxkKTtcbiAgfVxuXG4gIHJlbW92ZShrZXk6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCwgYXR0cjogc3RyaW5nKSB7XG4gICAgbGV0IHBhcmVudDogRm9ybUJhc2UgPSBudWxsO1xuICAgIGlmIChmaWVsZCA9PT0gbnVsbCkge1xuICAgICAgY29uc3Qgc2VhcmNoOiB7IGZpZWxkOiBGb3JtQmFzZSwgcGFyZW50OiBGb3JtQmFzZSB9ID0gdGhpcy5maWVsZFdpdGhQYXJlbnQoa2V5LCBhdHRyKTtcbiAgICAgIHBhcmVudCA9IHNlYXJjaC5wYXJlbnQ7XG4gICAgICBmaWVsZCA9IHNlYXJjaC5maWVsZDtcbiAgICB9XG4gICAgaWYgKHBhcmVudCA9PT0gbnVsbCkge1xuICAgICAgdGhpcy5zZXQuc3BsaWNlKHRoaXMuc2V0LmluZGV4T2YoZmllbGQpLCAxKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGFyZW50LmZpZWxkcy5yZW1vdmUoa2V5LCBmaWVsZCwgYXR0cik7XG4gICAgfVxuICB9XG5cbiAgcmVtb3ZlQnlOYW1lKG5hbWU6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCkge1xuICAgIHRoaXMucmVtb3ZlKG5hbWUsIGZpZWxkLCAnbmFtZScpO1xuICB9XG5cbiAgcmVtb3ZlQnlJZChpZDogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKSB7XG4gICAgdGhpcy5yZW1vdmUoaWQsIGZpZWxkLCAnaWQnKTtcbiAgfVxuXG4gIGZpZWxkKGtleTogc3RyaW5nLCBhdHRyOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLmZpZWxkV2l0aFBhcmVudChrZXksIGF0dHIpLmZpZWxkO1xuICB9XG5cbiAgZmllbGRCeU5hbWUobmFtZTogc3RyaW5nKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZChuYW1lLCAnbmFtZScpO1xuICB9XG5cbiAgZmllbGRCeUlkKGlkOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLmZpZWxkKGlkLCAnaWQnKTtcbiAgfVxuXG4gIGZpZWxkV2l0aFBhcmVudChrZXk6IHN0cmluZywgYXR0cjogc3RyaW5nKTogeyBmaWVsZDogYW55LCBwYXJlbnQ6IGFueSB9IHtcbiAgICBsZXQgZmllbGQ6IGFueSA9IG51bGw7XG4gICAgbGV0IHBhcmVudDogYW55ID0gbnVsbDtcbiAgICBjb25zdCBzZWFyY2g6IGFueVtdID0gdGhpcy5zZXQuZmlsdGVyKGNoaWxkID0+IGNoaWxkW2F0dHJdID09PSBrZXkpO1xuICAgIGlmIChzZWFyY2gubGVuZ3RoID4gMCkge1xuICAgICAgZmllbGQgPSBzZWFyY2hbMF07XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IHBhcmVudHM6IGFueVtdID0gdGhpcy5zZXQuZmlsdGVyKGNoaWxkID0+IGNoaWxkLmhhc0ZpZWxkcyA9PT0gdHJ1ZSk7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHBhcmVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgY29uc3QgY2hlY2s6IHsgZmllbGQ6IGFueSwgcGFyZW50OiBhbnkgfSA9IHBhcmVudHNbaV0uZmllbGRzLmZpZWxkV2l0aFBhcmVudChrZXksIGF0dHIpO1xuICAgICAgICBpZiAoY2hlY2suZmllbGQgIT0gbnVsbCkge1xuICAgICAgICAgIGZpZWxkID0gY2hlY2suZmllbGQ7XG4gICAgICAgICAgcGFyZW50ID0gcGFyZW50c1tpXTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4geyBmaWVsZDogZmllbGQsIHBhcmVudDogcGFyZW50IH07XG4gIH1cblxuICBmaWVsZEJ5TmFtZVdpdGhQYXJlbnQobmFtZTogc3RyaW5nKTogeyBmaWVsZDogYW55LCBwYXJlbnQ6IGFueSB9IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZFdpdGhQYXJlbnQobmFtZSwgJ25hbWUnKTtcbiAgfVxuXG4gIGZpZWxkQnlJZFdpdGhQYXJlbnQoaWQ6IHN0cmluZyk6IHsgZmllbGQ6IGFueSwgcGFyZW50OiBhbnkgfSB7XG4gICAgcmV0dXJuIHRoaXMuZmllbGRXaXRoUGFyZW50KGlkLCAnaWQnKTtcbiAgfVxuXG4gIHJlc2V0VmFsaWRhdGlvbigpOiB2b2lkIHtcbiAgICB0aGlzLmZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcbiAgICAgIGZpZWxkLnJlc2V0VmFsaWRhdGlvbigpO1xuICAgIH0pO1xuICB9XG59XG4iLCJpbXBvcnQgeyBGb3JtIH0gZnJvbSAnLi9mb3JtJztcbmltcG9ydCB7IEZpZWxkU2V0IH0gZnJvbSAnLi9maWVsZC1zZXQnO1xuXG5pbXBvcnQgeyBWYWxpZGF0aW9uUmVzdWx0IH0gZnJvbSAnLi92YWxpZGF0aW9uLXJlc3VsdC5pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuZXhwb3J0IGNsYXNzIEZvcm1CYXNlIHtcbiAgbmFtZTogc3RyaW5nO1xuICBpZDogc3RyaW5nO1xuICB0eXBlOiBzdHJpbmc7XG4gIGNsYXNzZXM6IHN0cmluZ1tdO1xuICBmaWVsZHM6IEZpZWxkU2V0O1xuICBtZXNzYWdlOiBzdHJpbmc7XG4gIHJlYWRPbmx5OiBib29sZWFuO1xuICBjaGFuZ2U6IChmaWVsZDogRm9ybUJhc2UsIHZhbHVlOiBhbnkpID0+IGJvb2xlYW47XG4gIGJsdXI6IChmaWVsZDogRm9ybUJhc2UsIHZhbHVlOiBhbnkpID0+IHZvaWQ7XG4gIHByb3RlY3RlZCBmb3JtOiBGb3JtO1xuXG4gIGlzRm9ybSA9IGZhbHNlO1xuICBoYXNEYXRhID0gZmFsc2U7XG4gIGhhc0ZpZWxkcyA9IGZhbHNlO1xuICBoYXNMYWJlbCA9IHRydWU7XG4gIGlzQWN0aW9uID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge1xuICAgIG5hbWU/OiBzdHJpbmcsXG4gICAgY2xhc3Nlcz86IHN0cmluZ1tdLFxuICAgIGZpZWxkcz86IEZvcm1CYXNlW10sXG4gICAgcmVhZE9ubHk/OiBib29sZWFuLFxuICAgIGNoYW5nZT86IChmaWVsZDogRm9ybUJhc2UsIHZhbHVlOiBhbnkpID0+IGJvb2xlYW4sXG4gICAgYmx1cj86IChmaWVsZDogRm9ybUJhc2UsIHZhbHVlOiBhbnkpID0+IHZvaWRcbiAgfSA9IHt9KSB7XG4gICAgdGhpcy5uYW1lID0gb3B0aW9ucy5uYW1lIHx8ICcnO1xuICAgIHRoaXMuaWQgPSB0aGlzLm5hbWU7XG4gICAgdGhpcy5jbGFzc2VzID0gb3B0aW9ucy5jbGFzc2VzIHx8IFtdO1xuICAgIHRoaXMuZmllbGRzID0gbmV3IEZpZWxkU2V0KG9wdGlvbnMuZmllbGRzIHx8IFtdKTtcbiAgICB0aGlzLnJlYWRPbmx5ID0gb3B0aW9ucy5yZWFkT25seSB8fCBmYWxzZTtcbiAgICB0aGlzLmNoYW5nZSA9IG9wdGlvbnMuY2hhbmdlIHx8IG51bGw7XG4gICAgdGhpcy5ibHVyID0gb3B0aW9ucy5ibHVyIHx8IG51bGw7XG4gIH1cblxuICBnZXRGb3JtKCk6IEZvcm0ge1xuICAgIHJldHVybiB0aGlzLmZvcm07XG4gIH1cblxuICBzZXRGb3JtKGZvcm06IEZvcm0pOiB2b2lkIHtcbiAgICB0aGlzLmZvcm0gPSBmb3JtO1xuICAgIGlmIChmb3JtLnByZWZpeCkge1xuICAgICAgdGhpcy5pZCA9IGAke2Zvcm0ucHJlZml4fSR7dGhpcy5uYW1lfWA7XG4gICAgfVxuICAgIGlmICghdGhpcy5pc0Zvcm0pIHtcbiAgICAgIHRoaXMuZmllbGRzLnNldEZvcm0oZm9ybSk7XG4gICAgfVxuICB9XG5cbiAgYXN5bmMgZGVyaXZlVmFsdWUoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShmYWxzZSk7XG4gIH1cblxuICBnZXQgY29udHJvbCgpOiBGb3JtQ29udHJvbCB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICBvbkNoYW5nZSh2YWx1ZSkge1xuICAgIGxldCBwcm9wYWdhdGUgPSB0cnVlO1xuICAgIGlmICh0aGlzLmNoYW5nZSAhPSBudWxsKSB7XG4gICAgICBwcm9wYWdhdGUgPSB0aGlzLmNoYW5nZSh0aGlzLCB2YWx1ZSk7XG4gICAgfVxuICAgIGlmIChwcm9wYWdhdGUpIHtcbiAgICAgIHRoaXMuZm9ybS5maWVsZENoYW5nZSh0aGlzLm5hbWUsIHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBvbkJsdXIodmFsdWUpIHtcbiAgICBpZiAodGhpcy5ibHVyICE9IG51bGwpIHtcbiAgICAgIHRoaXMuYmx1cih0aGlzLCB2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgb25DbGljaygpIHtcblxuICB9XG5cbiAgc2V0TWVzc2FnZShtZXNzYWdlOiBzdHJpbmcpOiB2b2lkIHtcbiAgICB0aGlzLm1lc3NhZ2UgPSBtZXNzYWdlO1xuICB9XG5cbiAgbG9hZFZhbGlkYXRpb24ocmVzdWx0OiBWYWxpZGF0aW9uUmVzdWx0KTogdm9pZCB7XG4gICAgdGhpcy5yZXNldFZhbGlkYXRpb24oKTtcbiAgICBpZiAocmVzdWx0Lm1lc3NhZ2VzICE9IG51bGwgJiYgcmVzdWx0Lm1lc3NhZ2VzLmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMuc2V0TWVzc2FnZShyZXN1bHQubWVzc2FnZXNbMF0pO1xuICAgIH1cbiAgICBmb3IgKGNvbnN0IGtleSBpbiByZXN1bHQuZmllbGRzKSB7XG4gICAgICBpZiAocmVzdWx0LmZpZWxkcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgIGNvbnN0IGZpZWxkOiBGb3JtQmFzZSA9IHRoaXMuZmllbGRzLmZpZWxkQnlOYW1lKGtleSk7XG4gICAgICAgIGlmIChmaWVsZCAhPSBudWxsKSB7XG4gICAgICAgICAgZmllbGQubG9hZFZhbGlkYXRpb24ocmVzdWx0LmZpZWxkc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJlc2V0VmFsaWRhdGlvbigpIHtcbiAgICB0aGlzLnNldE1lc3NhZ2UobnVsbCk7XG4gICAgaWYgKHRoaXMuZmllbGRzKSB7XG4gICAgICB0aGlzLmZpZWxkcy5yZXNldFZhbGlkYXRpb24oKTtcbiAgICB9XG4gIH1cbn1cbiIsImltcG9ydCB7IEZvcm0gfSBmcm9tICcuLi9mb3JtJztcbmltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuXG5leHBvcnQgY2xhc3MgRmllbGQ8VD4gZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIGlkOiBzdHJpbmc7XG4gIGhhc0RhdGEgPSB0cnVlO1xuICB2YWx1ZTogVDtcbiAgbGFiZWw6IHN0cmluZztcbiAgcGxhY2Vob2xkZXI6IHN0cmluZztcbiAgZGVyaXZlOiAoZmllbGQ6IEZpZWxkPFQ+KSA9PiBQcm9taXNlPGJvb2xlYW4+O1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHtcbiAgICB2YWx1ZT86IFQsXG4gICAgbGFiZWw/OiBzdHJpbmcsXG4gICAgY2xhc3Nlcz86IHN0cmluZ1tdLFxuICAgIHBsYWNlaG9sZGVyPzogc3RyaW5nLFxuICAgIGRlcml2ZT86IChmaWVsZDogRmllbGQ8VD4pID0+IFByb21pc2U8Ym9vbGVhbj5cbiAgfSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy52YWx1ZSA9IG9wdGlvbnMudmFsdWU7XG4gICAgdGhpcy5sYWJlbCA9IG9wdGlvbnMubGFiZWwgfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1maWVsZCcpO1xuICAgIHRoaXMucGxhY2Vob2xkZXIgPSBvcHRpb25zLnBsYWNlaG9sZGVyIHx8ICcnO1xuICAgIGlmICh0aGlzLnBsYWNlaG9sZGVyLmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtcGxhY2Vob2xkZXInKTtcbiAgICB9XG4gICAgdGhpcy5kZXJpdmUgPSBvcHRpb25zLmRlcml2ZSB8fCBudWxsO1xuICB9XG5cbiAgc2V0Rm9ybShmb3JtOiBGb3JtKTogdm9pZCB7XG4gICAgc3VwZXIuc2V0Rm9ybShmb3JtKTtcbiAgICB0aGlzLmRlcml2ZVZhbHVlKCk7XG4gIH1cblxuICBvbkNoYW5nZSh2YWx1ZSkge1xuICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcbiAgICBzdXBlci5vbkNoYW5nZSh0aGlzLnByZXBhcmVWYWx1ZSh2YWx1ZSkpO1xuICB9XG5cbiAgYXN5bmMgZGVyaXZlVmFsdWUoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgcmV0dXJuIHN1cGVyLmRlcml2ZVZhbHVlKCkudGhlbihkZXJpdmVkID0+IHtcbiAgICAgIGlmICghZGVyaXZlZCkge1xuICAgICAgICBpZiAodGhpcy5kZXJpdmUgIT0gbnVsbCkge1xuICAgICAgICAgIHJldHVybiB0aGlzLmRlcml2ZSh0aGlzKTtcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmZvcm0ub2JqLmhhc093blByb3BlcnR5KHRoaXMubmFtZSkpIHtcbiAgICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5mb3JtLm9ialt0aGlzLm5hbWVdO1xuICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUodHJ1ZSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZmFsc2UpO1xuICAgIH0pO1xuICB9XG5cbiAgcHJlcGFyZVZhbHVlKHZhbHVlOiBhbnkpOiBhbnkge1xuICAgIGlmICh2YWx1ZSA9PSBudWxsKSB7XG4gICAgICByZXR1cm4gJyc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9XG4gIH1cbn1cbiIsImltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi9maWVsZCc7XG5cblxuZXhwb3J0IGNsYXNzIENoZWNrYm94RmllbGQgZXh0ZW5kcyBGaWVsZDxib29sZWFuPiB7XG4gIHR5cGUgPSAnY2hlY2tib3gnO1xuICBoYXNMYWJlbCA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWNoZWNrYm94Jyk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi9maWVsZCc7XG5cbmV4cG9ydCBjbGFzcyBPcHRpb25TZXRGaWVsZDxUPiBleHRlbmRzIEZpZWxkPFQ+IHtcbiAgdHlwZSA9ICdvcHRpb25zZXQnO1xuICBvcHRpb25TZXQ6IHsga2V5OiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcsIGRpc2FibGVkPzogYm9vbGVhbiB9W10gPSBbXTtcbiAgb3B0aW9uczogeyBrZXk6IHN0cmluZywgdmFsdWU6IHN0cmluZywgZGlzYWJsZWQ/OiBib29sZWFuIH1bXSA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLW9wdGlvbnNldCcpO1xuICAgIHRoaXMub3B0aW9uU2V0ID0gb3B0aW9uc1snb3B0aW9ucyddIHx8IFtdO1xuICAgIFByb21pc2UucmVzb2x2ZSh0aGlzLm9wdGlvblNldCkudGhlbihvcHRpb25MaXN0ID0+IHtcbiAgICAgIHRoaXMuc2V0T3B0aW9ucyhvcHRpb25MaXN0KTtcbiAgICB9KTtcbiAgfVxuXG4gIHNldE9wdGlvbnMob3B0aW9uczogQXJyYXk8eyBrZXk6IHN0cmluZywgdmFsdWU6IHN0cmluZywgZGlzYWJsZWQ/OiBib29sZWFuIH0+KSB7XG4gICAgdGhpcy5vcHRpb25zID0gb3B0aW9ucztcbiAgfVxufVxuIiwiaW1wb3J0IHsgT3B0aW9uU2V0RmllbGQgfSBmcm9tICcuL29wdGlvbi1zZXQtZmllbGQnO1xuXG5cbmV4cG9ydCBjbGFzcyBDaGVja2JveFNldEZpZWxkPFQ+IGV4dGVuZHMgT3B0aW9uU2V0RmllbGQ8VD4ge1xuICB0eXBlID0gJ2NoZWNrYm94c2V0JztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1jaGVja2JveHNldCcpO1xuICB9XG5cbiAgb25DaGFuZ2UodmFsdWUpIHtcbiAgICBjb25zdCBvYmogPSB0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV07XG4gICAgaWYgKHRoaXMuaXNDaGVja2VkKHZhbHVlKSkge1xuICAgICAgb2JqLnNwbGljZShvYmouaW5kZXhPZih2YWx1ZSksIDEpO1xuICAgIH0gZWxzZSB7XG4gICAgICBvYmoucHVzaCh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgaXNDaGVja2VkKHZhbHVlKTogYm9vbGVhbiB7XG4gICAgY29uc3Qgc2VsZWN0ZWQgPSB0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV07XG4gICAgaWYgKHNlbGVjdGVkICE9IG51bGwpIHtcbiAgICAgIHJldHVybiBzZWxlY3RlZC5maWx0ZXIobyA9PiBvID09PSB2YWx1ZSkubGVuZ3RoID4gMDtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG59XG4iLCJpbXBvcnQgeyBGb3JtQmFzZSB9IGZyb20gJy4uL2Zvcm0tYmFzZSc7XG5cbmV4cG9ydCBjbGFzcyBDb250ZW50RmllbGQgZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIHR5cGUgPSAnY29udGVudCc7XG4gIGNvbnRlbnQ6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jb250ZW50ID0gb3B0aW9uc1snY29udGVudCddIHx8ICcnO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtZmllbGQnKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWNvbnRlbnQnKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuL2ZpZWxkJztcblxuaW1wb3J0IHsgSU15RGF0ZU1vZGVsIH0gZnJvbSAnbmd4LW15ZGF0ZXBpY2tlcic7XG5cbmV4cG9ydCBjbGFzcyBEYXRlRmllbGQgZXh0ZW5kcyBGaWVsZDxEYXRlPiB7XG4gIHR5cGUgPSAnZGF0ZSc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtZGF0ZScpO1xuICB9XG5cbiAgb25DaGFuZ2UodmFsdWU6IElNeURhdGVNb2RlbCkge1xuICAgIHN1cGVyLm9uQ2hhbmdlKCh2YWx1ZS5qc2RhdGUgPT0gbnVsbCkgPyBudWxsIDogbmV3IERhdGUodmFsdWUuanNkYXRlKSk7XG4gIH1cbn1cbiIsImltcG9ydCB7IE9wdGlvblNldEZpZWxkIH0gZnJvbSAnLi9vcHRpb24tc2V0LWZpZWxkJztcblxuZXhwb3J0IGNsYXNzIERyb3Bkb3duRmllbGQgZXh0ZW5kcyBPcHRpb25TZXRGaWVsZDxzdHJpbmc+IHtcbiAgdHlwZSA9ICdkcm9wZG93bic7XG4gIGVtcHR5VGV4dDogc3RyaW5nO1xuICBsb2FkZWQ6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1kcm9wZG93bicpO1xuICAgIHRoaXMubG9hZGVkID0gb3B0aW9uc1snZW1wdHlUZXh0J10gfHwgJyc7XG4gICAgdGhpcy5lbXB0eVRleHQgPSBvcHRpb25zWydsb2FkaW5nVGV4dCddIHx8ICcnO1xuICB9XG5cbiAgc2V0T3B0aW9ucyhvcHRpb25zOiBBcnJheTx7IGtleTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nLCBkaXNhYmxlZD86IGJvb2xlYW4gfT4pIHtcbiAgICBzdXBlci5zZXRPcHRpb25zKG9wdGlvbnMpO1xuICAgIHRoaXMuZW1wdHlUZXh0ID0gdGhpcy5sb2FkZWQ7XG4gIH1cbn1cbiIsImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuZXhwb3J0IGNsYXNzIEZpZWxkR3JvdXAgZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIGhhc0ZpZWxkcyA9IHRydWU7XG4gIHR5cGUgPSAnZ3JvdXAnO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgfVxuXG4gIGFzeW5jIGRlcml2ZVZhbHVlKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgIHJldHVybiBzdXBlci5kZXJpdmVWYWx1ZSgpLnRoZW4oZGVyaXZlZCA9PiB7XG4gICAgICB0aGlzLmZpZWxkcy51cGRhdGVWYWx1ZXMoKTtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZGVyaXZlZCk7XG4gICAgfSk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuZXhwb3J0IGNsYXNzIEZvcm1BY3Rpb24gZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIGlzQWN0aW9uID0gdHJ1ZTtcbiAgdHlwZSA9ICdhY3Rpb24nO1xuICB0aXRsZTogc3RyaW5nO1xuICBpY29uOiBzdHJpbmc7XG4gIGFjdGlvblR5cGU6IHN0cmluZztcbiAgbG9hZGluZzogc3RyaW5nO1xuICBjbGljazogKGFjdGlvbjogRm9ybUFjdGlvbikgPT4gdm9pZDtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy50aXRsZSA9IG9wdGlvbnNbJ3RpdGxlJ10gfHwgbnVsbDtcbiAgICB0aGlzLmljb24gPSBvcHRpb25zWydpY29uJ10gfHwgbnVsbDtcbiAgICB0aGlzLmFjdGlvblR5cGUgPSBvcHRpb25zWydhY3Rpb25UeXBlJ10gfHwgJ3N1Ym1pdCc7XG4gICAgdGhpcy5jbGljayA9IG9wdGlvbnNbJ2NsaWNrJ10gfHwgbnVsbDtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWFjdGlvbicpO1xuICB9XG5cbiAgc2hvd0xvYWRpbmcodGV4dDogc3RyaW5nKSB7XG4gICAgdGhpcy5sb2FkaW5nID0gdGV4dDtcbiAgfVxuXG4gIGNsZWFyTG9hZGluZygpIHtcbiAgICB0aGlzLmxvYWRpbmcgPSBudWxsO1xuICB9XG5cbiAgb25DbGljaygpIHtcbiAgICBpZiAodGhpcy5jbGljayAhPSBudWxsKSB7XG4gICAgICB0aGlzLmNsaWNrKHRoaXMpO1xuICAgIH1cbiAgfVxufVxuIiwiaW1wb3J0IHsgRm9ybUJhc2UgfSBmcm9tICcuLi9mb3JtLWJhc2UnO1xuXG5leHBvcnQgY2xhc3MgSGVhZGVyRmllbGQgZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIHR5cGUgPSAnaGVhZGVyJztcbiAgdGl0bGU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy50aXRsZSA9IG9wdGlvbnNbJ3RpdGxlJ10gfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1maWVsZCcpO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtaGVhZGVyJyk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEZpZWxkR3JvdXAgfSBmcm9tICcuL2ZpZWxkLWdyb3VwJztcbmltcG9ydCB7IEhlYWRlckZpZWxkIH0gZnJvbSAnLi9oZWFkZXItZmllbGQnO1xuaW1wb3J0IHsgRm9ybUFjdGlvbiB9IGZyb20gJy4vZm9ybS1hY3Rpb24nO1xuaW1wb3J0IHsgRm9ybSB9IGZyb20gJy4uL2Zvcm0nO1xuaW1wb3J0IHsgVmFsaWRhdGlvblJlc3VsdCB9IGZyb20gJy4uL3ZhbGlkYXRpb24tcmVzdWx0LmludGVyZmFjZSc7XG5cbmV4cG9ydCBjbGFzcyBNdWx0aUNoaWxkRmllbGQgZXh0ZW5kcyBGaWVsZEdyb3VwIHtcbiAgdHlwZTogJ211bHRpY2hpbGQnO1xuICBzdWJGb3JtOiAob2JqOiBhbnksIHByZWZpeDogc3RyaW5nKSA9PiBGb3JtO1xuICBuZXdPYmo6ICgpID0+IGFueTtcbiAgYWRkQnV0dG9uVGV4dDogc3RyaW5nO1xuICByZW1vdmVCdXR0b25UZXh0OiBzdHJpbmc7XG4gIGNoaWxkID0gZmFsc2U7XG4gIHByaXZhdGUgY291bnRlciA9IDA7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMuc3ViRm9ybSA9IG9wdGlvbnNbJ3N1YkZvcm0nXSB8fCBudWxsO1xuICAgIHRoaXMubmV3T2JqID0gb3B0aW9uc1snbmV3T2JqJ10gfHwgbnVsbDtcbiAgICB0aGlzLmFkZEJ1dHRvblRleHQgPSBvcHRpb25zWydhZGRCdXR0b25UZXh0J10gfHwgJ0FkZCBOZXcgTGluZSc7XG4gICAgdGhpcy5yZW1vdmVCdXR0b25UZXh0ID0gb3B0aW9uc1sncmVtb3ZlQnV0dG9uVGV4dCddIHx8ICdSZW1vdmUgTGluZSc7XG4gICAgdGhpcy5jaGlsZCA9IG9wdGlvbnNbJ2NoaWxkJ10gfHwgZmFsc2U7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1tdWx0aWNoaWxkJyk7XG4gICAgaWYgKG9wdGlvbnNbJ3RpdGxlJ10pIHtcbiAgICAgIHRoaXMuZmllbGRzLnB1c2gobmV3IEhlYWRlckZpZWxkKHtcbiAgICAgICAgdGl0bGU6IG9wdGlvbnNbJ3RpdGxlJ11cbiAgICAgIH0pKTtcbiAgICAgIHRoaXMuZmllbGRzLnB1c2gobmV3IEZpZWxkR3JvdXAoe1xuICAgICAgICBuYW1lOiBgJHt0aGlzLm5hbWV9X2NvbnRyb2xzYCxcbiAgICAgICAgZmllbGRzOiBbXG4gICAgICAgICAgbmV3IEZvcm1BY3Rpb24oe1xuICAgICAgICAgICAgdGl0bGU6IHRoaXMuYWRkQnV0dG9uVGV4dCxcbiAgICAgICAgICAgIGljb246ICdwbHVzJyxcbiAgICAgICAgICAgIGNsaWNrOiAoKSA9PiB7XG4gICAgICAgICAgICAgIGlmICh0aGlzLnN1YkZvcm0gIT09IG51bGwgJiYgdGhpcy5uZXdPYmogIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBvYmo6IGFueSA9IHRoaXMubmV3T2JqKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5saXN0LnB1c2gob2JqKTtcbiAgICAgICAgICAgICAgICB0aGlzLmFkZENoaWxkKG9iaik7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjbGFzc2VzIDogWyduZGYtYWRkJ11cbiAgICAgICAgICB9KVxuICAgICAgICBdLFxuICAgICAgICBjbGFzc2VzOiBbJ25kZi1mdWxsLWJvcmRlcmVkJywgJ25kZi1tdWx0aWNoaWxkLWZvb3RlciddXG4gICAgICB9KSk7XG4gICAgfVxuICB9XG5cbiAgZ2V0IGxpc3QoKTogQXJyYXk8YW55PiB7XG4gICAgaWYgKHRoaXMuZm9ybS5vYmpbdGhpcy5uYW1lXSA9PSBudWxsKSB7XG4gICAgICB0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV0gPSBbXTtcbiAgICB9XG4gICAgcmV0dXJuICh0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV0gYXMgQXJyYXk8YW55Pik7XG4gIH1cblxuICBzZXRGb3JtKGZvcm06IEZvcm0pOiB2b2lkIHtcbiAgICBzdXBlci5zZXRGb3JtKGZvcm0pO1xuICAgIGlmICh0aGlzLnN1YkZvcm0gIT09IG51bGwpIHtcbiAgICAgIHRoaXMubGlzdC5mb3JFYWNoKChvYmopID0+IHtcbiAgICAgICAgdGhpcy5hZGRDaGlsZChvYmopO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgbG9hZFZhbGlkYXRpb24ocmVzdWx0OiBWYWxpZGF0aW9uUmVzdWx0KTogdm9pZCB7XG4gICAgdGhpcy5yZXNldFZhbGlkYXRpb24oKTtcbiAgICBmb3IgKGNvbnN0IGtleSBpbiByZXN1bHQuZmllbGRzKSB7XG4gICAgICBpZiAocmVzdWx0LmZpZWxkcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgIGNvbnN0IGNoaWxkID0gYCR7dGhpcy5mb3JtLnByZWZpeH1saW5lXyR7a2V5fWA7XG4gICAgICAgIGNvbnN0IGZpZWxkID0gdGhpcy5maWVsZHMuZmllbGRCeU5hbWUoY2hpbGQpO1xuICAgICAgICBpZiAoZmllbGQgIT0gbnVsbCkge1xuICAgICAgICAgIGZpZWxkLmxvYWRWYWxpZGF0aW9uKHJlc3VsdC5maWVsZHNba2V5XSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGFkZENoaWxkKG9iajogYW55KTogdm9pZCB7XG4gICAgY29uc3QgZ3JvdXAgPSBgJHt0aGlzLmZvcm0ucHJlZml4fWxpbmVfJHt0aGlzLmNvdW50ZXJ9X2dyb3VwYDtcbiAgICB0aGlzLmZpZWxkcy5pbnNlcnRCZWZvcmVJZChcbiAgICAgIG5ldyBGaWVsZEdyb3VwKHtcbiAgICAgICAgbmFtZTogZ3JvdXAsXG4gICAgICAgIGZpZWxkczogW1xuICAgICAgICAgIHRoaXMuc3ViRm9ybShvYmosIGAke3RoaXMuZm9ybS5wcmVmaXh9bGluZV8ke3RoaXMuY291bnRlcn1gKSxcbiAgICAgICAgICBuZXcgRm9ybUFjdGlvbih7XG4gICAgICAgICAgICB0aXRsZTogdGhpcy5yZW1vdmVCdXR0b25UZXh0LFxuICAgICAgICAgICAgaWNvbjogJ3RyYXNoJyxcbiAgICAgICAgICAgIGNsaWNrOiAoKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZmllbGRzLnJlbW92ZUJ5SWQoZ3JvdXApO1xuICAgICAgICAgICAgICB0aGlzLmxpc3Quc3BsaWNlKHRoaXMubGlzdC5pbmRleE9mKG9iaiksIDEpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNsYXNzZXMgOiBbJ25kZi1yZW1vdmUnXVxuICAgICAgICAgIH0pXG4gICAgICAgIF0sXG4gICAgICAgIGNsYXNzZXM6IFsnbmRmLWZ1bGwtYm9yZGVyZWQnLCAnbmRmLW11bHRpY2hpbGQtcm93JywgKHRoaXMuY2hpbGQpID8gJ25kZi1jaGlsZCcgOiAnbmRmLXBhcmVudCddXG4gICAgICB9KSwgYCR7dGhpcy5uYW1lfV9jb250cm9sc2BcbiAgICApO1xuICAgIHRoaXMuY291bnRlcisrO1xuICB9XG59XG4iLCJpbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4vZmllbGQnO1xuXG5leHBvcnQgY2xhc3MgVGV4dEZpZWxkIGV4dGVuZHMgRmllbGQ8c3RyaW5nPiB7XG4gIHR5cGUgPSAndGV4dCc7XG4gIGlucHV0VHlwZTogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLnZhbHVlID0gb3B0aW9uc1sndmFsdWUnXSB8fCAnJztcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLXRleHQnKTtcbiAgICB0aGlzLmlucHV0VHlwZSA9IG9wdGlvbnNbJ2lucHV0VHlwZSddIHx8ICcnO1xuICB9XG59XG4iLCJpbXBvcnQgeyBUZXh0RmllbGQgfSBmcm9tICcuL3RleHQtZmllbGQnO1xuXG5cbmV4cG9ydCBjbGFzcyBOdW1iZXJGaWVsZCBleHRlbmRzIFRleHRGaWVsZCB7XG4gIHByZWNpc2lvbjogbnVtYmVyO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLnZhbHVlID0gb3B0aW9uc1sndmFsdWUnXSB8fCAwO1xuICAgIHRoaXMucHJlY2lzaW9uID0gKG9wdGlvbnNbJ3ByZWNpc2lvbiddICE9IG51bGwpID8gb3B0aW9uc1sncHJlY2lzaW9uJ10gOiAyO1xuICB9XG5cbiAgYXN5bmMgZGVyaXZlVmFsdWUoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgcmV0dXJuIHN1cGVyLmRlcml2ZVZhbHVlKCkudGhlbihkZXJpdmVkID0+IHtcbiAgICAgIGlmICghZGVyaXZlZCkge1xuICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5wcmVwYXJlVmFsdWUodGhpcy52YWx1ZSk7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUodHJ1ZSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcbiAgICB9KTtcbiAgfVxuXG4gIG9uQmx1cigpOiB2b2lkIHtcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5wcmVwYXJlVmFsdWUodGhpcy52YWx1ZSk7XG4gIH1cblxuICBwcmVwYXJlVmFsdWUodmFsdWU6IGFueSk6IGFueSB7XG4gICAgbGV0IHByZXBhcmVkOiBudW1iZXI7XG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ251bWJlcicpIHtcbiAgICAgIHByZXBhcmVkID0gdmFsdWUgYXMgbnVtYmVyO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlID09PSAndW5kZWZpbmVkJykge1xuICAgICAgcHJlcGFyZWQgPSAwO1xuICAgIH0gZWxzZSB7XG4gICAgICBwcmVwYXJlZCA9IHBhcnNlRmxvYXQodmFsdWUudG9TdHJpbmcoKS5yZXBsYWNlKC9bXjAtOS5dL2csICcnKSk7XG4gICAgfVxuICAgIHJldHVybiBwcmVwYXJlZC50b0ZpeGVkKHRoaXMucHJlY2lzaW9uKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuL2ZpZWxkJztcblxuZXhwb3J0IGNsYXNzIFRleHRhcmVhRmllbGQgZXh0ZW5kcyBGaWVsZDxzdHJpbmc+IHtcbiAgdHlwZSA9ICd0ZXh0YXJlYSc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMudmFsdWUgPSBvcHRpb25zWyd2YWx1ZSddIHx8ICcnO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtdGV4dGFyZWEnKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIEZvcm1Hcm91cCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuLi9maWVsZHMvZmllbGQnO1xuXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBGb3JtU2VydmljZSB7XG4gIHByaXZhdGUgZm9ybUdyb3VwOiBGb3JtR3JvdXA7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5mb3JtR3JvdXAgPSBuZXcgRm9ybUdyb3VwKHt9KTtcbiAgfVxuXG4gIGdldCBncm91cCgpOiBGb3JtR3JvdXAge1xuICAgIHJldHVybiB0aGlzLmZvcm1Hcm91cDtcbiAgfVxuXG4gIHJlZ2lzdGVyRmllbGQoZmllbGQ6IEZpZWxkPGFueT4pIHtcbiAgICBpZiAoIXRoaXMuZ3JvdXAuY29udGFpbnMoZmllbGQuaWQpKSB7XG4gICAgICB0aGlzLmdyb3VwLmFkZENvbnRyb2woZmllbGQuaWQsIHRoaXMuY29udHJvbEZyb21GaWVsZChmaWVsZCkpO1xuICAgIH1cbiAgfVxuXG4gIHJlZ2lzdGVyRmllbGRzKGZpZWxkczogRmllbGQ8YW55PltdKSB7XG4gICAgZmllbGRzLmZvckVhY2goZmllbGQgPT4ge1xuICAgICAgdGhpcy5yZWdpc3RlckZpZWxkKGZpZWxkKTtcbiAgICB9KTtcbiAgfVxuXG4gIGRlcmVnaXN0ZXJGaWVsZChmaWVsZDogRmllbGQ8YW55Pikge1xuICAgIHRoaXMuZ3JvdXAucmVtb3ZlQ29udHJvbChmaWVsZC5pZCk7XG4gIH1cblxuICBkZXJlZ2lzdGVyRmllbGRzKGZpZWxkczogRmllbGQ8YW55PltdKSB7XG4gICAgZmllbGRzLmZvckVhY2goZmllbGQgPT4ge1xuICAgICAgdGhpcy5kZXJlZ2lzdGVyRmllbGQoZmllbGQpO1xuICAgIH0pO1xuICB9XG5cbiAgY29udHJvbEZyb21GaWVsZChmaWVsZDogRmllbGQ8YW55Pik6IEZvcm1Db250cm9sIHtcbiAgICByZXR1cm4gbmV3IEZvcm1Db250cm9sKGZpZWxkLnZhbHVlIHx8ICcnKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgRm9ybUJhc2UgfSBmcm9tICcuL2Zvcm0tYmFzZSc7XG5cblxuZXhwb3J0IGNsYXNzIEZvcm0gZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIG9iajogYW55O1xuICBzdWJtaXQ6IChmb3JtOiBGb3JtKSA9PiB2b2lkO1xuICBoYXNGaWVsZHMgPSB0cnVlO1xuICBpc0Zvcm0gPSB0cnVlO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHtcbiAgICBuYW1lPzogc3RyaW5nLFxuICAgIGZpZWxkcz86IEZvcm1CYXNlW10sXG4gICAgb2JqPzogYW55LFxuICAgIHN1Ym1pdD86IChmb3JtOiBGb3JtKSA9PiB2b2lkO1xuICAgIGNsYXNzZXM/OiBzdHJpbmdbXVxuICB9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLm9iaiA9IG9wdGlvbnNbJ29iaiddIHx8IHt9O1xuICAgIHRoaXMuc3VibWl0ID0gb3B0aW9uc1snc3VibWl0J10gfHwgbnVsbDtcbiAgICB0aGlzLmZpZWxkcy5zZXRGb3JtKHRoaXMpO1xuICB9XG5cbiAgZ2V0IHByZWZpeCgpOiBzdHJpbmcge1xuICAgIGlmICh0aGlzLmZvcm0pIHtcbiAgICAgIHJldHVybiBgJHt0aGlzLmZvcm0ucHJlZml4fSR7dGhpcy5uYW1lfV9gO1xuICAgIH0gZWxzZSBpZiAodGhpcy5uYW1lKSB7XG4gICAgICByZXR1cm4gYCR7dGhpcy5uYW1lfV9gO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gJyc7XG4gICAgfVxuICB9XG5cbiAgdXBkYXRlKG9iajogYW55KSB7XG4gICAgdGhpcy5vYmogPSBvYmo7XG4gICAgdGhpcy5maWVsZHMudXBkYXRlVmFsdWVzKCk7XG4gIH1cblxuICBmaWVsZENoYW5nZShrZXk6IHN0cmluZywgdmFsdWU6IGFueSk6IHZvaWQge1xuICAgIHRoaXMub2JqW2tleV0gPSB2YWx1ZTtcbiAgfVxuXG4gIG9uU3VibWl0KCkge1xuICAgIGlmICh0aGlzLnN1Ym1pdCAhPT0gbnVsbCkge1xuICAgICAgdGhpcy5zdWJtaXQodGhpcyk7XG4gICAgfVxuICB9XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Hcm91cCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgRm9ybSB9IGZyb20gJy4uL2Zvcm0nO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1mb3JtJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Zvcm0uY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgZm9ybTogRm9ybTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSkge31cblxuICBuZ09uSW5pdCgpIHtcblxuICB9XG5cbiAgZ2V0IGdyb3VwKCk6IEZvcm1Hcm91cCB7XG4gICAgcmV0dXJuIHRoaXMuZm9ybVNlcnZpY2UuZ3JvdXA7XG4gIH1cblxuICBvblN1Ym1pdCgpIHtcbiAgICB0aGlzLmZvcm0ub25TdWJtaXQoKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Hcm91cCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgRm9ybUJhc2UgfSBmcm9tICcuLi9mb3JtLWJhc2UnO1xuaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuLi9maWVsZHMvZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtZm9ybS1pdGVtJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Zvcm0taXRlbS5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1JdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBpdGVtOiBGb3JtQmFzZTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSkge31cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZiAodGhpcy5pdGVtLmhhc0RhdGEpIHtcbiAgICAgIHRoaXMuZm9ybVNlcnZpY2UucmVnaXN0ZXJGaWVsZCh0aGlzLml0ZW0gYXMgRmllbGQ8YW55Pik7XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gICAgaWYgKHRoaXMuaXRlbS5oYXNEYXRhKSB7XG4gICAgICB0aGlzLmZvcm1TZXJ2aWNlLmRlcmVnaXN0ZXJGaWVsZCh0aGlzLml0ZW0gYXMgRmllbGQ8YW55Pik7XG4gICAgfVxuICB9XG5cbiAgZ2V0IGdyb3VwKCk6IEZvcm1Hcm91cCB7XG4gICAgcmV0dXJuIHRoaXMuZm9ybVNlcnZpY2UuZ3JvdXA7XG4gIH1cblxuICBvbkNoYW5nZSh2YWx1ZSkge1xuICAgIHRoaXMuaXRlbS5vbkNoYW5nZSh2YWx1ZSk7XG4gIH1cblxuICBvbkJsdXIodmFsdWUpIHtcbiAgICB0aGlzLml0ZW0ub25CbHVyKHZhbHVlKTtcbiAgfVxuXG4gIG9uQ2xpY2soKSB7XG4gICAgdGhpcy5pdGVtLm9uQ2xpY2soKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDaGVja2JveEZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL2NoZWNrYm94LWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWNoZWNrYm94LWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NoZWNrYm94LWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgQ2hlY2tib3hGaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogQ2hlY2tib3hGaWVsZDtcbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2hlY2tib3hTZXRGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9jaGVja2JveC1zZXQtZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtY2hlY2tib3gtc2V0LWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NoZWNrYm94LXNldC1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIENoZWNrYm94U2V0RmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IENoZWNrYm94U2V0RmllbGQ8YW55Pjtcbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ29udGVudEZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL2NvbnRlbnQtZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtY29udGVudC1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9jb250ZW50LWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgQ29udGVudEZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBDb250ZW50RmllbGQ7XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IERhdGVGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9kYXRlLWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWRhdGUtZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vZGF0ZS1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIERhdGVGaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogRGF0ZUZpZWxkO1xufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEcm9wZG93bkZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL2Ryb3Bkb3duLWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWRyb3Bkb3duLWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Ryb3Bkb3duLWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgRHJvcGRvd25GaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogRHJvcGRvd25GaWVsZDtcbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmllbGRHcm91cCB9IGZyb20gJy4uL2ZpZWxkcy9maWVsZC1ncm91cCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1maWVsZC1ncm91cCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9maWVsZC1ncm91cC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIEZpZWxkR3JvdXBDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IEZpZWxkR3JvdXA7XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IEZvcm1BY3Rpb24gfSBmcm9tICcuLi9maWVsZHMvZm9ybS1hY3Rpb24nO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtZm9ybS1hY3Rpb24nLFxuICB0ZW1wbGF0ZVVybDogJy4vZm9ybS1hY3Rpb24uY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBGb3JtQWN0aW9uQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBGb3JtQWN0aW9uO1xuXG4gIGdldCBsYWJlbCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLml0ZW0ubG9hZGluZyA/IHRoaXMuaXRlbS5sb2FkaW5nIDogdGhpcy5pdGVtLnRpdGxlO1xuICB9XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IEhlYWRlckZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL2hlYWRlci1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1oZWFkZXItZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vaGVhZGVyLWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgSGVhZGVyRmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IEhlYWRlckZpZWxkO1xufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBPcHRpb25TZXRGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9vcHRpb24tc2V0LWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLW9wdGlvbi1zZXQtZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vb3B0aW9uLXNldC1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIE9wdGlvblNldEZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBPcHRpb25TZXRGaWVsZDxhbnk+O1xufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBUZXh0RmllbGQgfSBmcm9tICcuLi9maWVsZHMvdGV4dC1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi10ZXh0LWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3RleHQtZmllbGQuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBUZXh0RmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IFRleHRGaWVsZDtcbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGV4dGFyZWFGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy90ZXh0YXJlYS1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi10ZXh0YXJlYS1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi90ZXh0YXJlYS1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIFRleHRhcmVhRmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IFRleHRhcmVhRmllbGQ7XG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5pbXBvcnQgeyBBbmd1bGFyRm9udEF3ZXNvbWVNb2R1bGUgfSBmcm9tICdhbmd1bGFyLWZvbnQtYXdlc29tZSc7XG5pbXBvcnQgeyBOZ3hNeURhdGVQaWNrZXJNb2R1bGUgfSBmcm9tICduZ3gtbXlkYXRlcGlja2VyJztcblxuaW1wb3J0IHsgRm9ybUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLWl0ZW0uY29tcG9uZW50JztcblxuaW1wb3J0IHsgQ2hlY2tib3hGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jaGVja2JveC1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2hlY2tib3hTZXRGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jaGVja2JveC1zZXQtZmllbGQuY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRlbnRGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb250ZW50LWZpZWxkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEYXRlRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZGF0ZS1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRHJvcGRvd25GaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9kcm9wZG93bi1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmllbGRHcm91cENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9maWVsZC1ncm91cC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9ybUFjdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLWFjdGlvbi5jb21wb25lbnQnO1xuaW1wb3J0IHsgSGVhZGVyRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvaGVhZGVyLWZpZWxkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBPcHRpb25TZXRGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9vcHRpb24tc2V0LWZpZWxkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBUZXh0RmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdGV4dC1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGV4dGFyZWFGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy90ZXh0YXJlYS1maWVsZC5jb21wb25lbnQnO1xuXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBGb3Jtc01vZHVsZSxcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgIEFuZ3VsYXJGb250QXdlc29tZU1vZHVsZSxcbiAgICBOZ3hNeURhdGVQaWNrZXJNb2R1bGUuZm9yUm9vdCgpXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIEZvcm1Db21wb25lbnQsXG4gICAgRm9ybUl0ZW1Db21wb25lbnQsXG5cbiAgICBDaGVja2JveEZpZWxkQ29tcG9uZW50LFxuICAgIENoZWNrYm94U2V0RmllbGRDb21wb25lbnQsXG4gICAgQ29udGVudEZpZWxkQ29tcG9uZW50LFxuICAgIERhdGVGaWVsZENvbXBvbmVudCxcbiAgICBEcm9wZG93bkZpZWxkQ29tcG9uZW50LFxuICAgIEZpZWxkR3JvdXBDb21wb25lbnQsXG4gICAgRm9ybUFjdGlvbkNvbXBvbmVudCxcbiAgICBIZWFkZXJGaWVsZENvbXBvbmVudCxcbiAgICBPcHRpb25TZXRGaWVsZENvbXBvbmVudCxcbiAgICBUZXh0RmllbGRDb21wb25lbnQsXG4gICAgVGV4dGFyZWFGaWVsZENvbXBvbmVudFxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgRm9ybUNvbXBvbmVudCxcbiAgICBGb3JtSXRlbUNvbXBvbmVudCxcblxuICAgIENoZWNrYm94RmllbGRDb21wb25lbnQsXG4gICAgQ2hlY2tib3hTZXRGaWVsZENvbXBvbmVudCxcbiAgICBDb250ZW50RmllbGRDb21wb25lbnQsXG4gICAgRGF0ZUZpZWxkQ29tcG9uZW50LFxuICAgIERyb3Bkb3duRmllbGRDb21wb25lbnQsXG4gICAgRmllbGRHcm91cENvbXBvbmVudCxcbiAgICBGb3JtQWN0aW9uQ29tcG9uZW50LFxuICAgIEhlYWRlckZpZWxkQ29tcG9uZW50LFxuICAgIE9wdGlvblNldEZpZWxkQ29tcG9uZW50LFxuICAgIFRleHRGaWVsZENvbXBvbmVudCxcbiAgICBUZXh0YXJlYUZpZWxkQ29tcG9uZW50XG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgTmdEeW5hbWljRm9ybXNNb2R1bGUge1xuICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IE5nRHluYW1pY0Zvcm1zTW9kdWxlLFxuICAgICAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbiAgICB9O1xuICB9XG59XG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFJQSxNQUFhLFFBQVE7Ozs7SUFDbkIsWUFBb0IsR0FBZTtRQUFmLFFBQUcsR0FBSCxHQUFHLENBQVk7S0FBSzs7Ozs7SUFFeEMsT0FBTyxDQUFDLElBQVU7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLO1lBQ3JCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDckIsQ0FBQyxDQUFDO0tBQ0o7Ozs7SUFFRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLO1lBQ3JCLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNyQixDQUFDLENBQUM7S0FDSjs7OztJQUVELElBQUksTUFBTTtRQUNSLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQztLQUNqQjs7Ozs7SUFFRCxJQUFJLENBQUMsSUFBYztRQUNqQixJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNyQjs7Ozs7Ozs7SUFFRCxZQUFZLENBQUMsSUFBYyxFQUFFLE1BQWMsRUFBRSxJQUFZLEVBQUUsUUFBa0IsSUFBSTs7WUFDM0UsTUFBTSxHQUFhLElBQUk7UUFDM0IsSUFBSSxLQUFLLEtBQUssSUFBSSxFQUFFOztrQkFDWixNQUFNLEdBQTBDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQztZQUN4RixNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztZQUN2QixLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztTQUN0QjtRQUNELElBQUksTUFBTSxLQUFLLElBQUksRUFBRTtZQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDbkQ7YUFBTTtZQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3ZEO0tBQ0Y7Ozs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxJQUFjLEVBQUUsTUFBYyxFQUFFLFFBQWtCLElBQUk7UUFDckUsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0tBQ3ZEOzs7Ozs7O0lBRUQsY0FBYyxDQUFDLElBQWMsRUFBRSxNQUFjLEVBQUUsUUFBa0IsSUFBSTtRQUNuRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7S0FDckQ7Ozs7Ozs7O0lBRUQsV0FBVyxDQUFDLElBQWMsRUFBRSxLQUFhLEVBQUUsSUFBWSxFQUFFLFFBQWtCLElBQUk7O1lBQ3pFLE1BQU0sR0FBYSxJQUFJO1FBQzNCLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTs7a0JBQ1osTUFBTSxHQUEwQyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUM7WUFDdkYsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7WUFDdkIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7U0FDdEI7UUFDRCxJQUFJLE1BQU0sS0FBSyxJQUFJLEVBQUU7WUFDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUN2RDthQUFNO1lBQ0wsTUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDckQ7S0FDRjs7Ozs7Ozs7SUFFRCxlQUFlLENBQUMsSUFBYyxFQUFFLEtBQWEsRUFBRSxLQUFhLEVBQUUsUUFBa0IsSUFBSTtRQUNsRixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0tBQzlDOzs7Ozs7OztJQUVELGFBQWEsQ0FBQyxJQUFjLEVBQUUsS0FBYSxFQUFFLEtBQWEsRUFBRSxRQUFrQixJQUFJO1FBQ2hGLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7S0FDNUM7Ozs7Ozs7SUFFRCxNQUFNLENBQUMsR0FBVyxFQUFFLFFBQWtCLElBQUksRUFBRSxJQUFZOztZQUNsRCxNQUFNLEdBQWEsSUFBSTtRQUMzQixJQUFJLEtBQUssS0FBSyxJQUFJLEVBQUU7O2tCQUNaLE1BQU0sR0FBMEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDO1lBQ3JGLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO1lBQ3ZCLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxNQUFNLEtBQUssSUFBSSxFQUFFO1lBQ25CLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzdDO2FBQU07WUFDTCxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3hDO0tBQ0Y7Ozs7OztJQUVELFlBQVksQ0FBQyxJQUFZLEVBQUUsUUFBa0IsSUFBSTtRQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7S0FDbEM7Ozs7OztJQUVELFVBQVUsQ0FBQyxFQUFVLEVBQUUsUUFBa0IsSUFBSTtRQUMzQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDOUI7Ozs7OztJQUVELEtBQUssQ0FBQyxHQUFXLEVBQUUsSUFBWTtRQUM3QixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQztLQUM5Qzs7Ozs7SUFFRCxXQUFXLENBQUMsSUFBWTtRQUN0QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0tBQ2pDOzs7OztJQUVELFNBQVMsQ0FBQyxFQUFVO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDN0I7Ozs7OztJQUVELGVBQWUsQ0FBQyxHQUFXLEVBQUUsSUFBWTs7WUFDbkMsS0FBSyxHQUFRLElBQUk7O1lBQ2pCLE1BQU0sR0FBUSxJQUFJOztjQUNoQixNQUFNLEdBQVUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUM7UUFDbkUsSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNyQixLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ25CO2FBQU07O2tCQUNDLE9BQU8sR0FBVSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUM7WUFDekUsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O3NCQUNqQyxLQUFLLEdBQWdDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUM7Z0JBQ3ZGLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7b0JBQ3ZCLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO29CQUNwQixNQUFNLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNwQixNQUFNO2lCQUNQO2FBQ0Y7U0FDRjtRQUNELE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQztLQUN6Qzs7Ozs7SUFFRCxxQkFBcUIsQ0FBQyxJQUFZO1FBQ2hDLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7S0FDM0M7Ozs7O0lBRUQsbUJBQW1CLENBQUMsRUFBVTtRQUM1QixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ3ZDOzs7O0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDdkIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1NBQ3pCLENBQUMsQ0FBQztLQUNKO0NBQ0Y7Ozs7OztNQ25JWSxRQUFROzs7O0lBa0JuQixZQUFZLFVBT1IsRUFBRTtRQWJOLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixZQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsYUFBUSxHQUFHLElBQUksQ0FBQztRQUNoQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBVWYsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksRUFBRSxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQztRQUMxQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUM7S0FDbEM7Ozs7SUFFRCxPQUFPO1FBQ0wsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ2xCOzs7OztJQUVELE9BQU8sQ0FBQyxJQUFVO1FBQ2hCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNmLElBQUksQ0FBQyxFQUFFLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUN4QztRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzNCO0tBQ0Y7Ozs7SUFFSyxXQUFXOztZQUNmLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMvQjtLQUFBOzs7O0lBRUQsSUFBSSxPQUFPO1FBQ1QsT0FBTyxJQUFJLENBQUM7S0FDYjs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBSzs7WUFDUixTQUFTLEdBQUcsSUFBSTtRQUNwQixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxFQUFFO1lBQ3ZCLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztTQUN0QztRQUNELElBQUksU0FBUyxFQUFFO1lBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztTQUN6QztLQUNGOzs7OztJQUVELE1BQU0sQ0FBQyxLQUFLO1FBQ1YsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksRUFBRTtZQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztTQUN4QjtLQUNGOzs7O0lBRUQsT0FBTztLQUVOOzs7OztJQUVELFVBQVUsQ0FBQyxPQUFlO1FBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0tBQ3hCOzs7OztJQUVELGNBQWMsQ0FBQyxNQUF3QjtRQUNyQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsSUFBSSxNQUFNLENBQUMsUUFBUSxJQUFJLElBQUksSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDekQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDckM7UUFDRCxLQUFLLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7WUFDL0IsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTs7c0JBQy9CLEtBQUssR0FBYSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUM7Z0JBQ3BELElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtvQkFDakIsS0FBSyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQzFDO2FBQ0Y7U0FDRjtLQUNGOzs7O0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQztTQUMvQjtLQUNGO0NBQ0Y7Ozs7Ozs7OztBQ3pHRCxNQUFhLEtBQVMsU0FBUSxRQUFROzs7O0lBUXBDLFlBQVksVUFNUixFQUFFO1FBQ0osS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBYmpCLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFjYixJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQztRQUNqQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFDO1FBQzdDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQy9CLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDdEM7UUFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDO0tBQ3RDOzs7OztJQUVELE9BQU8sQ0FBQyxJQUFVO1FBQ2hCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEIsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0tBQ3BCOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFLO1FBQ1osSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7S0FDMUM7Ozs7SUFFSyxXQUFXOzs7WUFDZixPQUFPLHFCQUFpQixZQUFHLElBQUksQ0FBQyxPQUFPO2dCQUNyQyxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUNaLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLEVBQUU7d0JBQ3ZCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDMUI7eUJBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUNsRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDdEMsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUM5QjtpQkFDRjtnQkFDRCxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDL0IsQ0FBQyxDQUFDO1NBQ0o7S0FBQTs7Ozs7SUFFRCxZQUFZLENBQUMsS0FBVTtRQUNyQixJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDakIsT0FBTyxFQUFFLENBQUM7U0FDWDthQUFNO1lBQ0wsT0FBTyxLQUFLLENBQUM7U0FDZDtLQUNGO0NBQ0Y7Ozs7OztBQzdERCxNQUdhLGFBQWMsU0FBUSxLQUFjOzs7O0lBSS9DLFlBQVksVUFBYyxFQUFFO1FBQzFCLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUpqQixTQUFJLEdBQUcsVUFBVSxDQUFDO1FBQ2xCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFJZixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztLQUNuQztDQUNGOzs7Ozs7QUNYRDs7O0FBRUEsTUFBYSxjQUFrQixTQUFRLEtBQVE7Ozs7SUFLN0MsWUFBWSxVQUFjLEVBQUU7UUFDMUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBTGpCLFNBQUksR0FBRyxXQUFXLENBQUM7UUFDbkIsY0FBUyxHQUF5RCxFQUFFLENBQUM7UUFDckUsWUFBTyxHQUF5RCxFQUFFLENBQUM7UUFJakUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVO1lBQzdDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDN0IsQ0FBQyxDQUFDO0tBQ0o7Ozs7O0lBRUQsVUFBVSxDQUFDLE9BQWtFO1FBQzNFLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0tBQ3hCO0NBQ0Y7Ozs7OztBQ25CRDs7O0FBR0EsTUFBYSxnQkFBb0IsU0FBUSxjQUFpQjs7OztJQUd4RCxZQUFZLFVBQWMsRUFBRTtRQUMxQixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFIakIsU0FBSSxHQUFHLGFBQWEsQ0FBQztRQUluQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0tBQ3RDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFLOztjQUNOLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3BDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN6QixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDbkM7YUFBTTtZQUNMLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDakI7S0FDRjs7Ozs7SUFFRCxTQUFTLENBQUMsS0FBSzs7Y0FDUCxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QyxJQUFJLFFBQVEsSUFBSSxJQUFJLEVBQUU7WUFDcEIsT0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztTQUNyRDtRQUNELE9BQU8sS0FBSyxDQUFDO0tBQ2Q7Q0FDRjs7Ozs7O0FDM0JELE1BRWEsWUFBYSxTQUFRLFFBQVE7Ozs7SUFJeEMsWUFBWSxVQUFjLEVBQUU7UUFDMUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBSmpCLFNBQUksR0FBRyxTQUFTLENBQUM7UUFLZixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDeEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7S0FDbEM7Q0FDRjs7Ozs7O0FDWkQsTUFJYSxTQUFVLFNBQVEsS0FBVzs7OztJQUd4QyxZQUFZLFVBQWMsRUFBRTtRQUMxQixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFIakIsU0FBSSxHQUFHLE1BQU0sQ0FBQztRQUlaLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0tBQy9COzs7OztJQUVELFFBQVEsQ0FBQyxLQUFtQjtRQUMxQixLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxJQUFJLElBQUksSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0tBQ3hFO0NBQ0Y7Ozs7OztBQ2ZELE1BRWEsYUFBYyxTQUFRLGNBQXNCOzs7O0lBS3ZELFlBQVksVUFBYyxFQUFFO1FBQzFCLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUxqQixTQUFJLEdBQUcsVUFBVSxDQUFDO1FBTWhCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN6QyxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7S0FDL0M7Ozs7O0lBRUQsVUFBVSxDQUFDLE9BQWtFO1FBQzNFLEtBQUssQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO0tBQzlCO0NBQ0Y7Ozs7OztNQ2hCWSxVQUFXLFNBQVEsUUFBUTs7OztJQUl0QyxZQUFZLFVBQWMsRUFBRTtRQUMxQixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFKakIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQixTQUFJLEdBQUcsT0FBTyxDQUFDO0tBSWQ7Ozs7SUFFSyxXQUFXOzs7WUFDZixPQUFPLHFCQUFpQixZQUFHLElBQUksQ0FBQyxPQUFPO2dCQUNyQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUMzQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDakMsQ0FBQyxDQUFDO1NBQ0o7S0FBQTtDQUNGOzs7Ozs7QUNoQkQsTUFFYSxVQUFXLFNBQVEsUUFBUTs7OztJQVN0QyxZQUFZLFVBQWMsRUFBRTtRQUMxQixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFUakIsYUFBUSxHQUFHLElBQUksQ0FBQztRQUNoQixTQUFJLEdBQUcsUUFBUSxDQUFDO1FBU2QsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQztRQUNwQyxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxRQUFRLENBQUM7UUFDcEQsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0tBQ2pDOzs7OztJQUVELFdBQVcsQ0FBQyxJQUFZO1FBQ3RCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0tBQ3JCOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0tBQ3JCOzs7O0lBRUQsT0FBTztRQUNMLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsQjtLQUNGO0NBQ0Y7Ozs7OztBQ2pDRCxNQUVhLFdBQVksU0FBUSxRQUFROzs7O0lBSXZDLFlBQVksVUFBYyxFQUFFO1FBQzFCLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUpqQixTQUFJLEdBQUcsUUFBUSxDQUFDO1FBS2QsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0tBQ2pDO0NBQ0Y7Ozs7OztBQ1pELE1BTWEsZUFBZ0IsU0FBUSxVQUFVOzs7O0lBUzdDLFlBQVksVUFBYyxFQUFFO1FBQzFCLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUpqQixVQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ04sWUFBTyxHQUFHLENBQUMsQ0FBQztRQUlsQixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUM7UUFDMUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxhQUFhLEdBQUcsT0FBTyxDQUFDLGVBQWUsQ0FBQyxJQUFJLGNBQWMsQ0FBQztRQUNoRSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsT0FBTyxDQUFDLGtCQUFrQixDQUFDLElBQUksYUFBYSxDQUFDO1FBQ3JFLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEtBQUssQ0FBQztRQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BDLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksV0FBVyxDQUFDO2dCQUMvQixLQUFLLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQzthQUN4QixDQUFDLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksVUFBVSxDQUFDO2dCQUM5QixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsSUFBSSxXQUFXO2dCQUM3QixNQUFNLEVBQUU7b0JBQ04sSUFBSSxVQUFVLENBQUM7d0JBQ2IsS0FBSyxFQUFFLElBQUksQ0FBQyxhQUFhO3dCQUN6QixJQUFJLEVBQUUsTUFBTTt3QkFDWixLQUFLLEVBQUU7NEJBQ0wsSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRTs7c0NBQzNDLEdBQUcsR0FBUSxJQUFJLENBQUMsTUFBTSxFQUFFO2dDQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQ0FDcEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQzs2QkFDcEI7eUJBQ0Y7d0JBQ0QsT0FBTyxFQUFHLENBQUMsU0FBUyxDQUFDO3FCQUN0QixDQUFDO2lCQUNIO2dCQUNELE9BQU8sRUFBRSxDQUFDLG1CQUFtQixFQUFFLHVCQUF1QixDQUFDO2FBQ3hELENBQUMsQ0FBQyxDQUFDO1NBQ0w7S0FDRjs7OztJQUVELElBQUksSUFBSTtRQUNOLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRTtZQUNwQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO1NBQy9CO1FBQ0QsMkJBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFnQjtLQUNqRDs7Ozs7SUFFRCxPQUFPLENBQUMsSUFBVTtRQUNoQixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BCLElBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxJQUFJLEVBQUU7WUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHO2dCQUNwQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3BCLENBQUMsQ0FBQztTQUNKO0tBQ0Y7Ozs7O0lBRUQsY0FBYyxDQUFDLE1BQXdCO1FBQ3JDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixLQUFLLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7WUFDL0IsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTs7c0JBQy9CLEtBQUssR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxRQUFRLEdBQUcsRUFBRTs7c0JBQ3hDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7Z0JBQzVDLElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtvQkFDakIsS0FBSyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQzFDO2FBQ0Y7U0FDRjtLQUNGOzs7OztJQUVPLFFBQVEsQ0FBQyxHQUFROztjQUNqQixLQUFLLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sUUFBUSxJQUFJLENBQUMsT0FBTyxRQUFRO1FBQzdELElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUN4QixJQUFJLFVBQVUsQ0FBQztZQUNiLElBQUksRUFBRSxLQUFLO1lBQ1gsTUFBTSxFQUFFO2dCQUNOLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLFFBQVEsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUM1RCxJQUFJLFVBQVUsQ0FBQztvQkFDYixLQUFLLEVBQUUsSUFBSSxDQUFDLGdCQUFnQjtvQkFDNUIsSUFBSSxFQUFFLE9BQU87b0JBQ2IsS0FBSyxFQUFFO3dCQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztxQkFDN0M7b0JBQ0QsT0FBTyxFQUFHLENBQUMsWUFBWSxDQUFDO2lCQUN6QixDQUFDO2FBQ0g7WUFDRCxPQUFPLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRSxvQkFBb0IsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksV0FBVyxHQUFHLFlBQVksQ0FBQztTQUNoRyxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsSUFBSSxXQUFXLENBQzVCLENBQUM7UUFDRixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7S0FDaEI7Q0FDRjs7Ozs7O0FDbkdELE1BRWEsU0FBVSxTQUFRLEtBQWE7Ozs7SUFJMUMsWUFBWSxVQUFjLEVBQUU7UUFDMUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBSmpCLFNBQUksR0FBRyxNQUFNLENBQUM7UUFLWixJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO0tBQzdDO0NBQ0Y7Ozs7OztNQ1RZLFdBQVksU0FBUSxTQUFTOzs7O0lBR3hDLFlBQVksVUFBYyxFQUFFO1FBQzFCLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNmLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksSUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0tBQzVFOzs7O0lBRUssV0FBVzs7O1lBQ2YsT0FBTyxxQkFBaUIsWUFBRyxJQUFJLENBQUMsT0FBTztnQkFDckMsSUFBSSxDQUFDLE9BQU8sRUFBRTtvQkFDWixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUMzQyxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQzlCO2dCQUNELE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUMvQixDQUFDLENBQUM7U0FDSjtLQUFBOzs7O0lBRUQsTUFBTTtRQUNKLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDNUM7Ozs7O0lBRUQsWUFBWSxDQUFDLEtBQVU7O1lBQ2pCLFFBQWdCO1FBQ3BCLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQzdCLFFBQVEsc0JBQUcsS0FBSyxFQUFVLENBQUM7U0FDNUI7YUFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLFdBQVcsRUFBRTtZQUN2QyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1NBQ2Q7YUFBTTtZQUNMLFFBQVEsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNqRTtRQUNELE9BQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7S0FDekM7Q0FDRjs7Ozs7O0FDckNELE1BRWEsYUFBYyxTQUFRLEtBQWE7Ozs7SUFHOUMsWUFBWSxVQUFjLEVBQUU7UUFDMUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBSGpCLFNBQUksR0FBRyxVQUFVLENBQUM7UUFJaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0tBQ25DO0NBQ0Y7Ozs7OztBQ1ZELE1BT2EsV0FBVztJQUd0QjtRQUNFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDcEM7Ozs7SUFFRCxJQUFJLEtBQUs7UUFDUCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7S0FDdkI7Ozs7O0lBRUQsYUFBYSxDQUFDLEtBQWlCO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztTQUMvRDtLQUNGOzs7OztJQUVELGNBQWMsQ0FBQyxNQUFvQjtRQUNqQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFDbEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQixDQUFDLENBQUM7S0FDSjs7Ozs7SUFFRCxlQUFlLENBQUMsS0FBaUI7UUFDL0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQ3BDOzs7OztJQUVELGdCQUFnQixDQUFDLE1BQW9CO1FBQ25DLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSztZQUNsQixJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzdCLENBQUMsQ0FBQztLQUNKOzs7OztJQUVELGdCQUFnQixDQUFDLEtBQWlCO1FBQ2hDLE9BQU8sSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQztLQUMzQzs7O1lBcENGLFVBQVU7Ozs7Ozs7OztBQ05YLE1BR2EsSUFBSyxTQUFRLFFBQVE7Ozs7SUFNaEMsWUFBWSxVQU1SLEVBQUU7UUFDSixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFWakIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQixXQUFNLEdBQUcsSUFBSSxDQUFDO1FBVVosSUFBSSxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQztRQUN4QyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUMzQjs7OztJQUVELElBQUksTUFBTTtRQUNSLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUNiLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUM7U0FDM0M7YUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDcEIsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQztTQUN4QjthQUFNO1lBQ0wsT0FBTyxFQUFFLENBQUM7U0FDWDtLQUNGOzs7OztJQUVELE1BQU0sQ0FBQyxHQUFRO1FBQ2IsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxDQUFDO0tBQzVCOzs7Ozs7SUFFRCxXQUFXLENBQUMsR0FBVyxFQUFFLEtBQVU7UUFDakMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLENBQUM7S0FDdkI7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRTtZQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ25CO0tBQ0Y7Q0FDRjs7Ozs7Ozs7Ozs7QUM5Q0QsTUFZYSxhQUFhOzs7O0lBR3hCLFlBQW9CLFdBQXdCO1FBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0tBQUk7Ozs7SUFFaEQsUUFBUTtLQUVQOzs7O0lBRUQsSUFBSSxLQUFLO1FBQ1AsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztLQUMvQjs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0tBQ3RCOzs7WUFwQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxVQUFVO2dCQUNwQixpUkFBb0M7Z0JBQ3BDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTthQUMzQjs7OztZQVBRLFdBQVc7OzttQkFTakIsS0FBSzs7Ozs7OztBQ2JSLE1BWWEsaUJBQWlCOzs7O0lBRzVCLFlBQW9CLFdBQXdCO1FBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0tBQUk7Ozs7SUFFaEQsUUFBUTtRQUNOLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLG9CQUFDLElBQUksQ0FBQyxJQUFJLEdBQWUsQ0FBQztTQUN6RDtLQUNGOzs7O0lBRUQsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLG9CQUFDLElBQUksQ0FBQyxJQUFJLEdBQWUsQ0FBQztTQUMzRDtLQUNGOzs7O0lBRUQsSUFBSSxLQUFLO1FBQ1AsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztLQUMvQjs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBSztRQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQzNCOzs7OztJQUVELE1BQU0sQ0FBQyxLQUFLO1FBQ1YsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDekI7Ozs7SUFFRCxPQUFPO1FBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztLQUNyQjs7O1lBcENGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsOGxDQUF5QztnQkFDekMsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO2FBQzNCOzs7O1lBTlEsV0FBVzs7O21CQVFqQixLQUFLOzs7Ozs7O0FDYlIsTUFXYSxzQkFBdUIsU0FBUSxpQkFBaUI7OztZQUw1RCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsb2FBQThDO2dCQUM5QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7YUFDM0I7OzttQkFFRSxLQUFLOzs7Ozs7O0FDWlIsTUFXYSx5QkFBMEIsU0FBUSxpQkFBaUI7OztZQUwvRCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtnQkFDbEMsb3VCQUFrRDtnQkFDbEQsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO2FBQzNCOzs7bUJBRUUsS0FBSzs7Ozs7OztBQ1pSLE1BV2EscUJBQXNCLFNBQVEsaUJBQWlCOzs7WUFMM0QsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxtQkFBbUI7Z0JBQzdCLHFMQUE2QztnQkFDN0MsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO2FBQzNCOzs7bUJBRUUsS0FBSzs7Ozs7OztBQ1pSLE1BV2Esa0JBQW1CLFNBQVEsaUJBQWlCOzs7WUFMeEQsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLG00QkFBMEM7Z0JBQzFDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTthQUMzQjs7O21CQUVFLEtBQUs7Ozs7Ozs7QUNaUixNQVdhLHNCQUF1QixTQUFRLGlCQUFpQjs7O1lBTDVELFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsb0JBQW9CO2dCQUM5QiwydkJBQThDO2dCQUM5QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7YUFDM0I7OzttQkFFRSxLQUFLOzs7Ozs7O0FDWlIsTUFXYSxtQkFBb0IsU0FBUSxpQkFBaUI7OztZQUx6RCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGlCQUFpQjtnQkFDM0IsdVBBQTJDO2dCQUMzQyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7YUFDM0I7OzttQkFFRSxLQUFLOzs7Ozs7O0FDWlIsTUFXYSxtQkFBb0IsU0FBUSxpQkFBaUI7Ozs7SUFHeEQsSUFBSSxLQUFLO1FBQ1AsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztLQUNoRTs7O1lBVkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCLDBXQUEyQztnQkFDM0MsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO2FBQzNCOzs7bUJBRUUsS0FBSzs7Ozs7OztBQ1pSLE1BV2Esb0JBQXFCLFNBQVEsaUJBQWlCOzs7WUFMMUQsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLHFMQUE0QztnQkFDNUMsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO2FBQzNCOzs7bUJBRUUsS0FBSzs7Ozs7OztBQ1pSLE1BV2EsdUJBQXdCLFNBQVEsaUJBQWlCOzs7WUFMN0QsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxzQkFBc0I7Z0JBQ2hDLGdzQkFBZ0Q7Z0JBQ2hELFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTthQUMzQjs7O21CQUVFLEtBQUs7Ozs7Ozs7QUNaUixNQVdhLGtCQUFtQixTQUFRLGlCQUFpQjs7O1lBTHhELFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO2dCQUMxQixxMUJBQTBDO2dCQUMxQyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7YUFDM0I7OzttQkFFRSxLQUFLOzs7Ozs7O0FDWlIsTUFXYSxzQkFBdUIsU0FBUSxpQkFBaUI7OztZQUw1RCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIscWJBQThDO2dCQUM5QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7YUFDM0I7OzttQkFFRSxLQUFLOzs7Ozs7O0FDWlIsTUFpRWEsb0JBQW9COzs7O0lBQy9CLE9BQU8sT0FBTztRQUNaLE9BQU87WUFDTCxRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtTQUMzQixDQUFDO0tBQ0g7OztZQS9DRixRQUFRLFNBQUM7Z0JBQ1IsT0FBTyxFQUFFO29CQUNQLFlBQVk7b0JBQ1osV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLHdCQUF3QjtvQkFDeEIscUJBQXFCLENBQUMsT0FBTyxFQUFFO2lCQUNoQztnQkFDRCxZQUFZLEVBQUU7b0JBQ1osYUFBYTtvQkFDYixpQkFBaUI7b0JBRWpCLHNCQUFzQjtvQkFDdEIseUJBQXlCO29CQUN6QixxQkFBcUI7b0JBQ3JCLGtCQUFrQjtvQkFDbEIsc0JBQXNCO29CQUN0QixtQkFBbUI7b0JBQ25CLG1CQUFtQjtvQkFDbkIsb0JBQW9CO29CQUNwQix1QkFBdUI7b0JBQ3ZCLGtCQUFrQjtvQkFDbEIsc0JBQXNCO2lCQUN2QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsYUFBYTtvQkFDYixpQkFBaUI7b0JBRWpCLHNCQUFzQjtvQkFDdEIseUJBQXlCO29CQUN6QixxQkFBcUI7b0JBQ3JCLGtCQUFrQjtvQkFDbEIsc0JBQXNCO29CQUN0QixtQkFBbUI7b0JBQ25CLG1CQUFtQjtvQkFDbkIsb0JBQW9CO29CQUNwQix1QkFBdUI7b0JBQ3ZCLGtCQUFrQjtvQkFDbEIsc0JBQXNCO2lCQUN2QjthQUNGOzs7Ozs7Ozs7Ozs7Ozs7In0=