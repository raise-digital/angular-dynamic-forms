/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Field } from './field';
export class CheckboxField extends Field {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'checkbox';
        this.hasLabel = false;
        this.classes.push('ndf-checkbox');
    }
}
if (false) {
    /** @type {?} */
    CheckboxField.prototype.type;
    /** @type {?} */
    CheckboxField.prototype.hasLabel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3gtZmllbGQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL2NoZWNrYm94LWZpZWxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBR2hDLE1BQU0sT0FBTyxhQUFjLFNBQVEsS0FBYzs7OztJQUkvQyxZQUFZLFVBQWMsRUFBRTtRQUMxQixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFKakIsU0FBSSxHQUFHLFVBQVUsQ0FBQztRQUNsQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBSWYsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDcEMsQ0FBQztDQUNGOzs7SUFQQyw2QkFBa0I7O0lBQ2xCLGlDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi9maWVsZCc7XG5cblxuZXhwb3J0IGNsYXNzIENoZWNrYm94RmllbGQgZXh0ZW5kcyBGaWVsZDxib29sZWFuPiB7XG4gIHR5cGUgPSAnY2hlY2tib3gnO1xuICBoYXNMYWJlbCA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWNoZWNrYm94Jyk7XG4gIH1cbn1cbiJdfQ==