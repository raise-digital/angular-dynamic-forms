/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
export class FormService {
    constructor() {
        this.formGroup = new FormGroup({});
    }
    /**
     * @return {?}
     */
    get group() {
        return this.formGroup;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    registerField(field) {
        if (!this.group.contains(field.id)) {
            this.group.addControl(field.id, this.controlFromField(field));
        }
    }
    /**
     * @param {?} fields
     * @return {?}
     */
    registerFields(fields) {
        fields.forEach(field => {
            this.registerField(field);
        });
    }
    /**
     * @param {?} field
     * @return {?}
     */
    deregisterField(field) {
        this.group.removeControl(field.id);
    }
    /**
     * @param {?} fields
     * @return {?}
     */
    deregisterFields(fields) {
        fields.forEach(field => {
            this.deregisterField(field);
        });
    }
    /**
     * @param {?} field
     * @return {?}
     */
    controlFromField(field) {
        return new FormControl(field.value || '');
    }
}
FormService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
FormService.ctorParameters = () => [];
if (false) {
    /** @type {?} */
    FormService.prototype.formGroup;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2Zvcm0uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBTXhELE1BQU0sT0FBTyxXQUFXO0lBR3RCO1FBQ0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNyQyxDQUFDOzs7O0lBRUQsSUFBSSxLQUFLO1FBQ1AsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3hCLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLEtBQWlCO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztTQUMvRDtJQUNILENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLE1BQW9CO1FBQ2pDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDckIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM1QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLEtBQWlCO1FBQy9CLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNyQyxDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLE1BQW9CO1FBQ25DLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDckIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsS0FBaUI7UUFDaEMsT0FBTyxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQzVDLENBQUM7OztZQXBDRixVQUFVOzs7Ozs7SUFFVCxnQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgRm9ybUdyb3VwIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5pbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9maWVsZCc7XG5cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEZvcm1TZXJ2aWNlIHtcbiAgcHJpdmF0ZSBmb3JtR3JvdXA6IEZvcm1Hcm91cDtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmZvcm1Hcm91cCA9IG5ldyBGb3JtR3JvdXAoe30pO1xuICB9XG5cbiAgZ2V0IGdyb3VwKCk6IEZvcm1Hcm91cCB7XG4gICAgcmV0dXJuIHRoaXMuZm9ybUdyb3VwO1xuICB9XG5cbiAgcmVnaXN0ZXJGaWVsZChmaWVsZDogRmllbGQ8YW55Pikge1xuICAgIGlmICghdGhpcy5ncm91cC5jb250YWlucyhmaWVsZC5pZCkpIHtcbiAgICAgIHRoaXMuZ3JvdXAuYWRkQ29udHJvbChmaWVsZC5pZCwgdGhpcy5jb250cm9sRnJvbUZpZWxkKGZpZWxkKSk7XG4gICAgfVxuICB9XG5cbiAgcmVnaXN0ZXJGaWVsZHMoZmllbGRzOiBGaWVsZDxhbnk+W10pIHtcbiAgICBmaWVsZHMuZm9yRWFjaChmaWVsZCA9PiB7XG4gICAgICB0aGlzLnJlZ2lzdGVyRmllbGQoZmllbGQpO1xuICAgIH0pO1xuICB9XG5cbiAgZGVyZWdpc3RlckZpZWxkKGZpZWxkOiBGaWVsZDxhbnk+KSB7XG4gICAgdGhpcy5ncm91cC5yZW1vdmVDb250cm9sKGZpZWxkLmlkKTtcbiAgfVxuXG4gIGRlcmVnaXN0ZXJGaWVsZHMoZmllbGRzOiBGaWVsZDxhbnk+W10pIHtcbiAgICBmaWVsZHMuZm9yRWFjaChmaWVsZCA9PiB7XG4gICAgICB0aGlzLmRlcmVnaXN0ZXJGaWVsZChmaWVsZCk7XG4gICAgfSk7XG4gIH1cblxuICBjb250cm9sRnJvbUZpZWxkKGZpZWxkOiBGaWVsZDxhbnk+KTogRm9ybUNvbnRyb2wge1xuICAgIHJldHVybiBuZXcgRm9ybUNvbnRyb2woZmllbGQudmFsdWUgfHwgJycpO1xuICB9XG59XG4iXX0=