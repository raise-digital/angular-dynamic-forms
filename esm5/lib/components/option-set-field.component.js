/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { OptionSetField } from '../fields/option-set-field';
import { FormService } from '../services/form.service';
var OptionSetFieldComponent = /** @class */ (function (_super) {
    tslib_1.__extends(OptionSetFieldComponent, _super);
    function OptionSetFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    OptionSetFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-option-set-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <ul>\n        <li *ngFor=\"let option of item.options\">\n            <label>\n                <div class=\"box\"><div *ngIf=\"item.value == option.key\"></div></div>\n                <input type=\"radio\" [formControlName]=\"item.id\" [checked]=\"item.value == option.key\" [value]=\"option.key\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" />\n                {{ option.value }}\n            </label>\n        </li>\n    </ul>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    OptionSetFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return OptionSetFieldComponent;
}(FormItemComponent));
export { OptionSetFieldComponent };
if (false) {
    /** @type {?} */
    OptionSetFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3B0aW9uLXNldC1maWVsZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9vcHRpb24tc2V0LWZpZWxkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWpELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFdkQ7SUFLNkMsbURBQWlCO0lBTDlEOztJQU9BLENBQUM7O2dCQVBBLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsc0JBQXNCO29CQUNoQyxnc0JBQWdEO29CQUNoRCxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFDUiw4QkFBQztDQUFBLEFBUEQsQ0FLNkMsaUJBQWlCLEdBRTdEO1NBRlksdUJBQXVCOzs7SUFDbEMsdUNBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBPcHRpb25TZXRGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9vcHRpb24tc2V0LWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLW9wdGlvbi1zZXQtZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vb3B0aW9uLXNldC1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIE9wdGlvblNldEZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBPcHRpb25TZXRGaWVsZDxhbnk+O1xufVxuIl19