/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FormBase } from '../form-base';
export class FieldGroup extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.hasFields = true;
        this.type = 'group';
    }
    /**
     * @return {?}
     */
    deriveValue() {
        const _super = name => super[name];
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return _super("deriveValue").call(this).then(derived => {
                this.fields.updateValues();
                return Promise.resolve(derived);
            });
        });
    }
}
if (false) {
    /** @type {?} */
    FieldGroup.prototype.hasFields;
    /** @type {?} */
    FieldGroup.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtZ3JvdXAuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL2ZpZWxkLWdyb3VwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUV4QyxNQUFNLE9BQU8sVUFBVyxTQUFRLFFBQVE7Ozs7SUFJdEMsWUFBWSxVQUFjLEVBQUU7UUFDMUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBSmpCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsU0FBSSxHQUFHLE9BQU8sQ0FBQztJQUlmLENBQUM7Ozs7SUFFSyxXQUFXOzs7WUFDZixPQUFPLHFCQUFpQixZQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDeEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDM0IsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2xDLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztLQUFBO0NBQ0Y7OztJQWJDLCtCQUFpQjs7SUFDakIsMEJBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtQmFzZSB9IGZyb20gJy4uL2Zvcm0tYmFzZSc7XG5cbmV4cG9ydCBjbGFzcyBGaWVsZEdyb3VwIGV4dGVuZHMgRm9ybUJhc2Uge1xuICBoYXNGaWVsZHMgPSB0cnVlO1xuICB0eXBlID0gJ2dyb3VwJztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gIH1cblxuICBhc3luYyBkZXJpdmVWYWx1ZSgpOiBQcm9taXNlPGJvb2xlYW4+IHtcbiAgICByZXR1cm4gc3VwZXIuZGVyaXZlVmFsdWUoKS50aGVuKGRlcml2ZWQgPT4ge1xuICAgICAgdGhpcy5maWVsZHMudXBkYXRlVmFsdWVzKCk7XG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGRlcml2ZWQpO1xuICAgIH0pO1xuICB9XG59XG4iXX0=