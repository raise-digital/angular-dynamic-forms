import { OptionSetField } from './option-set-field';
export declare class CheckboxSetField<T> extends OptionSetField<T> {
    type: string;
    constructor(options?: {});
    onChange(value: any): void;
    isChecked(value: any): boolean;
}
