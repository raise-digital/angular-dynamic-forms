import { FormItemComponent } from './form-item.component';
import { DropdownField } from '../fields/dropdown-field';
export declare class DropdownFieldComponent extends FormItemComponent {
    item: DropdownField;
}
