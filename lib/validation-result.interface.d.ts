export interface ValidationResult {
    messages: string[];
    fields: {
        [id: string]: ValidationResult;
    };
}
