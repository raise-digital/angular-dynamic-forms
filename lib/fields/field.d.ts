import { Form } from '../form';
import { FormBase } from '../form-base';
export declare class Field<T> extends FormBase {
    id: string;
    hasData: boolean;
    value: T;
    label: string;
    placeholder: string;
    derive: (field: Field<T>) => Promise<boolean>;
    constructor(options?: {
        value?: T;
        label?: string;
        classes?: string[];
        placeholder?: string;
        derive?: (field: Field<T>) => Promise<boolean>;
    });
    setForm(form: Form): void;
    onChange(value: any): void;
    deriveValue(): Promise<boolean>;
    prepareValue(value: any): any;
}
