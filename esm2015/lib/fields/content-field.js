/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { FormBase } from '../form-base';
export class ContentField extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'content';
        this.content = options['content'] || '';
        this.classes.push('ndf-field');
        this.classes.push('ndf-content');
    }
}
if (false) {
    /** @type {?} */
    ContentField.prototype.type;
    /** @type {?} */
    ContentField.prototype.content;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC1maWVsZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9maWVsZHMvY29udGVudC1maWVsZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUV4QyxNQUFNLE9BQU8sWUFBYSxTQUFRLFFBQVE7Ozs7SUFJeEMsWUFBWSxVQUFjLEVBQUU7UUFDMUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBSmpCLFNBQUksR0FBRyxTQUFTLENBQUM7UUFLZixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDeEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDbkMsQ0FBQztDQUNGOzs7SUFUQyw0QkFBaUI7O0lBQ2pCLCtCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuZXhwb3J0IGNsYXNzIENvbnRlbnRGaWVsZCBleHRlbmRzIEZvcm1CYXNlIHtcbiAgdHlwZSA9ICdjb250ZW50JztcbiAgY29udGVudDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLmNvbnRlbnQgPSBvcHRpb25zWydjb250ZW50J10gfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1maWVsZCcpO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtY29udGVudCcpO1xuICB9XG59XG4iXX0=