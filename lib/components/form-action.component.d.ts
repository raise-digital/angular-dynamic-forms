import { FormItemComponent } from './form-item.component';
import { FormAction } from '../fields/form-action';
export declare class FormActionComponent extends FormItemComponent {
    item: FormAction;
    readonly label: string;
}
