/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { FormBase } from './form-base';
export class Form extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.hasFields = true;
        this.isForm = true;
        this.obj = options['obj'] || {};
        this.submit = options['submit'] || null;
        this.fields.setForm(this);
    }
    /**
     * @return {?}
     */
    get prefix() {
        if (this.form) {
            return `${this.form.prefix}${this.name}_`;
        }
        else if (this.name) {
            return `${this.name}_`;
        }
        else {
            return '';
        }
    }
    /**
     * @param {?} obj
     * @return {?}
     */
    update(obj) {
        this.obj = obj;
        this.fields.updateValues();
    }
    /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    fieldChange(key, value) {
        this.obj[key] = value;
    }
    /**
     * @return {?}
     */
    onSubmit() {
        if (this.submit !== null) {
            this.submit(this);
        }
    }
}
if (false) {
    /** @type {?} */
    Form.prototype.obj;
    /** @type {?} */
    Form.prototype.submit;
    /** @type {?} */
    Form.prototype.hasFields;
    /** @type {?} */
    Form.prototype.isForm;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9mb3JtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBR3ZDLE1BQU0sT0FBTyxJQUFLLFNBQVEsUUFBUTs7OztJQU1oQyxZQUFZLFVBTVIsRUFBRTtRQUNKLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQVZqQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLFdBQU0sR0FBRyxJQUFJLENBQUM7UUFVWixJQUFJLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFRCxJQUFJLE1BQU07UUFDUixJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDYixPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDO1NBQzNDO2FBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ3BCLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUM7U0FDeEI7YUFBTTtZQUNMLE9BQU8sRUFBRSxDQUFDO1NBQ1g7SUFDSCxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxHQUFRO1FBQ2IsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQzdCLENBQUM7Ozs7OztJQUVELFdBQVcsQ0FBQyxHQUFXLEVBQUUsS0FBVTtRQUNqQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxJQUFJLEVBQUU7WUFDeEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNuQjtJQUNILENBQUM7Q0FDRjs7O0lBMUNDLG1CQUFTOztJQUNULHNCQUE2Qjs7SUFDN0IseUJBQWlCOztJQUNqQixzQkFBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi9mb3JtLWJhc2UnO1xuXG5cbmV4cG9ydCBjbGFzcyBGb3JtIGV4dGVuZHMgRm9ybUJhc2Uge1xuICBvYmo6IGFueTtcbiAgc3VibWl0OiAoZm9ybTogRm9ybSkgPT4gdm9pZDtcbiAgaGFzRmllbGRzID0gdHJ1ZTtcbiAgaXNGb3JtID0gdHJ1ZTtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7XG4gICAgbmFtZT86IHN0cmluZyxcbiAgICBmaWVsZHM/OiBGb3JtQmFzZVtdLFxuICAgIG9iaj86IGFueSxcbiAgICBzdWJtaXQ/OiAoZm9ybTogRm9ybSkgPT4gdm9pZDtcbiAgICBjbGFzc2VzPzogc3RyaW5nW11cbiAgfSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5vYmogPSBvcHRpb25zWydvYmonXSB8fCB7fTtcbiAgICB0aGlzLnN1Ym1pdCA9IG9wdGlvbnNbJ3N1Ym1pdCddIHx8IG51bGw7XG4gICAgdGhpcy5maWVsZHMuc2V0Rm9ybSh0aGlzKTtcbiAgfVxuXG4gIGdldCBwcmVmaXgoKTogc3RyaW5nIHtcbiAgICBpZiAodGhpcy5mb3JtKSB7XG4gICAgICByZXR1cm4gYCR7dGhpcy5mb3JtLnByZWZpeH0ke3RoaXMubmFtZX1fYDtcbiAgICB9IGVsc2UgaWYgKHRoaXMubmFtZSkge1xuICAgICAgcmV0dXJuIGAke3RoaXMubmFtZX1fYDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuICcnO1xuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZShvYmo6IGFueSkge1xuICAgIHRoaXMub2JqID0gb2JqO1xuICAgIHRoaXMuZmllbGRzLnVwZGF0ZVZhbHVlcygpO1xuICB9XG5cbiAgZmllbGRDaGFuZ2Uoa2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICB0aGlzLm9ialtrZXldID0gdmFsdWU7XG4gIH1cblxuICBvblN1Ym1pdCgpIHtcbiAgICBpZiAodGhpcy5zdWJtaXQgIT09IG51bGwpIHtcbiAgICAgIHRoaXMuc3VibWl0KHRoaXMpO1xuICAgIH1cbiAgfVxufVxuIl19