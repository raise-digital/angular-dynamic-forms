/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { DropdownField } from '../fields/dropdown-field';
import { FormService } from '../services/form.service';
export class DropdownFieldComponent extends FormItemComponent {
}
DropdownFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-dropdown-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <select [id]=\"item.id\" [formControlName]=\"item.id\" [value]=\"item.value ? item.value : ''\" (ngModelChange)=\"onChange($event)\">\n        <option *ngIf=\"item.emptyText && item.options.length < 1\" value=\"\">{{ item.emptyText }}</option>\n        <option *ngIf=\"item.placeholder && item.options.length > 0\" value=\"\">{{ item.placeholder }}</option>\n        <option *ngFor=\"let option of item.options\" [value]=\"option.key\" [disabled]=\"option.disabled\">{{ option.value }}</option>\n    </select>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
DropdownFieldComponent.propDecorators = {
    item: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    DropdownFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tZmllbGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZHJvcGRvd24tZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVqRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDekQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBT3ZELE1BQU0sT0FBTyxzQkFBdUIsU0FBUSxpQkFBaUI7OztZQUw1RCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsMnZCQUE4QztnQkFDOUMsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO2FBQzNCOzs7bUJBRUUsS0FBSzs7OztJQUFOLHNDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRHJvcGRvd25GaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9kcm9wZG93bi1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1kcm9wZG93bi1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9kcm9wZG93bi1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIERyb3Bkb3duRmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IERyb3Bkb3duRmllbGQ7XG59XG4iXX0=