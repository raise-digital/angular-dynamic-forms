/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Field } from './field';
export class TextareaField extends Field {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'textarea';
        this.value = options['value'] || '';
        this.classes.push('ndf-textarea');
    }
}
if (false) {
    /** @type {?} */
    TextareaField.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dGFyZWEtZmllbGQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL3RleHRhcmVhLWZpZWxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRWhDLE1BQU0sT0FBTyxhQUFjLFNBQVEsS0FBYTs7OztJQUc5QyxZQUFZLFVBQWMsRUFBRTtRQUMxQixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFIakIsU0FBSSxHQUFHLFVBQVUsQ0FBQztRQUloQixJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDcEMsQ0FBQztDQUNGOzs7SUFQQyw2QkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4vZmllbGQnO1xuXG5leHBvcnQgY2xhc3MgVGV4dGFyZWFGaWVsZCBleHRlbmRzIEZpZWxkPHN0cmluZz4ge1xuICB0eXBlID0gJ3RleHRhcmVhJztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy52YWx1ZSA9IG9wdGlvbnNbJ3ZhbHVlJ10gfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi10ZXh0YXJlYScpO1xuICB9XG59XG4iXX0=