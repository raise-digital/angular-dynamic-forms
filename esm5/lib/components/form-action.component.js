/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { FormAction } from '../fields/form-action';
import { FormService } from '../services/form.service';
var FormActionComponent = /** @class */ (function (_super) {
    tslib_1.__extends(FormActionComponent, _super);
    function FormActionComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(FormActionComponent.prototype, "label", {
        get: /**
         * @return {?}
         */
        function () {
            return this.item.loading ? this.item.loading : this.item.title;
        },
        enumerable: true,
        configurable: true
    });
    FormActionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-form-action',
                    template: "<button [attr.disabled]=\"item.loading != null ? '' : null\" [ngClass]=\"item.classes\" [type]=\"item.actionType\" (click)=\"onClick()\" [title]=\"item.title\">\n    <fa *ngIf=\"item.loading\" name=\"spinner\" size=\"lt\" animation=\"spin\"></fa><fa *ngIf=\"!item.loading && item.icon\" [name]=\"item.icon\" size=\"lt\"></fa> {{ label }}\n</button>\n",
                    providers: [FormService]
                }] }
    ];
    FormActionComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return FormActionComponent;
}(FormItemComponent));
export { FormActionComponent };
if (false) {
    /** @type {?} */
    FormActionComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1hY3Rpb24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZm9ybS1hY3Rpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFakQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDMUQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ25ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUV2RDtJQUt5QywrQ0FBaUI7SUFMMUQ7O0lBV0EsQ0FBQztJQUhDLHNCQUFJLHNDQUFLOzs7O1FBQVQ7WUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDakUsQ0FBQzs7O09BQUE7O2dCQVZGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQiwwV0FBMkM7b0JBQzNDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtpQkFDM0I7Ozt1QkFFRSxLQUFLOztJQUtSLDBCQUFDO0NBQUEsQUFYRCxDQUt5QyxpQkFBaUIsR0FNekQ7U0FOWSxtQkFBbUI7OztJQUM5QixtQ0FBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IEZvcm1BY3Rpb24gfSBmcm9tICcuLi9maWVsZHMvZm9ybS1hY3Rpb24nO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtZm9ybS1hY3Rpb24nLFxuICB0ZW1wbGF0ZVVybDogJy4vZm9ybS1hY3Rpb24uY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBGb3JtQWN0aW9uQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBGb3JtQWN0aW9uO1xuXG4gIGdldCBsYWJlbCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLml0ZW0ubG9hZGluZyA/IHRoaXMuaXRlbS5sb2FkaW5nIDogdGhpcy5pdGVtLnRpdGxlO1xuICB9XG59XG4iXX0=