/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { TextField } from './text-field';
var NumberField = /** @class */ (function (_super) {
    tslib_1.__extends(NumberField, _super);
    function NumberField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.value = options['value'] || 0;
        _this.precision = (options['precision'] != null) ? options['precision'] : 2;
        return _this;
    }
    /**
     * @return {?}
     */
    NumberField.prototype.deriveValue = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, _super.prototype.deriveValue.call(this).then(function (derived) {
                        if (!derived) {
                            _this.value = _this.prepareValue(_this.value);
                            return Promise.resolve(true);
                        }
                        return Promise.resolve(false);
                    })];
            });
        });
    };
    /**
     * @return {?}
     */
    NumberField.prototype.onBlur = /**
     * @return {?}
     */
    function () {
        this.value = this.prepareValue(this.value);
    };
    /**
     * @param {?} value
     * @return {?}
     */
    NumberField.prototype.prepareValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        /** @type {?} */
        var prepared;
        if (typeof value === 'number') {
            prepared = (/** @type {?} */ (value));
        }
        else if (typeof value === 'undefined') {
            prepared = 0;
        }
        else {
            prepared = parseFloat(value.toString().replace(/[^0-9.]/g, ''));
        }
        return prepared.toFixed(this.precision);
    };
    return NumberField;
}(TextField));
export { NumberField };
if (false) {
    /** @type {?} */
    NumberField.prototype.precision;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtYmVyLWZpZWxkLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2ZpZWxkcy9udW1iZXItZmllbGQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBR3pDO0lBQWlDLHVDQUFTO0lBR3hDLHFCQUFZLE9BQWdCO1FBQWhCLHdCQUFBLEVBQUEsWUFBZ0I7UUFBNUIsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FHZjtRQUZDLEtBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxLQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7SUFDN0UsQ0FBQzs7OztJQUVLLGlDQUFXOzs7SUFBakI7Ozs7Z0JBQ0Usc0JBQU8saUJBQU0sV0FBVyxXQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTzt3QkFDckMsSUFBSSxDQUFDLE9BQU8sRUFBRTs0QkFDWixLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUMzQyxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7eUJBQzlCO3dCQUNELE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDaEMsQ0FBQyxDQUFDLEVBQUM7OztLQUNKOzs7O0lBRUQsNEJBQU07OztJQUFOO1FBQ0UsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7OztJQUVELGtDQUFZOzs7O0lBQVosVUFBYSxLQUFVOztZQUNqQixRQUFnQjtRQUNwQixJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtZQUM3QixRQUFRLEdBQUcsbUJBQUEsS0FBSyxFQUFVLENBQUM7U0FDNUI7YUFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLFdBQVcsRUFBRTtZQUN2QyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1NBQ2Q7YUFBTTtZQUNMLFFBQVEsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNqRTtRQUNELE9BQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUNILGtCQUFDO0FBQUQsQ0FBQyxBQWxDRCxDQUFpQyxTQUFTLEdBa0N6Qzs7OztJQWpDQyxnQ0FBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUZXh0RmllbGQgfSBmcm9tICcuL3RleHQtZmllbGQnO1xuXG5cbmV4cG9ydCBjbGFzcyBOdW1iZXJGaWVsZCBleHRlbmRzIFRleHRGaWVsZCB7XG4gIHByZWNpc2lvbjogbnVtYmVyO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLnZhbHVlID0gb3B0aW9uc1sndmFsdWUnXSB8fCAwO1xuICAgIHRoaXMucHJlY2lzaW9uID0gKG9wdGlvbnNbJ3ByZWNpc2lvbiddICE9IG51bGwpID8gb3B0aW9uc1sncHJlY2lzaW9uJ10gOiAyO1xuICB9XG5cbiAgYXN5bmMgZGVyaXZlVmFsdWUoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgcmV0dXJuIHN1cGVyLmRlcml2ZVZhbHVlKCkudGhlbihkZXJpdmVkID0+IHtcbiAgICAgIGlmICghZGVyaXZlZCkge1xuICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5wcmVwYXJlVmFsdWUodGhpcy52YWx1ZSk7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUodHJ1ZSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcbiAgICB9KTtcbiAgfVxuXG4gIG9uQmx1cigpOiB2b2lkIHtcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5wcmVwYXJlVmFsdWUodGhpcy52YWx1ZSk7XG4gIH1cblxuICBwcmVwYXJlVmFsdWUodmFsdWU6IGFueSk6IGFueSB7XG4gICAgbGV0IHByZXBhcmVkOiBudW1iZXI7XG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ251bWJlcicpIHtcbiAgICAgIHByZXBhcmVkID0gdmFsdWUgYXMgbnVtYmVyO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlID09PSAndW5kZWZpbmVkJykge1xuICAgICAgcHJlcGFyZWQgPSAwO1xuICAgIH0gZWxzZSB7XG4gICAgICBwcmVwYXJlZCA9IHBhcnNlRmxvYXQodmFsdWUudG9TdHJpbmcoKS5yZXBsYWNlKC9bXjAtOS5dL2csICcnKSk7XG4gICAgfVxuICAgIHJldHVybiBwcmVwYXJlZC50b0ZpeGVkKHRoaXMucHJlY2lzaW9uKTtcbiAgfVxufVxuIl19