import { FormItemComponent } from './form-item.component';
import { OptionSetField } from '../fields/option-set-field';
export declare class OptionSetFieldComponent extends FormItemComponent {
    item: OptionSetField<any>;
}
