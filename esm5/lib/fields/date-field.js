/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Field } from './field';
var DateField = /** @class */ (function (_super) {
    tslib_1.__extends(DateField, _super);
    function DateField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'date';
        _this.classes.push('ndf-date');
        return _this;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    DateField.prototype.onChange = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        _super.prototype.onChange.call(this, (value.jsdate == null) ? null : new Date(value.jsdate));
    };
    return DateField;
}(Field));
export { DateField };
if (false) {
    /** @type {?} */
    DateField.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1maWVsZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9maWVsZHMvZGF0ZS1maWVsZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFJaEM7SUFBK0IscUNBQVc7SUFHeEMsbUJBQVksT0FBZ0I7UUFBaEIsd0JBQUEsRUFBQSxZQUFnQjtRQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUVmO1FBTEQsVUFBSSxHQUFHLE1BQU0sQ0FBQztRQUlaLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOztJQUNoQyxDQUFDOzs7OztJQUVELDRCQUFROzs7O0lBQVIsVUFBUyxLQUFtQjtRQUMxQixpQkFBTSxRQUFRLFlBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFDSCxnQkFBQztBQUFELENBQUMsQUFYRCxDQUErQixLQUFLLEdBV25DOzs7O0lBVkMseUJBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4vZmllbGQnO1xuXG5pbXBvcnQgeyBJTXlEYXRlTW9kZWwgfSBmcm9tICduZ3gtbXlkYXRlcGlja2VyJztcblxuZXhwb3J0IGNsYXNzIERhdGVGaWVsZCBleHRlbmRzIEZpZWxkPERhdGU+IHtcbiAgdHlwZSA9ICdkYXRlJztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1kYXRlJyk7XG4gIH1cblxuICBvbkNoYW5nZSh2YWx1ZTogSU15RGF0ZU1vZGVsKSB7XG4gICAgc3VwZXIub25DaGFuZ2UoKHZhbHVlLmpzZGF0ZSA9PSBudWxsKSA/IG51bGwgOiBuZXcgRGF0ZSh2YWx1ZS5qc2RhdGUpKTtcbiAgfVxufVxuIl19