/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FormBase } from '../form-base';
var ContentField = /** @class */ (function (_super) {
    tslib_1.__extends(ContentField, _super);
    function ContentField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'content';
        _this.content = options['content'] || '';
        _this.classes.push('ndf-field');
        _this.classes.push('ndf-content');
        return _this;
    }
    return ContentField;
}(FormBase));
export { ContentField };
if (false) {
    /** @type {?} */
    ContentField.prototype.type;
    /** @type {?} */
    ContentField.prototype.content;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC1maWVsZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9maWVsZHMvY29udGVudC1maWVsZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFeEM7SUFBa0Msd0NBQVE7SUFJeEMsc0JBQVksT0FBZ0I7UUFBaEIsd0JBQUEsRUFBQSxZQUFnQjtRQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUlmO1FBUkQsVUFBSSxHQUFHLFNBQVMsQ0FBQztRQUtmLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN4QyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMvQixLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzs7SUFDbkMsQ0FBQztJQUNILG1CQUFDO0FBQUQsQ0FBQyxBQVZELENBQWtDLFFBQVEsR0FVekM7Ozs7SUFUQyw0QkFBaUI7O0lBQ2pCLCtCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuZXhwb3J0IGNsYXNzIENvbnRlbnRGaWVsZCBleHRlbmRzIEZvcm1CYXNlIHtcbiAgdHlwZSA9ICdjb250ZW50JztcbiAgY29udGVudDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLmNvbnRlbnQgPSBvcHRpb25zWydjb250ZW50J10gfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1maWVsZCcpO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtY29udGVudCcpO1xuICB9XG59XG4iXX0=