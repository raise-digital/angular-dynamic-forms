/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FormBase } from './form-base';
var Form = /** @class */ (function (_super) {
    tslib_1.__extends(Form, _super);
    function Form(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.hasFields = true;
        _this.isForm = true;
        _this.obj = options['obj'] || {};
        _this.submit = options['submit'] || null;
        _this.fields.setForm(_this);
        return _this;
    }
    Object.defineProperty(Form.prototype, "prefix", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.form) {
                return "" + this.form.prefix + this.name + "_";
            }
            else if (this.name) {
                return this.name + "_";
            }
            else {
                return '';
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} obj
     * @return {?}
     */
    Form.prototype.update = /**
     * @param {?} obj
     * @return {?}
     */
    function (obj) {
        this.obj = obj;
        this.fields.updateValues();
    };
    /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    Form.prototype.fieldChange = /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    function (key, value) {
        this.obj[key] = value;
    };
    /**
     * @return {?}
     */
    Form.prototype.onSubmit = /**
     * @return {?}
     */
    function () {
        if (this.submit !== null) {
            this.submit(this);
        }
    };
    return Form;
}(FormBase));
export { Form };
if (false) {
    /** @type {?} */
    Form.prototype.obj;
    /** @type {?} */
    Form.prototype.submit;
    /** @type {?} */
    Form.prototype.hasFields;
    /** @type {?} */
    Form.prototype.isForm;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9mb3JtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUd2QztJQUEwQixnQ0FBUTtJQU1oQyxjQUFZLE9BTU47UUFOTSx3QkFBQSxFQUFBLFlBTU47UUFOTixZQU9FLGtCQUFNLE9BQU8sQ0FBQyxTQUlmO1FBZEQsZUFBUyxHQUFHLElBQUksQ0FBQztRQUNqQixZQUFNLEdBQUcsSUFBSSxDQUFDO1FBVVosS0FBSSxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hDLEtBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQztRQUN4QyxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsQ0FBQzs7SUFDNUIsQ0FBQztJQUVELHNCQUFJLHdCQUFNOzs7O1FBQVY7WUFDRSxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2IsT0FBTyxLQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLE1BQUcsQ0FBQzthQUMzQztpQkFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ3BCLE9BQVUsSUFBSSxDQUFDLElBQUksTUFBRyxDQUFDO2FBQ3hCO2lCQUFNO2dCQUNMLE9BQU8sRUFBRSxDQUFDO2FBQ1g7UUFDSCxDQUFDOzs7T0FBQTs7Ozs7SUFFRCxxQkFBTTs7OztJQUFOLFVBQU8sR0FBUTtRQUNiLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUM3QixDQUFDOzs7Ozs7SUFFRCwwQkFBVzs7Ozs7SUFBWCxVQUFZLEdBQVcsRUFBRSxLQUFVO1FBQ2pDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7Ozs7SUFFRCx1QkFBUTs7O0lBQVI7UUFDRSxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssSUFBSSxFQUFFO1lBQ3hCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbkI7SUFDSCxDQUFDO0lBQ0gsV0FBQztBQUFELENBQUMsQUEzQ0QsQ0FBMEIsUUFBUSxHQTJDakM7Ozs7SUExQ0MsbUJBQVM7O0lBQ1Qsc0JBQTZCOztJQUM3Qix5QkFBaUI7O0lBQ2pCLHNCQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRm9ybUJhc2UgfSBmcm9tICcuL2Zvcm0tYmFzZSc7XG5cblxuZXhwb3J0IGNsYXNzIEZvcm0gZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIG9iajogYW55O1xuICBzdWJtaXQ6IChmb3JtOiBGb3JtKSA9PiB2b2lkO1xuICBoYXNGaWVsZHMgPSB0cnVlO1xuICBpc0Zvcm0gPSB0cnVlO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHtcbiAgICBuYW1lPzogc3RyaW5nLFxuICAgIGZpZWxkcz86IEZvcm1CYXNlW10sXG4gICAgb2JqPzogYW55LFxuICAgIHN1Ym1pdD86IChmb3JtOiBGb3JtKSA9PiB2b2lkO1xuICAgIGNsYXNzZXM/OiBzdHJpbmdbXVxuICB9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLm9iaiA9IG9wdGlvbnNbJ29iaiddIHx8IHt9O1xuICAgIHRoaXMuc3VibWl0ID0gb3B0aW9uc1snc3VibWl0J10gfHwgbnVsbDtcbiAgICB0aGlzLmZpZWxkcy5zZXRGb3JtKHRoaXMpO1xuICB9XG5cbiAgZ2V0IHByZWZpeCgpOiBzdHJpbmcge1xuICAgIGlmICh0aGlzLmZvcm0pIHtcbiAgICAgIHJldHVybiBgJHt0aGlzLmZvcm0ucHJlZml4fSR7dGhpcy5uYW1lfV9gO1xuICAgIH0gZWxzZSBpZiAodGhpcy5uYW1lKSB7XG4gICAgICByZXR1cm4gYCR7dGhpcy5uYW1lfV9gO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gJyc7XG4gICAgfVxuICB9XG5cbiAgdXBkYXRlKG9iajogYW55KSB7XG4gICAgdGhpcy5vYmogPSBvYmo7XG4gICAgdGhpcy5maWVsZHMudXBkYXRlVmFsdWVzKCk7XG4gIH1cblxuICBmaWVsZENoYW5nZShrZXk6IHN0cmluZywgdmFsdWU6IGFueSk6IHZvaWQge1xuICAgIHRoaXMub2JqW2tleV0gPSB2YWx1ZTtcbiAgfVxuXG4gIG9uU3VibWl0KCkge1xuICAgIGlmICh0aGlzLnN1Ym1pdCAhPT0gbnVsbCkge1xuICAgICAgdGhpcy5zdWJtaXQodGhpcyk7XG4gICAgfVxuICB9XG59XG4iXX0=