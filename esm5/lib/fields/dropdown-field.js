/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { OptionSetField } from './option-set-field';
var DropdownField = /** @class */ (function (_super) {
    tslib_1.__extends(DropdownField, _super);
    function DropdownField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'dropdown';
        _this.classes.push('ndf-dropdown');
        _this.loaded = options['emptyText'] || '';
        _this.emptyText = options['loadingText'] || '';
        return _this;
    }
    /**
     * @param {?} options
     * @return {?}
     */
    DropdownField.prototype.setOptions = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        _super.prototype.setOptions.call(this, options);
        this.emptyText = this.loaded;
    };
    return DropdownField;
}(OptionSetField));
export { DropdownField };
if (false) {
    /** @type {?} */
    DropdownField.prototype.type;
    /** @type {?} */
    DropdownField.prototype.emptyText;
    /** @type {?} */
    DropdownField.prototype.loaded;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tZmllbGQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL2Ryb3Bkb3duLWZpZWxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBEO0lBQW1DLHlDQUFzQjtJQUt2RCx1QkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBSWY7UUFURCxVQUFJLEdBQUcsVUFBVSxDQUFDO1FBTWhCLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2xDLEtBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN6QyxLQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7O0lBQ2hELENBQUM7Ozs7O0lBRUQsa0NBQVU7Ozs7SUFBVixVQUFXLE9BQWtFO1FBQzNFLGlCQUFNLFVBQVUsWUFBQyxPQUFPLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDL0IsQ0FBQztJQUNILG9CQUFDO0FBQUQsQ0FBQyxBQWhCRCxDQUFtQyxjQUFjLEdBZ0JoRDs7OztJQWZDLDZCQUFrQjs7SUFDbEIsa0NBQWtCOztJQUNsQiwrQkFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9wdGlvblNldEZpZWxkIH0gZnJvbSAnLi9vcHRpb24tc2V0LWZpZWxkJztcblxuZXhwb3J0IGNsYXNzIERyb3Bkb3duRmllbGQgZXh0ZW5kcyBPcHRpb25TZXRGaWVsZDxzdHJpbmc+IHtcbiAgdHlwZSA9ICdkcm9wZG93bic7XG4gIGVtcHR5VGV4dDogc3RyaW5nO1xuICBsb2FkZWQ6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1kcm9wZG93bicpO1xuICAgIHRoaXMubG9hZGVkID0gb3B0aW9uc1snZW1wdHlUZXh0J10gfHwgJyc7XG4gICAgdGhpcy5lbXB0eVRleHQgPSBvcHRpb25zWydsb2FkaW5nVGV4dCddIHx8ICcnO1xuICB9XG5cbiAgc2V0T3B0aW9ucyhvcHRpb25zOiBBcnJheTx7IGtleTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nLCBkaXNhYmxlZD86IGJvb2xlYW4gfT4pIHtcbiAgICBzdXBlci5zZXRPcHRpb25zKG9wdGlvbnMpO1xuICAgIHRoaXMuZW1wdHlUZXh0ID0gdGhpcy5sb2FkZWQ7XG4gIH1cbn1cbiJdfQ==