/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FormBase } from '../form-base';
var FormAction = /** @class */ (function (_super) {
    tslib_1.__extends(FormAction, _super);
    function FormAction(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.isAction = true;
        _this.type = 'action';
        _this.title = options['title'] || null;
        _this.icon = options['icon'] || null;
        _this.actionType = options['actionType'] || 'submit';
        _this.click = options['click'] || null;
        _this.classes.push('ndf-action');
        return _this;
    }
    /**
     * @param {?} text
     * @return {?}
     */
    FormAction.prototype.showLoading = /**
     * @param {?} text
     * @return {?}
     */
    function (text) {
        this.loading = text;
    };
    /**
     * @return {?}
     */
    FormAction.prototype.clearLoading = /**
     * @return {?}
     */
    function () {
        this.loading = null;
    };
    /**
     * @return {?}
     */
    FormAction.prototype.onClick = /**
     * @return {?}
     */
    function () {
        if (this.click != null) {
            this.click(this);
        }
    };
    return FormAction;
}(FormBase));
export { FormAction };
if (false) {
    /** @type {?} */
    FormAction.prototype.isAction;
    /** @type {?} */
    FormAction.prototype.type;
    /** @type {?} */
    FormAction.prototype.title;
    /** @type {?} */
    FormAction.prototype.icon;
    /** @type {?} */
    FormAction.prototype.actionType;
    /** @type {?} */
    FormAction.prototype.loading;
    /** @type {?} */
    FormAction.prototype.click;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1hY3Rpb24uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL2Zvcm0tYWN0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUV4QztJQUFnQyxzQ0FBUTtJQVN0QyxvQkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBTWY7UUFmRCxjQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFVBQUksR0FBRyxRQUFRLENBQUM7UUFTZCxLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUM7UUFDdEMsS0FBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDO1FBQ3BDLEtBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLFFBQVEsQ0FBQztRQUNwRCxLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUM7UUFDdEMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7O0lBQ2xDLENBQUM7Ozs7O0lBRUQsZ0NBQVc7Ozs7SUFBWCxVQUFZLElBQVk7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDdEIsQ0FBQzs7OztJQUVELGlDQUFZOzs7SUFBWjtRQUNFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0lBQ3RCLENBQUM7Ozs7SUFFRCw0QkFBTzs7O0lBQVA7UUFDRSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbEI7SUFDSCxDQUFDO0lBQ0gsaUJBQUM7QUFBRCxDQUFDLEFBL0JELENBQWdDLFFBQVEsR0ErQnZDOzs7O0lBOUJDLDhCQUFnQjs7SUFDaEIsMEJBQWdCOztJQUNoQiwyQkFBYzs7SUFDZCwwQkFBYTs7SUFDYixnQ0FBbUI7O0lBQ25CLDZCQUFnQjs7SUFDaEIsMkJBQW9DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRm9ybUJhc2UgfSBmcm9tICcuLi9mb3JtLWJhc2UnO1xuXG5leHBvcnQgY2xhc3MgRm9ybUFjdGlvbiBleHRlbmRzIEZvcm1CYXNlIHtcbiAgaXNBY3Rpb24gPSB0cnVlO1xuICB0eXBlID0gJ2FjdGlvbic7XG4gIHRpdGxlOiBzdHJpbmc7XG4gIGljb246IHN0cmluZztcbiAgYWN0aW9uVHlwZTogc3RyaW5nO1xuICBsb2FkaW5nOiBzdHJpbmc7XG4gIGNsaWNrOiAoYWN0aW9uOiBGb3JtQWN0aW9uKSA9PiB2b2lkO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLnRpdGxlID0gb3B0aW9uc1sndGl0bGUnXSB8fCBudWxsO1xuICAgIHRoaXMuaWNvbiA9IG9wdGlvbnNbJ2ljb24nXSB8fCBudWxsO1xuICAgIHRoaXMuYWN0aW9uVHlwZSA9IG9wdGlvbnNbJ2FjdGlvblR5cGUnXSB8fCAnc3VibWl0JztcbiAgICB0aGlzLmNsaWNrID0gb3B0aW9uc1snY2xpY2snXSB8fCBudWxsO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtYWN0aW9uJyk7XG4gIH1cblxuICBzaG93TG9hZGluZyh0ZXh0OiBzdHJpbmcpIHtcbiAgICB0aGlzLmxvYWRpbmcgPSB0ZXh0O1xuICB9XG5cbiAgY2xlYXJMb2FkaW5nKCkge1xuICAgIHRoaXMubG9hZGluZyA9IG51bGw7XG4gIH1cblxuICBvbkNsaWNrKCkge1xuICAgIGlmICh0aGlzLmNsaWNrICE9IG51bGwpIHtcbiAgICAgIHRoaXMuY2xpY2sodGhpcyk7XG4gICAgfVxuICB9XG59XG4iXX0=