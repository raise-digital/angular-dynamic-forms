/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Field } from './field';
/**
 * @template T
 */
var /**
 * @template T
 */
OptionSetField = /** @class */ (function (_super) {
    tslib_1.__extends(OptionSetField, _super);
    function OptionSetField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'optionset';
        _this.optionSet = [];
        _this.options = [];
        _this.classes.push('ndf-optionset');
        _this.optionSet = options['options'] || [];
        Promise.resolve(_this.optionSet).then(function (optionList) {
            _this.setOptions(optionList);
        });
        return _this;
    }
    /**
     * @param {?} options
     * @return {?}
     */
    OptionSetField.prototype.setOptions = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        this.options = options;
    };
    return OptionSetField;
}(Field));
/**
 * @template T
 */
export { OptionSetField };
if (false) {
    /** @type {?} */
    OptionSetField.prototype.type;
    /** @type {?} */
    OptionSetField.prototype.optionSet;
    /** @type {?} */
    OptionSetField.prototype.options;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3B0aW9uLXNldC1maWVsZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9maWVsZHMvb3B0aW9uLXNldC1maWVsZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxTQUFTLENBQUM7Ozs7QUFFaEM7Ozs7SUFBdUMsMENBQVE7SUFLN0Msd0JBQVksT0FBZ0I7UUFBaEIsd0JBQUEsRUFBQSxZQUFnQjtRQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQU1mO1FBWEQsVUFBSSxHQUFHLFdBQVcsQ0FBQztRQUNuQixlQUFTLEdBQXlELEVBQUUsQ0FBQztRQUNyRSxhQUFPLEdBQXlELEVBQUUsQ0FBQztRQUlqRSxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUNuQyxLQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsVUFBVTtZQUM3QyxLQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDOztJQUNMLENBQUM7Ozs7O0lBRUQsbUNBQVU7Ozs7SUFBVixVQUFXLE9BQWtFO1FBQzNFLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0lBQ3pCLENBQUM7SUFDSCxxQkFBQztBQUFELENBQUMsQUFqQkQsQ0FBdUMsS0FBSyxHQWlCM0M7Ozs7Ozs7SUFoQkMsOEJBQW1COztJQUNuQixtQ0FBcUU7O0lBQ3JFLGlDQUFtRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi9maWVsZCc7XG5cbmV4cG9ydCBjbGFzcyBPcHRpb25TZXRGaWVsZDxUPiBleHRlbmRzIEZpZWxkPFQ+IHtcbiAgdHlwZSA9ICdvcHRpb25zZXQnO1xuICBvcHRpb25TZXQ6IHsga2V5OiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcsIGRpc2FibGVkPzogYm9vbGVhbiB9W10gPSBbXTtcbiAgb3B0aW9uczogeyBrZXk6IHN0cmluZywgdmFsdWU6IHN0cmluZywgZGlzYWJsZWQ/OiBib29sZWFuIH1bXSA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLW9wdGlvbnNldCcpO1xuICAgIHRoaXMub3B0aW9uU2V0ID0gb3B0aW9uc1snb3B0aW9ucyddIHx8IFtdO1xuICAgIFByb21pc2UucmVzb2x2ZSh0aGlzLm9wdGlvblNldCkudGhlbihvcHRpb25MaXN0ID0+IHtcbiAgICAgIHRoaXMuc2V0T3B0aW9ucyhvcHRpb25MaXN0KTtcbiAgICB9KTtcbiAgfVxuXG4gIHNldE9wdGlvbnMob3B0aW9uczogQXJyYXk8eyBrZXk6IHN0cmluZywgdmFsdWU6IHN0cmluZywgZGlzYWJsZWQ/OiBib29sZWFuIH0+KSB7XG4gICAgdGhpcy5vcHRpb25zID0gb3B0aW9ucztcbiAgfVxufVxuIl19