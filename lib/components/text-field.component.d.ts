import { FormItemComponent } from './form-item.component';
import { TextField } from '../fields/text-field';
export declare class TextFieldComponent extends FormItemComponent {
    item: TextField;
}
