import { Field } from './field';
export declare class TextareaField extends Field<string> {
    type: string;
    constructor(options?: {});
}
