/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { OptionSetField } from './option-set-field';
/**
 * @template T
 */
export class CheckboxSetField extends OptionSetField {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'checkboxset';
        this.classes.push('ndf-checkboxset');
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onChange(value) {
        /** @type {?} */
        const obj = this.form.obj[this.name];
        if (this.isChecked(value)) {
            obj.splice(obj.indexOf(value), 1);
        }
        else {
            obj.push(value);
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    isChecked(value) {
        /** @type {?} */
        const selected = this.form.obj[this.name];
        if (selected != null) {
            return selected.filter(o => o === value).length > 0;
        }
        return false;
    }
}
if (false) {
    /** @type {?} */
    CheckboxSetField.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3gtc2V0LWZpZWxkLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2ZpZWxkcy9jaGVja2JveC1zZXQtZmllbGQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQzs7OztBQUdwRCxNQUFNLE9BQU8sZ0JBQW9CLFNBQVEsY0FBaUI7Ozs7SUFHeEQsWUFBWSxVQUFjLEVBQUU7UUFDMUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBSGpCLFNBQUksR0FBRyxhQUFhLENBQUM7UUFJbkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFLOztjQUNOLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3BDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN6QixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDbkM7YUFBTTtZQUNMLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDakI7SUFDSCxDQUFDOzs7OztJQUVELFNBQVMsQ0FBQyxLQUFLOztjQUNQLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3pDLElBQUksUUFBUSxJQUFJLElBQUksRUFBRTtZQUNwQixPQUFPLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztTQUNyRDtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztDQUNGOzs7SUF2QkMsZ0NBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT3B0aW9uU2V0RmllbGQgfSBmcm9tICcuL29wdGlvbi1zZXQtZmllbGQnO1xuXG5cbmV4cG9ydCBjbGFzcyBDaGVja2JveFNldEZpZWxkPFQ+IGV4dGVuZHMgT3B0aW9uU2V0RmllbGQ8VD4ge1xuICB0eXBlID0gJ2NoZWNrYm94c2V0JztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1jaGVja2JveHNldCcpO1xuICB9XG5cbiAgb25DaGFuZ2UodmFsdWUpIHtcbiAgICBjb25zdCBvYmogPSB0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV07XG4gICAgaWYgKHRoaXMuaXNDaGVja2VkKHZhbHVlKSkge1xuICAgICAgb2JqLnNwbGljZShvYmouaW5kZXhPZih2YWx1ZSksIDEpO1xuICAgIH0gZWxzZSB7XG4gICAgICBvYmoucHVzaCh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgaXNDaGVja2VkKHZhbHVlKTogYm9vbGVhbiB7XG4gICAgY29uc3Qgc2VsZWN0ZWQgPSB0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV07XG4gICAgaWYgKHNlbGVjdGVkICE9IG51bGwpIHtcbiAgICAgIHJldHVybiBzZWxlY3RlZC5maWx0ZXIobyA9PiBvID09PSB2YWx1ZSkubGVuZ3RoID4gMDtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG59XG4iXX0=