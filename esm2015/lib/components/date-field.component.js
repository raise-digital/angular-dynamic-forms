/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { DateField } from '../fields/date-field';
import { FormService } from '../services/form.service';
export class DateFieldComponent extends FormItemComponent {
}
DateFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-date-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <div class=\"ndf-date-picker\">\n        <input ngx-mydatepicker [name]=\"item.id\" [value]=\"item.value | date:'dd/MM/yyyy'\" [readonly]=\"item.readOnly\" (dateChanged)=\"onChange($event)\" [options]=\"{ dateFormat: 'dd/mm/yyyy', selectorHeight: 'auto' }\" #dp=\"ngx-mydatepicker\" />\n        <button *ngIf=\"item.value != null\" type=\"button\" class=\"ndf-date-clear\" (click)=\"dp.clearDate()\">\n            <fa name=\"times\" size=\"lt\"></fa>\n        </button>\n        <button type=\"button\" class=\"ndf-date-open\" (click)=\"dp.toggleCalendar()\">\n            <fa name=\"calendar-o\" size=\"lt\"></fa>\n        </button>\n    </div>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
DateFieldComponent.propDecorators = {
    item: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    DateFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1maWVsZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9kYXRlLWZpZWxkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFakQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDMUQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQU92RCxNQUFNLE9BQU8sa0JBQW1CLFNBQVEsaUJBQWlCOzs7WUFMeEQsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLG00QkFBMEM7Z0JBQzFDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTthQUMzQjs7O21CQUVFLEtBQUs7Ozs7SUFBTixrQ0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IERhdGVGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9kYXRlLWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWRhdGUtZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vZGF0ZS1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIERhdGVGaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogRGF0ZUZpZWxkO1xufVxuIl19