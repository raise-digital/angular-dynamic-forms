import { FormItemComponent } from './form-item.component';
import { HeaderField } from '../fields/header-field';
export declare class HeaderFieldComponent extends FormItemComponent {
    item: HeaderField;
}
