import { ModuleWithProviders } from '@angular/core';
export declare class NgDynamicFormsModule {
    static forRoot(): ModuleWithProviders;
}
