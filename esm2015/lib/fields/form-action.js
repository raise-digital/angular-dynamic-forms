/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { FormBase } from '../form-base';
export class FormAction extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.isAction = true;
        this.type = 'action';
        this.title = options['title'] || null;
        this.icon = options['icon'] || null;
        this.actionType = options['actionType'] || 'submit';
        this.click = options['click'] || null;
        this.classes.push('ndf-action');
    }
    /**
     * @param {?} text
     * @return {?}
     */
    showLoading(text) {
        this.loading = text;
    }
    /**
     * @return {?}
     */
    clearLoading() {
        this.loading = null;
    }
    /**
     * @return {?}
     */
    onClick() {
        if (this.click != null) {
            this.click(this);
        }
    }
}
if (false) {
    /** @type {?} */
    FormAction.prototype.isAction;
    /** @type {?} */
    FormAction.prototype.type;
    /** @type {?} */
    FormAction.prototype.title;
    /** @type {?} */
    FormAction.prototype.icon;
    /** @type {?} */
    FormAction.prototype.actionType;
    /** @type {?} */
    FormAction.prototype.loading;
    /** @type {?} */
    FormAction.prototype.click;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1hY3Rpb24uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL2Zvcm0tYWN0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBRXhDLE1BQU0sT0FBTyxVQUFXLFNBQVEsUUFBUTs7OztJQVN0QyxZQUFZLFVBQWMsRUFBRTtRQUMxQixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFUakIsYUFBUSxHQUFHLElBQUksQ0FBQztRQUNoQixTQUFJLEdBQUcsUUFBUSxDQUFDO1FBU2QsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQztRQUNwQyxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxRQUFRLENBQUM7UUFDcEQsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLElBQVk7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDdEIsQ0FBQzs7OztJQUVELFlBQVk7UUFDVixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztJQUN0QixDQUFDOzs7O0lBRUQsT0FBTztRQUNMLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsQjtJQUNILENBQUM7Q0FDRjs7O0lBOUJDLDhCQUFnQjs7SUFDaEIsMEJBQWdCOztJQUNoQiwyQkFBYzs7SUFDZCwwQkFBYTs7SUFDYixnQ0FBbUI7O0lBQ25CLDZCQUFnQjs7SUFDaEIsMkJBQW9DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRm9ybUJhc2UgfSBmcm9tICcuLi9mb3JtLWJhc2UnO1xuXG5leHBvcnQgY2xhc3MgRm9ybUFjdGlvbiBleHRlbmRzIEZvcm1CYXNlIHtcbiAgaXNBY3Rpb24gPSB0cnVlO1xuICB0eXBlID0gJ2FjdGlvbic7XG4gIHRpdGxlOiBzdHJpbmc7XG4gIGljb246IHN0cmluZztcbiAgYWN0aW9uVHlwZTogc3RyaW5nO1xuICBsb2FkaW5nOiBzdHJpbmc7XG4gIGNsaWNrOiAoYWN0aW9uOiBGb3JtQWN0aW9uKSA9PiB2b2lkO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLnRpdGxlID0gb3B0aW9uc1sndGl0bGUnXSB8fCBudWxsO1xuICAgIHRoaXMuaWNvbiA9IG9wdGlvbnNbJ2ljb24nXSB8fCBudWxsO1xuICAgIHRoaXMuYWN0aW9uVHlwZSA9IG9wdGlvbnNbJ2FjdGlvblR5cGUnXSB8fCAnc3VibWl0JztcbiAgICB0aGlzLmNsaWNrID0gb3B0aW9uc1snY2xpY2snXSB8fCBudWxsO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtYWN0aW9uJyk7XG4gIH1cblxuICBzaG93TG9hZGluZyh0ZXh0OiBzdHJpbmcpIHtcbiAgICB0aGlzLmxvYWRpbmcgPSB0ZXh0O1xuICB9XG5cbiAgY2xlYXJMb2FkaW5nKCkge1xuICAgIHRoaXMubG9hZGluZyA9IG51bGw7XG4gIH1cblxuICBvbkNsaWNrKCkge1xuICAgIGlmICh0aGlzLmNsaWNrICE9IG51bGwpIHtcbiAgICAgIHRoaXMuY2xpY2sodGhpcyk7XG4gICAgfVxuICB9XG59XG4iXX0=