import { Field } from './field';
export declare class CheckboxField extends Field<boolean> {
    type: string;
    hasLabel: boolean;
    constructor(options?: {});
}
