/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Field } from './field';
export class DateField extends Field {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'date';
        this.classes.push('ndf-date');
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onChange(value) {
        super.onChange((value.jsdate == null) ? null : new Date(value.jsdate));
    }
}
if (false) {
    /** @type {?} */
    DateField.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1maWVsZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9maWVsZHMvZGF0ZS1maWVsZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUloQyxNQUFNLE9BQU8sU0FBVSxTQUFRLEtBQVc7Ozs7SUFHeEMsWUFBWSxVQUFjLEVBQUU7UUFDMUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBSGpCLFNBQUksR0FBRyxNQUFNLENBQUM7UUFJWixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFtQjtRQUMxQixLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUN6RSxDQUFDO0NBQ0Y7OztJQVZDLHlCQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuL2ZpZWxkJztcblxuaW1wb3J0IHsgSU15RGF0ZU1vZGVsIH0gZnJvbSAnbmd4LW15ZGF0ZXBpY2tlcic7XG5cbmV4cG9ydCBjbGFzcyBEYXRlRmllbGQgZXh0ZW5kcyBGaWVsZDxEYXRlPiB7XG4gIHR5cGUgPSAnZGF0ZSc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtZGF0ZScpO1xuICB9XG5cbiAgb25DaGFuZ2UodmFsdWU6IElNeURhdGVNb2RlbCkge1xuICAgIHN1cGVyLm9uQ2hhbmdlKCh2YWx1ZS5qc2RhdGUgPT0gbnVsbCkgPyBudWxsIDogbmV3IERhdGUodmFsdWUuanNkYXRlKSk7XG4gIH1cbn1cbiJdfQ==