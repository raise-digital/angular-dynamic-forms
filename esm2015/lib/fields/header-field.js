/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { FormBase } from '../form-base';
export class HeaderField extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'header';
        this.title = options['title'] || '';
        this.classes.push('ndf-field');
        this.classes.push('ndf-header');
    }
}
if (false) {
    /** @type {?} */
    HeaderField.prototype.type;
    /** @type {?} */
    HeaderField.prototype.title;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLWZpZWxkLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2ZpZWxkcy9oZWFkZXItZmllbGQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFeEMsTUFBTSxPQUFPLFdBQVksU0FBUSxRQUFROzs7O0lBSXZDLFlBQVksVUFBYyxFQUFFO1FBQzFCLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUpqQixTQUFJLEdBQUcsUUFBUSxDQUFDO1FBS2QsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Q0FDRjs7O0lBVEMsMkJBQWdCOztJQUNoQiw0QkFBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuZXhwb3J0IGNsYXNzIEhlYWRlckZpZWxkIGV4dGVuZHMgRm9ybUJhc2Uge1xuICB0eXBlID0gJ2hlYWRlcic7XG4gIHRpdGxlOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMudGl0bGUgPSBvcHRpb25zWyd0aXRsZSddIHx8ICcnO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtZmllbGQnKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWhlYWRlcicpO1xuICB9XG59XG4iXX0=