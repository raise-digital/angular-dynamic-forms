import { TextField } from './text-field';
export declare class NumberField extends TextField {
    precision: number;
    constructor(options?: {});
    deriveValue(): Promise<boolean>;
    onBlur(): void;
    prepareValue(value: any): any;
}
