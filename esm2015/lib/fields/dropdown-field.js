/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { OptionSetField } from './option-set-field';
export class DropdownField extends OptionSetField {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'dropdown';
        this.classes.push('ndf-dropdown');
        this.loaded = options['emptyText'] || '';
        this.emptyText = options['loadingText'] || '';
    }
    /**
     * @param {?} options
     * @return {?}
     */
    setOptions(options) {
        super.setOptions(options);
        this.emptyText = this.loaded;
    }
}
if (false) {
    /** @type {?} */
    DropdownField.prototype.type;
    /** @type {?} */
    DropdownField.prototype.emptyText;
    /** @type {?} */
    DropdownField.prototype.loaded;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tZmllbGQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL2Ryb3Bkb3duLWZpZWxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFFcEQsTUFBTSxPQUFPLGFBQWMsU0FBUSxjQUFzQjs7OztJQUt2RCxZQUFZLFVBQWMsRUFBRTtRQUMxQixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFMakIsU0FBSSxHQUFHLFVBQVUsQ0FBQztRQU1oQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDekMsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2hELENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLE9BQWtFO1FBQzNFLEtBQUssQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQy9CLENBQUM7Q0FDRjs7O0lBZkMsNkJBQWtCOztJQUNsQixrQ0FBa0I7O0lBQ2xCLCtCQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT3B0aW9uU2V0RmllbGQgfSBmcm9tICcuL29wdGlvbi1zZXQtZmllbGQnO1xuXG5leHBvcnQgY2xhc3MgRHJvcGRvd25GaWVsZCBleHRlbmRzIE9wdGlvblNldEZpZWxkPHN0cmluZz4ge1xuICB0eXBlID0gJ2Ryb3Bkb3duJztcbiAgZW1wdHlUZXh0OiBzdHJpbmc7XG4gIGxvYWRlZDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWRyb3Bkb3duJyk7XG4gICAgdGhpcy5sb2FkZWQgPSBvcHRpb25zWydlbXB0eVRleHQnXSB8fCAnJztcbiAgICB0aGlzLmVtcHR5VGV4dCA9IG9wdGlvbnNbJ2xvYWRpbmdUZXh0J10gfHwgJyc7XG4gIH1cblxuICBzZXRPcHRpb25zKG9wdGlvbnM6IEFycmF5PHsga2V5OiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcsIGRpc2FibGVkPzogYm9vbGVhbiB9Pikge1xuICAgIHN1cGVyLnNldE9wdGlvbnMob3B0aW9ucyk7XG4gICAgdGhpcy5lbXB0eVRleHQgPSB0aGlzLmxvYWRlZDtcbiAgfVxufVxuIl19