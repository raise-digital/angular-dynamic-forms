/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { OptionSetField } from '../fields/option-set-field';
import { FormService } from '../services/form.service';
export class OptionSetFieldComponent extends FormItemComponent {
}
OptionSetFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-option-set-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <ul>\n        <li *ngFor=\"let option of item.options\">\n            <label>\n                <div class=\"box\"><div *ngIf=\"item.value == option.key\"></div></div>\n                <input type=\"radio\" [formControlName]=\"item.id\" [checked]=\"item.value == option.key\" [value]=\"option.key\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" />\n                {{ option.value }}\n            </label>\n        </li>\n    </ul>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
OptionSetFieldComponent.propDecorators = {
    item: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    OptionSetFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3B0aW9uLXNldC1maWVsZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9vcHRpb24tc2V0LWZpZWxkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFakQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzVELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQU92RCxNQUFNLE9BQU8sdUJBQXdCLFNBQVEsaUJBQWlCOzs7WUFMN0QsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxzQkFBc0I7Z0JBQ2hDLGdzQkFBZ0Q7Z0JBQ2hELFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTthQUMzQjs7O21CQUVFLEtBQUs7Ozs7SUFBTix1Q0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IE9wdGlvblNldEZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL29wdGlvbi1zZXQtZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtb3B0aW9uLXNldC1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9vcHRpb24tc2V0LWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgT3B0aW9uU2V0RmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IE9wdGlvblNldEZpZWxkPGFueT47XG59XG4iXX0=