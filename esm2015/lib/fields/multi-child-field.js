/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { FieldGroup } from './field-group';
import { HeaderField } from './header-field';
import { FormAction } from './form-action';
export class MultiChildField extends FieldGroup {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.child = false;
        this.counter = 0;
        this.subForm = options['subForm'] || null;
        this.newObj = options['newObj'] || null;
        this.addButtonText = options['addButtonText'] || 'Add New Line';
        this.removeButtonText = options['removeButtonText'] || 'Remove Line';
        this.child = options['child'] || false;
        this.classes.push('ndf-multichild');
        if (options['title']) {
            this.fields.push(new HeaderField({
                title: options['title']
            }));
            this.fields.push(new FieldGroup({
                name: `${this.name}_controls`,
                fields: [
                    new FormAction({
                        title: this.addButtonText,
                        icon: 'plus',
                        click: () => {
                            if (this.subForm !== null && this.newObj !== null) {
                                /** @type {?} */
                                const obj = this.newObj();
                                this.list.push(obj);
                                this.addChild(obj);
                            }
                        },
                        classes: ['ndf-add']
                    })
                ],
                classes: ['ndf-full-bordered', 'ndf-multichild-footer']
            }));
        }
    }
    /**
     * @return {?}
     */
    get list() {
        if (this.form.obj[this.name] == null) {
            this.form.obj[this.name] = [];
        }
        return ((/** @type {?} */ (this.form.obj[this.name])));
    }
    /**
     * @param {?} form
     * @return {?}
     */
    setForm(form) {
        super.setForm(form);
        if (this.subForm !== null) {
            this.list.forEach((obj) => {
                this.addChild(obj);
            });
        }
    }
    /**
     * @param {?} result
     * @return {?}
     */
    loadValidation(result) {
        this.resetValidation();
        for (const key in result.fields) {
            if (result.fields.hasOwnProperty(key)) {
                /** @type {?} */
                const child = `${this.form.prefix}line_${key}`;
                /** @type {?} */
                const field = this.fields.fieldByName(child);
                if (field != null) {
                    field.loadValidation(result.fields[key]);
                }
            }
        }
    }
    /**
     * @param {?} obj
     * @return {?}
     */
    addChild(obj) {
        /** @type {?} */
        const group = `${this.form.prefix}line_${this.counter}_group`;
        this.fields.insertBeforeId(new FieldGroup({
            name: group,
            fields: [
                this.subForm(obj, `${this.form.prefix}line_${this.counter}`),
                new FormAction({
                    title: this.removeButtonText,
                    icon: 'trash',
                    click: () => {
                        this.fields.removeById(group);
                        this.list.splice(this.list.indexOf(obj), 1);
                    },
                    classes: ['ndf-remove']
                })
            ],
            classes: ['ndf-full-bordered', 'ndf-multichild-row', (this.child) ? 'ndf-child' : 'ndf-parent']
        }), `${this.name}_controls`);
        this.counter++;
    }
}
if (false) {
    /** @type {?} */
    MultiChildField.prototype.type;
    /** @type {?} */
    MultiChildField.prototype.subForm;
    /** @type {?} */
    MultiChildField.prototype.newObj;
    /** @type {?} */
    MultiChildField.prototype.addButtonText;
    /** @type {?} */
    MultiChildField.prototype.removeButtonText;
    /** @type {?} */
    MultiChildField.prototype.child;
    /** @type {?} */
    MultiChildField.prototype.counter;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGktY2hpbGQtZmllbGQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL211bHRpLWNoaWxkLWZpZWxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBSTNDLE1BQU0sT0FBTyxlQUFnQixTQUFRLFVBQVU7Ozs7SUFTN0MsWUFBWSxVQUFjLEVBQUU7UUFDMUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBSmpCLFVBQUssR0FBRyxLQUFLLENBQUM7UUFDTixZQUFPLEdBQUcsQ0FBQyxDQUFDO1FBSWxCLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksQ0FBQztRQUMxQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLENBQUM7UUFDeEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLElBQUksY0FBYyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsa0JBQWtCLENBQUMsSUFBSSxhQUFhLENBQUM7UUFDckUsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDcEMsSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxXQUFXLENBQUM7Z0JBQy9CLEtBQUssRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDO2FBQ3hCLENBQUMsQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxVQUFVLENBQUM7Z0JBQzlCLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLFdBQVc7Z0JBQzdCLE1BQU0sRUFBRTtvQkFDTixJQUFJLFVBQVUsQ0FBQzt3QkFDYixLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWE7d0JBQ3pCLElBQUksRUFBRSxNQUFNO3dCQUNaLEtBQUssRUFBRSxHQUFHLEVBQUU7NEJBQ1YsSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRTs7c0NBQzNDLEdBQUcsR0FBUSxJQUFJLENBQUMsTUFBTSxFQUFFO2dDQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQ0FDcEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQzs2QkFDcEI7d0JBQ0gsQ0FBQzt3QkFDRCxPQUFPLEVBQUcsQ0FBQyxTQUFTLENBQUM7cUJBQ3RCLENBQUM7aUJBQ0g7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsbUJBQW1CLEVBQUUsdUJBQXVCLENBQUM7YUFDeEQsQ0FBQyxDQUFDLENBQUM7U0FDTDtJQUNILENBQUM7Ozs7SUFFRCxJQUFJLElBQUk7UUFDTixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUU7WUFDcEMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUMvQjtRQUNELE9BQU8sQ0FBQyxtQkFBQSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQWMsQ0FBQyxDQUFDO0lBQ2xELENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLElBQVU7UUFDaEIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQixJQUFJLElBQUksQ0FBQyxPQUFPLEtBQUssSUFBSSxFQUFFO1lBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLE1BQXdCO1FBQ3JDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixLQUFLLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7WUFDL0IsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTs7c0JBQy9CLEtBQUssR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxRQUFRLEdBQUcsRUFBRTs7c0JBQ3hDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7Z0JBQzVDLElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtvQkFDakIsS0FBSyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQzFDO2FBQ0Y7U0FDRjtJQUNILENBQUM7Ozs7O0lBRU8sUUFBUSxDQUFDLEdBQVE7O2NBQ2pCLEtBQUssR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxRQUFRLElBQUksQ0FBQyxPQUFPLFFBQVE7UUFDN0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQ3hCLElBQUksVUFBVSxDQUFDO1lBQ2IsSUFBSSxFQUFFLEtBQUs7WUFDWCxNQUFNLEVBQUU7Z0JBQ04sSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sUUFBUSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQzVELElBQUksVUFBVSxDQUFDO29CQUNiLEtBQUssRUFBRSxJQUFJLENBQUMsZ0JBQWdCO29CQUM1QixJQUFJLEVBQUUsT0FBTztvQkFDYixLQUFLLEVBQUUsR0FBRyxFQUFFO3dCQUNWLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDOUMsQ0FBQztvQkFDRCxPQUFPLEVBQUcsQ0FBQyxZQUFZLENBQUM7aUJBQ3pCLENBQUM7YUFDSDtZQUNELE9BQU8sRUFBRSxDQUFDLG1CQUFtQixFQUFFLG9CQUFvQixFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQztTQUNoRyxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsSUFBSSxXQUFXLENBQzVCLENBQUM7UUFDRixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDakIsQ0FBQztDQUNGOzs7SUE1RkMsK0JBQW1COztJQUNuQixrQ0FBNEM7O0lBQzVDLGlDQUFrQjs7SUFDbEIsd0NBQXNCOztJQUN0QiwyQ0FBeUI7O0lBQ3pCLGdDQUFjOztJQUNkLGtDQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZpZWxkR3JvdXAgfSBmcm9tICcuL2ZpZWxkLWdyb3VwJztcbmltcG9ydCB7IEhlYWRlckZpZWxkIH0gZnJvbSAnLi9oZWFkZXItZmllbGQnO1xuaW1wb3J0IHsgRm9ybUFjdGlvbiB9IGZyb20gJy4vZm9ybS1hY3Rpb24nO1xuaW1wb3J0IHsgRm9ybSB9IGZyb20gJy4uL2Zvcm0nO1xuaW1wb3J0IHsgVmFsaWRhdGlvblJlc3VsdCB9IGZyb20gJy4uL3ZhbGlkYXRpb24tcmVzdWx0LmludGVyZmFjZSc7XG5cbmV4cG9ydCBjbGFzcyBNdWx0aUNoaWxkRmllbGQgZXh0ZW5kcyBGaWVsZEdyb3VwIHtcbiAgdHlwZTogJ211bHRpY2hpbGQnO1xuICBzdWJGb3JtOiAob2JqOiBhbnksIHByZWZpeDogc3RyaW5nKSA9PiBGb3JtO1xuICBuZXdPYmo6ICgpID0+IGFueTtcbiAgYWRkQnV0dG9uVGV4dDogc3RyaW5nO1xuICByZW1vdmVCdXR0b25UZXh0OiBzdHJpbmc7XG4gIGNoaWxkID0gZmFsc2U7XG4gIHByaXZhdGUgY291bnRlciA9IDA7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMuc3ViRm9ybSA9IG9wdGlvbnNbJ3N1YkZvcm0nXSB8fCBudWxsO1xuICAgIHRoaXMubmV3T2JqID0gb3B0aW9uc1snbmV3T2JqJ10gfHwgbnVsbDtcbiAgICB0aGlzLmFkZEJ1dHRvblRleHQgPSBvcHRpb25zWydhZGRCdXR0b25UZXh0J10gfHwgJ0FkZCBOZXcgTGluZSc7XG4gICAgdGhpcy5yZW1vdmVCdXR0b25UZXh0ID0gb3B0aW9uc1sncmVtb3ZlQnV0dG9uVGV4dCddIHx8ICdSZW1vdmUgTGluZSc7XG4gICAgdGhpcy5jaGlsZCA9IG9wdGlvbnNbJ2NoaWxkJ10gfHwgZmFsc2U7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1tdWx0aWNoaWxkJyk7XG4gICAgaWYgKG9wdGlvbnNbJ3RpdGxlJ10pIHtcbiAgICAgIHRoaXMuZmllbGRzLnB1c2gobmV3IEhlYWRlckZpZWxkKHtcbiAgICAgICAgdGl0bGU6IG9wdGlvbnNbJ3RpdGxlJ11cbiAgICAgIH0pKTtcbiAgICAgIHRoaXMuZmllbGRzLnB1c2gobmV3IEZpZWxkR3JvdXAoe1xuICAgICAgICBuYW1lOiBgJHt0aGlzLm5hbWV9X2NvbnRyb2xzYCxcbiAgICAgICAgZmllbGRzOiBbXG4gICAgICAgICAgbmV3IEZvcm1BY3Rpb24oe1xuICAgICAgICAgICAgdGl0bGU6IHRoaXMuYWRkQnV0dG9uVGV4dCxcbiAgICAgICAgICAgIGljb246ICdwbHVzJyxcbiAgICAgICAgICAgIGNsaWNrOiAoKSA9PiB7XG4gICAgICAgICAgICAgIGlmICh0aGlzLnN1YkZvcm0gIT09IG51bGwgJiYgdGhpcy5uZXdPYmogIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBvYmo6IGFueSA9IHRoaXMubmV3T2JqKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5saXN0LnB1c2gob2JqKTtcbiAgICAgICAgICAgICAgICB0aGlzLmFkZENoaWxkKG9iaik7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjbGFzc2VzIDogWyduZGYtYWRkJ11cbiAgICAgICAgICB9KVxuICAgICAgICBdLFxuICAgICAgICBjbGFzc2VzOiBbJ25kZi1mdWxsLWJvcmRlcmVkJywgJ25kZi1tdWx0aWNoaWxkLWZvb3RlciddXG4gICAgICB9KSk7XG4gICAgfVxuICB9XG5cbiAgZ2V0IGxpc3QoKTogQXJyYXk8YW55PiB7XG4gICAgaWYgKHRoaXMuZm9ybS5vYmpbdGhpcy5uYW1lXSA9PSBudWxsKSB7XG4gICAgICB0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV0gPSBbXTtcbiAgICB9XG4gICAgcmV0dXJuICh0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV0gYXMgQXJyYXk8YW55Pik7XG4gIH1cblxuICBzZXRGb3JtKGZvcm06IEZvcm0pOiB2b2lkIHtcbiAgICBzdXBlci5zZXRGb3JtKGZvcm0pO1xuICAgIGlmICh0aGlzLnN1YkZvcm0gIT09IG51bGwpIHtcbiAgICAgIHRoaXMubGlzdC5mb3JFYWNoKChvYmopID0+IHtcbiAgICAgICAgdGhpcy5hZGRDaGlsZChvYmopO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgbG9hZFZhbGlkYXRpb24ocmVzdWx0OiBWYWxpZGF0aW9uUmVzdWx0KTogdm9pZCB7XG4gICAgdGhpcy5yZXNldFZhbGlkYXRpb24oKTtcbiAgICBmb3IgKGNvbnN0IGtleSBpbiByZXN1bHQuZmllbGRzKSB7XG4gICAgICBpZiAocmVzdWx0LmZpZWxkcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgIGNvbnN0IGNoaWxkID0gYCR7dGhpcy5mb3JtLnByZWZpeH1saW5lXyR7a2V5fWA7XG4gICAgICAgIGNvbnN0IGZpZWxkID0gdGhpcy5maWVsZHMuZmllbGRCeU5hbWUoY2hpbGQpO1xuICAgICAgICBpZiAoZmllbGQgIT0gbnVsbCkge1xuICAgICAgICAgIGZpZWxkLmxvYWRWYWxpZGF0aW9uKHJlc3VsdC5maWVsZHNba2V5XSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGFkZENoaWxkKG9iajogYW55KTogdm9pZCB7XG4gICAgY29uc3QgZ3JvdXAgPSBgJHt0aGlzLmZvcm0ucHJlZml4fWxpbmVfJHt0aGlzLmNvdW50ZXJ9X2dyb3VwYDtcbiAgICB0aGlzLmZpZWxkcy5pbnNlcnRCZWZvcmVJZChcbiAgICAgIG5ldyBGaWVsZEdyb3VwKHtcbiAgICAgICAgbmFtZTogZ3JvdXAsXG4gICAgICAgIGZpZWxkczogW1xuICAgICAgICAgIHRoaXMuc3ViRm9ybShvYmosIGAke3RoaXMuZm9ybS5wcmVmaXh9bGluZV8ke3RoaXMuY291bnRlcn1gKSxcbiAgICAgICAgICBuZXcgRm9ybUFjdGlvbih7XG4gICAgICAgICAgICB0aXRsZTogdGhpcy5yZW1vdmVCdXR0b25UZXh0LFxuICAgICAgICAgICAgaWNvbjogJ3RyYXNoJyxcbiAgICAgICAgICAgIGNsaWNrOiAoKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZmllbGRzLnJlbW92ZUJ5SWQoZ3JvdXApO1xuICAgICAgICAgICAgICB0aGlzLmxpc3Quc3BsaWNlKHRoaXMubGlzdC5pbmRleE9mKG9iaiksIDEpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNsYXNzZXMgOiBbJ25kZi1yZW1vdmUnXVxuICAgICAgICAgIH0pXG4gICAgICAgIF0sXG4gICAgICAgIGNsYXNzZXM6IFsnbmRmLWZ1bGwtYm9yZGVyZWQnLCAnbmRmLW11bHRpY2hpbGQtcm93JywgKHRoaXMuY2hpbGQpID8gJ25kZi1jaGlsZCcgOiAnbmRmLXBhcmVudCddXG4gICAgICB9KSwgYCR7dGhpcy5uYW1lfV9jb250cm9sc2BcbiAgICApO1xuICAgIHRoaXMuY291bnRlcisrO1xuICB9XG59XG4iXX0=