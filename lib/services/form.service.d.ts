import { FormControl, FormGroup } from '@angular/forms';
import { Field } from '../fields/field';
export declare class FormService {
    private formGroup;
    constructor();
    readonly group: FormGroup;
    registerField(field: Field<any>): void;
    registerFields(fields: Field<any>[]): void;
    deregisterField(field: Field<any>): void;
    deregisterFields(fields: Field<any>[]): void;
    controlFromField(field: Field<any>): FormControl;
}
