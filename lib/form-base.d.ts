import { Form } from './form';
import { FieldSet } from './field-set';
import { ValidationResult } from './validation-result.interface';
import { FormControl } from '@angular/forms';
export declare class FormBase {
    name: string;
    id: string;
    type: string;
    classes: string[];
    fields: FieldSet;
    message: string;
    readOnly: boolean;
    change: (field: FormBase, value: any) => boolean;
    blur: (field: FormBase, value: any) => void;
    protected form: Form;
    isForm: boolean;
    hasData: boolean;
    hasFields: boolean;
    hasLabel: boolean;
    isAction: boolean;
    constructor(options?: {
        name?: string;
        classes?: string[];
        fields?: FormBase[];
        readOnly?: boolean;
        change?: (field: FormBase, value: any) => boolean;
        blur?: (field: FormBase, value: any) => void;
    });
    getForm(): Form;
    setForm(form: Form): void;
    deriveValue(): Promise<boolean>;
    readonly control: FormControl;
    onChange(value: any): void;
    onBlur(value: any): void;
    onClick(): void;
    setMessage(message: string): void;
    loadValidation(result: ValidationResult): void;
    resetValidation(): void;
}
