/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Field } from './field';
var CheckboxField = /** @class */ (function (_super) {
    tslib_1.__extends(CheckboxField, _super);
    function CheckboxField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'checkbox';
        _this.hasLabel = false;
        _this.classes.push('ndf-checkbox');
        return _this;
    }
    return CheckboxField;
}(Field));
export { CheckboxField };
if (false) {
    /** @type {?} */
    CheckboxField.prototype.type;
    /** @type {?} */
    CheckboxField.prototype.hasLabel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3gtZmllbGQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL2NoZWNrYm94LWZpZWxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUdoQztJQUFtQyx5Q0FBYztJQUkvQyx1QkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBRWY7UUFORCxVQUFJLEdBQUcsVUFBVSxDQUFDO1FBQ2xCLGNBQVEsR0FBRyxLQUFLLENBQUM7UUFJZixLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzs7SUFDcEMsQ0FBQztJQUNILG9CQUFDO0FBQUQsQ0FBQyxBQVJELENBQW1DLEtBQUssR0FRdkM7Ozs7SUFQQyw2QkFBa0I7O0lBQ2xCLGlDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi9maWVsZCc7XG5cblxuZXhwb3J0IGNsYXNzIENoZWNrYm94RmllbGQgZXh0ZW5kcyBGaWVsZDxib29sZWFuPiB7XG4gIHR5cGUgPSAnY2hlY2tib3gnO1xuICBoYXNMYWJlbCA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWNoZWNrYm94Jyk7XG4gIH1cbn1cbiJdfQ==