/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FormBase } from '../form-base';
/**
 * @template T
 */
export class Field extends FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.hasData = true;
        this.value = options.value;
        this.label = options.label || '';
        this.classes.push('ndf-field');
        this.placeholder = options.placeholder || '';
        if (this.placeholder.length > 0) {
            this.classes.push('ndf-placeholder');
        }
        this.derive = options.derive || null;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    setForm(form) {
        super.setForm(form);
        this.deriveValue();
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onChange(value) {
        this.value = value;
        super.onChange(this.prepareValue(value));
    }
    /**
     * @return {?}
     */
    deriveValue() {
        const _super = name => super[name];
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return _super("deriveValue").call(this).then(derived => {
                if (!derived) {
                    if (this.derive != null) {
                        return this.derive(this);
                    }
                    else if (this.form.obj.hasOwnProperty(this.name)) {
                        this.value = this.form.obj[this.name];
                        return Promise.resolve(true);
                    }
                }
                return Promise.resolve(false);
            });
        });
    }
    /**
     * @param {?} value
     * @return {?}
     */
    prepareValue(value) {
        if (value == null) {
            return '';
        }
        else {
            return value;
        }
    }
}
if (false) {
    /** @type {?} */
    Field.prototype.id;
    /** @type {?} */
    Field.prototype.hasData;
    /** @type {?} */
    Field.prototype.value;
    /** @type {?} */
    Field.prototype.label;
    /** @type {?} */
    Field.prototype.placeholder;
    /** @type {?} */
    Field.prototype.derive;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL2ZpZWxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQ0EsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGNBQWMsQ0FBQzs7OztBQUd4QyxNQUFNLE9BQU8sS0FBUyxTQUFRLFFBQVE7Ozs7SUFRcEMsWUFBWSxVQU1SLEVBQUU7UUFDSixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFiakIsWUFBTyxHQUFHLElBQUksQ0FBQztRQWNiLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUM7UUFDN0MsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDL0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUN0QztRQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUM7SUFDdkMsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsSUFBVTtRQUNoQixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFLO1FBQ1osSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDM0MsQ0FBQzs7OztJQUVLLFdBQVc7OztZQUNmLE9BQU8scUJBQWlCLFlBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN4QyxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUNaLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLEVBQUU7d0JBQ3ZCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDMUI7eUJBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUNsRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDdEMsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUM5QjtpQkFDRjtnQkFDRCxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDO0tBQUE7Ozs7O0lBRUQsWUFBWSxDQUFDLEtBQVU7UUFDckIsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ2pCLE9BQU8sRUFBRSxDQUFDO1NBQ1g7YUFBTTtZQUNMLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7SUFDSCxDQUFDO0NBQ0Y7OztJQXhEQyxtQkFBVzs7SUFDWCx3QkFBZTs7SUFDZixzQkFBUzs7SUFDVCxzQkFBYzs7SUFDZCw0QkFBb0I7O0lBQ3BCLHVCQUE4QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm0gfSBmcm9tICcuLi9mb3JtJztcbmltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuXG5leHBvcnQgY2xhc3MgRmllbGQ8VD4gZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIGlkOiBzdHJpbmc7XG4gIGhhc0RhdGEgPSB0cnVlO1xuICB2YWx1ZTogVDtcbiAgbGFiZWw6IHN0cmluZztcbiAgcGxhY2Vob2xkZXI6IHN0cmluZztcbiAgZGVyaXZlOiAoZmllbGQ6IEZpZWxkPFQ+KSA9PiBQcm9taXNlPGJvb2xlYW4+O1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHtcbiAgICB2YWx1ZT86IFQsXG4gICAgbGFiZWw/OiBzdHJpbmcsXG4gICAgY2xhc3Nlcz86IHN0cmluZ1tdLFxuICAgIHBsYWNlaG9sZGVyPzogc3RyaW5nLFxuICAgIGRlcml2ZT86IChmaWVsZDogRmllbGQ8VD4pID0+IFByb21pc2U8Ym9vbGVhbj5cbiAgfSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy52YWx1ZSA9IG9wdGlvbnMudmFsdWU7XG4gICAgdGhpcy5sYWJlbCA9IG9wdGlvbnMubGFiZWwgfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1maWVsZCcpO1xuICAgIHRoaXMucGxhY2Vob2xkZXIgPSBvcHRpb25zLnBsYWNlaG9sZGVyIHx8ICcnO1xuICAgIGlmICh0aGlzLnBsYWNlaG9sZGVyLmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtcGxhY2Vob2xkZXInKTtcbiAgICB9XG4gICAgdGhpcy5kZXJpdmUgPSBvcHRpb25zLmRlcml2ZSB8fCBudWxsO1xuICB9XG5cbiAgc2V0Rm9ybShmb3JtOiBGb3JtKTogdm9pZCB7XG4gICAgc3VwZXIuc2V0Rm9ybShmb3JtKTtcbiAgICB0aGlzLmRlcml2ZVZhbHVlKCk7XG4gIH1cblxuICBvbkNoYW5nZSh2YWx1ZSkge1xuICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcbiAgICBzdXBlci5vbkNoYW5nZSh0aGlzLnByZXBhcmVWYWx1ZSh2YWx1ZSkpO1xuICB9XG5cbiAgYXN5bmMgZGVyaXZlVmFsdWUoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgcmV0dXJuIHN1cGVyLmRlcml2ZVZhbHVlKCkudGhlbihkZXJpdmVkID0+IHtcbiAgICAgIGlmICghZGVyaXZlZCkge1xuICAgICAgICBpZiAodGhpcy5kZXJpdmUgIT0gbnVsbCkge1xuICAgICAgICAgIHJldHVybiB0aGlzLmRlcml2ZSh0aGlzKTtcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmZvcm0ub2JqLmhhc093blByb3BlcnR5KHRoaXMubmFtZSkpIHtcbiAgICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5mb3JtLm9ialt0aGlzLm5hbWVdO1xuICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUodHJ1ZSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZmFsc2UpO1xuICAgIH0pO1xuICB9XG5cbiAgcHJlcGFyZVZhbHVlKHZhbHVlOiBhbnkpOiBhbnkge1xuICAgIGlmICh2YWx1ZSA9PSBudWxsKSB7XG4gICAgICByZXR1cm4gJyc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==