/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { FieldGroup } from '../fields/field-group';
import { FormService } from '../services/form.service';
var FieldGroupComponent = /** @class */ (function (_super) {
    tslib_1.__extends(FieldGroupComponent, _super);
    function FieldGroupComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FieldGroupComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-field-group',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <ndf-form-item *ngFor=\"let child of item.fields.fields\" [item]=\"child\"></ndf-form-item>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    FieldGroupComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return FieldGroupComponent;
}(FormItemComponent));
export { FieldGroupComponent };
if (false) {
    /** @type {?} */
    FieldGroupComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtZ3JvdXAuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZmllbGQtZ3JvdXAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFakQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDMUQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ25ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUV2RDtJQUt5QywrQ0FBaUI7SUFMMUQ7O0lBT0EsQ0FBQzs7Z0JBUEEsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxpQkFBaUI7b0JBQzNCLHVQQUEyQztvQkFDM0MsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO2lCQUMzQjs7O3VCQUVFLEtBQUs7O0lBQ1IsMEJBQUM7Q0FBQSxBQVBELENBS3lDLGlCQUFpQixHQUV6RDtTQUZZLG1CQUFtQjs7O0lBQzlCLG1DQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmllbGRHcm91cCB9IGZyb20gJy4uL2ZpZWxkcy9maWVsZC1ncm91cCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1maWVsZC1ncm91cCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9maWVsZC1ncm91cC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIEZpZWxkR3JvdXBDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IEZpZWxkR3JvdXA7XG59XG4iXX0=