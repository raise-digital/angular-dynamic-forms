import { FormBase } from '../form-base';
export declare class FieldGroup extends FormBase {
    hasFields: boolean;
    type: string;
    constructor(options?: {});
    deriveValue(): Promise<boolean>;
}
