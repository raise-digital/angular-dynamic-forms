import { FormBase } from '../form-base';
export declare class HeaderField extends FormBase {
    type: string;
    title: string;
    constructor(options?: {});
}
