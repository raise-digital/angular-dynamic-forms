/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var FieldSet = /** @class */ (function () {
    function FieldSet(set) {
        this.set = set;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    FieldSet.prototype.setForm = /**
     * @param {?} form
     * @return {?}
     */
    function (form) {
        this.set.forEach(function (field) {
            field.setForm(form);
        });
    };
    /**
     * @return {?}
     */
    FieldSet.prototype.updateValues = /**
     * @return {?}
     */
    function () {
        this.set.forEach(function (field) {
            field.deriveValue();
        });
    };
    Object.defineProperty(FieldSet.prototype, "fields", {
        get: /**
         * @return {?}
         */
        function () {
            return this.set;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} item
     * @return {?}
     */
    FieldSet.prototype.push = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        this.set.push(item);
    };
    /**
     * @param {?} item
     * @param {?} before
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertBefore = /**
     * @param {?} item
     * @param {?} before
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    function (item, before, attr, field) {
        if (field === void 0) { field = null; }
        /** @type {?} */
        var parent = null;
        if (field === null) {
            /** @type {?} */
            var search = this.fieldWithParent(before, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field), 0, item);
        }
        else {
            parent.fields.insertBefore(item, before, attr, field);
        }
    };
    /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertBeforeName = /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    function (item, before, field) {
        if (field === void 0) { field = null; }
        return this.insertBefore(item, before, 'name', field);
    };
    /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertBeforeId = /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    function (item, before, field) {
        if (field === void 0) { field = null; }
        return this.insertBefore(item, before, 'id', field);
    };
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertAfter = /**
     * @param {?} item
     * @param {?} after
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    function (item, after, attr, field) {
        if (field === void 0) { field = null; }
        /** @type {?} */
        var parent = null;
        if (field === null) {
            /** @type {?} */
            var search = this.fieldWithParent(after, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field) + 1, 0, item);
        }
        else {
            parent.fields.insertAfter(item, after, attr, field);
        }
    };
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertAfterName = /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    function (item, after, _attr, field) {
        if (field === void 0) { field = null; }
        this.insertAfter(item, after, 'name', field);
    };
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertAfterId = /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    function (item, after, _attr, field) {
        if (field === void 0) { field = null; }
        this.insertAfter(item, after, 'id', field);
    };
    /**
     * @param {?} key
     * @param {?=} field
     * @param {?=} attr
     * @return {?}
     */
    FieldSet.prototype.remove = /**
     * @param {?} key
     * @param {?=} field
     * @param {?=} attr
     * @return {?}
     */
    function (key, field, attr) {
        if (field === void 0) { field = null; }
        /** @type {?} */
        var parent = null;
        if (field === null) {
            /** @type {?} */
            var search = this.fieldWithParent(key, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field), 1);
        }
        else {
            parent.fields.remove(key, field, attr);
        }
    };
    /**
     * @param {?} name
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.removeByName = /**
     * @param {?} name
     * @param {?=} field
     * @return {?}
     */
    function (name, field) {
        if (field === void 0) { field = null; }
        this.remove(name, field, 'name');
    };
    /**
     * @param {?} id
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.removeById = /**
     * @param {?} id
     * @param {?=} field
     * @return {?}
     */
    function (id, field) {
        if (field === void 0) { field = null; }
        this.remove(id, field, 'id');
    };
    /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    FieldSet.prototype.field = /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    function (key, attr) {
        return this.fieldWithParent(key, attr).field;
    };
    /**
     * @param {?} name
     * @return {?}
     */
    FieldSet.prototype.fieldByName = /**
     * @param {?} name
     * @return {?}
     */
    function (name) {
        return this.field(name, 'name');
    };
    /**
     * @param {?} id
     * @return {?}
     */
    FieldSet.prototype.fieldById = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.field(id, 'id');
    };
    /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    FieldSet.prototype.fieldWithParent = /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    function (key, attr) {
        /** @type {?} */
        var field = null;
        /** @type {?} */
        var parent = null;
        /** @type {?} */
        var search = this.set.filter(function (child) { return child[attr] === key; });
        if (search.length > 0) {
            field = search[0];
        }
        else {
            /** @type {?} */
            var parents = this.set.filter(function (child) { return child.hasFields === true; });
            for (var i = 0; i < parents.length; i++) {
                /** @type {?} */
                var check = parents[i].fields.fieldWithParent(key, attr);
                if (check.field != null) {
                    field = check.field;
                    parent = parents[i];
                    break;
                }
            }
        }
        return { field: field, parent: parent };
    };
    /**
     * @param {?} name
     * @return {?}
     */
    FieldSet.prototype.fieldByNameWithParent = /**
     * @param {?} name
     * @return {?}
     */
    function (name) {
        return this.fieldWithParent(name, 'name');
    };
    /**
     * @param {?} id
     * @return {?}
     */
    FieldSet.prototype.fieldByIdWithParent = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.fieldWithParent(id, 'id');
    };
    /**
     * @return {?}
     */
    FieldSet.prototype.resetValidation = /**
     * @return {?}
     */
    function () {
        this.fields.forEach(function (field) {
            field.resetValidation();
        });
    };
    return FieldSet;
}());
export { FieldSet };
if (false) {
    /** @type {?} */
    FieldSet.prototype.set;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtc2V0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2ZpZWxkLXNldC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBSUE7SUFDRSxrQkFBb0IsR0FBZTtRQUFmLFFBQUcsR0FBSCxHQUFHLENBQVk7SUFBSSxDQUFDOzs7OztJQUV4QywwQkFBTzs7OztJQUFQLFVBQVEsSUFBVTtRQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQUs7WUFDckIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCwrQkFBWTs7O0lBQVo7UUFDRSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQUs7WUFDckIsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHNCQUFJLDRCQUFNOzs7O1FBQVY7WUFDRSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDbEIsQ0FBQzs7O09BQUE7Ozs7O0lBRUQsdUJBQUk7Ozs7SUFBSixVQUFLLElBQWM7UUFDakIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdEIsQ0FBQzs7Ozs7Ozs7SUFFRCwrQkFBWTs7Ozs7OztJQUFaLFVBQWEsSUFBYyxFQUFFLE1BQWMsRUFBRSxJQUFZLEVBQUUsS0FBc0I7UUFBdEIsc0JBQUEsRUFBQSxZQUFzQjs7WUFDM0UsTUFBTSxHQUFhLElBQUk7UUFDM0IsSUFBSSxLQUFLLEtBQUssSUFBSSxFQUFFOztnQkFDWixNQUFNLEdBQTBDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQztZQUN4RixNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztZQUN2QixLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztTQUN0QjtRQUNELElBQUksTUFBTSxLQUFLLElBQUksRUFBRTtZQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDbkQ7YUFBTTtZQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3ZEO0lBQ0gsQ0FBQzs7Ozs7OztJQUVELG1DQUFnQjs7Ozs7O0lBQWhCLFVBQWlCLElBQWMsRUFBRSxNQUFjLEVBQUUsS0FBc0I7UUFBdEIsc0JBQUEsRUFBQSxZQUFzQjtRQUNyRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDeEQsQ0FBQzs7Ozs7OztJQUVELGlDQUFjOzs7Ozs7SUFBZCxVQUFlLElBQWMsRUFBRSxNQUFjLEVBQUUsS0FBc0I7UUFBdEIsc0JBQUEsRUFBQSxZQUFzQjtRQUNuRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7Ozs7Ozs7SUFFRCw4QkFBVzs7Ozs7OztJQUFYLFVBQVksSUFBYyxFQUFFLEtBQWEsRUFBRSxJQUFZLEVBQUUsS0FBc0I7UUFBdEIsc0JBQUEsRUFBQSxZQUFzQjs7WUFDekUsTUFBTSxHQUFhLElBQUk7UUFDM0IsSUFBSSxLQUFLLEtBQUssSUFBSSxFQUFFOztnQkFDWixNQUFNLEdBQTBDLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQztZQUN2RixNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztZQUN2QixLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztTQUN0QjtRQUNELElBQUksTUFBTSxLQUFLLElBQUksRUFBRTtZQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3ZEO2FBQU07WUFDTCxNQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztTQUNyRDtJQUNILENBQUM7Ozs7Ozs7O0lBRUQsa0NBQWU7Ozs7Ozs7SUFBZixVQUFnQixJQUFjLEVBQUUsS0FBYSxFQUFFLEtBQWEsRUFBRSxLQUFzQjtRQUF0QixzQkFBQSxFQUFBLFlBQXNCO1FBQ2xGLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Ozs7Ozs7SUFFRCxnQ0FBYTs7Ozs7OztJQUFiLFVBQWMsSUFBYyxFQUFFLEtBQWEsRUFBRSxLQUFhLEVBQUUsS0FBc0I7UUFBdEIsc0JBQUEsRUFBQSxZQUFzQjtRQUNoRixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7Ozs7SUFFRCx5QkFBTTs7Ozs7O0lBQU4sVUFBTyxHQUFXLEVBQUUsS0FBc0IsRUFBRSxJQUFZO1FBQXBDLHNCQUFBLEVBQUEsWUFBc0I7O1lBQ3BDLE1BQU0sR0FBYSxJQUFJO1FBQzNCLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTs7Z0JBQ1osTUFBTSxHQUEwQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUM7WUFDckYsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7WUFDdkIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7U0FDdEI7UUFDRCxJQUFJLE1BQU0sS0FBSyxJQUFJLEVBQUU7WUFDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDN0M7YUFBTTtZQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDeEM7SUFDSCxDQUFDOzs7Ozs7SUFFRCwrQkFBWTs7Ozs7SUFBWixVQUFhLElBQVksRUFBRSxLQUFzQjtRQUF0QixzQkFBQSxFQUFBLFlBQXNCO1FBQy9DLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7Ozs7SUFFRCw2QkFBVTs7Ozs7SUFBVixVQUFXLEVBQVUsRUFBRSxLQUFzQjtRQUF0QixzQkFBQSxFQUFBLFlBQXNCO1FBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMvQixDQUFDOzs7Ozs7SUFFRCx3QkFBSzs7Ozs7SUFBTCxVQUFNLEdBQVcsRUFBRSxJQUFZO1FBQzdCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQy9DLENBQUM7Ozs7O0lBRUQsOEJBQVc7Ozs7SUFBWCxVQUFZLElBQVk7UUFDdEIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNsQyxDQUFDOzs7OztJQUVELDRCQUFTOzs7O0lBQVQsVUFBVSxFQUFVO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7Ozs7O0lBRUQsa0NBQWU7Ozs7O0lBQWYsVUFBZ0IsR0FBVyxFQUFFLElBQVk7O1lBQ25DLEtBQUssR0FBUSxJQUFJOztZQUNqQixNQUFNLEdBQVEsSUFBSTs7WUFDaEIsTUFBTSxHQUFVLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBbkIsQ0FBbUIsQ0FBQztRQUNuRSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3JCLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbkI7YUFBTTs7Z0JBQ0MsT0FBTyxHQUFVLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxDQUFDLFNBQVMsS0FBSyxJQUFJLEVBQXhCLENBQXdCLENBQUM7WUFDekUsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O29CQUNqQyxLQUFLLEdBQWdDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUM7Z0JBQ3ZGLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7b0JBQ3ZCLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO29CQUNwQixNQUFNLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNwQixNQUFNO2lCQUNQO2FBQ0Y7U0FDRjtRQUNELE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQztJQUMxQyxDQUFDOzs7OztJQUVELHdDQUFxQjs7OztJQUFyQixVQUFzQixJQUFZO1FBQ2hDLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDNUMsQ0FBQzs7Ozs7SUFFRCxzQ0FBbUI7Ozs7SUFBbkIsVUFBb0IsRUFBVTtRQUM1QixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3hDLENBQUM7Ozs7SUFFRCxrQ0FBZTs7O0lBQWY7UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFBLEtBQUs7WUFDdkIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNILGVBQUM7QUFBRCxDQUFDLEFBdElELElBc0lDOzs7O0lBcklhLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi9mb3JtLWJhc2UnO1xuaW1wb3J0IHsgRm9ybSB9IGZyb20gJy4vZm9ybSc7XG5cblxuZXhwb3J0IGNsYXNzIEZpZWxkU2V0IHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzZXQ6IEZvcm1CYXNlW10pIHsgfVxuXG4gIHNldEZvcm0oZm9ybTogRm9ybSkge1xuICAgIHRoaXMuc2V0LmZvckVhY2goKGZpZWxkKSA9PiB7XG4gICAgICBmaWVsZC5zZXRGb3JtKGZvcm0pO1xuICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlVmFsdWVzKCk6IHZvaWQge1xuICAgIHRoaXMuc2V0LmZvckVhY2goKGZpZWxkKSA9PiB7XG4gICAgICBmaWVsZC5kZXJpdmVWYWx1ZSgpO1xuICAgIH0pO1xuICB9XG5cbiAgZ2V0IGZpZWxkcygpIHtcbiAgICByZXR1cm4gdGhpcy5zZXQ7XG4gIH1cblxuICBwdXNoKGl0ZW06IEZvcm1CYXNlKTogdm9pZCB7XG4gICAgdGhpcy5zZXQucHVzaChpdGVtKTtcbiAgfVxuXG4gIGluc2VydEJlZm9yZShpdGVtOiBGb3JtQmFzZSwgYmVmb3JlOiBzdHJpbmcsIGF0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIGxldCBwYXJlbnQ6IEZvcm1CYXNlID0gbnVsbDtcbiAgICBpZiAoZmllbGQgPT09IG51bGwpIHtcbiAgICAgIGNvbnN0IHNlYXJjaDogeyBmaWVsZDogRm9ybUJhc2UsIHBhcmVudDogRm9ybUJhc2UgfSA9IHRoaXMuZmllbGRXaXRoUGFyZW50KGJlZm9yZSwgYXR0cik7XG4gICAgICBwYXJlbnQgPSBzZWFyY2gucGFyZW50O1xuICAgICAgZmllbGQgPSBzZWFyY2guZmllbGQ7XG4gICAgfVxuICAgIGlmIChwYXJlbnQgPT09IG51bGwpIHtcbiAgICAgIHRoaXMuc2V0LnNwbGljZSh0aGlzLnNldC5pbmRleE9mKGZpZWxkKSwgMCwgaXRlbSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHBhcmVudC5maWVsZHMuaW5zZXJ0QmVmb3JlKGl0ZW0sIGJlZm9yZSwgYXR0ciwgZmllbGQpO1xuICAgIH1cbiAgfVxuXG4gIGluc2VydEJlZm9yZU5hbWUoaXRlbTogRm9ybUJhc2UsIGJlZm9yZTogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKTogdm9pZCB7XG4gICAgcmV0dXJuIHRoaXMuaW5zZXJ0QmVmb3JlKGl0ZW0sIGJlZm9yZSwgJ25hbWUnLCBmaWVsZCk7XG4gIH1cblxuICBpbnNlcnRCZWZvcmVJZChpdGVtOiBGb3JtQmFzZSwgYmVmb3JlOiBzdHJpbmcsIGZpZWxkOiBGb3JtQmFzZSA9IG51bGwpOiB2b2lkIHtcbiAgICByZXR1cm4gdGhpcy5pbnNlcnRCZWZvcmUoaXRlbSwgYmVmb3JlLCAnaWQnLCBmaWVsZCk7XG4gIH1cblxuICBpbnNlcnRBZnRlcihpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgYXR0cjogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKTogdm9pZCB7XG4gICAgbGV0IHBhcmVudDogRm9ybUJhc2UgPSBudWxsO1xuICAgIGlmIChmaWVsZCA9PT0gbnVsbCkge1xuICAgICAgY29uc3Qgc2VhcmNoOiB7IGZpZWxkOiBGb3JtQmFzZSwgcGFyZW50OiBGb3JtQmFzZSB9ID0gdGhpcy5maWVsZFdpdGhQYXJlbnQoYWZ0ZXIsIGF0dHIpO1xuICAgICAgcGFyZW50ID0gc2VhcmNoLnBhcmVudDtcbiAgICAgIGZpZWxkID0gc2VhcmNoLmZpZWxkO1xuICAgIH1cbiAgICBpZiAocGFyZW50ID09PSBudWxsKSB7XG4gICAgICB0aGlzLnNldC5zcGxpY2UodGhpcy5zZXQuaW5kZXhPZihmaWVsZCkgKyAxLCAwLCBpdGVtKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGFyZW50LmZpZWxkcy5pbnNlcnRBZnRlcihpdGVtLCBhZnRlciwgYXR0ciwgZmllbGQpO1xuICAgIH1cbiAgfVxuXG4gIGluc2VydEFmdGVyTmFtZShpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgX2F0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIHRoaXMuaW5zZXJ0QWZ0ZXIoaXRlbSwgYWZ0ZXIsICduYW1lJywgZmllbGQpO1xuICB9XG5cbiAgaW5zZXJ0QWZ0ZXJJZChpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgX2F0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIHRoaXMuaW5zZXJ0QWZ0ZXIoaXRlbSwgYWZ0ZXIsICdpZCcsIGZpZWxkKTtcbiAgfVxuXG4gIHJlbW92ZShrZXk6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCwgYXR0cjogc3RyaW5nKSB7XG4gICAgbGV0IHBhcmVudDogRm9ybUJhc2UgPSBudWxsO1xuICAgIGlmIChmaWVsZCA9PT0gbnVsbCkge1xuICAgICAgY29uc3Qgc2VhcmNoOiB7IGZpZWxkOiBGb3JtQmFzZSwgcGFyZW50OiBGb3JtQmFzZSB9ID0gdGhpcy5maWVsZFdpdGhQYXJlbnQoa2V5LCBhdHRyKTtcbiAgICAgIHBhcmVudCA9IHNlYXJjaC5wYXJlbnQ7XG4gICAgICBmaWVsZCA9IHNlYXJjaC5maWVsZDtcbiAgICB9XG4gICAgaWYgKHBhcmVudCA9PT0gbnVsbCkge1xuICAgICAgdGhpcy5zZXQuc3BsaWNlKHRoaXMuc2V0LmluZGV4T2YoZmllbGQpLCAxKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGFyZW50LmZpZWxkcy5yZW1vdmUoa2V5LCBmaWVsZCwgYXR0cik7XG4gICAgfVxuICB9XG5cbiAgcmVtb3ZlQnlOYW1lKG5hbWU6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCkge1xuICAgIHRoaXMucmVtb3ZlKG5hbWUsIGZpZWxkLCAnbmFtZScpO1xuICB9XG5cbiAgcmVtb3ZlQnlJZChpZDogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKSB7XG4gICAgdGhpcy5yZW1vdmUoaWQsIGZpZWxkLCAnaWQnKTtcbiAgfVxuXG4gIGZpZWxkKGtleTogc3RyaW5nLCBhdHRyOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLmZpZWxkV2l0aFBhcmVudChrZXksIGF0dHIpLmZpZWxkO1xuICB9XG5cbiAgZmllbGRCeU5hbWUobmFtZTogc3RyaW5nKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZChuYW1lLCAnbmFtZScpO1xuICB9XG5cbiAgZmllbGRCeUlkKGlkOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLmZpZWxkKGlkLCAnaWQnKTtcbiAgfVxuXG4gIGZpZWxkV2l0aFBhcmVudChrZXk6IHN0cmluZywgYXR0cjogc3RyaW5nKTogeyBmaWVsZDogYW55LCBwYXJlbnQ6IGFueSB9IHtcbiAgICBsZXQgZmllbGQ6IGFueSA9IG51bGw7XG4gICAgbGV0IHBhcmVudDogYW55ID0gbnVsbDtcbiAgICBjb25zdCBzZWFyY2g6IGFueVtdID0gdGhpcy5zZXQuZmlsdGVyKGNoaWxkID0+IGNoaWxkW2F0dHJdID09PSBrZXkpO1xuICAgIGlmIChzZWFyY2gubGVuZ3RoID4gMCkge1xuICAgICAgZmllbGQgPSBzZWFyY2hbMF07XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IHBhcmVudHM6IGFueVtdID0gdGhpcy5zZXQuZmlsdGVyKGNoaWxkID0+IGNoaWxkLmhhc0ZpZWxkcyA9PT0gdHJ1ZSk7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHBhcmVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgY29uc3QgY2hlY2s6IHsgZmllbGQ6IGFueSwgcGFyZW50OiBhbnkgfSA9IHBhcmVudHNbaV0uZmllbGRzLmZpZWxkV2l0aFBhcmVudChrZXksIGF0dHIpO1xuICAgICAgICBpZiAoY2hlY2suZmllbGQgIT0gbnVsbCkge1xuICAgICAgICAgIGZpZWxkID0gY2hlY2suZmllbGQ7XG4gICAgICAgICAgcGFyZW50ID0gcGFyZW50c1tpXTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4geyBmaWVsZDogZmllbGQsIHBhcmVudDogcGFyZW50IH07XG4gIH1cblxuICBmaWVsZEJ5TmFtZVdpdGhQYXJlbnQobmFtZTogc3RyaW5nKTogeyBmaWVsZDogYW55LCBwYXJlbnQ6IGFueSB9IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZFdpdGhQYXJlbnQobmFtZSwgJ25hbWUnKTtcbiAgfVxuXG4gIGZpZWxkQnlJZFdpdGhQYXJlbnQoaWQ6IHN0cmluZyk6IHsgZmllbGQ6IGFueSwgcGFyZW50OiBhbnkgfSB7XG4gICAgcmV0dXJuIHRoaXMuZmllbGRXaXRoUGFyZW50KGlkLCAnaWQnKTtcbiAgfVxuXG4gIHJlc2V0VmFsaWRhdGlvbigpOiB2b2lkIHtcbiAgICB0aGlzLmZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcbiAgICAgIGZpZWxkLnJlc2V0VmFsaWRhdGlvbigpO1xuICAgIH0pO1xuICB9XG59XG4iXX0=