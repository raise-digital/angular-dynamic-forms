import { OptionSetField } from './option-set-field';
export declare class DropdownField extends OptionSetField<string> {
    type: string;
    emptyText: string;
    loaded: string;
    constructor(options?: {});
    setOptions(options: Array<{
        key: string;
        value: string;
        disabled?: boolean;
    }>): void;
}
