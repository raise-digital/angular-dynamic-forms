import { OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Form } from '../form';
import { FormService } from '../services/form.service';
export declare class FormComponent implements OnInit {
    private formService;
    form: Form;
    constructor(formService: FormService);
    ngOnInit(): void;
    readonly group: FormGroup;
    onSubmit(): void;
}
