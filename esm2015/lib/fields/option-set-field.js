/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Field } from './field';
/**
 * @template T
 */
export class OptionSetField extends Field {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'optionset';
        this.optionSet = [];
        this.options = [];
        this.classes.push('ndf-optionset');
        this.optionSet = options['options'] || [];
        Promise.resolve(this.optionSet).then(optionList => {
            this.setOptions(optionList);
        });
    }
    /**
     * @param {?} options
     * @return {?}
     */
    setOptions(options) {
        this.options = options;
    }
}
if (false) {
    /** @type {?} */
    OptionSetField.prototype.type;
    /** @type {?} */
    OptionSetField.prototype.optionSet;
    /** @type {?} */
    OptionSetField.prototype.options;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3B0aW9uLXNldC1maWVsZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9maWVsZHMvb3B0aW9uLXNldC1maWVsZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFNBQVMsQ0FBQzs7OztBQUVoQyxNQUFNLE9BQU8sY0FBa0IsU0FBUSxLQUFROzs7O0lBSzdDLFlBQVksVUFBYyxFQUFFO1FBQzFCLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUxqQixTQUFJLEdBQUcsV0FBVyxDQUFDO1FBQ25CLGNBQVMsR0FBeUQsRUFBRSxDQUFDO1FBQ3JFLFlBQU8sR0FBeUQsRUFBRSxDQUFDO1FBSWpFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDaEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLE9BQWtFO1FBQzNFLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0lBQ3pCLENBQUM7Q0FDRjs7O0lBaEJDLDhCQUFtQjs7SUFDbkIsbUNBQXFFOztJQUNyRSxpQ0FBbUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4vZmllbGQnO1xuXG5leHBvcnQgY2xhc3MgT3B0aW9uU2V0RmllbGQ8VD4gZXh0ZW5kcyBGaWVsZDxUPiB7XG4gIHR5cGUgPSAnb3B0aW9uc2V0JztcbiAgb3B0aW9uU2V0OiB7IGtleTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nLCBkaXNhYmxlZD86IGJvb2xlYW4gfVtdID0gW107XG4gIG9wdGlvbnM6IHsga2V5OiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcsIGRpc2FibGVkPzogYm9vbGVhbiB9W10gPSBbXTtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1vcHRpb25zZXQnKTtcbiAgICB0aGlzLm9wdGlvblNldCA9IG9wdGlvbnNbJ29wdGlvbnMnXSB8fCBbXTtcbiAgICBQcm9taXNlLnJlc29sdmUodGhpcy5vcHRpb25TZXQpLnRoZW4ob3B0aW9uTGlzdCA9PiB7XG4gICAgICB0aGlzLnNldE9wdGlvbnMob3B0aW9uTGlzdCk7XG4gICAgfSk7XG4gIH1cblxuICBzZXRPcHRpb25zKG9wdGlvbnM6IEFycmF5PHsga2V5OiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcsIGRpc2FibGVkPzogYm9vbGVhbiB9Pikge1xuICAgIHRoaXMub3B0aW9ucyA9IG9wdGlvbnM7XG4gIH1cbn1cbiJdfQ==