/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Field } from './field';
export class TextField extends Field {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.type = 'text';
        this.value = options['value'] || '';
        this.classes.push('ndf-text');
        this.inputType = options['inputType'] || '';
    }
}
if (false) {
    /** @type {?} */
    TextField.prototype.type;
    /** @type {?} */
    TextField.prototype.inputType;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1maWVsZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9maWVsZHMvdGV4dC1maWVsZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUVoQyxNQUFNLE9BQU8sU0FBVSxTQUFRLEtBQWE7Ozs7SUFJMUMsWUFBWSxVQUFjLEVBQUU7UUFDMUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBSmpCLFNBQUksR0FBRyxNQUFNLENBQUM7UUFLWixJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzlDLENBQUM7Q0FDRjs7O0lBVEMseUJBQWM7O0lBQ2QsOEJBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuL2ZpZWxkJztcblxuZXhwb3J0IGNsYXNzIFRleHRGaWVsZCBleHRlbmRzIEZpZWxkPHN0cmluZz4ge1xuICB0eXBlID0gJ3RleHQnO1xuICBpbnB1dFR5cGU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy52YWx1ZSA9IG9wdGlvbnNbJ3ZhbHVlJ10gfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi10ZXh0Jyk7XG4gICAgdGhpcy5pbnB1dFR5cGUgPSBvcHRpb25zWydpbnB1dFR5cGUnXSB8fCAnJztcbiAgfVxufVxuIl19