import { Field } from './field';
export declare class TextField extends Field<string> {
    type: string;
    inputType: string;
    constructor(options?: {});
}
