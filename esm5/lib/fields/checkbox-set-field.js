/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { OptionSetField } from './option-set-field';
/**
 * @template T
 */
var /**
 * @template T
 */
CheckboxSetField = /** @class */ (function (_super) {
    tslib_1.__extends(CheckboxSetField, _super);
    function CheckboxSetField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'checkboxset';
        _this.classes.push('ndf-checkboxset');
        return _this;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    CheckboxSetField.prototype.onChange = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        /** @type {?} */
        var obj = this.form.obj[this.name];
        if (this.isChecked(value)) {
            obj.splice(obj.indexOf(value), 1);
        }
        else {
            obj.push(value);
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    CheckboxSetField.prototype.isChecked = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        /** @type {?} */
        var selected = this.form.obj[this.name];
        if (selected != null) {
            return selected.filter(function (o) { return o === value; }).length > 0;
        }
        return false;
    };
    return CheckboxSetField;
}(OptionSetField));
/**
 * @template T
 */
export { CheckboxSetField };
if (false) {
    /** @type {?} */
    CheckboxSetField.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3gtc2V0LWZpZWxkLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2ZpZWxkcy9jaGVja2JveC1zZXQtZmllbGQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7Ozs7QUFHcEQ7Ozs7SUFBeUMsNENBQWlCO0lBR3hELDBCQUFZLE9BQWdCO1FBQWhCLHdCQUFBLEVBQUEsWUFBZ0I7UUFBNUIsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FFZjtRQUxELFVBQUksR0FBRyxhQUFhLENBQUM7UUFJbkIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzs7SUFDdkMsQ0FBQzs7Ozs7SUFFRCxtQ0FBUTs7OztJQUFSLFVBQVMsS0FBSzs7WUFDTixHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNwQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDekIsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ25DO2FBQU07WUFDTCxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2pCO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxvQ0FBUzs7OztJQUFULFVBQVUsS0FBSzs7WUFDUCxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QyxJQUFJLFFBQVEsSUFBSSxJQUFJLEVBQUU7WUFDcEIsT0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxLQUFLLEtBQUssRUFBWCxDQUFXLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1NBQ3JEO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBQ0gsdUJBQUM7QUFBRCxDQUFDLEFBeEJELENBQXlDLGNBQWMsR0F3QnREOzs7Ozs7O0lBdkJDLGdDQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9wdGlvblNldEZpZWxkIH0gZnJvbSAnLi9vcHRpb24tc2V0LWZpZWxkJztcblxuXG5leHBvcnQgY2xhc3MgQ2hlY2tib3hTZXRGaWVsZDxUPiBleHRlbmRzIE9wdGlvblNldEZpZWxkPFQ+IHtcbiAgdHlwZSA9ICdjaGVja2JveHNldCc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtY2hlY2tib3hzZXQnKTtcbiAgfVxuXG4gIG9uQ2hhbmdlKHZhbHVlKSB7XG4gICAgY29uc3Qgb2JqID0gdGhpcy5mb3JtLm9ialt0aGlzLm5hbWVdO1xuICAgIGlmICh0aGlzLmlzQ2hlY2tlZCh2YWx1ZSkpIHtcbiAgICAgIG9iai5zcGxpY2Uob2JqLmluZGV4T2YodmFsdWUpLCAxKTtcbiAgICB9IGVsc2Uge1xuICAgICAgb2JqLnB1c2godmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIGlzQ2hlY2tlZCh2YWx1ZSk6IGJvb2xlYW4ge1xuICAgIGNvbnN0IHNlbGVjdGVkID0gdGhpcy5mb3JtLm9ialt0aGlzLm5hbWVdO1xuICAgIGlmIChzZWxlY3RlZCAhPSBudWxsKSB7XG4gICAgICByZXR1cm4gc2VsZWN0ZWQuZmlsdGVyKG8gPT4gbyA9PT0gdmFsdWUpLmxlbmd0aCA+IDA7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxufVxuIl19