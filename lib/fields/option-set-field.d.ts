import { Field } from './field';
export declare class OptionSetField<T> extends Field<T> {
    type: string;
    optionSet: {
        key: string;
        value: string;
        disabled?: boolean;
    }[];
    options: {
        key: string;
        value: string;
        disabled?: boolean;
    }[];
    constructor(options?: {});
    setOptions(options: Array<{
        key: string;
        value: string;
        disabled?: boolean;
    }>): void;
}
