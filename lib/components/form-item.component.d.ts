import { OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBase } from '../form-base';
import { FormService } from '../services/form.service';
export declare class FormItemComponent implements OnInit, OnDestroy {
    private formService;
    item: FormBase;
    constructor(formService: FormService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    readonly group: FormGroup;
    onChange(value: any): void;
    onBlur(value: any): void;
    onClick(): void;
}
