/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { DropdownField } from '../fields/dropdown-field';
import { FormService } from '../services/form.service';
var DropdownFieldComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DropdownFieldComponent, _super);
    function DropdownFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DropdownFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-dropdown-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <select [id]=\"item.id\" [formControlName]=\"item.id\" [value]=\"item.value ? item.value : ''\" (ngModelChange)=\"onChange($event)\">\n        <option *ngIf=\"item.emptyText && item.options.length < 1\" value=\"\">{{ item.emptyText }}</option>\n        <option *ngIf=\"item.placeholder && item.options.length > 0\" value=\"\">{{ item.placeholder }}</option>\n        <option *ngFor=\"let option of item.options\" [value]=\"option.key\" [disabled]=\"option.disabled\">{{ option.value }}</option>\n    </select>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    DropdownFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return DropdownFieldComponent;
}(FormItemComponent));
export { DropdownFieldComponent };
if (false) {
    /** @type {?} */
    DropdownFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tZmllbGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZHJvcGRvd24tZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFakQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUV2RDtJQUs0QyxrREFBaUI7SUFMN0Q7O0lBT0EsQ0FBQzs7Z0JBUEEsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLDJ2QkFBOEM7b0JBQzlDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtpQkFDM0I7Ozt1QkFFRSxLQUFLOztJQUNSLDZCQUFDO0NBQUEsQUFQRCxDQUs0QyxpQkFBaUIsR0FFNUQ7U0FGWSxzQkFBc0I7OztJQUNqQyxzQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IERyb3Bkb3duRmllbGQgfSBmcm9tICcuLi9maWVsZHMvZHJvcGRvd24tZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtZHJvcGRvd24tZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vZHJvcGRvd24tZmllbGQuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBEcm9wZG93bkZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBEcm9wZG93bkZpZWxkO1xufVxuIl19