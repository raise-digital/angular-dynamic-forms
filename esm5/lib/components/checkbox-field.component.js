/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { CheckboxField } from '../fields/checkbox-field';
import { FormService } from '../services/form.service';
var CheckboxFieldComponent = /** @class */ (function (_super) {
    tslib_1.__extends(CheckboxFieldComponent, _super);
    function CheckboxFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CheckboxFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-checkbox-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label>\n        <div class=\"box\"><fa *ngIf=\"item.value\" name=\"check\" size=\"lt\"></fa></div>\n        <input type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.value\" (ngModelChange)=\"onChange($event)\" />\n        {{item.label}}\n    </label>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{item.message}}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    CheckboxFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return CheckboxFieldComponent;
}(FormItemComponent));
export { CheckboxFieldComponent };
if (false) {
    /** @type {?} */
    CheckboxFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3gtZmllbGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY2hlY2tib3gtZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFakQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUV2RDtJQUs0QyxrREFBaUI7SUFMN0Q7O0lBT0EsQ0FBQzs7Z0JBUEEsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLG9hQUE4QztvQkFDOUMsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO2lCQUMzQjs7O3VCQUVFLEtBQUs7O0lBQ1IsNkJBQUM7Q0FBQSxBQVBELENBSzRDLGlCQUFpQixHQUU1RDtTQUZZLHNCQUFzQjs7O0lBQ2pDLHNDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2hlY2tib3hGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9jaGVja2JveC1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1jaGVja2JveC1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9jaGVja2JveC1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIENoZWNrYm94RmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IENoZWNrYm94RmllbGQ7XG59XG4iXX0=