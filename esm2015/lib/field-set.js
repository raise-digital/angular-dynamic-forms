/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
export class FieldSet {
    /**
     * @param {?} set
     */
    constructor(set) {
        this.set = set;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    setForm(form) {
        this.set.forEach((field) => {
            field.setForm(form);
        });
    }
    /**
     * @return {?}
     */
    updateValues() {
        this.set.forEach((field) => {
            field.deriveValue();
        });
    }
    /**
     * @return {?}
     */
    get fields() {
        return this.set;
    }
    /**
     * @param {?} item
     * @return {?}
     */
    push(item) {
        this.set.push(item);
    }
    /**
     * @param {?} item
     * @param {?} before
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    insertBefore(item, before, attr, field = null) {
        /** @type {?} */
        let parent = null;
        if (field === null) {
            /** @type {?} */
            const search = this.fieldWithParent(before, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field), 0, item);
        }
        else {
            parent.fields.insertBefore(item, before, attr, field);
        }
    }
    /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    insertBeforeName(item, before, field = null) {
        return this.insertBefore(item, before, 'name', field);
    }
    /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    insertBeforeId(item, before, field = null) {
        return this.insertBefore(item, before, 'id', field);
    }
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    insertAfter(item, after, attr, field = null) {
        /** @type {?} */
        let parent = null;
        if (field === null) {
            /** @type {?} */
            const search = this.fieldWithParent(after, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field) + 1, 0, item);
        }
        else {
            parent.fields.insertAfter(item, after, attr, field);
        }
    }
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    insertAfterName(item, after, _attr, field = null) {
        this.insertAfter(item, after, 'name', field);
    }
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    insertAfterId(item, after, _attr, field = null) {
        this.insertAfter(item, after, 'id', field);
    }
    /**
     * @param {?} key
     * @param {?=} field
     * @param {?=} attr
     * @return {?}
     */
    remove(key, field = null, attr) {
        /** @type {?} */
        let parent = null;
        if (field === null) {
            /** @type {?} */
            const search = this.fieldWithParent(key, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field), 1);
        }
        else {
            parent.fields.remove(key, field, attr);
        }
    }
    /**
     * @param {?} name
     * @param {?=} field
     * @return {?}
     */
    removeByName(name, field = null) {
        this.remove(name, field, 'name');
    }
    /**
     * @param {?} id
     * @param {?=} field
     * @return {?}
     */
    removeById(id, field = null) {
        this.remove(id, field, 'id');
    }
    /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    field(key, attr) {
        return this.fieldWithParent(key, attr).field;
    }
    /**
     * @param {?} name
     * @return {?}
     */
    fieldByName(name) {
        return this.field(name, 'name');
    }
    /**
     * @param {?} id
     * @return {?}
     */
    fieldById(id) {
        return this.field(id, 'id');
    }
    /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    fieldWithParent(key, attr) {
        /** @type {?} */
        let field = null;
        /** @type {?} */
        let parent = null;
        /** @type {?} */
        const search = this.set.filter(child => child[attr] === key);
        if (search.length > 0) {
            field = search[0];
        }
        else {
            /** @type {?} */
            const parents = this.set.filter(child => child.hasFields === true);
            for (let i = 0; i < parents.length; i++) {
                /** @type {?} */
                const check = parents[i].fields.fieldWithParent(key, attr);
                if (check.field != null) {
                    field = check.field;
                    parent = parents[i];
                    break;
                }
            }
        }
        return { field: field, parent: parent };
    }
    /**
     * @param {?} name
     * @return {?}
     */
    fieldByNameWithParent(name) {
        return this.fieldWithParent(name, 'name');
    }
    /**
     * @param {?} id
     * @return {?}
     */
    fieldByIdWithParent(id) {
        return this.fieldWithParent(id, 'id');
    }
    /**
     * @return {?}
     */
    resetValidation() {
        this.fields.forEach(field => {
            field.resetValidation();
        });
    }
}
if (false) {
    /** @type {?} */
    FieldSet.prototype.set;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtc2V0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2ZpZWxkLXNldC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBSUEsTUFBTSxPQUFPLFFBQVE7Ozs7SUFDbkIsWUFBb0IsR0FBZTtRQUFmLFFBQUcsR0FBSCxHQUFHLENBQVk7SUFBSSxDQUFDOzs7OztJQUV4QyxPQUFPLENBQUMsSUFBVTtRQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ3pCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDekIsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELElBQUksTUFBTTtRQUNSLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQztJQUNsQixDQUFDOzs7OztJQUVELElBQUksQ0FBQyxJQUFjO1FBQ2pCLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3RCLENBQUM7Ozs7Ozs7O0lBRUQsWUFBWSxDQUFDLElBQWMsRUFBRSxNQUFjLEVBQUUsSUFBWSxFQUFFLFFBQWtCLElBQUk7O1lBQzNFLE1BQU0sR0FBYSxJQUFJO1FBQzNCLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTs7a0JBQ1osTUFBTSxHQUEwQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUM7WUFDeEYsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7WUFDdkIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7U0FDdEI7UUFDRCxJQUFJLE1BQU0sS0FBSyxJQUFJLEVBQUU7WUFDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ25EO2FBQU07WUFDTCxNQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztTQUN2RDtJQUNILENBQUM7Ozs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxJQUFjLEVBQUUsTUFBYyxFQUFFLFFBQWtCLElBQUk7UUFDckUsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3hELENBQUM7Ozs7Ozs7SUFFRCxjQUFjLENBQUMsSUFBYyxFQUFFLE1BQWMsRUFBRSxRQUFrQixJQUFJO1FBQ25FLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztJQUN0RCxDQUFDOzs7Ozs7OztJQUVELFdBQVcsQ0FBQyxJQUFjLEVBQUUsS0FBYSxFQUFFLElBQVksRUFBRSxRQUFrQixJQUFJOztZQUN6RSxNQUFNLEdBQWEsSUFBSTtRQUMzQixJQUFJLEtBQUssS0FBSyxJQUFJLEVBQUU7O2tCQUNaLE1BQU0sR0FBMEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO1lBQ3ZGLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO1lBQ3ZCLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxNQUFNLEtBQUssSUFBSSxFQUFFO1lBQ25CLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDdkQ7YUFBTTtZQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3JEO0lBQ0gsQ0FBQzs7Ozs7Ozs7SUFFRCxlQUFlLENBQUMsSUFBYyxFQUFFLEtBQWEsRUFBRSxLQUFhLEVBQUUsUUFBa0IsSUFBSTtRQUNsRixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQy9DLENBQUM7Ozs7Ozs7O0lBRUQsYUFBYSxDQUFDLElBQWMsRUFBRSxLQUFhLEVBQUUsS0FBYSxFQUFFLFFBQWtCLElBQUk7UUFDaEYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7Ozs7O0lBRUQsTUFBTSxDQUFDLEdBQVcsRUFBRSxRQUFrQixJQUFJLEVBQUUsSUFBWTs7WUFDbEQsTUFBTSxHQUFhLElBQUk7UUFDM0IsSUFBSSxLQUFLLEtBQUssSUFBSSxFQUFFOztrQkFDWixNQUFNLEdBQTBDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQztZQUNyRixNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztZQUN2QixLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztTQUN0QjtRQUNELElBQUksTUFBTSxLQUFLLElBQUksRUFBRTtZQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUM3QzthQUFNO1lBQ0wsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztTQUN4QztJQUNILENBQUM7Ozs7OztJQUVELFlBQVksQ0FBQyxJQUFZLEVBQUUsUUFBa0IsSUFBSTtRQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7O0lBRUQsVUFBVSxDQUFDLEVBQVUsRUFBRSxRQUFrQixJQUFJO1FBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMvQixDQUFDOzs7Ozs7SUFFRCxLQUFLLENBQUMsR0FBVyxFQUFFLElBQVk7UUFDN0IsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDL0MsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsSUFBWTtRQUN0QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Ozs7O0lBRUQsU0FBUyxDQUFDLEVBQVU7UUFDbEIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7Ozs7SUFFRCxlQUFlLENBQUMsR0FBVyxFQUFFLElBQVk7O1lBQ25DLEtBQUssR0FBUSxJQUFJOztZQUNqQixNQUFNLEdBQVEsSUFBSTs7Y0FDaEIsTUFBTSxHQUFVLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQztRQUNuRSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3JCLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbkI7YUFBTTs7a0JBQ0MsT0FBTyxHQUFVLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUM7WUFDekUsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O3NCQUNqQyxLQUFLLEdBQWdDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUM7Z0JBQ3ZGLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7b0JBQ3ZCLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO29CQUNwQixNQUFNLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNwQixNQUFNO2lCQUNQO2FBQ0Y7U0FDRjtRQUNELE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQztJQUMxQyxDQUFDOzs7OztJQUVELHFCQUFxQixDQUFDLElBQVk7UUFDaEMsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztJQUM1QyxDQUFDOzs7OztJQUVELG1CQUFtQixDQUFDLEVBQVU7UUFDNUIsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN4QyxDQUFDOzs7O0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzFCLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Q0FDRjs7O0lBcklhLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi9mb3JtLWJhc2UnO1xuaW1wb3J0IHsgRm9ybSB9IGZyb20gJy4vZm9ybSc7XG5cblxuZXhwb3J0IGNsYXNzIEZpZWxkU2V0IHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzZXQ6IEZvcm1CYXNlW10pIHsgfVxuXG4gIHNldEZvcm0oZm9ybTogRm9ybSkge1xuICAgIHRoaXMuc2V0LmZvckVhY2goKGZpZWxkKSA9PiB7XG4gICAgICBmaWVsZC5zZXRGb3JtKGZvcm0pO1xuICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlVmFsdWVzKCk6IHZvaWQge1xuICAgIHRoaXMuc2V0LmZvckVhY2goKGZpZWxkKSA9PiB7XG4gICAgICBmaWVsZC5kZXJpdmVWYWx1ZSgpO1xuICAgIH0pO1xuICB9XG5cbiAgZ2V0IGZpZWxkcygpIHtcbiAgICByZXR1cm4gdGhpcy5zZXQ7XG4gIH1cblxuICBwdXNoKGl0ZW06IEZvcm1CYXNlKTogdm9pZCB7XG4gICAgdGhpcy5zZXQucHVzaChpdGVtKTtcbiAgfVxuXG4gIGluc2VydEJlZm9yZShpdGVtOiBGb3JtQmFzZSwgYmVmb3JlOiBzdHJpbmcsIGF0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIGxldCBwYXJlbnQ6IEZvcm1CYXNlID0gbnVsbDtcbiAgICBpZiAoZmllbGQgPT09IG51bGwpIHtcbiAgICAgIGNvbnN0IHNlYXJjaDogeyBmaWVsZDogRm9ybUJhc2UsIHBhcmVudDogRm9ybUJhc2UgfSA9IHRoaXMuZmllbGRXaXRoUGFyZW50KGJlZm9yZSwgYXR0cik7XG4gICAgICBwYXJlbnQgPSBzZWFyY2gucGFyZW50O1xuICAgICAgZmllbGQgPSBzZWFyY2guZmllbGQ7XG4gICAgfVxuICAgIGlmIChwYXJlbnQgPT09IG51bGwpIHtcbiAgICAgIHRoaXMuc2V0LnNwbGljZSh0aGlzLnNldC5pbmRleE9mKGZpZWxkKSwgMCwgaXRlbSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHBhcmVudC5maWVsZHMuaW5zZXJ0QmVmb3JlKGl0ZW0sIGJlZm9yZSwgYXR0ciwgZmllbGQpO1xuICAgIH1cbiAgfVxuXG4gIGluc2VydEJlZm9yZU5hbWUoaXRlbTogRm9ybUJhc2UsIGJlZm9yZTogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKTogdm9pZCB7XG4gICAgcmV0dXJuIHRoaXMuaW5zZXJ0QmVmb3JlKGl0ZW0sIGJlZm9yZSwgJ25hbWUnLCBmaWVsZCk7XG4gIH1cblxuICBpbnNlcnRCZWZvcmVJZChpdGVtOiBGb3JtQmFzZSwgYmVmb3JlOiBzdHJpbmcsIGZpZWxkOiBGb3JtQmFzZSA9IG51bGwpOiB2b2lkIHtcbiAgICByZXR1cm4gdGhpcy5pbnNlcnRCZWZvcmUoaXRlbSwgYmVmb3JlLCAnaWQnLCBmaWVsZCk7XG4gIH1cblxuICBpbnNlcnRBZnRlcihpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgYXR0cjogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKTogdm9pZCB7XG4gICAgbGV0IHBhcmVudDogRm9ybUJhc2UgPSBudWxsO1xuICAgIGlmIChmaWVsZCA9PT0gbnVsbCkge1xuICAgICAgY29uc3Qgc2VhcmNoOiB7IGZpZWxkOiBGb3JtQmFzZSwgcGFyZW50OiBGb3JtQmFzZSB9ID0gdGhpcy5maWVsZFdpdGhQYXJlbnQoYWZ0ZXIsIGF0dHIpO1xuICAgICAgcGFyZW50ID0gc2VhcmNoLnBhcmVudDtcbiAgICAgIGZpZWxkID0gc2VhcmNoLmZpZWxkO1xuICAgIH1cbiAgICBpZiAocGFyZW50ID09PSBudWxsKSB7XG4gICAgICB0aGlzLnNldC5zcGxpY2UodGhpcy5zZXQuaW5kZXhPZihmaWVsZCkgKyAxLCAwLCBpdGVtKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGFyZW50LmZpZWxkcy5pbnNlcnRBZnRlcihpdGVtLCBhZnRlciwgYXR0ciwgZmllbGQpO1xuICAgIH1cbiAgfVxuXG4gIGluc2VydEFmdGVyTmFtZShpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgX2F0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIHRoaXMuaW5zZXJ0QWZ0ZXIoaXRlbSwgYWZ0ZXIsICduYW1lJywgZmllbGQpO1xuICB9XG5cbiAgaW5zZXJ0QWZ0ZXJJZChpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgX2F0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIHRoaXMuaW5zZXJ0QWZ0ZXIoaXRlbSwgYWZ0ZXIsICdpZCcsIGZpZWxkKTtcbiAgfVxuXG4gIHJlbW92ZShrZXk6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCwgYXR0cjogc3RyaW5nKSB7XG4gICAgbGV0IHBhcmVudDogRm9ybUJhc2UgPSBudWxsO1xuICAgIGlmIChmaWVsZCA9PT0gbnVsbCkge1xuICAgICAgY29uc3Qgc2VhcmNoOiB7IGZpZWxkOiBGb3JtQmFzZSwgcGFyZW50OiBGb3JtQmFzZSB9ID0gdGhpcy5maWVsZFdpdGhQYXJlbnQoa2V5LCBhdHRyKTtcbiAgICAgIHBhcmVudCA9IHNlYXJjaC5wYXJlbnQ7XG4gICAgICBmaWVsZCA9IHNlYXJjaC5maWVsZDtcbiAgICB9XG4gICAgaWYgKHBhcmVudCA9PT0gbnVsbCkge1xuICAgICAgdGhpcy5zZXQuc3BsaWNlKHRoaXMuc2V0LmluZGV4T2YoZmllbGQpLCAxKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGFyZW50LmZpZWxkcy5yZW1vdmUoa2V5LCBmaWVsZCwgYXR0cik7XG4gICAgfVxuICB9XG5cbiAgcmVtb3ZlQnlOYW1lKG5hbWU6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCkge1xuICAgIHRoaXMucmVtb3ZlKG5hbWUsIGZpZWxkLCAnbmFtZScpO1xuICB9XG5cbiAgcmVtb3ZlQnlJZChpZDogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKSB7XG4gICAgdGhpcy5yZW1vdmUoaWQsIGZpZWxkLCAnaWQnKTtcbiAgfVxuXG4gIGZpZWxkKGtleTogc3RyaW5nLCBhdHRyOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLmZpZWxkV2l0aFBhcmVudChrZXksIGF0dHIpLmZpZWxkO1xuICB9XG5cbiAgZmllbGRCeU5hbWUobmFtZTogc3RyaW5nKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZChuYW1lLCAnbmFtZScpO1xuICB9XG5cbiAgZmllbGRCeUlkKGlkOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLmZpZWxkKGlkLCAnaWQnKTtcbiAgfVxuXG4gIGZpZWxkV2l0aFBhcmVudChrZXk6IHN0cmluZywgYXR0cjogc3RyaW5nKTogeyBmaWVsZDogYW55LCBwYXJlbnQ6IGFueSB9IHtcbiAgICBsZXQgZmllbGQ6IGFueSA9IG51bGw7XG4gICAgbGV0IHBhcmVudDogYW55ID0gbnVsbDtcbiAgICBjb25zdCBzZWFyY2g6IGFueVtdID0gdGhpcy5zZXQuZmlsdGVyKGNoaWxkID0+IGNoaWxkW2F0dHJdID09PSBrZXkpO1xuICAgIGlmIChzZWFyY2gubGVuZ3RoID4gMCkge1xuICAgICAgZmllbGQgPSBzZWFyY2hbMF07XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IHBhcmVudHM6IGFueVtdID0gdGhpcy5zZXQuZmlsdGVyKGNoaWxkID0+IGNoaWxkLmhhc0ZpZWxkcyA9PT0gdHJ1ZSk7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHBhcmVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgY29uc3QgY2hlY2s6IHsgZmllbGQ6IGFueSwgcGFyZW50OiBhbnkgfSA9IHBhcmVudHNbaV0uZmllbGRzLmZpZWxkV2l0aFBhcmVudChrZXksIGF0dHIpO1xuICAgICAgICBpZiAoY2hlY2suZmllbGQgIT0gbnVsbCkge1xuICAgICAgICAgIGZpZWxkID0gY2hlY2suZmllbGQ7XG4gICAgICAgICAgcGFyZW50ID0gcGFyZW50c1tpXTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4geyBmaWVsZDogZmllbGQsIHBhcmVudDogcGFyZW50IH07XG4gIH1cblxuICBmaWVsZEJ5TmFtZVdpdGhQYXJlbnQobmFtZTogc3RyaW5nKTogeyBmaWVsZDogYW55LCBwYXJlbnQ6IGFueSB9IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZFdpdGhQYXJlbnQobmFtZSwgJ25hbWUnKTtcbiAgfVxuXG4gIGZpZWxkQnlJZFdpdGhQYXJlbnQoaWQ6IHN0cmluZyk6IHsgZmllbGQ6IGFueSwgcGFyZW50OiBhbnkgfSB7XG4gICAgcmV0dXJuIHRoaXMuZmllbGRXaXRoUGFyZW50KGlkLCAnaWQnKTtcbiAgfVxuXG4gIHJlc2V0VmFsaWRhdGlvbigpOiB2b2lkIHtcbiAgICB0aGlzLmZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcbiAgICAgIGZpZWxkLnJlc2V0VmFsaWRhdGlvbigpO1xuICAgIH0pO1xuICB9XG59XG4iXX0=