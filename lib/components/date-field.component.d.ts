import { FormItemComponent } from './form-item.component';
import { DateField } from '../fields/date-field';
export declare class DateFieldComponent extends FormItemComponent {
    item: DateField;
}
