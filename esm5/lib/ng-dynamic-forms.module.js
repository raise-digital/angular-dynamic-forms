/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { FormComponent } from './components/form.component';
import { FormItemComponent } from './components/form-item.component';
import { CheckboxFieldComponent } from './components/checkbox-field.component';
import { CheckboxSetFieldComponent } from './components/checkbox-set-field.component';
import { ContentFieldComponent } from './components/content-field.component';
import { DateFieldComponent } from './components/date-field.component';
import { DropdownFieldComponent } from './components/dropdown-field.component';
import { FieldGroupComponent } from './components/field-group.component';
import { FormActionComponent } from './components/form-action.component';
import { HeaderFieldComponent } from './components/header-field.component';
import { OptionSetFieldComponent } from './components/option-set-field.component';
import { TextFieldComponent } from './components/text-field.component';
import { TextareaFieldComponent } from './components/textarea-field.component';
import { FormService } from './services/form.service';
var NgDynamicFormsModule = /** @class */ (function () {
    function NgDynamicFormsModule() {
    }
    /**
     * @return {?}
     */
    NgDynamicFormsModule.forRoot = /**
     * @return {?}
     */
    function () {
        return {
            ngModule: NgDynamicFormsModule,
            providers: [FormService]
        };
    };
    NgDynamicFormsModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        FormsModule,
                        ReactiveFormsModule,
                        AngularFontAwesomeModule,
                        NgxMyDatePickerModule.forRoot()
                    ],
                    declarations: [
                        FormComponent,
                        FormItemComponent,
                        CheckboxFieldComponent,
                        CheckboxSetFieldComponent,
                        ContentFieldComponent,
                        DateFieldComponent,
                        DropdownFieldComponent,
                        FieldGroupComponent,
                        FormActionComponent,
                        HeaderFieldComponent,
                        OptionSetFieldComponent,
                        TextFieldComponent,
                        TextareaFieldComponent
                    ],
                    exports: [
                        FormComponent,
                        FormItemComponent,
                        CheckboxFieldComponent,
                        CheckboxSetFieldComponent,
                        ContentFieldComponent,
                        DateFieldComponent,
                        DropdownFieldComponent,
                        FieldGroupComponent,
                        FormActionComponent,
                        HeaderFieldComponent,
                        OptionSetFieldComponent,
                        TextFieldComponent,
                        TextareaFieldComponent
                    ]
                },] }
    ];
    return NgDynamicFormsModule;
}());
export { NgDynamicFormsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmctZHluYW1pYy1mb3Jtcy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvbmctZHluYW1pYy1mb3Jtcy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFbEUsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDaEUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFekQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzVELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBRXJFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBRS9FLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUV0RDtJQUFBO0lBZ0RBLENBQUM7Ozs7SUFOUSw0QkFBTzs7O0lBQWQ7UUFDRSxPQUFPO1lBQ0wsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7U0FDM0IsQ0FBQztJQUNKLENBQUM7O2dCQS9DRixRQUFRLFNBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBQ1osV0FBVzt3QkFDWCxtQkFBbUI7d0JBQ25CLHdCQUF3Qjt3QkFDeEIscUJBQXFCLENBQUMsT0FBTyxFQUFFO3FCQUNoQztvQkFDRCxZQUFZLEVBQUU7d0JBQ1osYUFBYTt3QkFDYixpQkFBaUI7d0JBRWpCLHNCQUFzQjt3QkFDdEIseUJBQXlCO3dCQUN6QixxQkFBcUI7d0JBQ3JCLGtCQUFrQjt3QkFDbEIsc0JBQXNCO3dCQUN0QixtQkFBbUI7d0JBQ25CLG1CQUFtQjt3QkFDbkIsb0JBQW9CO3dCQUNwQix1QkFBdUI7d0JBQ3ZCLGtCQUFrQjt3QkFDbEIsc0JBQXNCO3FCQUN2QjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsYUFBYTt3QkFDYixpQkFBaUI7d0JBRWpCLHNCQUFzQjt3QkFDdEIseUJBQXlCO3dCQUN6QixxQkFBcUI7d0JBQ3JCLGtCQUFrQjt3QkFDbEIsc0JBQXNCO3dCQUN0QixtQkFBbUI7d0JBQ25CLG1CQUFtQjt3QkFDbkIsb0JBQW9CO3dCQUNwQix1QkFBdUI7d0JBQ3ZCLGtCQUFrQjt3QkFDbEIsc0JBQXNCO3FCQUN2QjtpQkFDRjs7SUFRRCwyQkFBQztDQUFBLEFBaERELElBZ0RDO1NBUFksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgQW5ndWxhckZvbnRBd2Vzb21lTW9kdWxlIH0gZnJvbSAnYW5ndWxhci1mb250LWF3ZXNvbWUnO1xuaW1wb3J0IHsgTmd4TXlEYXRlUGlja2VyTW9kdWxlIH0gZnJvbSAnbmd4LW15ZGF0ZXBpY2tlcic7XG5cbmltcG9ydCB7IEZvcm1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5cbmltcG9ydCB7IENoZWNrYm94RmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2hlY2tib3gtZmllbGQuY29tcG9uZW50JztcbmltcG9ydCB7IENoZWNrYm94U2V0RmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2hlY2tib3gtc2V0LWZpZWxkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb250ZW50RmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29udGVudC1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRGF0ZUZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2RhdGUtZmllbGQuY29tcG9uZW50JztcbmltcG9ydCB7IERyb3Bkb3duRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZHJvcGRvd24tZmllbGQuY29tcG9uZW50JztcbmltcG9ydCB7IEZpZWxkR3JvdXBDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmllbGQtZ3JvdXAuY29tcG9uZW50JztcbmltcG9ydCB7IEZvcm1BY3Rpb25Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybS1hY3Rpb24uY29tcG9uZW50JztcbmltcG9ydCB7IEhlYWRlckZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2hlYWRlci1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgT3B0aW9uU2V0RmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvb3B0aW9uLXNldC1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGV4dEZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3RleHQtZmllbGQuY29tcG9uZW50JztcbmltcG9ydCB7IFRleHRhcmVhRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdGV4dGFyZWEtZmllbGQuY29tcG9uZW50JztcblxuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcbiAgICBBbmd1bGFyRm9udEF3ZXNvbWVNb2R1bGUsXG4gICAgTmd4TXlEYXRlUGlja2VyTW9kdWxlLmZvclJvb3QoKVxuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBGb3JtQ29tcG9uZW50LFxuICAgIEZvcm1JdGVtQ29tcG9uZW50LFxuXG4gICAgQ2hlY2tib3hGaWVsZENvbXBvbmVudCxcbiAgICBDaGVja2JveFNldEZpZWxkQ29tcG9uZW50LFxuICAgIENvbnRlbnRGaWVsZENvbXBvbmVudCxcbiAgICBEYXRlRmllbGRDb21wb25lbnQsXG4gICAgRHJvcGRvd25GaWVsZENvbXBvbmVudCxcbiAgICBGaWVsZEdyb3VwQ29tcG9uZW50LFxuICAgIEZvcm1BY3Rpb25Db21wb25lbnQsXG4gICAgSGVhZGVyRmllbGRDb21wb25lbnQsXG4gICAgT3B0aW9uU2V0RmllbGRDb21wb25lbnQsXG4gICAgVGV4dEZpZWxkQ29tcG9uZW50LFxuICAgIFRleHRhcmVhRmllbGRDb21wb25lbnRcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIEZvcm1Db21wb25lbnQsXG4gICAgRm9ybUl0ZW1Db21wb25lbnQsXG5cbiAgICBDaGVja2JveEZpZWxkQ29tcG9uZW50LFxuICAgIENoZWNrYm94U2V0RmllbGRDb21wb25lbnQsXG4gICAgQ29udGVudEZpZWxkQ29tcG9uZW50LFxuICAgIERhdGVGaWVsZENvbXBvbmVudCxcbiAgICBEcm9wZG93bkZpZWxkQ29tcG9uZW50LFxuICAgIEZpZWxkR3JvdXBDb21wb25lbnQsXG4gICAgRm9ybUFjdGlvbkNvbXBvbmVudCxcbiAgICBIZWFkZXJGaWVsZENvbXBvbmVudCxcbiAgICBPcHRpb25TZXRGaWVsZENvbXBvbmVudCxcbiAgICBUZXh0RmllbGRDb21wb25lbnQsXG4gICAgVGV4dGFyZWFGaWVsZENvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIE5nRHluYW1pY0Zvcm1zTW9kdWxlIHtcbiAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5nTW9kdWxlOiBOZ0R5bmFtaWNGb3Jtc01vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG4gICAgfTtcbiAgfVxufVxuIl19