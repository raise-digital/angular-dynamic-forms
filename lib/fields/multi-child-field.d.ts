import { FieldGroup } from './field-group';
import { Form } from '../form';
import { ValidationResult } from '../validation-result.interface';
export declare class MultiChildField extends FieldGroup {
    type: 'multichild';
    subForm: (obj: any, prefix: string) => Form;
    newObj: () => any;
    addButtonText: string;
    removeButtonText: string;
    child: boolean;
    private counter;
    constructor(options?: {});
    readonly list: Array<any>;
    setForm(form: Form): void;
    loadValidation(result: ValidationResult): void;
    private addChild;
}
