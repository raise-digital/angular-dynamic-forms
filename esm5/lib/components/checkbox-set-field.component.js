/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { CheckboxSetField } from '../fields/checkbox-set-field';
import { FormService } from '../services/form.service';
var CheckboxSetFieldComponent = /** @class */ (function (_super) {
    tslib_1.__extends(CheckboxSetFieldComponent, _super);
    function CheckboxSetFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CheckboxSetFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-checkbox-set-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <ul>\n        <li *ngFor=\"let option of item.options\">\n            <label>\n                <div class=\"box\"><fa *ngIf=\"item.isChecked(option.key)\" name=\"check\" size=\"lt\"></fa></div>\n                <input type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.isChecked(option.key)\" [value]=\"option.key\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange(option.key)\" />\n                {{ option.value }}\n            </label>\n        </li>\n    </ul>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    CheckboxSetFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return CheckboxSetFieldComponent;
}(FormItemComponent));
export { CheckboxSetFieldComponent };
if (false) {
    /** @type {?} */
    CheckboxSetFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3gtc2V0LWZpZWxkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NoZWNrYm94LXNldC1maWVsZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVqRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFdkQ7SUFLK0MscURBQWlCO0lBTGhFOztJQU9BLENBQUM7O2dCQVBBLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsd0JBQXdCO29CQUNsQyxvdUJBQWtEO29CQUNsRCxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFDUixnQ0FBQztDQUFBLEFBUEQsQ0FLK0MsaUJBQWlCLEdBRS9EO1NBRlkseUJBQXlCOzs7SUFDcEMseUNBQXFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDaGVja2JveFNldEZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL2NoZWNrYm94LXNldC1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1jaGVja2JveC1zZXQtZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vY2hlY2tib3gtc2V0LWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgQ2hlY2tib3hTZXRGaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogQ2hlY2tib3hTZXRGaWVsZDxhbnk+O1xufVxuIl19