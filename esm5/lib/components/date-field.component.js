/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { DateField } from '../fields/date-field';
import { FormService } from '../services/form.service';
var DateFieldComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DateFieldComponent, _super);
    function DateFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DateFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-date-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <div class=\"ndf-date-picker\">\n        <input ngx-mydatepicker [name]=\"item.id\" [value]=\"item.value | date:'dd/MM/yyyy'\" [readonly]=\"item.readOnly\" (dateChanged)=\"onChange($event)\" [options]=\"{ dateFormat: 'dd/mm/yyyy', selectorHeight: 'auto' }\" #dp=\"ngx-mydatepicker\" />\n        <button *ngIf=\"item.value != null\" type=\"button\" class=\"ndf-date-clear\" (click)=\"dp.clearDate()\">\n            <fa name=\"times\" size=\"lt\"></fa>\n        </button>\n        <button type=\"button\" class=\"ndf-date-open\" (click)=\"dp.toggleCalendar()\">\n            <fa name=\"calendar-o\" size=\"lt\"></fa>\n        </button>\n    </div>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    DateFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return DateFieldComponent;
}(FormItemComponent));
export { DateFieldComponent };
if (false) {
    /** @type {?} */
    DateFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1maWVsZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9kYXRlLWZpZWxkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWpELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFdkQ7SUFLd0MsOENBQWlCO0lBTHpEOztJQU9BLENBQUM7O2dCQVBBLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixtNEJBQTBDO29CQUMxQyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFDUix5QkFBQztDQUFBLEFBUEQsQ0FLd0MsaUJBQWlCLEdBRXhEO1NBRlksa0JBQWtCOzs7SUFDN0Isa0NBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEYXRlRmllbGQgfSBmcm9tICcuLi9maWVsZHMvZGF0ZS1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1kYXRlLWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2RhdGUtZmllbGQuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBEYXRlRmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IERhdGVGaWVsZDtcbn1cbiJdfQ==