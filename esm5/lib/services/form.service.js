/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
var FormService = /** @class */ (function () {
    function FormService() {
        this.formGroup = new FormGroup({});
    }
    Object.defineProperty(FormService.prototype, "group", {
        get: /**
         * @return {?}
         */
        function () {
            return this.formGroup;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} field
     * @return {?}
     */
    FormService.prototype.registerField = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (!this.group.contains(field.id)) {
            this.group.addControl(field.id, this.controlFromField(field));
        }
    };
    /**
     * @param {?} fields
     * @return {?}
     */
    FormService.prototype.registerFields = /**
     * @param {?} fields
     * @return {?}
     */
    function (fields) {
        var _this = this;
        fields.forEach(function (field) {
            _this.registerField(field);
        });
    };
    /**
     * @param {?} field
     * @return {?}
     */
    FormService.prototype.deregisterField = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        this.group.removeControl(field.id);
    };
    /**
     * @param {?} fields
     * @return {?}
     */
    FormService.prototype.deregisterFields = /**
     * @param {?} fields
     * @return {?}
     */
    function (fields) {
        var _this = this;
        fields.forEach(function (field) {
            _this.deregisterField(field);
        });
    };
    /**
     * @param {?} field
     * @return {?}
     */
    FormService.prototype.controlFromField = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return new FormControl(field.value || '');
    };
    FormService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    FormService.ctorParameters = function () { return []; };
    return FormService;
}());
export { FormService };
if (false) {
    /** @type {?} */
    FormService.prototype.formGroup;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2Zvcm0uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBS3hEO0lBSUU7UUFDRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCxzQkFBSSw4QkFBSzs7OztRQUFUO1lBQ0UsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3hCLENBQUM7OztPQUFBOzs7OztJQUVELG1DQUFhOzs7O0lBQWIsVUFBYyxLQUFpQjtRQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7U0FDL0Q7SUFDSCxDQUFDOzs7OztJQUVELG9DQUFjOzs7O0lBQWQsVUFBZSxNQUFvQjtRQUFuQyxpQkFJQztRQUhDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO1lBQ2xCLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELHFDQUFlOzs7O0lBQWYsVUFBZ0IsS0FBaUI7UUFDL0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7Ozs7O0lBRUQsc0NBQWdCOzs7O0lBQWhCLFVBQWlCLE1BQW9CO1FBQXJDLGlCQUlDO1FBSEMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFBLEtBQUs7WUFDbEIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsc0NBQWdCOzs7O0lBQWhCLFVBQWlCLEtBQWlCO1FBQ2hDLE9BQU8sSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQztJQUM1QyxDQUFDOztnQkFwQ0YsVUFBVTs7OztJQXFDWCxrQkFBQztDQUFBLEFBckNELElBcUNDO1NBcENZLFdBQVc7OztJQUN0QixnQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgRm9ybUdyb3VwIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5pbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9maWVsZCc7XG5cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEZvcm1TZXJ2aWNlIHtcbiAgcHJpdmF0ZSBmb3JtR3JvdXA6IEZvcm1Hcm91cDtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmZvcm1Hcm91cCA9IG5ldyBGb3JtR3JvdXAoe30pO1xuICB9XG5cbiAgZ2V0IGdyb3VwKCk6IEZvcm1Hcm91cCB7XG4gICAgcmV0dXJuIHRoaXMuZm9ybUdyb3VwO1xuICB9XG5cbiAgcmVnaXN0ZXJGaWVsZChmaWVsZDogRmllbGQ8YW55Pikge1xuICAgIGlmICghdGhpcy5ncm91cC5jb250YWlucyhmaWVsZC5pZCkpIHtcbiAgICAgIHRoaXMuZ3JvdXAuYWRkQ29udHJvbChmaWVsZC5pZCwgdGhpcy5jb250cm9sRnJvbUZpZWxkKGZpZWxkKSk7XG4gICAgfVxuICB9XG5cbiAgcmVnaXN0ZXJGaWVsZHMoZmllbGRzOiBGaWVsZDxhbnk+W10pIHtcbiAgICBmaWVsZHMuZm9yRWFjaChmaWVsZCA9PiB7XG4gICAgICB0aGlzLnJlZ2lzdGVyRmllbGQoZmllbGQpO1xuICAgIH0pO1xuICB9XG5cbiAgZGVyZWdpc3RlckZpZWxkKGZpZWxkOiBGaWVsZDxhbnk+KSB7XG4gICAgdGhpcy5ncm91cC5yZW1vdmVDb250cm9sKGZpZWxkLmlkKTtcbiAgfVxuXG4gIGRlcmVnaXN0ZXJGaWVsZHMoZmllbGRzOiBGaWVsZDxhbnk+W10pIHtcbiAgICBmaWVsZHMuZm9yRWFjaChmaWVsZCA9PiB7XG4gICAgICB0aGlzLmRlcmVnaXN0ZXJGaWVsZChmaWVsZCk7XG4gICAgfSk7XG4gIH1cblxuICBjb250cm9sRnJvbUZpZWxkKGZpZWxkOiBGaWVsZDxhbnk+KTogRm9ybUNvbnRyb2wge1xuICAgIHJldHVybiBuZXcgRm9ybUNvbnRyb2woZmllbGQudmFsdWUgfHwgJycpO1xuICB9XG59XG4iXX0=