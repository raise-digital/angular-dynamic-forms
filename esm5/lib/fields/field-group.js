/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FormBase } from '../form-base';
var FieldGroup = /** @class */ (function (_super) {
    tslib_1.__extends(FieldGroup, _super);
    function FieldGroup(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.hasFields = true;
        _this.type = 'group';
        return _this;
    }
    /**
     * @return {?}
     */
    FieldGroup.prototype.deriveValue = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, _super.prototype.deriveValue.call(this).then(function (derived) {
                        _this.fields.updateValues();
                        return Promise.resolve(derived);
                    })];
            });
        });
    };
    return FieldGroup;
}(FormBase));
export { FieldGroup };
if (false) {
    /** @type {?} */
    FieldGroup.prototype.hasFields;
    /** @type {?} */
    FieldGroup.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtZ3JvdXAuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL2ZpZWxkLWdyb3VwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUV4QztJQUFnQyxzQ0FBUTtJQUl0QyxvQkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBQ2Y7UUFMRCxlQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLFVBQUksR0FBRyxPQUFPLENBQUM7O0lBSWYsQ0FBQzs7OztJQUVLLGdDQUFXOzs7SUFBakI7Ozs7Z0JBQ0Usc0JBQU8saUJBQU0sV0FBVyxXQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTzt3QkFDckMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQzt3QkFDM0IsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNsQyxDQUFDLENBQUMsRUFBQzs7O0tBQ0o7SUFDSCxpQkFBQztBQUFELENBQUMsQUFkRCxDQUFnQyxRQUFRLEdBY3ZDOzs7O0lBYkMsK0JBQWlCOztJQUNqQiwwQkFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuZXhwb3J0IGNsYXNzIEZpZWxkR3JvdXAgZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIGhhc0ZpZWxkcyA9IHRydWU7XG4gIHR5cGUgPSAnZ3JvdXAnO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgfVxuXG4gIGFzeW5jIGRlcml2ZVZhbHVlKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgIHJldHVybiBzdXBlci5kZXJpdmVWYWx1ZSgpLnRoZW4oZGVyaXZlZCA9PiB7XG4gICAgICB0aGlzLmZpZWxkcy51cGRhdGVWYWx1ZXMoKTtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZGVyaXZlZCk7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==