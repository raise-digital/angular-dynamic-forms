/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { CheckboxSetField } from '../fields/checkbox-set-field';
import { FormService } from '../services/form.service';
export class CheckboxSetFieldComponent extends FormItemComponent {
}
CheckboxSetFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-checkbox-set-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <ul>\n        <li *ngFor=\"let option of item.options\">\n            <label>\n                <div class=\"box\"><fa *ngIf=\"item.isChecked(option.key)\" name=\"check\" size=\"lt\"></fa></div>\n                <input type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.isChecked(option.key)\" [value]=\"option.key\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange(option.key)\" />\n                {{ option.value }}\n            </label>\n        </li>\n    </ul>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
CheckboxSetFieldComponent.propDecorators = {
    item: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    CheckboxSetFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3gtc2V0LWZpZWxkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NoZWNrYm94LXNldC1maWVsZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWpELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ2hFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQU92RCxNQUFNLE9BQU8seUJBQTBCLFNBQVEsaUJBQWlCOzs7WUFML0QsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSx3QkFBd0I7Z0JBQ2xDLG91QkFBa0Q7Z0JBQ2xELFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTthQUMzQjs7O21CQUVFLEtBQUs7Ozs7SUFBTix5Q0FBcUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IENoZWNrYm94U2V0RmllbGQgfSBmcm9tICcuLi9maWVsZHMvY2hlY2tib3gtc2V0LWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWNoZWNrYm94LXNldC1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9jaGVja2JveC1zZXQtZmllbGQuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBDaGVja2JveFNldEZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBDaGVja2JveFNldEZpZWxkPGFueT47XG59XG4iXX0=