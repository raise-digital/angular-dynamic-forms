import { FormBase } from '../form-base';
export declare class FormAction extends FormBase {
    isAction: boolean;
    type: string;
    title: string;
    icon: string;
    actionType: string;
    loading: string;
    click: (action: FormAction) => void;
    constructor(options?: {});
    showLoading(text: string): void;
    clearLoading(): void;
    onClick(): void;
}
