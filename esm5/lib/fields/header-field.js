/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FormBase } from '../form-base';
var HeaderField = /** @class */ (function (_super) {
    tslib_1.__extends(HeaderField, _super);
    function HeaderField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'header';
        _this.title = options['title'] || '';
        _this.classes.push('ndf-field');
        _this.classes.push('ndf-header');
        return _this;
    }
    return HeaderField;
}(FormBase));
export { HeaderField };
if (false) {
    /** @type {?} */
    HeaderField.prototype.type;
    /** @type {?} */
    HeaderField.prototype.title;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLWZpZWxkLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2ZpZWxkcy9oZWFkZXItZmllbGQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBRXhDO0lBQWlDLHVDQUFRO0lBSXZDLHFCQUFZLE9BQWdCO1FBQWhCLHdCQUFBLEVBQUEsWUFBZ0I7UUFBNUIsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FJZjtRQVJELFVBQUksR0FBRyxRQUFRLENBQUM7UUFLZCxLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDcEMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDL0IsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7O0lBQ2xDLENBQUM7SUFDSCxrQkFBQztBQUFELENBQUMsQUFWRCxDQUFpQyxRQUFRLEdBVXhDOzs7O0lBVEMsMkJBQWdCOztJQUNoQiw0QkFBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuZXhwb3J0IGNsYXNzIEhlYWRlckZpZWxkIGV4dGVuZHMgRm9ybUJhc2Uge1xuICB0eXBlID0gJ2hlYWRlcic7XG4gIHRpdGxlOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMudGl0bGUgPSBvcHRpb25zWyd0aXRsZSddIHx8ICcnO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtZmllbGQnKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWhlYWRlcicpO1xuICB9XG59XG4iXX0=