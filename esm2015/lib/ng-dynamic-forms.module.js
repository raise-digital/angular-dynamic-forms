/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { FormComponent } from './components/form.component';
import { FormItemComponent } from './components/form-item.component';
import { CheckboxFieldComponent } from './components/checkbox-field.component';
import { CheckboxSetFieldComponent } from './components/checkbox-set-field.component';
import { ContentFieldComponent } from './components/content-field.component';
import { DateFieldComponent } from './components/date-field.component';
import { DropdownFieldComponent } from './components/dropdown-field.component';
import { FieldGroupComponent } from './components/field-group.component';
import { FormActionComponent } from './components/form-action.component';
import { HeaderFieldComponent } from './components/header-field.component';
import { OptionSetFieldComponent } from './components/option-set-field.component';
import { TextFieldComponent } from './components/text-field.component';
import { TextareaFieldComponent } from './components/textarea-field.component';
import { FormService } from './services/form.service';
export class NgDynamicFormsModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return {
            ngModule: NgDynamicFormsModule,
            providers: [FormService]
        };
    }
}
NgDynamicFormsModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    AngularFontAwesomeModule,
                    NgxMyDatePickerModule.forRoot()
                ],
                declarations: [
                    FormComponent,
                    FormItemComponent,
                    CheckboxFieldComponent,
                    CheckboxSetFieldComponent,
                    ContentFieldComponent,
                    DateFieldComponent,
                    DropdownFieldComponent,
                    FieldGroupComponent,
                    FormActionComponent,
                    HeaderFieldComponent,
                    OptionSetFieldComponent,
                    TextFieldComponent,
                    TextareaFieldComponent
                ],
                exports: [
                    FormComponent,
                    FormItemComponent,
                    CheckboxFieldComponent,
                    CheckboxSetFieldComponent,
                    ContentFieldComponent,
                    DateFieldComponent,
                    DropdownFieldComponent,
                    FieldGroupComponent,
                    FormActionComponent,
                    HeaderFieldComponent,
                    OptionSetFieldComponent,
                    TextFieldComponent,
                    TextareaFieldComponent
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmctZHluYW1pYy1mb3Jtcy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvbmctZHluYW1pYy1mb3Jtcy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFbEUsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDaEUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFekQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzVELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBRXJFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBRS9FLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQTJDdEQsTUFBTSxPQUFPLG9CQUFvQjs7OztJQUMvQixNQUFNLENBQUMsT0FBTztRQUNaLE9BQU87WUFDTCxRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtTQUMzQixDQUFDO0lBQ0osQ0FBQzs7O1lBL0NGLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixXQUFXO29CQUNYLG1CQUFtQjtvQkFDbkIsd0JBQXdCO29CQUN4QixxQkFBcUIsQ0FBQyxPQUFPLEVBQUU7aUJBQ2hDO2dCQUNELFlBQVksRUFBRTtvQkFDWixhQUFhO29CQUNiLGlCQUFpQjtvQkFFakIsc0JBQXNCO29CQUN0Qix5QkFBeUI7b0JBQ3pCLHFCQUFxQjtvQkFDckIsa0JBQWtCO29CQUNsQixzQkFBc0I7b0JBQ3RCLG1CQUFtQjtvQkFDbkIsbUJBQW1CO29CQUNuQixvQkFBb0I7b0JBQ3BCLHVCQUF1QjtvQkFDdkIsa0JBQWtCO29CQUNsQixzQkFBc0I7aUJBQ3ZCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxhQUFhO29CQUNiLGlCQUFpQjtvQkFFakIsc0JBQXNCO29CQUN0Qix5QkFBeUI7b0JBQ3pCLHFCQUFxQjtvQkFDckIsa0JBQWtCO29CQUNsQixzQkFBc0I7b0JBQ3RCLG1CQUFtQjtvQkFDbkIsbUJBQW1CO29CQUNuQixvQkFBb0I7b0JBQ3BCLHVCQUF1QjtvQkFDdkIsa0JBQWtCO29CQUNsQixzQkFBc0I7aUJBQ3ZCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5pbXBvcnQgeyBBbmd1bGFyRm9udEF3ZXNvbWVNb2R1bGUgfSBmcm9tICdhbmd1bGFyLWZvbnQtYXdlc29tZSc7XG5pbXBvcnQgeyBOZ3hNeURhdGVQaWNrZXJNb2R1bGUgfSBmcm9tICduZ3gtbXlkYXRlcGlja2VyJztcblxuaW1wb3J0IHsgRm9ybUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLWl0ZW0uY29tcG9uZW50JztcblxuaW1wb3J0IHsgQ2hlY2tib3hGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jaGVja2JveC1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2hlY2tib3hTZXRGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jaGVja2JveC1zZXQtZmllbGQuY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRlbnRGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb250ZW50LWZpZWxkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEYXRlRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZGF0ZS1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRHJvcGRvd25GaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9kcm9wZG93bi1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmllbGRHcm91cENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9maWVsZC1ncm91cC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9ybUFjdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLWFjdGlvbi5jb21wb25lbnQnO1xuaW1wb3J0IHsgSGVhZGVyRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvaGVhZGVyLWZpZWxkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBPcHRpb25TZXRGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9vcHRpb24tc2V0LWZpZWxkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBUZXh0RmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdGV4dC1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGV4dGFyZWFGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy90ZXh0YXJlYS1maWVsZC5jb21wb25lbnQnO1xuXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBGb3Jtc01vZHVsZSxcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgIEFuZ3VsYXJGb250QXdlc29tZU1vZHVsZSxcbiAgICBOZ3hNeURhdGVQaWNrZXJNb2R1bGUuZm9yUm9vdCgpXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIEZvcm1Db21wb25lbnQsXG4gICAgRm9ybUl0ZW1Db21wb25lbnQsXG5cbiAgICBDaGVja2JveEZpZWxkQ29tcG9uZW50LFxuICAgIENoZWNrYm94U2V0RmllbGRDb21wb25lbnQsXG4gICAgQ29udGVudEZpZWxkQ29tcG9uZW50LFxuICAgIERhdGVGaWVsZENvbXBvbmVudCxcbiAgICBEcm9wZG93bkZpZWxkQ29tcG9uZW50LFxuICAgIEZpZWxkR3JvdXBDb21wb25lbnQsXG4gICAgRm9ybUFjdGlvbkNvbXBvbmVudCxcbiAgICBIZWFkZXJGaWVsZENvbXBvbmVudCxcbiAgICBPcHRpb25TZXRGaWVsZENvbXBvbmVudCxcbiAgICBUZXh0RmllbGRDb21wb25lbnQsXG4gICAgVGV4dGFyZWFGaWVsZENvbXBvbmVudFxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgRm9ybUNvbXBvbmVudCxcbiAgICBGb3JtSXRlbUNvbXBvbmVudCxcblxuICAgIENoZWNrYm94RmllbGRDb21wb25lbnQsXG4gICAgQ2hlY2tib3hTZXRGaWVsZENvbXBvbmVudCxcbiAgICBDb250ZW50RmllbGRDb21wb25lbnQsXG4gICAgRGF0ZUZpZWxkQ29tcG9uZW50LFxuICAgIERyb3Bkb3duRmllbGRDb21wb25lbnQsXG4gICAgRmllbGRHcm91cENvbXBvbmVudCxcbiAgICBGb3JtQWN0aW9uQ29tcG9uZW50LFxuICAgIEhlYWRlckZpZWxkQ29tcG9uZW50LFxuICAgIE9wdGlvblNldEZpZWxkQ29tcG9uZW50LFxuICAgIFRleHRGaWVsZENvbXBvbmVudCxcbiAgICBUZXh0YXJlYUZpZWxkQ29tcG9uZW50XG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgTmdEeW5hbWljRm9ybXNNb2R1bGUge1xuICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IE5nRHluYW1pY0Zvcm1zTW9kdWxlLFxuICAgICAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbiAgICB9O1xuICB9XG59XG4iXX0=