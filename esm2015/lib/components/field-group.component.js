/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { FieldGroup } from '../fields/field-group';
import { FormService } from '../services/form.service';
export class FieldGroupComponent extends FormItemComponent {
}
FieldGroupComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-field-group',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <ndf-form-item *ngFor=\"let child of item.fields.fields\" [item]=\"child\"></ndf-form-item>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
FieldGroupComponent.propDecorators = {
    item: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FieldGroupComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtZ3JvdXAuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZmllbGQtZ3JvdXAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVqRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDbkQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBT3ZELE1BQU0sT0FBTyxtQkFBb0IsU0FBUSxpQkFBaUI7OztZQUx6RCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGlCQUFpQjtnQkFDM0IsdVBBQTJDO2dCQUMzQyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7YUFDM0I7OzttQkFFRSxLQUFLOzs7O0lBQU4sbUNBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGaWVsZEdyb3VwIH0gZnJvbSAnLi4vZmllbGRzL2ZpZWxkLWdyb3VwJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWZpZWxkLWdyb3VwJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2ZpZWxkLWdyb3VwLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgRmllbGRHcm91cENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogRmllbGRHcm91cDtcbn1cbiJdfQ==