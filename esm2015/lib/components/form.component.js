/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { Form } from '../form';
import { FormService } from '../services/form.service';
export class FormComponent {
    /**
     * @param {?} formService
     */
    constructor(formService) {
        this.formService = formService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    get group() {
        return this.formService.group;
    }
    /**
     * @return {?}
     */
    onSubmit() {
        this.form.onSubmit();
    }
}
FormComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-form',
                template: "<form (ngSubmit)=\"onSubmit()\" [ngClass]=\"form.classes\" [formGroup]=\"group\">\n    <p *ngIf=\"form.message\" class=\"ndf-error\">{{ form.message }}</p>\n    <ndf-form-item *ngFor=\"let item of form.fields.fields\" [item]=\"item\"></ndf-form-item>\n</form>\n",
                providers: [FormService]
            }] }
];
/** @nocollapse */
FormComponent.ctorParameters = () => [
    { type: FormService }
];
FormComponent.propDecorators = {
    form: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FormComponent.prototype.form;
    /** @type {?} */
    FormComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9mb3JtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFHekQsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUMvQixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFRdkQsTUFBTSxPQUFPLGFBQWE7Ozs7SUFHeEIsWUFBb0IsV0FBd0I7UUFBeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7SUFBRyxDQUFDOzs7O0lBRWhELFFBQVE7SUFFUixDQUFDOzs7O0lBRUQsSUFBSSxLQUFLO1FBQ1AsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztJQUNoQyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDdkIsQ0FBQzs7O1lBcEJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsaVJBQW9DO2dCQUNwQyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7YUFDM0I7Ozs7WUFQUSxXQUFXOzs7bUJBU2pCLEtBQUs7Ozs7SUFBTiw2QkFBb0I7O0lBRVIsb0NBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbmltcG9ydCB7IEZvcm0gfSBmcm9tICcuLi9mb3JtJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtZm9ybScsXG4gIHRlbXBsYXRlVXJsOiAnLi9mb3JtLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIGZvcm06IEZvcm07XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBmb3JtU2VydmljZTogRm9ybVNlcnZpY2UpIHt9XG5cbiAgbmdPbkluaXQoKSB7XG5cbiAgfVxuXG4gIGdldCBncm91cCgpOiBGb3JtR3JvdXAge1xuICAgIHJldHVybiB0aGlzLmZvcm1TZXJ2aWNlLmdyb3VwO1xuICB9XG5cbiAgb25TdWJtaXQoKSB7XG4gICAgdGhpcy5mb3JtLm9uU3VibWl0KCk7XG4gIH1cbn1cbiJdfQ==