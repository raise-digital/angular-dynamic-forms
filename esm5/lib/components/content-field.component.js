/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { ContentField } from '../fields/content-field';
import { FormService } from '../services/form.service';
var ContentFieldComponent = /** @class */ (function (_super) {
    tslib_1.__extends(ContentFieldComponent, _super);
    function ContentFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ContentFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-content-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <p>{{ item.content }}</p>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    ContentFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return ContentFieldComponent;
}(FormItemComponent));
export { ContentFieldComponent };
if (false) {
    /** @type {?} */
    ContentFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC1maWVsZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9jb250ZW50LWZpZWxkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWpELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFdkQ7SUFLMkMsaURBQWlCO0lBTDVEOztJQU9BLENBQUM7O2dCQVBBLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsbUJBQW1CO29CQUM3QixxTEFBNkM7b0JBQzdDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtpQkFDM0I7Ozt1QkFFRSxLQUFLOztJQUNSLDRCQUFDO0NBQUEsQUFQRCxDQUsyQyxpQkFBaUIsR0FFM0Q7U0FGWSxxQkFBcUI7OztJQUNoQyxxQ0FBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRlbnRGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9jb250ZW50LWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWNvbnRlbnQtZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vY29udGVudC1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIENvbnRlbnRGaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogQ29udGVudEZpZWxkO1xufVxuIl19