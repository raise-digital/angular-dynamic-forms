/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Field } from './field';
var TextareaField = /** @class */ (function (_super) {
    tslib_1.__extends(TextareaField, _super);
    function TextareaField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'textarea';
        _this.value = options['value'] || '';
        _this.classes.push('ndf-textarea');
        return _this;
    }
    return TextareaField;
}(Field));
export { TextareaField };
if (false) {
    /** @type {?} */
    TextareaField.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dGFyZWEtZmllbGQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL3RleHRhcmVhLWZpZWxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUVoQztJQUFtQyx5Q0FBYTtJQUc5Qyx1QkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBR2Y7UUFORCxVQUFJLEdBQUcsVUFBVSxDQUFDO1FBSWhCLEtBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNwQyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzs7SUFDcEMsQ0FBQztJQUNILG9CQUFDO0FBQUQsQ0FBQyxBQVJELENBQW1DLEtBQUssR0FRdkM7Ozs7SUFQQyw2QkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4vZmllbGQnO1xuXG5leHBvcnQgY2xhc3MgVGV4dGFyZWFGaWVsZCBleHRlbmRzIEZpZWxkPHN0cmluZz4ge1xuICB0eXBlID0gJ3RleHRhcmVhJztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy52YWx1ZSA9IG9wdGlvbnNbJ3ZhbHVlJ10gfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi10ZXh0YXJlYScpO1xuICB9XG59XG4iXX0=