import { Field } from './field';
import { IMyDateModel } from 'ngx-mydatepicker';
export declare class DateField extends Field<Date> {
    type: string;
    constructor(options?: {});
    onChange(value: IMyDateModel): void;
}
