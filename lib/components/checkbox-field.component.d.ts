import { FormItemComponent } from './form-item.component';
import { CheckboxField } from '../fields/checkbox-field';
export declare class CheckboxFieldComponent extends FormItemComponent {
    item: CheckboxField;
}
