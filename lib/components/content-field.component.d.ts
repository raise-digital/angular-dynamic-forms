import { FormItemComponent } from './form-item.component';
import { ContentField } from '../fields/content-field';
export declare class ContentFieldComponent extends FormItemComponent {
    item: ContentField;
}
