import { __extends, __awaiter, __generator } from 'tslib';
import { Injectable, Component, Input, NgModule } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var FieldSet = /** @class */ (function () {
    function FieldSet(set) {
        this.set = set;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    FieldSet.prototype.setForm = /**
     * @param {?} form
     * @return {?}
     */
    function (form) {
        this.set.forEach(function (field) {
            field.setForm(form);
        });
    };
    /**
     * @return {?}
     */
    FieldSet.prototype.updateValues = /**
     * @return {?}
     */
    function () {
        this.set.forEach(function (field) {
            field.deriveValue();
        });
    };
    Object.defineProperty(FieldSet.prototype, "fields", {
        get: /**
         * @return {?}
         */
        function () {
            return this.set;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} item
     * @return {?}
     */
    FieldSet.prototype.push = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        this.set.push(item);
    };
    /**
     * @param {?} item
     * @param {?} before
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertBefore = /**
     * @param {?} item
     * @param {?} before
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    function (item, before, attr, field) {
        if (field === void 0) { field = null; }
        /** @type {?} */
        var parent = null;
        if (field === null) {
            /** @type {?} */
            var search = this.fieldWithParent(before, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field), 0, item);
        }
        else {
            parent.fields.insertBefore(item, before, attr, field);
        }
    };
    /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertBeforeName = /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    function (item, before, field) {
        if (field === void 0) { field = null; }
        return this.insertBefore(item, before, 'name', field);
    };
    /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertBeforeId = /**
     * @param {?} item
     * @param {?} before
     * @param {?=} field
     * @return {?}
     */
    function (item, before, field) {
        if (field === void 0) { field = null; }
        return this.insertBefore(item, before, 'id', field);
    };
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertAfter = /**
     * @param {?} item
     * @param {?} after
     * @param {?} attr
     * @param {?=} field
     * @return {?}
     */
    function (item, after, attr, field) {
        if (field === void 0) { field = null; }
        /** @type {?} */
        var parent = null;
        if (field === null) {
            /** @type {?} */
            var search = this.fieldWithParent(after, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field) + 1, 0, item);
        }
        else {
            parent.fields.insertAfter(item, after, attr, field);
        }
    };
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertAfterName = /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    function (item, after, _attr, field) {
        if (field === void 0) { field = null; }
        this.insertAfter(item, after, 'name', field);
    };
    /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.insertAfterId = /**
     * @param {?} item
     * @param {?} after
     * @param {?} _attr
     * @param {?=} field
     * @return {?}
     */
    function (item, after, _attr, field) {
        if (field === void 0) { field = null; }
        this.insertAfter(item, after, 'id', field);
    };
    /**
     * @param {?} key
     * @param {?=} field
     * @param {?=} attr
     * @return {?}
     */
    FieldSet.prototype.remove = /**
     * @param {?} key
     * @param {?=} field
     * @param {?=} attr
     * @return {?}
     */
    function (key, field, attr) {
        if (field === void 0) { field = null; }
        /** @type {?} */
        var parent = null;
        if (field === null) {
            /** @type {?} */
            var search = this.fieldWithParent(key, attr);
            parent = search.parent;
            field = search.field;
        }
        if (parent === null) {
            this.set.splice(this.set.indexOf(field), 1);
        }
        else {
            parent.fields.remove(key, field, attr);
        }
    };
    /**
     * @param {?} name
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.removeByName = /**
     * @param {?} name
     * @param {?=} field
     * @return {?}
     */
    function (name, field) {
        if (field === void 0) { field = null; }
        this.remove(name, field, 'name');
    };
    /**
     * @param {?} id
     * @param {?=} field
     * @return {?}
     */
    FieldSet.prototype.removeById = /**
     * @param {?} id
     * @param {?=} field
     * @return {?}
     */
    function (id, field) {
        if (field === void 0) { field = null; }
        this.remove(id, field, 'id');
    };
    /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    FieldSet.prototype.field = /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    function (key, attr) {
        return this.fieldWithParent(key, attr).field;
    };
    /**
     * @param {?} name
     * @return {?}
     */
    FieldSet.prototype.fieldByName = /**
     * @param {?} name
     * @return {?}
     */
    function (name) {
        return this.field(name, 'name');
    };
    /**
     * @param {?} id
     * @return {?}
     */
    FieldSet.prototype.fieldById = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.field(id, 'id');
    };
    /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    FieldSet.prototype.fieldWithParent = /**
     * @param {?} key
     * @param {?} attr
     * @return {?}
     */
    function (key, attr) {
        /** @type {?} */
        var field = null;
        /** @type {?} */
        var parent = null;
        /** @type {?} */
        var search = this.set.filter(function (child) { return child[attr] === key; });
        if (search.length > 0) {
            field = search[0];
        }
        else {
            /** @type {?} */
            var parents = this.set.filter(function (child) { return child.hasFields === true; });
            for (var i = 0; i < parents.length; i++) {
                /** @type {?} */
                var check = parents[i].fields.fieldWithParent(key, attr);
                if (check.field != null) {
                    field = check.field;
                    parent = parents[i];
                    break;
                }
            }
        }
        return { field: field, parent: parent };
    };
    /**
     * @param {?} name
     * @return {?}
     */
    FieldSet.prototype.fieldByNameWithParent = /**
     * @param {?} name
     * @return {?}
     */
    function (name) {
        return this.fieldWithParent(name, 'name');
    };
    /**
     * @param {?} id
     * @return {?}
     */
    FieldSet.prototype.fieldByIdWithParent = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.fieldWithParent(id, 'id');
    };
    /**
     * @return {?}
     */
    FieldSet.prototype.resetValidation = /**
     * @return {?}
     */
    function () {
        this.fields.forEach(function (field) {
            field.resetValidation();
        });
    };
    return FieldSet;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var FormBase = /** @class */ (function () {
    function FormBase(options) {
        if (options === void 0) { options = {}; }
        this.isForm = false;
        this.hasData = false;
        this.hasFields = false;
        this.hasLabel = true;
        this.isAction = false;
        this.name = options.name || '';
        this.id = this.name;
        this.classes = options.classes || [];
        this.fields = new FieldSet(options.fields || []);
        this.readOnly = options.readOnly || false;
        this.change = options.change || null;
        this.blur = options.blur || null;
    }
    /**
     * @return {?}
     */
    FormBase.prototype.getForm = /**
     * @return {?}
     */
    function () {
        return this.form;
    };
    /**
     * @param {?} form
     * @return {?}
     */
    FormBase.prototype.setForm = /**
     * @param {?} form
     * @return {?}
     */
    function (form) {
        this.form = form;
        if (form.prefix) {
            this.id = "" + form.prefix + this.name;
        }
        if (!this.isForm) {
            this.fields.setForm(form);
        }
    };
    /**
     * @return {?}
     */
    FormBase.prototype.deriveValue = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, Promise.resolve(false)];
            });
        });
    };
    Object.defineProperty(FormBase.prototype, "control", {
        get: /**
         * @return {?}
         */
        function () {
            return null;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} value
     * @return {?}
     */
    FormBase.prototype.onChange = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        /** @type {?} */
        var propagate = true;
        if (this.change != null) {
            propagate = this.change(this, value);
        }
        if (propagate) {
            this.form.fieldChange(this.name, value);
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    FormBase.prototype.onBlur = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (this.blur != null) {
            this.blur(this, value);
        }
    };
    /**
     * @return {?}
     */
    FormBase.prototype.onClick = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} message
     * @return {?}
     */
    FormBase.prototype.setMessage = /**
     * @param {?} message
     * @return {?}
     */
    function (message) {
        this.message = message;
    };
    /**
     * @param {?} result
     * @return {?}
     */
    FormBase.prototype.loadValidation = /**
     * @param {?} result
     * @return {?}
     */
    function (result) {
        this.resetValidation();
        if (result.messages != null && result.messages.length > 0) {
            this.setMessage(result.messages[0]);
        }
        for (var key in result.fields) {
            if (result.fields.hasOwnProperty(key)) {
                /** @type {?} */
                var field = this.fields.fieldByName(key);
                if (field != null) {
                    field.loadValidation(result.fields[key]);
                }
            }
        }
    };
    /**
     * @return {?}
     */
    FormBase.prototype.resetValidation = /**
     * @return {?}
     */
    function () {
        this.setMessage(null);
        if (this.fields) {
            this.fields.resetValidation();
        }
    };
    return FormBase;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @template T
 */
var  /**
 * @template T
 */
Field = /** @class */ (function (_super) {
    __extends(Field, _super);
    function Field(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.hasData = true;
        _this.value = options.value;
        _this.label = options.label || '';
        _this.classes.push('ndf-field');
        _this.placeholder = options.placeholder || '';
        if (_this.placeholder.length > 0) {
            _this.classes.push('ndf-placeholder');
        }
        _this.derive = options.derive || null;
        return _this;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    Field.prototype.setForm = /**
     * @param {?} form
     * @return {?}
     */
    function (form) {
        _super.prototype.setForm.call(this, form);
        this.deriveValue();
    };
    /**
     * @param {?} value
     * @return {?}
     */
    Field.prototype.onChange = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.value = value;
        _super.prototype.onChange.call(this, this.prepareValue(value));
    };
    /**
     * @return {?}
     */
    Field.prototype.deriveValue = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, _super.prototype.deriveValue.call(this).then(function (derived) {
                        if (!derived) {
                            if (_this.derive != null) {
                                return _this.derive(_this);
                            }
                            else if (_this.form.obj.hasOwnProperty(_this.name)) {
                                _this.value = _this.form.obj[_this.name];
                                return Promise.resolve(true);
                            }
                        }
                        return Promise.resolve(false);
                    })];
            });
        });
    };
    /**
     * @param {?} value
     * @return {?}
     */
    Field.prototype.prepareValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (value == null) {
            return '';
        }
        else {
            return value;
        }
    };
    return Field;
}(FormBase));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var CheckboxField = /** @class */ (function (_super) {
    __extends(CheckboxField, _super);
    function CheckboxField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'checkbox';
        _this.hasLabel = false;
        _this.classes.push('ndf-checkbox');
        return _this;
    }
    return CheckboxField;
}(Field));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @template T
 */
var  /**
 * @template T
 */
OptionSetField = /** @class */ (function (_super) {
    __extends(OptionSetField, _super);
    function OptionSetField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'optionset';
        _this.optionSet = [];
        _this.options = [];
        _this.classes.push('ndf-optionset');
        _this.optionSet = options['options'] || [];
        Promise.resolve(_this.optionSet).then(function (optionList) {
            _this.setOptions(optionList);
        });
        return _this;
    }
    /**
     * @param {?} options
     * @return {?}
     */
    OptionSetField.prototype.setOptions = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        this.options = options;
    };
    return OptionSetField;
}(Field));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @template T
 */
var  /**
 * @template T
 */
CheckboxSetField = /** @class */ (function (_super) {
    __extends(CheckboxSetField, _super);
    function CheckboxSetField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'checkboxset';
        _this.classes.push('ndf-checkboxset');
        return _this;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    CheckboxSetField.prototype.onChange = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        /** @type {?} */
        var obj = this.form.obj[this.name];
        if (this.isChecked(value)) {
            obj.splice(obj.indexOf(value), 1);
        }
        else {
            obj.push(value);
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    CheckboxSetField.prototype.isChecked = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        /** @type {?} */
        var selected = this.form.obj[this.name];
        if (selected != null) {
            return selected.filter(function (o) { return o === value; }).length > 0;
        }
        return false;
    };
    return CheckboxSetField;
}(OptionSetField));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var ContentField = /** @class */ (function (_super) {
    __extends(ContentField, _super);
    function ContentField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'content';
        _this.content = options['content'] || '';
        _this.classes.push('ndf-field');
        _this.classes.push('ndf-content');
        return _this;
    }
    return ContentField;
}(FormBase));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var DateField = /** @class */ (function (_super) {
    __extends(DateField, _super);
    function DateField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'date';
        _this.classes.push('ndf-date');
        return _this;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    DateField.prototype.onChange = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        _super.prototype.onChange.call(this, (value.jsdate == null) ? null : new Date(value.jsdate));
    };
    return DateField;
}(Field));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var DropdownField = /** @class */ (function (_super) {
    __extends(DropdownField, _super);
    function DropdownField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'dropdown';
        _this.classes.push('ndf-dropdown');
        _this.loaded = options['emptyText'] || '';
        _this.emptyText = options['loadingText'] || '';
        return _this;
    }
    /**
     * @param {?} options
     * @return {?}
     */
    DropdownField.prototype.setOptions = /**
     * @param {?} options
     * @return {?}
     */
    function (options) {
        _super.prototype.setOptions.call(this, options);
        this.emptyText = this.loaded;
    };
    return DropdownField;
}(OptionSetField));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var FieldGroup = /** @class */ (function (_super) {
    __extends(FieldGroup, _super);
    function FieldGroup(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.hasFields = true;
        _this.type = 'group';
        return _this;
    }
    /**
     * @return {?}
     */
    FieldGroup.prototype.deriveValue = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, _super.prototype.deriveValue.call(this).then(function (derived) {
                        _this.fields.updateValues();
                        return Promise.resolve(derived);
                    })];
            });
        });
    };
    return FieldGroup;
}(FormBase));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var FormAction = /** @class */ (function (_super) {
    __extends(FormAction, _super);
    function FormAction(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.isAction = true;
        _this.type = 'action';
        _this.title = options['title'] || null;
        _this.icon = options['icon'] || null;
        _this.actionType = options['actionType'] || 'submit';
        _this.click = options['click'] || null;
        _this.classes.push('ndf-action');
        return _this;
    }
    /**
     * @param {?} text
     * @return {?}
     */
    FormAction.prototype.showLoading = /**
     * @param {?} text
     * @return {?}
     */
    function (text) {
        this.loading = text;
    };
    /**
     * @return {?}
     */
    FormAction.prototype.clearLoading = /**
     * @return {?}
     */
    function () {
        this.loading = null;
    };
    /**
     * @return {?}
     */
    FormAction.prototype.onClick = /**
     * @return {?}
     */
    function () {
        if (this.click != null) {
            this.click(this);
        }
    };
    return FormAction;
}(FormBase));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var HeaderField = /** @class */ (function (_super) {
    __extends(HeaderField, _super);
    function HeaderField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'header';
        _this.title = options['title'] || '';
        _this.classes.push('ndf-field');
        _this.classes.push('ndf-header');
        return _this;
    }
    return HeaderField;
}(FormBase));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var MultiChildField = /** @class */ (function (_super) {
    __extends(MultiChildField, _super);
    function MultiChildField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.child = false;
        _this.counter = 0;
        _this.subForm = options['subForm'] || null;
        _this.newObj = options['newObj'] || null;
        _this.addButtonText = options['addButtonText'] || 'Add New Line';
        _this.removeButtonText = options['removeButtonText'] || 'Remove Line';
        _this.child = options['child'] || false;
        _this.classes.push('ndf-multichild');
        if (options['title']) {
            _this.fields.push(new HeaderField({
                title: options['title']
            }));
            _this.fields.push(new FieldGroup({
                name: _this.name + "_controls",
                fields: [
                    new FormAction({
                        title: _this.addButtonText,
                        icon: 'plus',
                        click: function () {
                            if (_this.subForm !== null && _this.newObj !== null) {
                                /** @type {?} */
                                var obj = _this.newObj();
                                _this.list.push(obj);
                                _this.addChild(obj);
                            }
                        },
                        classes: ['ndf-add']
                    })
                ],
                classes: ['ndf-full-bordered', 'ndf-multichild-footer']
            }));
        }
        return _this;
    }
    Object.defineProperty(MultiChildField.prototype, "list", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.form.obj[this.name] == null) {
                this.form.obj[this.name] = [];
            }
            return ((/** @type {?} */ (this.form.obj[this.name])));
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} form
     * @return {?}
     */
    MultiChildField.prototype.setForm = /**
     * @param {?} form
     * @return {?}
     */
    function (form) {
        var _this = this;
        _super.prototype.setForm.call(this, form);
        if (this.subForm !== null) {
            this.list.forEach(function (obj) {
                _this.addChild(obj);
            });
        }
    };
    /**
     * @param {?} result
     * @return {?}
     */
    MultiChildField.prototype.loadValidation = /**
     * @param {?} result
     * @return {?}
     */
    function (result) {
        this.resetValidation();
        for (var key in result.fields) {
            if (result.fields.hasOwnProperty(key)) {
                /** @type {?} */
                var child = this.form.prefix + "line_" + key;
                /** @type {?} */
                var field = this.fields.fieldByName(child);
                if (field != null) {
                    field.loadValidation(result.fields[key]);
                }
            }
        }
    };
    /**
     * @param {?} obj
     * @return {?}
     */
    MultiChildField.prototype.addChild = /**
     * @param {?} obj
     * @return {?}
     */
    function (obj) {
        var _this = this;
        /** @type {?} */
        var group = this.form.prefix + "line_" + this.counter + "_group";
        this.fields.insertBeforeId(new FieldGroup({
            name: group,
            fields: [
                this.subForm(obj, this.form.prefix + "line_" + this.counter),
                new FormAction({
                    title: this.removeButtonText,
                    icon: 'trash',
                    click: function () {
                        _this.fields.removeById(group);
                        _this.list.splice(_this.list.indexOf(obj), 1);
                    },
                    classes: ['ndf-remove']
                })
            ],
            classes: ['ndf-full-bordered', 'ndf-multichild-row', (this.child) ? 'ndf-child' : 'ndf-parent']
        }), this.name + "_controls");
        this.counter++;
    };
    return MultiChildField;
}(FieldGroup));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var TextField = /** @class */ (function (_super) {
    __extends(TextField, _super);
    function TextField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'text';
        _this.value = options['value'] || '';
        _this.classes.push('ndf-text');
        _this.inputType = options['inputType'] || '';
        return _this;
    }
    return TextField;
}(Field));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var NumberField = /** @class */ (function (_super) {
    __extends(NumberField, _super);
    function NumberField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.value = options['value'] || 0;
        _this.precision = (options['precision'] != null) ? options['precision'] : 2;
        return _this;
    }
    /**
     * @return {?}
     */
    NumberField.prototype.deriveValue = /**
     * @return {?}
     */
    function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, _super.prototype.deriveValue.call(this).then(function (derived) {
                        if (!derived) {
                            _this.value = _this.prepareValue(_this.value);
                            return Promise.resolve(true);
                        }
                        return Promise.resolve(false);
                    })];
            });
        });
    };
    /**
     * @return {?}
     */
    NumberField.prototype.onBlur = /**
     * @return {?}
     */
    function () {
        this.value = this.prepareValue(this.value);
    };
    /**
     * @param {?} value
     * @return {?}
     */
    NumberField.prototype.prepareValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        /** @type {?} */
        var prepared;
        if (typeof value === 'number') {
            prepared = (/** @type {?} */ (value));
        }
        else if (typeof value === 'undefined') {
            prepared = 0;
        }
        else {
            prepared = parseFloat(value.toString().replace(/[^0-9.]/g, ''));
        }
        return prepared.toFixed(this.precision);
    };
    return NumberField;
}(TextField));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var TextareaField = /** @class */ (function (_super) {
    __extends(TextareaField, _super);
    function TextareaField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'textarea';
        _this.value = options['value'] || '';
        _this.classes.push('ndf-textarea');
        return _this;
    }
    return TextareaField;
}(Field));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var FormService = /** @class */ (function () {
    function FormService() {
        this.formGroup = new FormGroup({});
    }
    Object.defineProperty(FormService.prototype, "group", {
        get: /**
         * @return {?}
         */
        function () {
            return this.formGroup;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} field
     * @return {?}
     */
    FormService.prototype.registerField = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (!this.group.contains(field.id)) {
            this.group.addControl(field.id, this.controlFromField(field));
        }
    };
    /**
     * @param {?} fields
     * @return {?}
     */
    FormService.prototype.registerFields = /**
     * @param {?} fields
     * @return {?}
     */
    function (fields) {
        var _this = this;
        fields.forEach(function (field) {
            _this.registerField(field);
        });
    };
    /**
     * @param {?} field
     * @return {?}
     */
    FormService.prototype.deregisterField = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        this.group.removeControl(field.id);
    };
    /**
     * @param {?} fields
     * @return {?}
     */
    FormService.prototype.deregisterFields = /**
     * @param {?} fields
     * @return {?}
     */
    function (fields) {
        var _this = this;
        fields.forEach(function (field) {
            _this.deregisterField(field);
        });
    };
    /**
     * @param {?} field
     * @return {?}
     */
    FormService.prototype.controlFromField = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return new FormControl(field.value || '');
    };
    FormService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    FormService.ctorParameters = function () { return []; };
    return FormService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var Form = /** @class */ (function (_super) {
    __extends(Form, _super);
    function Form(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.hasFields = true;
        _this.isForm = true;
        _this.obj = options['obj'] || {};
        _this.submit = options['submit'] || null;
        _this.fields.setForm(_this);
        return _this;
    }
    Object.defineProperty(Form.prototype, "prefix", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.form) {
                return "" + this.form.prefix + this.name + "_";
            }
            else if (this.name) {
                return this.name + "_";
            }
            else {
                return '';
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} obj
     * @return {?}
     */
    Form.prototype.update = /**
     * @param {?} obj
     * @return {?}
     */
    function (obj) {
        this.obj = obj;
        this.fields.updateValues();
    };
    /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    Form.prototype.fieldChange = /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    function (key, value) {
        this.obj[key] = value;
    };
    /**
     * @return {?}
     */
    Form.prototype.onSubmit = /**
     * @return {?}
     */
    function () {
        if (this.submit !== null) {
            this.submit(this);
        }
    };
    return Form;
}(FormBase));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var FormComponent = /** @class */ (function () {
    function FormComponent(formService) {
        this.formService = formService;
    }
    /**
     * @return {?}
     */
    FormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    Object.defineProperty(FormComponent.prototype, "group", {
        get: /**
         * @return {?}
         */
        function () {
            return this.formService.group;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    FormComponent.prototype.onSubmit = /**
     * @return {?}
     */
    function () {
        this.form.onSubmit();
    };
    FormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-form',
                    template: "<form (ngSubmit)=\"onSubmit()\" [ngClass]=\"form.classes\" [formGroup]=\"group\">\n    <p *ngIf=\"form.message\" class=\"ndf-error\">{{ form.message }}</p>\n    <ndf-form-item *ngFor=\"let item of form.fields.fields\" [item]=\"item\"></ndf-form-item>\n</form>\n",
                    providers: [FormService]
                }] }
    ];
    /** @nocollapse */
    FormComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    FormComponent.propDecorators = {
        form: [{ type: Input }]
    };
    return FormComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var FormItemComponent = /** @class */ (function () {
    function FormItemComponent(formService) {
        this.formService = formService;
    }
    /**
     * @return {?}
     */
    FormItemComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.item.hasData) {
            this.formService.registerField((/** @type {?} */ (this.item)));
        }
    };
    /**
     * @return {?}
     */
    FormItemComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.item.hasData) {
            this.formService.deregisterField((/** @type {?} */ (this.item)));
        }
    };
    Object.defineProperty(FormItemComponent.prototype, "group", {
        get: /**
         * @return {?}
         */
        function () {
            return this.formService.group;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} value
     * @return {?}
     */
    FormItemComponent.prototype.onChange = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.item.onChange(value);
    };
    /**
     * @param {?} value
     * @return {?}
     */
    FormItemComponent.prototype.onBlur = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.item.onBlur(value);
    };
    /**
     * @return {?}
     */
    FormItemComponent.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this.item.onClick();
    };
    FormItemComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-form-item',
                    template: "<div [ngSwitch]=\"item.type\">\n    <ndf-checkbox-field *ngSwitchCase=\"'checkbox'\" [item]=\"item\"></ndf-checkbox-field>\n    <ndf-checkbox-set-field *ngSwitchCase=\"'checkboxset'\" [item]=\"item\"></ndf-checkbox-set-field>\n    <ndf-content-field *ngSwitchCase=\"'content'\" [item]=\"item\"></ndf-content-field>\n    <ndf-date-field *ngSwitchCase=\"'date'\" [item]=\"item\"></ndf-date-field>\n    <ndf-dropdown-field *ngSwitchCase=\"'dropdown'\" [item]=\"item\"></ndf-dropdown-field>\n    <ndf-field-group *ngSwitchCase=\"'group'\" [item]=\"item\"></ndf-field-group>\n    <ndf-field-group *ngSwitchCase=\"'multichild'\" [item]=\"item\"></ndf-field-group>\n    <ndf-form-action *ngSwitchCase=\"'action'\" [item]=\"item\"></ndf-form-action>\n    <ndf-header-field *ngSwitchCase=\"'header'\" [item]=\"item\"></ndf-header-field>\n    <ndf-option-set-field *ngSwitchCase=\"'optionset'\" [item]=\"item\"></ndf-option-set-field>\n    <ndf-text-field *ngSwitchCase=\"'text'\" [item]=\"item\"></ndf-text-field>\n    <ndf-textarea-field *ngSwitchCase=\"'textarea'\" [item]=\"item\"></ndf-textarea-field>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    /** @nocollapse */
    FormItemComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    FormItemComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return FormItemComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var CheckboxFieldComponent = /** @class */ (function (_super) {
    __extends(CheckboxFieldComponent, _super);
    function CheckboxFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CheckboxFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-checkbox-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label>\n        <div class=\"box\"><fa *ngIf=\"item.value\" name=\"check\" size=\"lt\"></fa></div>\n        <input type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.value\" (ngModelChange)=\"onChange($event)\" />\n        {{item.label}}\n    </label>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{item.message}}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    CheckboxFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return CheckboxFieldComponent;
}(FormItemComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var CheckboxSetFieldComponent = /** @class */ (function (_super) {
    __extends(CheckboxSetFieldComponent, _super);
    function CheckboxSetFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CheckboxSetFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-checkbox-set-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <ul>\n        <li *ngFor=\"let option of item.options\">\n            <label>\n                <div class=\"box\"><fa *ngIf=\"item.isChecked(option.key)\" name=\"check\" size=\"lt\"></fa></div>\n                <input type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.isChecked(option.key)\" [value]=\"option.key\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange(option.key)\" />\n                {{ option.value }}\n            </label>\n        </li>\n    </ul>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    CheckboxSetFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return CheckboxSetFieldComponent;
}(FormItemComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var ContentFieldComponent = /** @class */ (function (_super) {
    __extends(ContentFieldComponent, _super);
    function ContentFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ContentFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-content-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <p>{{ item.content }}</p>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    ContentFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return ContentFieldComponent;
}(FormItemComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var DateFieldComponent = /** @class */ (function (_super) {
    __extends(DateFieldComponent, _super);
    function DateFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DateFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-date-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <div class=\"ndf-date-picker\">\n        <input ngx-mydatepicker [name]=\"item.id\" [value]=\"item.value | date:'dd/MM/yyyy'\" [readonly]=\"item.readOnly\" (dateChanged)=\"onChange($event)\" [options]=\"{ dateFormat: 'dd/mm/yyyy', selectorHeight: 'auto' }\" #dp=\"ngx-mydatepicker\" />\n        <button *ngIf=\"item.value != null\" type=\"button\" class=\"ndf-date-clear\" (click)=\"dp.clearDate()\">\n            <fa name=\"times\" size=\"lt\"></fa>\n        </button>\n        <button type=\"button\" class=\"ndf-date-open\" (click)=\"dp.toggleCalendar()\">\n            <fa name=\"calendar-o\" size=\"lt\"></fa>\n        </button>\n    </div>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    DateFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return DateFieldComponent;
}(FormItemComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var DropdownFieldComponent = /** @class */ (function (_super) {
    __extends(DropdownFieldComponent, _super);
    function DropdownFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DropdownFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-dropdown-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <select [id]=\"item.id\" [formControlName]=\"item.id\" [value]=\"item.value ? item.value : ''\" (ngModelChange)=\"onChange($event)\">\n        <option *ngIf=\"item.emptyText && item.options.length < 1\" value=\"\">{{ item.emptyText }}</option>\n        <option *ngIf=\"item.placeholder && item.options.length > 0\" value=\"\">{{ item.placeholder }}</option>\n        <option *ngFor=\"let option of item.options\" [value]=\"option.key\" [disabled]=\"option.disabled\">{{ option.value }}</option>\n    </select>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    DropdownFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return DropdownFieldComponent;
}(FormItemComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var FieldGroupComponent = /** @class */ (function (_super) {
    __extends(FieldGroupComponent, _super);
    function FieldGroupComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FieldGroupComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-field-group',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <ndf-form-item *ngFor=\"let child of item.fields.fields\" [item]=\"child\"></ndf-form-item>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    FieldGroupComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return FieldGroupComponent;
}(FormItemComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var FormActionComponent = /** @class */ (function (_super) {
    __extends(FormActionComponent, _super);
    function FormActionComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(FormActionComponent.prototype, "label", {
        get: /**
         * @return {?}
         */
        function () {
            return this.item.loading ? this.item.loading : this.item.title;
        },
        enumerable: true,
        configurable: true
    });
    FormActionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-form-action',
                    template: "<button [attr.disabled]=\"item.loading != null ? '' : null\" [ngClass]=\"item.classes\" [type]=\"item.actionType\" (click)=\"onClick()\" [title]=\"item.title\">\n    <fa *ngIf=\"item.loading\" name=\"spinner\" size=\"lt\" animation=\"spin\"></fa><fa *ngIf=\"!item.loading && item.icon\" [name]=\"item.icon\" size=\"lt\"></fa> {{ label }}\n</button>\n",
                    providers: [FormService]
                }] }
    ];
    FormActionComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return FormActionComponent;
}(FormItemComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var HeaderFieldComponent = /** @class */ (function (_super) {
    __extends(HeaderFieldComponent, _super);
    function HeaderFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HeaderFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-header-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <h3>{{ item.title }}</h3>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    HeaderFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return HeaderFieldComponent;
}(FormItemComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var OptionSetFieldComponent = /** @class */ (function (_super) {
    __extends(OptionSetFieldComponent, _super);
    function OptionSetFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    OptionSetFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-option-set-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <ul>\n        <li *ngFor=\"let option of item.options\">\n            <label>\n                <div class=\"box\"><div *ngIf=\"item.value == option.key\"></div></div>\n                <input type=\"radio\" [formControlName]=\"item.id\" [checked]=\"item.value == option.key\" [value]=\"option.key\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" />\n                {{ option.value }}\n            </label>\n        </li>\n    </ul>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    OptionSetFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return OptionSetFieldComponent;
}(FormItemComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var TextFieldComponent = /** @class */ (function (_super) {
    __extends(TextFieldComponent, _super);
    function TextFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TextFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-text-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <label *ngIf=\"!item.hasLabel\">\n        <div *ngIf=\"item.type == 'checkbox'\" class=\"box\"><fa *ngIf=\"item.value\" name=\"check\" size=\"lt\"></fa></div>\n        <input *ngIf=\"item.type == 'checkbox'\" type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.value\" (ngModelChange)=\"onChange($event)\" />\n        {{ item.label }}\n    </label>\n    <input [formControlName]=\"item.id\" [id]=\"item.id\" [type]=\"item.inputType\" [placeholder]=\"item.placeholder\" [value]=\"item.value\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" (blur)=\"onBlur($event)\" />\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    TextFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return TextFieldComponent;
}(FormItemComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var TextareaFieldComponent = /** @class */ (function (_super) {
    __extends(TextareaFieldComponent, _super);
    function TextareaFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TextareaFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-textarea-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <textarea [formControlName]=\"item.id\" [id]=\"item.id\" [value]=\"item.value\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" (blur)=\"onBlur($event)\"></textarea>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    TextareaFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return TextareaFieldComponent;
}(FormItemComponent));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var NgDynamicFormsModule = /** @class */ (function () {
    function NgDynamicFormsModule() {
    }
    /**
     * @return {?}
     */
    NgDynamicFormsModule.forRoot = /**
     * @return {?}
     */
    function () {
        return {
            ngModule: NgDynamicFormsModule,
            providers: [FormService]
        };
    };
    NgDynamicFormsModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        FormsModule,
                        ReactiveFormsModule,
                        AngularFontAwesomeModule,
                        NgxMyDatePickerModule.forRoot()
                    ],
                    declarations: [
                        FormComponent,
                        FormItemComponent,
                        CheckboxFieldComponent,
                        CheckboxSetFieldComponent,
                        ContentFieldComponent,
                        DateFieldComponent,
                        DropdownFieldComponent,
                        FieldGroupComponent,
                        FormActionComponent,
                        HeaderFieldComponent,
                        OptionSetFieldComponent,
                        TextFieldComponent,
                        TextareaFieldComponent
                    ],
                    exports: [
                        FormComponent,
                        FormItemComponent,
                        CheckboxFieldComponent,
                        CheckboxSetFieldComponent,
                        ContentFieldComponent,
                        DateFieldComponent,
                        DropdownFieldComponent,
                        FieldGroupComponent,
                        FormActionComponent,
                        HeaderFieldComponent,
                        OptionSetFieldComponent,
                        TextFieldComponent,
                        TextareaFieldComponent
                    ]
                },] }
    ];
    return NgDynamicFormsModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { CheckboxField, CheckboxSetField, ContentField, DateField, DropdownField, FieldGroup, Field, FormAction, HeaderField, MultiChildField, NumberField, OptionSetField, TextField, TextareaField, FormService, FieldSet, FormBase, Form, NgDynamicFormsModule, CheckboxFieldComponent as ɵc, CheckboxSetFieldComponent as ɵd, ContentFieldComponent as ɵe, DateFieldComponent as ɵf, DropdownFieldComponent as ɵg, FieldGroupComponent as ɵh, FormActionComponent as ɵi, FormItemComponent as ɵb, FormComponent as ɵa, HeaderFieldComponent as ɵj, OptionSetFieldComponent as ɵk, TextFieldComponent as ɵl, TextareaFieldComponent as ɵm };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFpc2VkaWdpdGFsLW5nLWR5bmFtaWMtZm9ybXMuanMubWFwIiwic291cmNlcyI6WyJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGQtc2V0LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2Zvcm0tYmFzZS50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2NoZWNrYm94LWZpZWxkLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2ZpZWxkcy9vcHRpb24tc2V0LWZpZWxkLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2ZpZWxkcy9jaGVja2JveC1zZXQtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2NvbnRlbnQtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2RhdGUtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2Ryb3Bkb3duLWZpZWxkLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2ZpZWxkcy9maWVsZC1ncm91cC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvZm9ybS1hY3Rpb24udHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL2hlYWRlci1maWVsZC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvbXVsdGktY2hpbGQtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL3RleHQtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvZmllbGRzL251bWJlci1maWVsZC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9maWVsZHMvdGV4dGFyZWEtZmllbGQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvc2VydmljZXMvZm9ybS5zZXJ2aWNlLnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2Zvcm0udHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy9mb3JtLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2Zvcm0taXRlbS5jb21wb25lbnQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy9jaGVja2JveC1maWVsZC5jb21wb25lbnQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy9jaGVja2JveC1zZXQtZmllbGQuY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2NvbXBvbmVudHMvY29udGVudC1maWVsZC5jb21wb25lbnQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy9kYXRlLWZpZWxkLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2Ryb3Bkb3duLWZpZWxkLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2ZpZWxkLWdyb3VwLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2Zvcm0tYWN0aW9uLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL2hlYWRlci1maWVsZC5jb21wb25lbnQudHMiLCJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy9saWIvY29tcG9uZW50cy9vcHRpb24tc2V0LWZpZWxkLmNvbXBvbmVudC50cyIsIm5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zL2xpYi9jb21wb25lbnRzL3RleHQtZmllbGQuY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL2NvbXBvbmVudHMvdGV4dGFyZWEtZmllbGQuY29tcG9uZW50LnRzIiwibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvbGliL25nLWR5bmFtaWMtZm9ybXMubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi9mb3JtLWJhc2UnO1xuaW1wb3J0IHsgRm9ybSB9IGZyb20gJy4vZm9ybSc7XG5cblxuZXhwb3J0IGNsYXNzIEZpZWxkU2V0IHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzZXQ6IEZvcm1CYXNlW10pIHsgfVxuXG4gIHNldEZvcm0oZm9ybTogRm9ybSkge1xuICAgIHRoaXMuc2V0LmZvckVhY2goKGZpZWxkKSA9PiB7XG4gICAgICBmaWVsZC5zZXRGb3JtKGZvcm0pO1xuICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlVmFsdWVzKCk6IHZvaWQge1xuICAgIHRoaXMuc2V0LmZvckVhY2goKGZpZWxkKSA9PiB7XG4gICAgICBmaWVsZC5kZXJpdmVWYWx1ZSgpO1xuICAgIH0pO1xuICB9XG5cbiAgZ2V0IGZpZWxkcygpIHtcbiAgICByZXR1cm4gdGhpcy5zZXQ7XG4gIH1cblxuICBwdXNoKGl0ZW06IEZvcm1CYXNlKTogdm9pZCB7XG4gICAgdGhpcy5zZXQucHVzaChpdGVtKTtcbiAgfVxuXG4gIGluc2VydEJlZm9yZShpdGVtOiBGb3JtQmFzZSwgYmVmb3JlOiBzdHJpbmcsIGF0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIGxldCBwYXJlbnQ6IEZvcm1CYXNlID0gbnVsbDtcbiAgICBpZiAoZmllbGQgPT09IG51bGwpIHtcbiAgICAgIGNvbnN0IHNlYXJjaDogeyBmaWVsZDogRm9ybUJhc2UsIHBhcmVudDogRm9ybUJhc2UgfSA9IHRoaXMuZmllbGRXaXRoUGFyZW50KGJlZm9yZSwgYXR0cik7XG4gICAgICBwYXJlbnQgPSBzZWFyY2gucGFyZW50O1xuICAgICAgZmllbGQgPSBzZWFyY2guZmllbGQ7XG4gICAgfVxuICAgIGlmIChwYXJlbnQgPT09IG51bGwpIHtcbiAgICAgIHRoaXMuc2V0LnNwbGljZSh0aGlzLnNldC5pbmRleE9mKGZpZWxkKSwgMCwgaXRlbSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHBhcmVudC5maWVsZHMuaW5zZXJ0QmVmb3JlKGl0ZW0sIGJlZm9yZSwgYXR0ciwgZmllbGQpO1xuICAgIH1cbiAgfVxuXG4gIGluc2VydEJlZm9yZU5hbWUoaXRlbTogRm9ybUJhc2UsIGJlZm9yZTogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKTogdm9pZCB7XG4gICAgcmV0dXJuIHRoaXMuaW5zZXJ0QmVmb3JlKGl0ZW0sIGJlZm9yZSwgJ25hbWUnLCBmaWVsZCk7XG4gIH1cblxuICBpbnNlcnRCZWZvcmVJZChpdGVtOiBGb3JtQmFzZSwgYmVmb3JlOiBzdHJpbmcsIGZpZWxkOiBGb3JtQmFzZSA9IG51bGwpOiB2b2lkIHtcbiAgICByZXR1cm4gdGhpcy5pbnNlcnRCZWZvcmUoaXRlbSwgYmVmb3JlLCAnaWQnLCBmaWVsZCk7XG4gIH1cblxuICBpbnNlcnRBZnRlcihpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgYXR0cjogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKTogdm9pZCB7XG4gICAgbGV0IHBhcmVudDogRm9ybUJhc2UgPSBudWxsO1xuICAgIGlmIChmaWVsZCA9PT0gbnVsbCkge1xuICAgICAgY29uc3Qgc2VhcmNoOiB7IGZpZWxkOiBGb3JtQmFzZSwgcGFyZW50OiBGb3JtQmFzZSB9ID0gdGhpcy5maWVsZFdpdGhQYXJlbnQoYWZ0ZXIsIGF0dHIpO1xuICAgICAgcGFyZW50ID0gc2VhcmNoLnBhcmVudDtcbiAgICAgIGZpZWxkID0gc2VhcmNoLmZpZWxkO1xuICAgIH1cbiAgICBpZiAocGFyZW50ID09PSBudWxsKSB7XG4gICAgICB0aGlzLnNldC5zcGxpY2UodGhpcy5zZXQuaW5kZXhPZihmaWVsZCkgKyAxLCAwLCBpdGVtKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGFyZW50LmZpZWxkcy5pbnNlcnRBZnRlcihpdGVtLCBhZnRlciwgYXR0ciwgZmllbGQpO1xuICAgIH1cbiAgfVxuXG4gIGluc2VydEFmdGVyTmFtZShpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgX2F0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIHRoaXMuaW5zZXJ0QWZ0ZXIoaXRlbSwgYWZ0ZXIsICduYW1lJywgZmllbGQpO1xuICB9XG5cbiAgaW5zZXJ0QWZ0ZXJJZChpdGVtOiBGb3JtQmFzZSwgYWZ0ZXI6IHN0cmluZywgX2F0dHI6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCk6IHZvaWQge1xuICAgIHRoaXMuaW5zZXJ0QWZ0ZXIoaXRlbSwgYWZ0ZXIsICdpZCcsIGZpZWxkKTtcbiAgfVxuXG4gIHJlbW92ZShrZXk6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCwgYXR0cjogc3RyaW5nKSB7XG4gICAgbGV0IHBhcmVudDogRm9ybUJhc2UgPSBudWxsO1xuICAgIGlmIChmaWVsZCA9PT0gbnVsbCkge1xuICAgICAgY29uc3Qgc2VhcmNoOiB7IGZpZWxkOiBGb3JtQmFzZSwgcGFyZW50OiBGb3JtQmFzZSB9ID0gdGhpcy5maWVsZFdpdGhQYXJlbnQoa2V5LCBhdHRyKTtcbiAgICAgIHBhcmVudCA9IHNlYXJjaC5wYXJlbnQ7XG4gICAgICBmaWVsZCA9IHNlYXJjaC5maWVsZDtcbiAgICB9XG4gICAgaWYgKHBhcmVudCA9PT0gbnVsbCkge1xuICAgICAgdGhpcy5zZXQuc3BsaWNlKHRoaXMuc2V0LmluZGV4T2YoZmllbGQpLCAxKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGFyZW50LmZpZWxkcy5yZW1vdmUoa2V5LCBmaWVsZCwgYXR0cik7XG4gICAgfVxuICB9XG5cbiAgcmVtb3ZlQnlOYW1lKG5hbWU6IHN0cmluZywgZmllbGQ6IEZvcm1CYXNlID0gbnVsbCkge1xuICAgIHRoaXMucmVtb3ZlKG5hbWUsIGZpZWxkLCAnbmFtZScpO1xuICB9XG5cbiAgcmVtb3ZlQnlJZChpZDogc3RyaW5nLCBmaWVsZDogRm9ybUJhc2UgPSBudWxsKSB7XG4gICAgdGhpcy5yZW1vdmUoaWQsIGZpZWxkLCAnaWQnKTtcbiAgfVxuXG4gIGZpZWxkKGtleTogc3RyaW5nLCBhdHRyOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLmZpZWxkV2l0aFBhcmVudChrZXksIGF0dHIpLmZpZWxkO1xuICB9XG5cbiAgZmllbGRCeU5hbWUobmFtZTogc3RyaW5nKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZChuYW1lLCAnbmFtZScpO1xuICB9XG5cbiAgZmllbGRCeUlkKGlkOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLmZpZWxkKGlkLCAnaWQnKTtcbiAgfVxuXG4gIGZpZWxkV2l0aFBhcmVudChrZXk6IHN0cmluZywgYXR0cjogc3RyaW5nKTogeyBmaWVsZDogYW55LCBwYXJlbnQ6IGFueSB9IHtcbiAgICBsZXQgZmllbGQ6IGFueSA9IG51bGw7XG4gICAgbGV0IHBhcmVudDogYW55ID0gbnVsbDtcbiAgICBjb25zdCBzZWFyY2g6IGFueVtdID0gdGhpcy5zZXQuZmlsdGVyKGNoaWxkID0+IGNoaWxkW2F0dHJdID09PSBrZXkpO1xuICAgIGlmIChzZWFyY2gubGVuZ3RoID4gMCkge1xuICAgICAgZmllbGQgPSBzZWFyY2hbMF07XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IHBhcmVudHM6IGFueVtdID0gdGhpcy5zZXQuZmlsdGVyKGNoaWxkID0+IGNoaWxkLmhhc0ZpZWxkcyA9PT0gdHJ1ZSk7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHBhcmVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgY29uc3QgY2hlY2s6IHsgZmllbGQ6IGFueSwgcGFyZW50OiBhbnkgfSA9IHBhcmVudHNbaV0uZmllbGRzLmZpZWxkV2l0aFBhcmVudChrZXksIGF0dHIpO1xuICAgICAgICBpZiAoY2hlY2suZmllbGQgIT0gbnVsbCkge1xuICAgICAgICAgIGZpZWxkID0gY2hlY2suZmllbGQ7XG4gICAgICAgICAgcGFyZW50ID0gcGFyZW50c1tpXTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4geyBmaWVsZDogZmllbGQsIHBhcmVudDogcGFyZW50IH07XG4gIH1cblxuICBmaWVsZEJ5TmFtZVdpdGhQYXJlbnQobmFtZTogc3RyaW5nKTogeyBmaWVsZDogYW55LCBwYXJlbnQ6IGFueSB9IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZFdpdGhQYXJlbnQobmFtZSwgJ25hbWUnKTtcbiAgfVxuXG4gIGZpZWxkQnlJZFdpdGhQYXJlbnQoaWQ6IHN0cmluZyk6IHsgZmllbGQ6IGFueSwgcGFyZW50OiBhbnkgfSB7XG4gICAgcmV0dXJuIHRoaXMuZmllbGRXaXRoUGFyZW50KGlkLCAnaWQnKTtcbiAgfVxuXG4gIHJlc2V0VmFsaWRhdGlvbigpOiB2b2lkIHtcbiAgICB0aGlzLmZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcbiAgICAgIGZpZWxkLnJlc2V0VmFsaWRhdGlvbigpO1xuICAgIH0pO1xuICB9XG59XG4iLCJpbXBvcnQgeyBGb3JtIH0gZnJvbSAnLi9mb3JtJztcbmltcG9ydCB7IEZpZWxkU2V0IH0gZnJvbSAnLi9maWVsZC1zZXQnO1xuXG5pbXBvcnQgeyBWYWxpZGF0aW9uUmVzdWx0IH0gZnJvbSAnLi92YWxpZGF0aW9uLXJlc3VsdC5pbnRlcmZhY2UnO1xuXG5pbXBvcnQgeyBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuZXhwb3J0IGNsYXNzIEZvcm1CYXNlIHtcbiAgbmFtZTogc3RyaW5nO1xuICBpZDogc3RyaW5nO1xuICB0eXBlOiBzdHJpbmc7XG4gIGNsYXNzZXM6IHN0cmluZ1tdO1xuICBmaWVsZHM6IEZpZWxkU2V0O1xuICBtZXNzYWdlOiBzdHJpbmc7XG4gIHJlYWRPbmx5OiBib29sZWFuO1xuICBjaGFuZ2U6IChmaWVsZDogRm9ybUJhc2UsIHZhbHVlOiBhbnkpID0+IGJvb2xlYW47XG4gIGJsdXI6IChmaWVsZDogRm9ybUJhc2UsIHZhbHVlOiBhbnkpID0+IHZvaWQ7XG4gIHByb3RlY3RlZCBmb3JtOiBGb3JtO1xuXG4gIGlzRm9ybSA9IGZhbHNlO1xuICBoYXNEYXRhID0gZmFsc2U7XG4gIGhhc0ZpZWxkcyA9IGZhbHNlO1xuICBoYXNMYWJlbCA9IHRydWU7XG4gIGlzQWN0aW9uID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge1xuICAgIG5hbWU/OiBzdHJpbmcsXG4gICAgY2xhc3Nlcz86IHN0cmluZ1tdLFxuICAgIGZpZWxkcz86IEZvcm1CYXNlW10sXG4gICAgcmVhZE9ubHk/OiBib29sZWFuLFxuICAgIGNoYW5nZT86IChmaWVsZDogRm9ybUJhc2UsIHZhbHVlOiBhbnkpID0+IGJvb2xlYW4sXG4gICAgYmx1cj86IChmaWVsZDogRm9ybUJhc2UsIHZhbHVlOiBhbnkpID0+IHZvaWRcbiAgfSA9IHt9KSB7XG4gICAgdGhpcy5uYW1lID0gb3B0aW9ucy5uYW1lIHx8ICcnO1xuICAgIHRoaXMuaWQgPSB0aGlzLm5hbWU7XG4gICAgdGhpcy5jbGFzc2VzID0gb3B0aW9ucy5jbGFzc2VzIHx8IFtdO1xuICAgIHRoaXMuZmllbGRzID0gbmV3IEZpZWxkU2V0KG9wdGlvbnMuZmllbGRzIHx8IFtdKTtcbiAgICB0aGlzLnJlYWRPbmx5ID0gb3B0aW9ucy5yZWFkT25seSB8fCBmYWxzZTtcbiAgICB0aGlzLmNoYW5nZSA9IG9wdGlvbnMuY2hhbmdlIHx8IG51bGw7XG4gICAgdGhpcy5ibHVyID0gb3B0aW9ucy5ibHVyIHx8IG51bGw7XG4gIH1cblxuICBnZXRGb3JtKCk6IEZvcm0ge1xuICAgIHJldHVybiB0aGlzLmZvcm07XG4gIH1cblxuICBzZXRGb3JtKGZvcm06IEZvcm0pOiB2b2lkIHtcbiAgICB0aGlzLmZvcm0gPSBmb3JtO1xuICAgIGlmIChmb3JtLnByZWZpeCkge1xuICAgICAgdGhpcy5pZCA9IGAke2Zvcm0ucHJlZml4fSR7dGhpcy5uYW1lfWA7XG4gICAgfVxuICAgIGlmICghdGhpcy5pc0Zvcm0pIHtcbiAgICAgIHRoaXMuZmllbGRzLnNldEZvcm0oZm9ybSk7XG4gICAgfVxuICB9XG5cbiAgYXN5bmMgZGVyaXZlVmFsdWUoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShmYWxzZSk7XG4gIH1cblxuICBnZXQgY29udHJvbCgpOiBGb3JtQ29udHJvbCB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICBvbkNoYW5nZSh2YWx1ZSkge1xuICAgIGxldCBwcm9wYWdhdGUgPSB0cnVlO1xuICAgIGlmICh0aGlzLmNoYW5nZSAhPSBudWxsKSB7XG4gICAgICBwcm9wYWdhdGUgPSB0aGlzLmNoYW5nZSh0aGlzLCB2YWx1ZSk7XG4gICAgfVxuICAgIGlmIChwcm9wYWdhdGUpIHtcbiAgICAgIHRoaXMuZm9ybS5maWVsZENoYW5nZSh0aGlzLm5hbWUsIHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBvbkJsdXIodmFsdWUpIHtcbiAgICBpZiAodGhpcy5ibHVyICE9IG51bGwpIHtcbiAgICAgIHRoaXMuYmx1cih0aGlzLCB2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgb25DbGljaygpIHtcblxuICB9XG5cbiAgc2V0TWVzc2FnZShtZXNzYWdlOiBzdHJpbmcpOiB2b2lkIHtcbiAgICB0aGlzLm1lc3NhZ2UgPSBtZXNzYWdlO1xuICB9XG5cbiAgbG9hZFZhbGlkYXRpb24ocmVzdWx0OiBWYWxpZGF0aW9uUmVzdWx0KTogdm9pZCB7XG4gICAgdGhpcy5yZXNldFZhbGlkYXRpb24oKTtcbiAgICBpZiAocmVzdWx0Lm1lc3NhZ2VzICE9IG51bGwgJiYgcmVzdWx0Lm1lc3NhZ2VzLmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMuc2V0TWVzc2FnZShyZXN1bHQubWVzc2FnZXNbMF0pO1xuICAgIH1cbiAgICBmb3IgKGNvbnN0IGtleSBpbiByZXN1bHQuZmllbGRzKSB7XG4gICAgICBpZiAocmVzdWx0LmZpZWxkcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgIGNvbnN0IGZpZWxkOiBGb3JtQmFzZSA9IHRoaXMuZmllbGRzLmZpZWxkQnlOYW1lKGtleSk7XG4gICAgICAgIGlmIChmaWVsZCAhPSBudWxsKSB7XG4gICAgICAgICAgZmllbGQubG9hZFZhbGlkYXRpb24ocmVzdWx0LmZpZWxkc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJlc2V0VmFsaWRhdGlvbigpIHtcbiAgICB0aGlzLnNldE1lc3NhZ2UobnVsbCk7XG4gICAgaWYgKHRoaXMuZmllbGRzKSB7XG4gICAgICB0aGlzLmZpZWxkcy5yZXNldFZhbGlkYXRpb24oKTtcbiAgICB9XG4gIH1cbn1cbiIsImltcG9ydCB7IEZvcm0gfSBmcm9tICcuLi9mb3JtJztcbmltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuXG5leHBvcnQgY2xhc3MgRmllbGQ8VD4gZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIGlkOiBzdHJpbmc7XG4gIGhhc0RhdGEgPSB0cnVlO1xuICB2YWx1ZTogVDtcbiAgbGFiZWw6IHN0cmluZztcbiAgcGxhY2Vob2xkZXI6IHN0cmluZztcbiAgZGVyaXZlOiAoZmllbGQ6IEZpZWxkPFQ+KSA9PiBQcm9taXNlPGJvb2xlYW4+O1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHtcbiAgICB2YWx1ZT86IFQsXG4gICAgbGFiZWw/OiBzdHJpbmcsXG4gICAgY2xhc3Nlcz86IHN0cmluZ1tdLFxuICAgIHBsYWNlaG9sZGVyPzogc3RyaW5nLFxuICAgIGRlcml2ZT86IChmaWVsZDogRmllbGQ8VD4pID0+IFByb21pc2U8Ym9vbGVhbj5cbiAgfSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy52YWx1ZSA9IG9wdGlvbnMudmFsdWU7XG4gICAgdGhpcy5sYWJlbCA9IG9wdGlvbnMubGFiZWwgfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1maWVsZCcpO1xuICAgIHRoaXMucGxhY2Vob2xkZXIgPSBvcHRpb25zLnBsYWNlaG9sZGVyIHx8ICcnO1xuICAgIGlmICh0aGlzLnBsYWNlaG9sZGVyLmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtcGxhY2Vob2xkZXInKTtcbiAgICB9XG4gICAgdGhpcy5kZXJpdmUgPSBvcHRpb25zLmRlcml2ZSB8fCBudWxsO1xuICB9XG5cbiAgc2V0Rm9ybShmb3JtOiBGb3JtKTogdm9pZCB7XG4gICAgc3VwZXIuc2V0Rm9ybShmb3JtKTtcbiAgICB0aGlzLmRlcml2ZVZhbHVlKCk7XG4gIH1cblxuICBvbkNoYW5nZSh2YWx1ZSkge1xuICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcbiAgICBzdXBlci5vbkNoYW5nZSh0aGlzLnByZXBhcmVWYWx1ZSh2YWx1ZSkpO1xuICB9XG5cbiAgYXN5bmMgZGVyaXZlVmFsdWUoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgcmV0dXJuIHN1cGVyLmRlcml2ZVZhbHVlKCkudGhlbihkZXJpdmVkID0+IHtcbiAgICAgIGlmICghZGVyaXZlZCkge1xuICAgICAgICBpZiAodGhpcy5kZXJpdmUgIT0gbnVsbCkge1xuICAgICAgICAgIHJldHVybiB0aGlzLmRlcml2ZSh0aGlzKTtcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmZvcm0ub2JqLmhhc093blByb3BlcnR5KHRoaXMubmFtZSkpIHtcbiAgICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5mb3JtLm9ialt0aGlzLm5hbWVdO1xuICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUodHJ1ZSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZmFsc2UpO1xuICAgIH0pO1xuICB9XG5cbiAgcHJlcGFyZVZhbHVlKHZhbHVlOiBhbnkpOiBhbnkge1xuICAgIGlmICh2YWx1ZSA9PSBudWxsKSB7XG4gICAgICByZXR1cm4gJyc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9XG4gIH1cbn1cbiIsImltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi9maWVsZCc7XG5cblxuZXhwb3J0IGNsYXNzIENoZWNrYm94RmllbGQgZXh0ZW5kcyBGaWVsZDxib29sZWFuPiB7XG4gIHR5cGUgPSAnY2hlY2tib3gnO1xuICBoYXNMYWJlbCA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWNoZWNrYm94Jyk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi9maWVsZCc7XG5cbmV4cG9ydCBjbGFzcyBPcHRpb25TZXRGaWVsZDxUPiBleHRlbmRzIEZpZWxkPFQ+IHtcbiAgdHlwZSA9ICdvcHRpb25zZXQnO1xuICBvcHRpb25TZXQ6IHsga2V5OiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcsIGRpc2FibGVkPzogYm9vbGVhbiB9W10gPSBbXTtcbiAgb3B0aW9uczogeyBrZXk6IHN0cmluZywgdmFsdWU6IHN0cmluZywgZGlzYWJsZWQ/OiBib29sZWFuIH1bXSA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLW9wdGlvbnNldCcpO1xuICAgIHRoaXMub3B0aW9uU2V0ID0gb3B0aW9uc1snb3B0aW9ucyddIHx8IFtdO1xuICAgIFByb21pc2UucmVzb2x2ZSh0aGlzLm9wdGlvblNldCkudGhlbihvcHRpb25MaXN0ID0+IHtcbiAgICAgIHRoaXMuc2V0T3B0aW9ucyhvcHRpb25MaXN0KTtcbiAgICB9KTtcbiAgfVxuXG4gIHNldE9wdGlvbnMob3B0aW9uczogQXJyYXk8eyBrZXk6IHN0cmluZywgdmFsdWU6IHN0cmluZywgZGlzYWJsZWQ/OiBib29sZWFuIH0+KSB7XG4gICAgdGhpcy5vcHRpb25zID0gb3B0aW9ucztcbiAgfVxufVxuIiwiaW1wb3J0IHsgT3B0aW9uU2V0RmllbGQgfSBmcm9tICcuL29wdGlvbi1zZXQtZmllbGQnO1xuXG5cbmV4cG9ydCBjbGFzcyBDaGVja2JveFNldEZpZWxkPFQ+IGV4dGVuZHMgT3B0aW9uU2V0RmllbGQ8VD4ge1xuICB0eXBlID0gJ2NoZWNrYm94c2V0JztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1jaGVja2JveHNldCcpO1xuICB9XG5cbiAgb25DaGFuZ2UodmFsdWUpIHtcbiAgICBjb25zdCBvYmogPSB0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV07XG4gICAgaWYgKHRoaXMuaXNDaGVja2VkKHZhbHVlKSkge1xuICAgICAgb2JqLnNwbGljZShvYmouaW5kZXhPZih2YWx1ZSksIDEpO1xuICAgIH0gZWxzZSB7XG4gICAgICBvYmoucHVzaCh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgaXNDaGVja2VkKHZhbHVlKTogYm9vbGVhbiB7XG4gICAgY29uc3Qgc2VsZWN0ZWQgPSB0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV07XG4gICAgaWYgKHNlbGVjdGVkICE9IG51bGwpIHtcbiAgICAgIHJldHVybiBzZWxlY3RlZC5maWx0ZXIobyA9PiBvID09PSB2YWx1ZSkubGVuZ3RoID4gMDtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG59XG4iLCJpbXBvcnQgeyBGb3JtQmFzZSB9IGZyb20gJy4uL2Zvcm0tYmFzZSc7XG5cbmV4cG9ydCBjbGFzcyBDb250ZW50RmllbGQgZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIHR5cGUgPSAnY29udGVudCc7XG4gIGNvbnRlbnQ6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jb250ZW50ID0gb3B0aW9uc1snY29udGVudCddIHx8ICcnO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtZmllbGQnKTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWNvbnRlbnQnKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuL2ZpZWxkJztcblxuaW1wb3J0IHsgSU15RGF0ZU1vZGVsIH0gZnJvbSAnbmd4LW15ZGF0ZXBpY2tlcic7XG5cbmV4cG9ydCBjbGFzcyBEYXRlRmllbGQgZXh0ZW5kcyBGaWVsZDxEYXRlPiB7XG4gIHR5cGUgPSAnZGF0ZSc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtZGF0ZScpO1xuICB9XG5cbiAgb25DaGFuZ2UodmFsdWU6IElNeURhdGVNb2RlbCkge1xuICAgIHN1cGVyLm9uQ2hhbmdlKCh2YWx1ZS5qc2RhdGUgPT0gbnVsbCkgPyBudWxsIDogbmV3IERhdGUodmFsdWUuanNkYXRlKSk7XG4gIH1cbn1cbiIsImltcG9ydCB7IE9wdGlvblNldEZpZWxkIH0gZnJvbSAnLi9vcHRpb24tc2V0LWZpZWxkJztcblxuZXhwb3J0IGNsYXNzIERyb3Bkb3duRmllbGQgZXh0ZW5kcyBPcHRpb25TZXRGaWVsZDxzdHJpbmc+IHtcbiAgdHlwZSA9ICdkcm9wZG93bic7XG4gIGVtcHR5VGV4dDogc3RyaW5nO1xuICBsb2FkZWQ6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1kcm9wZG93bicpO1xuICAgIHRoaXMubG9hZGVkID0gb3B0aW9uc1snZW1wdHlUZXh0J10gfHwgJyc7XG4gICAgdGhpcy5lbXB0eVRleHQgPSBvcHRpb25zWydsb2FkaW5nVGV4dCddIHx8ICcnO1xuICB9XG5cbiAgc2V0T3B0aW9ucyhvcHRpb25zOiBBcnJheTx7IGtleTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nLCBkaXNhYmxlZD86IGJvb2xlYW4gfT4pIHtcbiAgICBzdXBlci5zZXRPcHRpb25zKG9wdGlvbnMpO1xuICAgIHRoaXMuZW1wdHlUZXh0ID0gdGhpcy5sb2FkZWQ7XG4gIH1cbn1cbiIsImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuZXhwb3J0IGNsYXNzIEZpZWxkR3JvdXAgZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIGhhc0ZpZWxkcyA9IHRydWU7XG4gIHR5cGUgPSAnZ3JvdXAnO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgfVxuXG4gIGFzeW5jIGRlcml2ZVZhbHVlKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgIHJldHVybiBzdXBlci5kZXJpdmVWYWx1ZSgpLnRoZW4oZGVyaXZlZCA9PiB7XG4gICAgICB0aGlzLmZpZWxkcy51cGRhdGVWYWx1ZXMoKTtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZGVyaXZlZCk7XG4gICAgfSk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEZvcm1CYXNlIH0gZnJvbSAnLi4vZm9ybS1iYXNlJztcblxuZXhwb3J0IGNsYXNzIEZvcm1BY3Rpb24gZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIGlzQWN0aW9uID0gdHJ1ZTtcbiAgdHlwZSA9ICdhY3Rpb24nO1xuICB0aXRsZTogc3RyaW5nO1xuICBpY29uOiBzdHJpbmc7XG4gIGFjdGlvblR5cGU6IHN0cmluZztcbiAgbG9hZGluZzogc3RyaW5nO1xuICBjbGljazogKGFjdGlvbjogRm9ybUFjdGlvbikgPT4gdm9pZDtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy50aXRsZSA9IG9wdGlvbnNbJ3RpdGxlJ10gfHwgbnVsbDtcbiAgICB0aGlzLmljb24gPSBvcHRpb25zWydpY29uJ10gfHwgbnVsbDtcbiAgICB0aGlzLmFjdGlvblR5cGUgPSBvcHRpb25zWydhY3Rpb25UeXBlJ10gfHwgJ3N1Ym1pdCc7XG4gICAgdGhpcy5jbGljayA9IG9wdGlvbnNbJ2NsaWNrJ10gfHwgbnVsbDtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLWFjdGlvbicpO1xuICB9XG5cbiAgc2hvd0xvYWRpbmcodGV4dDogc3RyaW5nKSB7XG4gICAgdGhpcy5sb2FkaW5nID0gdGV4dDtcbiAgfVxuXG4gIGNsZWFyTG9hZGluZygpIHtcbiAgICB0aGlzLmxvYWRpbmcgPSBudWxsO1xuICB9XG5cbiAgb25DbGljaygpIHtcbiAgICBpZiAodGhpcy5jbGljayAhPSBudWxsKSB7XG4gICAgICB0aGlzLmNsaWNrKHRoaXMpO1xuICAgIH1cbiAgfVxufVxuIiwiaW1wb3J0IHsgRm9ybUJhc2UgfSBmcm9tICcuLi9mb3JtLWJhc2UnO1xuXG5leHBvcnQgY2xhc3MgSGVhZGVyRmllbGQgZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIHR5cGUgPSAnaGVhZGVyJztcbiAgdGl0bGU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy50aXRsZSA9IG9wdGlvbnNbJ3RpdGxlJ10gfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1maWVsZCcpO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtaGVhZGVyJyk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEZpZWxkR3JvdXAgfSBmcm9tICcuL2ZpZWxkLWdyb3VwJztcbmltcG9ydCB7IEhlYWRlckZpZWxkIH0gZnJvbSAnLi9oZWFkZXItZmllbGQnO1xuaW1wb3J0IHsgRm9ybUFjdGlvbiB9IGZyb20gJy4vZm9ybS1hY3Rpb24nO1xuaW1wb3J0IHsgRm9ybSB9IGZyb20gJy4uL2Zvcm0nO1xuaW1wb3J0IHsgVmFsaWRhdGlvblJlc3VsdCB9IGZyb20gJy4uL3ZhbGlkYXRpb24tcmVzdWx0LmludGVyZmFjZSc7XG5cbmV4cG9ydCBjbGFzcyBNdWx0aUNoaWxkRmllbGQgZXh0ZW5kcyBGaWVsZEdyb3VwIHtcbiAgdHlwZTogJ211bHRpY2hpbGQnO1xuICBzdWJGb3JtOiAob2JqOiBhbnksIHByZWZpeDogc3RyaW5nKSA9PiBGb3JtO1xuICBuZXdPYmo6ICgpID0+IGFueTtcbiAgYWRkQnV0dG9uVGV4dDogc3RyaW5nO1xuICByZW1vdmVCdXR0b25UZXh0OiBzdHJpbmc7XG4gIGNoaWxkID0gZmFsc2U7XG4gIHByaXZhdGUgY291bnRlciA9IDA7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMuc3ViRm9ybSA9IG9wdGlvbnNbJ3N1YkZvcm0nXSB8fCBudWxsO1xuICAgIHRoaXMubmV3T2JqID0gb3B0aW9uc1snbmV3T2JqJ10gfHwgbnVsbDtcbiAgICB0aGlzLmFkZEJ1dHRvblRleHQgPSBvcHRpb25zWydhZGRCdXR0b25UZXh0J10gfHwgJ0FkZCBOZXcgTGluZSc7XG4gICAgdGhpcy5yZW1vdmVCdXR0b25UZXh0ID0gb3B0aW9uc1sncmVtb3ZlQnV0dG9uVGV4dCddIHx8ICdSZW1vdmUgTGluZSc7XG4gICAgdGhpcy5jaGlsZCA9IG9wdGlvbnNbJ2NoaWxkJ10gfHwgZmFsc2U7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi1tdWx0aWNoaWxkJyk7XG4gICAgaWYgKG9wdGlvbnNbJ3RpdGxlJ10pIHtcbiAgICAgIHRoaXMuZmllbGRzLnB1c2gobmV3IEhlYWRlckZpZWxkKHtcbiAgICAgICAgdGl0bGU6IG9wdGlvbnNbJ3RpdGxlJ11cbiAgICAgIH0pKTtcbiAgICAgIHRoaXMuZmllbGRzLnB1c2gobmV3IEZpZWxkR3JvdXAoe1xuICAgICAgICBuYW1lOiBgJHt0aGlzLm5hbWV9X2NvbnRyb2xzYCxcbiAgICAgICAgZmllbGRzOiBbXG4gICAgICAgICAgbmV3IEZvcm1BY3Rpb24oe1xuICAgICAgICAgICAgdGl0bGU6IHRoaXMuYWRkQnV0dG9uVGV4dCxcbiAgICAgICAgICAgIGljb246ICdwbHVzJyxcbiAgICAgICAgICAgIGNsaWNrOiAoKSA9PiB7XG4gICAgICAgICAgICAgIGlmICh0aGlzLnN1YkZvcm0gIT09IG51bGwgJiYgdGhpcy5uZXdPYmogIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBvYmo6IGFueSA9IHRoaXMubmV3T2JqKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5saXN0LnB1c2gob2JqKTtcbiAgICAgICAgICAgICAgICB0aGlzLmFkZENoaWxkKG9iaik7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjbGFzc2VzIDogWyduZGYtYWRkJ11cbiAgICAgICAgICB9KVxuICAgICAgICBdLFxuICAgICAgICBjbGFzc2VzOiBbJ25kZi1mdWxsLWJvcmRlcmVkJywgJ25kZi1tdWx0aWNoaWxkLWZvb3RlciddXG4gICAgICB9KSk7XG4gICAgfVxuICB9XG5cbiAgZ2V0IGxpc3QoKTogQXJyYXk8YW55PiB7XG4gICAgaWYgKHRoaXMuZm9ybS5vYmpbdGhpcy5uYW1lXSA9PSBudWxsKSB7XG4gICAgICB0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV0gPSBbXTtcbiAgICB9XG4gICAgcmV0dXJuICh0aGlzLmZvcm0ub2JqW3RoaXMubmFtZV0gYXMgQXJyYXk8YW55Pik7XG4gIH1cblxuICBzZXRGb3JtKGZvcm06IEZvcm0pOiB2b2lkIHtcbiAgICBzdXBlci5zZXRGb3JtKGZvcm0pO1xuICAgIGlmICh0aGlzLnN1YkZvcm0gIT09IG51bGwpIHtcbiAgICAgIHRoaXMubGlzdC5mb3JFYWNoKChvYmopID0+IHtcbiAgICAgICAgdGhpcy5hZGRDaGlsZChvYmopO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgbG9hZFZhbGlkYXRpb24ocmVzdWx0OiBWYWxpZGF0aW9uUmVzdWx0KTogdm9pZCB7XG4gICAgdGhpcy5yZXNldFZhbGlkYXRpb24oKTtcbiAgICBmb3IgKGNvbnN0IGtleSBpbiByZXN1bHQuZmllbGRzKSB7XG4gICAgICBpZiAocmVzdWx0LmZpZWxkcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgIGNvbnN0IGNoaWxkID0gYCR7dGhpcy5mb3JtLnByZWZpeH1saW5lXyR7a2V5fWA7XG4gICAgICAgIGNvbnN0IGZpZWxkID0gdGhpcy5maWVsZHMuZmllbGRCeU5hbWUoY2hpbGQpO1xuICAgICAgICBpZiAoZmllbGQgIT0gbnVsbCkge1xuICAgICAgICAgIGZpZWxkLmxvYWRWYWxpZGF0aW9uKHJlc3VsdC5maWVsZHNba2V5XSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGFkZENoaWxkKG9iajogYW55KTogdm9pZCB7XG4gICAgY29uc3QgZ3JvdXAgPSBgJHt0aGlzLmZvcm0ucHJlZml4fWxpbmVfJHt0aGlzLmNvdW50ZXJ9X2dyb3VwYDtcbiAgICB0aGlzLmZpZWxkcy5pbnNlcnRCZWZvcmVJZChcbiAgICAgIG5ldyBGaWVsZEdyb3VwKHtcbiAgICAgICAgbmFtZTogZ3JvdXAsXG4gICAgICAgIGZpZWxkczogW1xuICAgICAgICAgIHRoaXMuc3ViRm9ybShvYmosIGAke3RoaXMuZm9ybS5wcmVmaXh9bGluZV8ke3RoaXMuY291bnRlcn1gKSxcbiAgICAgICAgICBuZXcgRm9ybUFjdGlvbih7XG4gICAgICAgICAgICB0aXRsZTogdGhpcy5yZW1vdmVCdXR0b25UZXh0LFxuICAgICAgICAgICAgaWNvbjogJ3RyYXNoJyxcbiAgICAgICAgICAgIGNsaWNrOiAoKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZmllbGRzLnJlbW92ZUJ5SWQoZ3JvdXApO1xuICAgICAgICAgICAgICB0aGlzLmxpc3Quc3BsaWNlKHRoaXMubGlzdC5pbmRleE9mKG9iaiksIDEpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNsYXNzZXMgOiBbJ25kZi1yZW1vdmUnXVxuICAgICAgICAgIH0pXG4gICAgICAgIF0sXG4gICAgICAgIGNsYXNzZXM6IFsnbmRmLWZ1bGwtYm9yZGVyZWQnLCAnbmRmLW11bHRpY2hpbGQtcm93JywgKHRoaXMuY2hpbGQpID8gJ25kZi1jaGlsZCcgOiAnbmRmLXBhcmVudCddXG4gICAgICB9KSwgYCR7dGhpcy5uYW1lfV9jb250cm9sc2BcbiAgICApO1xuICAgIHRoaXMuY291bnRlcisrO1xuICB9XG59XG4iLCJpbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4vZmllbGQnO1xuXG5leHBvcnQgY2xhc3MgVGV4dEZpZWxkIGV4dGVuZHMgRmllbGQ8c3RyaW5nPiB7XG4gIHR5cGUgPSAndGV4dCc7XG4gIGlucHV0VHlwZTogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLnZhbHVlID0gb3B0aW9uc1sndmFsdWUnXSB8fCAnJztcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLXRleHQnKTtcbiAgICB0aGlzLmlucHV0VHlwZSA9IG9wdGlvbnNbJ2lucHV0VHlwZSddIHx8ICcnO1xuICB9XG59XG4iLCJpbXBvcnQgeyBUZXh0RmllbGQgfSBmcm9tICcuL3RleHQtZmllbGQnO1xuXG5cbmV4cG9ydCBjbGFzcyBOdW1iZXJGaWVsZCBleHRlbmRzIFRleHRGaWVsZCB7XG4gIHByZWNpc2lvbjogbnVtYmVyO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHt9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLnZhbHVlID0gb3B0aW9uc1sndmFsdWUnXSB8fCAwO1xuICAgIHRoaXMucHJlY2lzaW9uID0gKG9wdGlvbnNbJ3ByZWNpc2lvbiddICE9IG51bGwpID8gb3B0aW9uc1sncHJlY2lzaW9uJ10gOiAyO1xuICB9XG5cbiAgYXN5bmMgZGVyaXZlVmFsdWUoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgcmV0dXJuIHN1cGVyLmRlcml2ZVZhbHVlKCkudGhlbihkZXJpdmVkID0+IHtcbiAgICAgIGlmICghZGVyaXZlZCkge1xuICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5wcmVwYXJlVmFsdWUodGhpcy52YWx1ZSk7XG4gICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUodHJ1ZSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcbiAgICB9KTtcbiAgfVxuXG4gIG9uQmx1cigpOiB2b2lkIHtcbiAgICB0aGlzLnZhbHVlID0gdGhpcy5wcmVwYXJlVmFsdWUodGhpcy52YWx1ZSk7XG4gIH1cblxuICBwcmVwYXJlVmFsdWUodmFsdWU6IGFueSk6IGFueSB7XG4gICAgbGV0IHByZXBhcmVkOiBudW1iZXI7XG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ251bWJlcicpIHtcbiAgICAgIHByZXBhcmVkID0gdmFsdWUgYXMgbnVtYmVyO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlID09PSAndW5kZWZpbmVkJykge1xuICAgICAgcHJlcGFyZWQgPSAwO1xuICAgIH0gZWxzZSB7XG4gICAgICBwcmVwYXJlZCA9IHBhcnNlRmxvYXQodmFsdWUudG9TdHJpbmcoKS5yZXBsYWNlKC9bXjAtOS5dL2csICcnKSk7XG4gICAgfVxuICAgIHJldHVybiBwcmVwYXJlZC50b0ZpeGVkKHRoaXMucHJlY2lzaW9uKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuL2ZpZWxkJztcblxuZXhwb3J0IGNsYXNzIFRleHRhcmVhRmllbGQgZXh0ZW5kcyBGaWVsZDxzdHJpbmc+IHtcbiAgdHlwZSA9ICd0ZXh0YXJlYSc7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMudmFsdWUgPSBvcHRpb25zWyd2YWx1ZSddIHx8ICcnO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtdGV4dGFyZWEnKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIEZvcm1Hcm91cCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuLi9maWVsZHMvZmllbGQnO1xuXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBGb3JtU2VydmljZSB7XG4gIHByaXZhdGUgZm9ybUdyb3VwOiBGb3JtR3JvdXA7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5mb3JtR3JvdXAgPSBuZXcgRm9ybUdyb3VwKHt9KTtcbiAgfVxuXG4gIGdldCBncm91cCgpOiBGb3JtR3JvdXAge1xuICAgIHJldHVybiB0aGlzLmZvcm1Hcm91cDtcbiAgfVxuXG4gIHJlZ2lzdGVyRmllbGQoZmllbGQ6IEZpZWxkPGFueT4pIHtcbiAgICBpZiAoIXRoaXMuZ3JvdXAuY29udGFpbnMoZmllbGQuaWQpKSB7XG4gICAgICB0aGlzLmdyb3VwLmFkZENvbnRyb2woZmllbGQuaWQsIHRoaXMuY29udHJvbEZyb21GaWVsZChmaWVsZCkpO1xuICAgIH1cbiAgfVxuXG4gIHJlZ2lzdGVyRmllbGRzKGZpZWxkczogRmllbGQ8YW55PltdKSB7XG4gICAgZmllbGRzLmZvckVhY2goZmllbGQgPT4ge1xuICAgICAgdGhpcy5yZWdpc3RlckZpZWxkKGZpZWxkKTtcbiAgICB9KTtcbiAgfVxuXG4gIGRlcmVnaXN0ZXJGaWVsZChmaWVsZDogRmllbGQ8YW55Pikge1xuICAgIHRoaXMuZ3JvdXAucmVtb3ZlQ29udHJvbChmaWVsZC5pZCk7XG4gIH1cblxuICBkZXJlZ2lzdGVyRmllbGRzKGZpZWxkczogRmllbGQ8YW55PltdKSB7XG4gICAgZmllbGRzLmZvckVhY2goZmllbGQgPT4ge1xuICAgICAgdGhpcy5kZXJlZ2lzdGVyRmllbGQoZmllbGQpO1xuICAgIH0pO1xuICB9XG5cbiAgY29udHJvbEZyb21GaWVsZChmaWVsZDogRmllbGQ8YW55Pik6IEZvcm1Db250cm9sIHtcbiAgICByZXR1cm4gbmV3IEZvcm1Db250cm9sKGZpZWxkLnZhbHVlIHx8ICcnKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgRm9ybUJhc2UgfSBmcm9tICcuL2Zvcm0tYmFzZSc7XG5cblxuZXhwb3J0IGNsYXNzIEZvcm0gZXh0ZW5kcyBGb3JtQmFzZSB7XG4gIG9iajogYW55O1xuICBzdWJtaXQ6IChmb3JtOiBGb3JtKSA9PiB2b2lkO1xuICBoYXNGaWVsZHMgPSB0cnVlO1xuICBpc0Zvcm0gPSB0cnVlO1xuXG4gIGNvbnN0cnVjdG9yKG9wdGlvbnM6IHtcbiAgICBuYW1lPzogc3RyaW5nLFxuICAgIGZpZWxkcz86IEZvcm1CYXNlW10sXG4gICAgb2JqPzogYW55LFxuICAgIHN1Ym1pdD86IChmb3JtOiBGb3JtKSA9PiB2b2lkO1xuICAgIGNsYXNzZXM/OiBzdHJpbmdbXVxuICB9ID0ge30pIHtcbiAgICBzdXBlcihvcHRpb25zKTtcbiAgICB0aGlzLm9iaiA9IG9wdGlvbnNbJ29iaiddIHx8IHt9O1xuICAgIHRoaXMuc3VibWl0ID0gb3B0aW9uc1snc3VibWl0J10gfHwgbnVsbDtcbiAgICB0aGlzLmZpZWxkcy5zZXRGb3JtKHRoaXMpO1xuICB9XG5cbiAgZ2V0IHByZWZpeCgpOiBzdHJpbmcge1xuICAgIGlmICh0aGlzLmZvcm0pIHtcbiAgICAgIHJldHVybiBgJHt0aGlzLmZvcm0ucHJlZml4fSR7dGhpcy5uYW1lfV9gO1xuICAgIH0gZWxzZSBpZiAodGhpcy5uYW1lKSB7XG4gICAgICByZXR1cm4gYCR7dGhpcy5uYW1lfV9gO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gJyc7XG4gICAgfVxuICB9XG5cbiAgdXBkYXRlKG9iajogYW55KSB7XG4gICAgdGhpcy5vYmogPSBvYmo7XG4gICAgdGhpcy5maWVsZHMudXBkYXRlVmFsdWVzKCk7XG4gIH1cblxuICBmaWVsZENoYW5nZShrZXk6IHN0cmluZywgdmFsdWU6IGFueSk6IHZvaWQge1xuICAgIHRoaXMub2JqW2tleV0gPSB2YWx1ZTtcbiAgfVxuXG4gIG9uU3VibWl0KCkge1xuICAgIGlmICh0aGlzLnN1Ym1pdCAhPT0gbnVsbCkge1xuICAgICAgdGhpcy5zdWJtaXQodGhpcyk7XG4gICAgfVxuICB9XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Hcm91cCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgRm9ybSB9IGZyb20gJy4uL2Zvcm0nO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1mb3JtJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Zvcm0uY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgZm9ybTogRm9ybTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSkge31cblxuICBuZ09uSW5pdCgpIHtcblxuICB9XG5cbiAgZ2V0IGdyb3VwKCk6IEZvcm1Hcm91cCB7XG4gICAgcmV0dXJuIHRoaXMuZm9ybVNlcnZpY2UuZ3JvdXA7XG4gIH1cblxuICBvblN1Ym1pdCgpIHtcbiAgICB0aGlzLmZvcm0ub25TdWJtaXQoKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Hcm91cCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgRm9ybUJhc2UgfSBmcm9tICcuLi9mb3JtLWJhc2UnO1xuaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuLi9maWVsZHMvZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtZm9ybS1pdGVtJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Zvcm0taXRlbS5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1JdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBpdGVtOiBGb3JtQmFzZTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSkge31cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZiAodGhpcy5pdGVtLmhhc0RhdGEpIHtcbiAgICAgIHRoaXMuZm9ybVNlcnZpY2UucmVnaXN0ZXJGaWVsZCh0aGlzLml0ZW0gYXMgRmllbGQ8YW55Pik7XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gICAgaWYgKHRoaXMuaXRlbS5oYXNEYXRhKSB7XG4gICAgICB0aGlzLmZvcm1TZXJ2aWNlLmRlcmVnaXN0ZXJGaWVsZCh0aGlzLml0ZW0gYXMgRmllbGQ8YW55Pik7XG4gICAgfVxuICB9XG5cbiAgZ2V0IGdyb3VwKCk6IEZvcm1Hcm91cCB7XG4gICAgcmV0dXJuIHRoaXMuZm9ybVNlcnZpY2UuZ3JvdXA7XG4gIH1cblxuICBvbkNoYW5nZSh2YWx1ZSkge1xuICAgIHRoaXMuaXRlbS5vbkNoYW5nZSh2YWx1ZSk7XG4gIH1cblxuICBvbkJsdXIodmFsdWUpIHtcbiAgICB0aGlzLml0ZW0ub25CbHVyKHZhbHVlKTtcbiAgfVxuXG4gIG9uQ2xpY2soKSB7XG4gICAgdGhpcy5pdGVtLm9uQ2xpY2soKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDaGVja2JveEZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL2NoZWNrYm94LWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWNoZWNrYm94LWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NoZWNrYm94LWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgQ2hlY2tib3hGaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogQ2hlY2tib3hGaWVsZDtcbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2hlY2tib3hTZXRGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9jaGVja2JveC1zZXQtZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtY2hlY2tib3gtc2V0LWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NoZWNrYm94LXNldC1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIENoZWNrYm94U2V0RmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IENoZWNrYm94U2V0RmllbGQ8YW55Pjtcbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ29udGVudEZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL2NvbnRlbnQtZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtY29udGVudC1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9jb250ZW50LWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgQ29udGVudEZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBDb250ZW50RmllbGQ7XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IERhdGVGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9kYXRlLWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWRhdGUtZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vZGF0ZS1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIERhdGVGaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogRGF0ZUZpZWxkO1xufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEcm9wZG93bkZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL2Ryb3Bkb3duLWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLWRyb3Bkb3duLWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Ryb3Bkb3duLWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgRHJvcGRvd25GaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogRHJvcGRvd25GaWVsZDtcbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmllbGRHcm91cCB9IGZyb20gJy4uL2ZpZWxkcy9maWVsZC1ncm91cCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1maWVsZC1ncm91cCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9maWVsZC1ncm91cC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIEZpZWxkR3JvdXBDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IEZpZWxkR3JvdXA7XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IEZvcm1BY3Rpb24gfSBmcm9tICcuLi9maWVsZHMvZm9ybS1hY3Rpb24nO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtZm9ybS1hY3Rpb24nLFxuICB0ZW1wbGF0ZVVybDogJy4vZm9ybS1hY3Rpb24uY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBGb3JtQWN0aW9uQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBGb3JtQWN0aW9uO1xuXG4gIGdldCBsYWJlbCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLml0ZW0ubG9hZGluZyA/IHRoaXMuaXRlbS5sb2FkaW5nIDogdGhpcy5pdGVtLnRpdGxlO1xuICB9XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEZvcm1JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9mb3JtLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IEhlYWRlckZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL2hlYWRlci1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1oZWFkZXItZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vaGVhZGVyLWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgSGVhZGVyRmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IEhlYWRlckZpZWxkO1xufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBPcHRpb25TZXRGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9vcHRpb24tc2V0LWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLW9wdGlvbi1zZXQtZmllbGQnLFxuICB0ZW1wbGF0ZVVybDogJy4vb3B0aW9uLXNldC1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIE9wdGlvblNldEZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBPcHRpb25TZXRGaWVsZDxhbnk+O1xufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBUZXh0RmllbGQgfSBmcm9tICcuLi9maWVsZHMvdGV4dC1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi10ZXh0LWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3RleHQtZmllbGQuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBUZXh0RmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IFRleHRGaWVsZDtcbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGV4dGFyZWFGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy90ZXh0YXJlYS1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi10ZXh0YXJlYS1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi90ZXh0YXJlYS1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIFRleHRhcmVhRmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IFRleHRhcmVhRmllbGQ7XG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5pbXBvcnQgeyBBbmd1bGFyRm9udEF3ZXNvbWVNb2R1bGUgfSBmcm9tICdhbmd1bGFyLWZvbnQtYXdlc29tZSc7XG5pbXBvcnQgeyBOZ3hNeURhdGVQaWNrZXJNb2R1bGUgfSBmcm9tICduZ3gtbXlkYXRlcGlja2VyJztcblxuaW1wb3J0IHsgRm9ybUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLWl0ZW0uY29tcG9uZW50JztcblxuaW1wb3J0IHsgQ2hlY2tib3hGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jaGVja2JveC1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2hlY2tib3hTZXRGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jaGVja2JveC1zZXQtZmllbGQuY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRlbnRGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb250ZW50LWZpZWxkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEYXRlRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZGF0ZS1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRHJvcGRvd25GaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9kcm9wZG93bi1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmllbGRHcm91cENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9maWVsZC1ncm91cC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9ybUFjdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLWFjdGlvbi5jb21wb25lbnQnO1xuaW1wb3J0IHsgSGVhZGVyRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvaGVhZGVyLWZpZWxkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBPcHRpb25TZXRGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9vcHRpb24tc2V0LWZpZWxkLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBUZXh0RmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdGV4dC1maWVsZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGV4dGFyZWFGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy90ZXh0YXJlYS1maWVsZC5jb21wb25lbnQnO1xuXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBGb3Jtc01vZHVsZSxcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgIEFuZ3VsYXJGb250QXdlc29tZU1vZHVsZSxcbiAgICBOZ3hNeURhdGVQaWNrZXJNb2R1bGUuZm9yUm9vdCgpXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIEZvcm1Db21wb25lbnQsXG4gICAgRm9ybUl0ZW1Db21wb25lbnQsXG5cbiAgICBDaGVja2JveEZpZWxkQ29tcG9uZW50LFxuICAgIENoZWNrYm94U2V0RmllbGRDb21wb25lbnQsXG4gICAgQ29udGVudEZpZWxkQ29tcG9uZW50LFxuICAgIERhdGVGaWVsZENvbXBvbmVudCxcbiAgICBEcm9wZG93bkZpZWxkQ29tcG9uZW50LFxuICAgIEZpZWxkR3JvdXBDb21wb25lbnQsXG4gICAgRm9ybUFjdGlvbkNvbXBvbmVudCxcbiAgICBIZWFkZXJGaWVsZENvbXBvbmVudCxcbiAgICBPcHRpb25TZXRGaWVsZENvbXBvbmVudCxcbiAgICBUZXh0RmllbGRDb21wb25lbnQsXG4gICAgVGV4dGFyZWFGaWVsZENvbXBvbmVudFxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgRm9ybUNvbXBvbmVudCxcbiAgICBGb3JtSXRlbUNvbXBvbmVudCxcblxuICAgIENoZWNrYm94RmllbGRDb21wb25lbnQsXG4gICAgQ2hlY2tib3hTZXRGaWVsZENvbXBvbmVudCxcbiAgICBDb250ZW50RmllbGRDb21wb25lbnQsXG4gICAgRGF0ZUZpZWxkQ29tcG9uZW50LFxuICAgIERyb3Bkb3duRmllbGRDb21wb25lbnQsXG4gICAgRmllbGRHcm91cENvbXBvbmVudCxcbiAgICBGb3JtQWN0aW9uQ29tcG9uZW50LFxuICAgIEhlYWRlckZpZWxkQ29tcG9uZW50LFxuICAgIE9wdGlvblNldEZpZWxkQ29tcG9uZW50LFxuICAgIFRleHRGaWVsZENvbXBvbmVudCxcbiAgICBUZXh0YXJlYUZpZWxkQ29tcG9uZW50XG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgTmdEeW5hbWljRm9ybXNNb2R1bGUge1xuICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IE5nRHluYW1pY0Zvcm1zTW9kdWxlLFxuICAgICAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbiAgICB9O1xuICB9XG59XG4iXSwibmFtZXMiOlsidHNsaWJfMS5fX2V4dGVuZHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBSUE7SUFDRSxrQkFBb0IsR0FBZTtRQUFmLFFBQUcsR0FBSCxHQUFHLENBQVk7S0FBSzs7Ozs7SUFFeEMsMEJBQU87Ozs7SUFBUCxVQUFRLElBQVU7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsVUFBQyxLQUFLO1lBQ3JCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDckIsQ0FBQyxDQUFDO0tBQ0o7Ozs7SUFFRCwrQkFBWTs7O0lBQVo7UUFDRSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQUs7WUFDckIsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3JCLENBQUMsQ0FBQztLQUNKO0lBRUQsc0JBQUksNEJBQU07Ozs7UUFBVjtZQUNFLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQztTQUNqQjs7O09BQUE7Ozs7O0lBRUQsdUJBQUk7Ozs7SUFBSixVQUFLLElBQWM7UUFDakIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDckI7Ozs7Ozs7O0lBRUQsK0JBQVk7Ozs7Ozs7SUFBWixVQUFhLElBQWMsRUFBRSxNQUFjLEVBQUUsSUFBWSxFQUFFLEtBQXNCO1FBQXRCLHNCQUFBLEVBQUEsWUFBc0I7O1lBQzNFLE1BQU0sR0FBYSxJQUFJO1FBQzNCLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTs7Z0JBQ1osTUFBTSxHQUEwQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUM7WUFDeEYsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7WUFDdkIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7U0FDdEI7UUFDRCxJQUFJLE1BQU0sS0FBSyxJQUFJLEVBQUU7WUFDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ25EO2FBQU07WUFDTCxNQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztTQUN2RDtLQUNGOzs7Ozs7O0lBRUQsbUNBQWdCOzs7Ozs7SUFBaEIsVUFBaUIsSUFBYyxFQUFFLE1BQWMsRUFBRSxLQUFzQjtRQUF0QixzQkFBQSxFQUFBLFlBQXNCO1FBQ3JFLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztLQUN2RDs7Ozs7OztJQUVELGlDQUFjOzs7Ozs7SUFBZCxVQUFlLElBQWMsRUFBRSxNQUFjLEVBQUUsS0FBc0I7UUFBdEIsc0JBQUEsRUFBQSxZQUFzQjtRQUNuRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7S0FDckQ7Ozs7Ozs7O0lBRUQsOEJBQVc7Ozs7Ozs7SUFBWCxVQUFZLElBQWMsRUFBRSxLQUFhLEVBQUUsSUFBWSxFQUFFLEtBQXNCO1FBQXRCLHNCQUFBLEVBQUEsWUFBc0I7O1lBQ3pFLE1BQU0sR0FBYSxJQUFJO1FBQzNCLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTs7Z0JBQ1osTUFBTSxHQUEwQyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUM7WUFDdkYsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7WUFDdkIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7U0FDdEI7UUFDRCxJQUFJLE1BQU0sS0FBSyxJQUFJLEVBQUU7WUFDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUN2RDthQUFNO1lBQ0wsTUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDckQ7S0FDRjs7Ozs7Ozs7SUFFRCxrQ0FBZTs7Ozs7OztJQUFmLFVBQWdCLElBQWMsRUFBRSxLQUFhLEVBQUUsS0FBYSxFQUFFLEtBQXNCO1FBQXRCLHNCQUFBLEVBQUEsWUFBc0I7UUFDbEYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztLQUM5Qzs7Ozs7Ozs7SUFFRCxnQ0FBYTs7Ozs7OztJQUFiLFVBQWMsSUFBYyxFQUFFLEtBQWEsRUFBRSxLQUFhLEVBQUUsS0FBc0I7UUFBdEIsc0JBQUEsRUFBQSxZQUFzQjtRQUNoRixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO0tBQzVDOzs7Ozs7O0lBRUQseUJBQU07Ozs7OztJQUFOLFVBQU8sR0FBVyxFQUFFLEtBQXNCLEVBQUUsSUFBWTtRQUFwQyxzQkFBQSxFQUFBLFlBQXNCOztZQUNwQyxNQUFNLEdBQWEsSUFBSTtRQUMzQixJQUFJLEtBQUssS0FBSyxJQUFJLEVBQUU7O2dCQUNaLE1BQU0sR0FBMEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDO1lBQ3JGLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO1lBQ3ZCLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxNQUFNLEtBQUssSUFBSSxFQUFFO1lBQ25CLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzdDO2FBQU07WUFDTCxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3hDO0tBQ0Y7Ozs7OztJQUVELCtCQUFZOzs7OztJQUFaLFVBQWEsSUFBWSxFQUFFLEtBQXNCO1FBQXRCLHNCQUFBLEVBQUEsWUFBc0I7UUFDL0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0tBQ2xDOzs7Ozs7SUFFRCw2QkFBVTs7Ozs7SUFBVixVQUFXLEVBQVUsRUFBRSxLQUFzQjtRQUF0QixzQkFBQSxFQUFBLFlBQXNCO1FBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztLQUM5Qjs7Ozs7O0lBRUQsd0JBQUs7Ozs7O0lBQUwsVUFBTSxHQUFXLEVBQUUsSUFBWTtRQUM3QixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQztLQUM5Qzs7Ozs7SUFFRCw4QkFBVzs7OztJQUFYLFVBQVksSUFBWTtRQUN0QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0tBQ2pDOzs7OztJQUVELDRCQUFTOzs7O0lBQVQsVUFBVSxFQUFVO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDN0I7Ozs7OztJQUVELGtDQUFlOzs7OztJQUFmLFVBQWdCLEdBQVcsRUFBRSxJQUFZOztZQUNuQyxLQUFLLEdBQVEsSUFBSTs7WUFDakIsTUFBTSxHQUFRLElBQUk7O1lBQ2hCLE1BQU0sR0FBVSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUEsQ0FBQztRQUNuRSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3JCLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbkI7YUFBTTs7Z0JBQ0MsT0FBTyxHQUFVLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxDQUFDLFNBQVMsS0FBSyxJQUFJLEdBQUEsQ0FBQztZQUN6RSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7b0JBQ2pDLEtBQUssR0FBZ0MsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQztnQkFDdkYsSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLElBQUksRUFBRTtvQkFDdkIsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7b0JBQ3BCLE1BQU0sR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3BCLE1BQU07aUJBQ1A7YUFDRjtTQUNGO1FBQ0QsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDO0tBQ3pDOzs7OztJQUVELHdDQUFxQjs7OztJQUFyQixVQUFzQixJQUFZO1FBQ2hDLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7S0FDM0M7Ozs7O0lBRUQsc0NBQW1COzs7O0lBQW5CLFVBQW9CLEVBQVU7UUFDNUIsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztLQUN2Qzs7OztJQUVELGtDQUFlOzs7SUFBZjtRQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztZQUN2QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDekIsQ0FBQyxDQUFDO0tBQ0o7SUFDSCxlQUFDO0NBQUE7Ozs7Ozs7SUNqSEMsa0JBQVksT0FPTjtRQVBNLHdCQUFBLEVBQUEsWUFPTjtRQWJOLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixZQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsYUFBUSxHQUFHLElBQUksQ0FBQztRQUNoQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBVWYsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksRUFBRSxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQztRQUMxQyxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUM7S0FDbEM7Ozs7SUFFRCwwQkFBTzs7O0lBQVA7UUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7S0FDbEI7Ozs7O0lBRUQsMEJBQU87Ozs7SUFBUCxVQUFRLElBQVU7UUFDaEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLEVBQUUsR0FBRyxLQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQU0sQ0FBQztTQUN4QztRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzNCO0tBQ0Y7Ozs7SUFFSyw4QkFBVzs7O0lBQWpCOzs7Z0JBQ0Usc0JBQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBQzs7O0tBQy9CO0lBRUQsc0JBQUksNkJBQU87Ozs7UUFBWDtZQUNFLE9BQU8sSUFBSSxDQUFDO1NBQ2I7OztPQUFBOzs7OztJQUVELDJCQUFROzs7O0lBQVIsVUFBUyxLQUFLOztZQUNSLFNBQVMsR0FBRyxJQUFJO1FBQ3BCLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLEVBQUU7WUFDdkIsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3RDO1FBQ0QsSUFBSSxTQUFTLEVBQUU7WUFDYixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3pDO0tBQ0Y7Ozs7O0lBRUQseUJBQU07Ozs7SUFBTixVQUFPLEtBQUs7UUFDVixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3hCO0tBQ0Y7Ozs7SUFFRCwwQkFBTzs7O0lBQVA7S0FFQzs7Ozs7SUFFRCw2QkFBVTs7OztJQUFWLFVBQVcsT0FBZTtRQUN4QixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztLQUN4Qjs7Ozs7SUFFRCxpQ0FBYzs7OztJQUFkLFVBQWUsTUFBd0I7UUFDckMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3ZCLElBQUksTUFBTSxDQUFDLFFBQVEsSUFBSSxJQUFJLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3pELElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3JDO1FBQ0QsS0FBSyxJQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFFO1lBQy9CLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7O29CQUMvQixLQUFLLEdBQWEsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDO2dCQUNwRCxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7b0JBQ2pCLEtBQUssQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2lCQUMxQzthQUNGO1NBQ0Y7S0FDRjs7OztJQUVELGtDQUFlOzs7SUFBZjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQztTQUMvQjtLQUNGO0lBQ0gsZUFBQztDQUFBOzs7Ozs7Ozs7QUN6R0Q7Ozs7SUFBOEJBLHlCQUFRO0lBUXBDLGVBQVksT0FNTjtRQU5NLHdCQUFBLEVBQUEsWUFNTjtRQU5OLFlBT0Usa0JBQU0sT0FBTyxDQUFDLFNBU2Y7UUF0QkQsYUFBTyxHQUFHLElBQUksQ0FBQztRQWNiLEtBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUMzQixLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO1FBQ2pDLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9CLEtBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUM7UUFDN0MsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDL0IsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUN0QztRQUNELEtBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUM7O0tBQ3RDOzs7OztJQUVELHVCQUFPOzs7O0lBQVAsVUFBUSxJQUFVO1FBQ2hCLGlCQUFNLE9BQU8sWUFBQyxJQUFJLENBQUMsQ0FBQztRQUNwQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7S0FDcEI7Ozs7O0lBRUQsd0JBQVE7Ozs7SUFBUixVQUFTLEtBQUs7UUFDWixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixpQkFBTSxRQUFRLFlBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0tBQzFDOzs7O0lBRUssMkJBQVc7OztJQUFqQjs7OztnQkFDRSxzQkFBTyxpQkFBTSxXQUFXLFdBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO3dCQUNyQyxJQUFJLENBQUMsT0FBTyxFQUFFOzRCQUNaLElBQUksS0FBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLEVBQUU7Z0NBQ3ZCLE9BQU8sS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsQ0FBQzs2QkFDMUI7aUNBQU0sSUFBSSxLQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxFQUFFO2dDQUNsRCxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQ0FDdEMsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUM5Qjt5QkFDRjt3QkFDRCxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQy9CLENBQUMsRUFBQzs7O0tBQ0o7Ozs7O0lBRUQsNEJBQVk7Ozs7SUFBWixVQUFhLEtBQVU7UUFDckIsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ2pCLE9BQU8sRUFBRSxDQUFDO1NBQ1g7YUFBTTtZQUNMLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7S0FDRjtJQUNILFlBQUM7Q0F6REQsQ0FBOEIsUUFBUTs7Ozs7OztJQ0RIQSxpQ0FBYztJQUkvQyx1QkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBRWY7UUFORCxVQUFJLEdBQUcsVUFBVSxDQUFDO1FBQ2xCLGNBQVEsR0FBRyxLQUFLLENBQUM7UUFJZixLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzs7S0FDbkM7SUFDSCxvQkFBQztDQVJELENBQW1DLEtBQUs7Ozs7Ozs7OztBQ0R4Qzs7OztJQUF1Q0Esa0NBQVE7SUFLN0Msd0JBQVksT0FBZ0I7UUFBaEIsd0JBQUEsRUFBQSxZQUFnQjtRQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQU1mO1FBWEQsVUFBSSxHQUFHLFdBQVcsQ0FBQztRQUNuQixlQUFTLEdBQXlELEVBQUUsQ0FBQztRQUNyRSxhQUFPLEdBQXlELEVBQUUsQ0FBQztRQUlqRSxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUNuQyxLQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsVUFBVTtZQUM3QyxLQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQzdCLENBQUMsQ0FBQzs7S0FDSjs7Ozs7SUFFRCxtQ0FBVTs7OztJQUFWLFVBQVcsT0FBa0U7UUFDM0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7S0FDeEI7SUFDSCxxQkFBQztDQWpCRCxDQUF1QyxLQUFLOzs7Ozs7Ozs7QUNDNUM7Ozs7SUFBeUNBLG9DQUFpQjtJQUd4RCwwQkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBRWY7UUFMRCxVQUFJLEdBQUcsYUFBYSxDQUFDO1FBSW5CLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7O0tBQ3RDOzs7OztJQUVELG1DQUFROzs7O0lBQVIsVUFBUyxLQUFLOztZQUNOLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3BDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN6QixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDbkM7YUFBTTtZQUNMLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDakI7S0FDRjs7Ozs7SUFFRCxvQ0FBUzs7OztJQUFULFVBQVUsS0FBSzs7WUFDUCxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QyxJQUFJLFFBQVEsSUFBSSxJQUFJLEVBQUU7WUFDcEIsT0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxLQUFLLEtBQUssR0FBQSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztTQUNyRDtRQUNELE9BQU8sS0FBSyxDQUFDO0tBQ2Q7SUFDSCx1QkFBQztDQXhCRCxDQUF5QyxjQUFjOzs7Ozs7O0lDRHJCQSxnQ0FBUTtJQUl4QyxzQkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBSWY7UUFSRCxVQUFJLEdBQUcsU0FBUyxDQUFDO1FBS2YsS0FBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3hDLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9CLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDOztLQUNsQztJQUNILG1CQUFDO0NBVkQsQ0FBa0MsUUFBUTs7Ozs7OztJQ0VYQSw2QkFBVztJQUd4QyxtQkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBRWY7UUFMRCxVQUFJLEdBQUcsTUFBTSxDQUFDO1FBSVosS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7O0tBQy9COzs7OztJQUVELDRCQUFROzs7O0lBQVIsVUFBUyxLQUFtQjtRQUMxQixpQkFBTSxRQUFRLFlBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLElBQUksSUFBSSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7S0FDeEU7SUFDSCxnQkFBQztDQVhELENBQStCLEtBQUs7Ozs7Ozs7SUNGREEsaUNBQXNCO0lBS3ZELHVCQUFZLE9BQWdCO1FBQWhCLHdCQUFBLEVBQUEsWUFBZ0I7UUFBNUIsWUFDRSxrQkFBTSxPQUFPLENBQUMsU0FJZjtRQVRELFVBQUksR0FBRyxVQUFVLENBQUM7UUFNaEIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDbEMsS0FBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs7S0FDL0M7Ozs7O0lBRUQsa0NBQVU7Ozs7SUFBVixVQUFXLE9BQWtFO1FBQzNFLGlCQUFNLFVBQVUsWUFBQyxPQUFPLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7S0FDOUI7SUFDSCxvQkFBQztDQWhCRCxDQUFtQyxjQUFjOzs7Ozs7O0lDQWpCQSw4QkFBUTtJQUl0QyxvQkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBQ2Y7UUFMRCxlQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLFVBQUksR0FBRyxPQUFPLENBQUM7O0tBSWQ7Ozs7SUFFSyxnQ0FBVzs7O0lBQWpCOzs7O2dCQUNFLHNCQUFPLGlCQUFNLFdBQVcsV0FBRSxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU87d0JBQ3JDLEtBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7d0JBQzNCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDakMsQ0FBQyxFQUFDOzs7S0FDSjtJQUNILGlCQUFDO0NBZEQsQ0FBZ0MsUUFBUTs7Ozs7OztJQ0FSQSw4QkFBUTtJQVN0QyxvQkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBTWY7UUFmRCxjQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFVBQUksR0FBRyxRQUFRLENBQUM7UUFTZCxLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUM7UUFDdEMsS0FBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDO1FBQ3BDLEtBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLFFBQVEsQ0FBQztRQUNwRCxLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUM7UUFDdEMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7O0tBQ2pDOzs7OztJQUVELGdDQUFXOzs7O0lBQVgsVUFBWSxJQUFZO1FBQ3RCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0tBQ3JCOzs7O0lBRUQsaUNBQVk7OztJQUFaO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7S0FDckI7Ozs7SUFFRCw0QkFBTzs7O0lBQVA7UUFDRSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbEI7S0FDRjtJQUNILGlCQUFDO0NBL0JELENBQWdDLFFBQVE7Ozs7Ozs7SUNBUEEsK0JBQVE7SUFJdkMscUJBQVksT0FBZ0I7UUFBaEIsd0JBQUEsRUFBQSxZQUFnQjtRQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUlmO1FBUkQsVUFBSSxHQUFHLFFBQVEsQ0FBQztRQUtkLEtBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNwQyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMvQixLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzs7S0FDakM7SUFDSCxrQkFBQztDQVZELENBQWlDLFFBQVE7Ozs7Ozs7SUNJSkEsbUNBQVU7SUFTN0MseUJBQVksT0FBZ0I7UUFBaEIsd0JBQUEsRUFBQSxZQUFnQjtRQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQThCZjtRQWxDRCxXQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ04sYUFBTyxHQUFHLENBQUMsQ0FBQztRQUlsQixLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUM7UUFDMUMsS0FBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDO1FBQ3hDLEtBQUksQ0FBQyxhQUFhLEdBQUcsT0FBTyxDQUFDLGVBQWUsQ0FBQyxJQUFJLGNBQWMsQ0FBQztRQUNoRSxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsT0FBTyxDQUFDLGtCQUFrQixDQUFDLElBQUksYUFBYSxDQUFDO1FBQ3JFLEtBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEtBQUssQ0FBQztRQUN2QyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BDLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3BCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksV0FBVyxDQUFDO2dCQUMvQixLQUFLLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQzthQUN4QixDQUFDLENBQUMsQ0FBQztZQUNKLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksVUFBVSxDQUFDO2dCQUM5QixJQUFJLEVBQUssS0FBSSxDQUFDLElBQUksY0FBVztnQkFDN0IsTUFBTSxFQUFFO29CQUNOLElBQUksVUFBVSxDQUFDO3dCQUNiLEtBQUssRUFBRSxLQUFJLENBQUMsYUFBYTt3QkFDekIsSUFBSSxFQUFFLE1BQU07d0JBQ1osS0FBSyxFQUFFOzRCQUNMLElBQUksS0FBSSxDQUFDLE9BQU8sS0FBSyxJQUFJLElBQUksS0FBSSxDQUFDLE1BQU0sS0FBSyxJQUFJLEVBQUU7O29DQUMzQyxHQUFHLEdBQVEsS0FBSSxDQUFDLE1BQU0sRUFBRTtnQ0FDOUIsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7Z0NBQ3BCLEtBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7NkJBQ3BCO3lCQUNGO3dCQUNELE9BQU8sRUFBRyxDQUFDLFNBQVMsQ0FBQztxQkFDdEIsQ0FBQztpQkFDSDtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRSx1QkFBdUIsQ0FBQzthQUN4RCxDQUFDLENBQUMsQ0FBQztTQUNMOztLQUNGO0lBRUQsc0JBQUksaUNBQUk7Ozs7UUFBUjtZQUNFLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRTtnQkFDcEMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUMvQjtZQUNELDJCQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBZ0I7U0FDakQ7OztPQUFBOzs7OztJQUVELGlDQUFPOzs7O0lBQVAsVUFBUSxJQUFVO1FBQWxCLGlCQU9DO1FBTkMsaUJBQU0sT0FBTyxZQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BCLElBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxJQUFJLEVBQUU7WUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFHO2dCQUNwQixLQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3BCLENBQUMsQ0FBQztTQUNKO0tBQ0Y7Ozs7O0lBRUQsd0NBQWM7Ozs7SUFBZCxVQUFlLE1BQXdCO1FBQ3JDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixLQUFLLElBQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7WUFDL0IsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTs7b0JBQy9CLEtBQUssR0FBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sYUFBUSxHQUFLOztvQkFDeEMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztnQkFDNUMsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO29CQUNqQixLQUFLLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDMUM7YUFDRjtTQUNGO0tBQ0Y7Ozs7O0lBRU8sa0NBQVE7Ozs7SUFBaEIsVUFBaUIsR0FBUTtRQUF6QixpQkFxQkM7O1lBcEJPLEtBQUssR0FBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sYUFBUSxJQUFJLENBQUMsT0FBTyxXQUFRO1FBQzdELElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUN4QixJQUFJLFVBQVUsQ0FBQztZQUNiLElBQUksRUFBRSxLQUFLO1lBQ1gsTUFBTSxFQUFFO2dCQUNOLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxhQUFRLElBQUksQ0FBQyxPQUFTLENBQUM7Z0JBQzVELElBQUksVUFBVSxDQUFDO29CQUNiLEtBQUssRUFBRSxJQUFJLENBQUMsZ0JBQWdCO29CQUM1QixJQUFJLEVBQUUsT0FBTztvQkFDYixLQUFLLEVBQUU7d0JBQ0wsS0FBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzlCLEtBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO3FCQUM3QztvQkFDRCxPQUFPLEVBQUcsQ0FBQyxZQUFZLENBQUM7aUJBQ3pCLENBQUM7YUFDSDtZQUNELE9BQU8sRUFBRSxDQUFDLG1CQUFtQixFQUFFLG9CQUFvQixFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxXQUFXLEdBQUcsWUFBWSxDQUFDO1NBQ2hHLENBQUMsRUFBSyxJQUFJLENBQUMsSUFBSSxjQUFXLENBQzVCLENBQUM7UUFDRixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7S0FDaEI7SUFDSCxzQkFBQztDQTdGRCxDQUFxQyxVQUFVOzs7Ozs7O0lDSmhCQSw2QkFBYTtJQUkxQyxtQkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBSWY7UUFSRCxVQUFJLEdBQUcsTUFBTSxDQUFDO1FBS1osS0FBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3BDLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzlCLEtBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs7S0FDN0M7SUFDSCxnQkFBQztDQVZELENBQStCLEtBQUs7Ozs7Ozs7SUNDSEEsK0JBQVM7SUFHeEMscUJBQVksT0FBZ0I7UUFBaEIsd0JBQUEsRUFBQSxZQUFnQjtRQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUdmO1FBRkMsS0FBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25DLEtBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksSUFBSSxJQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7O0tBQzVFOzs7O0lBRUssaUNBQVc7OztJQUFqQjs7OztnQkFDRSxzQkFBTyxpQkFBTSxXQUFXLFdBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO3dCQUNyQyxJQUFJLENBQUMsT0FBTyxFQUFFOzRCQUNaLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQzNDLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDOUI7d0JBQ0QsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUMvQixDQUFDLEVBQUM7OztLQUNKOzs7O0lBRUQsNEJBQU07OztJQUFOO1FBQ0UsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUM1Qzs7Ozs7SUFFRCxrQ0FBWTs7OztJQUFaLFVBQWEsS0FBVTs7WUFDakIsUUFBZ0I7UUFDcEIsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDN0IsUUFBUSxzQkFBRyxLQUFLLEVBQVUsQ0FBQztTQUM1QjthQUFNLElBQUksT0FBTyxLQUFLLEtBQUssV0FBVyxFQUFFO1lBQ3ZDLFFBQVEsR0FBRyxDQUFDLENBQUM7U0FDZDthQUFNO1lBQ0wsUUFBUSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2pFO1FBQ0QsT0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztLQUN6QztJQUNILGtCQUFDO0NBbENELENBQWlDLFNBQVM7Ozs7Ozs7SUNEUEEsaUNBQWE7SUFHOUMsdUJBQVksT0FBZ0I7UUFBaEIsd0JBQUEsRUFBQSxZQUFnQjtRQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUdmO1FBTkQsVUFBSSxHQUFHLFVBQVUsQ0FBQztRQUloQixLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDcEMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7O0tBQ25DO0lBQ0gsb0JBQUM7Q0FSRCxDQUFtQyxLQUFLOzs7Ozs7QUNGeEM7SUFVRTtRQUNFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDcEM7SUFFRCxzQkFBSSw4QkFBSzs7OztRQUFUO1lBQ0UsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1NBQ3ZCOzs7T0FBQTs7Ozs7SUFFRCxtQ0FBYTs7OztJQUFiLFVBQWMsS0FBaUI7UUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUNsQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1NBQy9EO0tBQ0Y7Ozs7O0lBRUQsb0NBQWM7Ozs7SUFBZCxVQUFlLE1BQW9CO1FBQW5DLGlCQUlDO1FBSEMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFBLEtBQUs7WUFDbEIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQixDQUFDLENBQUM7S0FDSjs7Ozs7SUFFRCxxQ0FBZTs7OztJQUFmLFVBQWdCLEtBQWlCO1FBQy9CLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUNwQzs7Ozs7SUFFRCxzQ0FBZ0I7Ozs7SUFBaEIsVUFBaUIsTUFBb0I7UUFBckMsaUJBSUM7UUFIQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztZQUNsQixLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzdCLENBQUMsQ0FBQztLQUNKOzs7OztJQUVELHNDQUFnQjs7OztJQUFoQixVQUFpQixLQUFpQjtRQUNoQyxPQUFPLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDLENBQUM7S0FDM0M7O2dCQXBDRixVQUFVOzs7O0lBcUNYLGtCQUFDO0NBckNEOzs7Ozs7O0lDSDBCQSx3QkFBUTtJQU1oQyxjQUFZLE9BTU47UUFOTSx3QkFBQSxFQUFBLFlBTU47UUFOTixZQU9FLGtCQUFNLE9BQU8sQ0FBQyxTQUlmO1FBZEQsZUFBUyxHQUFHLElBQUksQ0FBQztRQUNqQixZQUFNLEdBQUcsSUFBSSxDQUFDO1FBVVosS0FBSSxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hDLEtBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQztRQUN4QyxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsQ0FBQzs7S0FDM0I7SUFFRCxzQkFBSSx3QkFBTTs7OztRQUFWO1lBQ0UsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNiLE9BQU8sS0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxNQUFHLENBQUM7YUFDM0M7aUJBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNwQixPQUFVLElBQUksQ0FBQyxJQUFJLE1BQUcsQ0FBQzthQUN4QjtpQkFBTTtnQkFDTCxPQUFPLEVBQUUsQ0FBQzthQUNYO1NBQ0Y7OztPQUFBOzs7OztJQUVELHFCQUFNOzs7O0lBQU4sVUFBTyxHQUFRO1FBQ2IsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxDQUFDO0tBQzVCOzs7Ozs7SUFFRCwwQkFBVzs7Ozs7SUFBWCxVQUFZLEdBQVcsRUFBRSxLQUFVO1FBQ2pDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxDQUFDO0tBQ3ZCOzs7O0lBRUQsdUJBQVE7OztJQUFSO1FBQ0UsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRTtZQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ25CO0tBQ0Y7SUFDSCxXQUFDO0NBM0NELENBQTBCLFFBQVE7Ozs7Ozs7Ozs7O0FDSGxDO0lBZUUsdUJBQW9CLFdBQXdCO1FBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0tBQUk7Ozs7SUFFaEQsZ0NBQVE7OztJQUFSO0tBRUM7SUFFRCxzQkFBSSxnQ0FBSzs7OztRQUFUO1lBQ0UsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztTQUMvQjs7O09BQUE7Ozs7SUFFRCxnQ0FBUTs7O0lBQVI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0tBQ3RCOztnQkFwQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxVQUFVO29CQUNwQixpUkFBb0M7b0JBQ3BDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtpQkFDM0I7Ozs7Z0JBUFEsV0FBVzs7O3VCQVNqQixLQUFLOztJQWVSLG9CQUFDO0NBckJEOzs7Ozs7QUNQQTtJQWVFLDJCQUFvQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtLQUFJOzs7O0lBRWhELG9DQUFROzs7SUFBUjtRQUNFLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLG9CQUFDLElBQUksQ0FBQyxJQUFJLEdBQWUsQ0FBQztTQUN6RDtLQUNGOzs7O0lBRUQsdUNBQVc7OztJQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNyQixJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsb0JBQUMsSUFBSSxDQUFDLElBQUksR0FBZSxDQUFDO1NBQzNEO0tBQ0Y7SUFFRCxzQkFBSSxvQ0FBSzs7OztRQUFUO1lBQ0UsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztTQUMvQjs7O09BQUE7Ozs7O0lBRUQsb0NBQVE7Ozs7SUFBUixVQUFTLEtBQUs7UUFDWixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUMzQjs7Ozs7SUFFRCxrQ0FBTTs7OztJQUFOLFVBQU8sS0FBSztRQUNWLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ3pCOzs7O0lBRUQsbUNBQU87OztJQUFQO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztLQUNyQjs7Z0JBcENGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZUFBZTtvQkFDekIsOGxDQUF5QztvQkFDekMsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO2lCQUMzQjs7OztnQkFOUSxXQUFXOzs7dUJBUWpCLEtBQUs7O0lBK0JSLHdCQUFDO0NBckNEOzs7Ozs7O0lDSTRDQSwwQ0FBaUI7SUFMN0Q7O0tBT0M7O2dCQVBBLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixvYUFBOEM7b0JBQzlDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtpQkFDM0I7Ozt1QkFFRSxLQUFLOztJQUNSLDZCQUFDO0NBQUEsQ0FGMkMsaUJBQWlCOzs7Ozs7O0lDQWRBLDZDQUFpQjtJQUxoRTs7S0FPQzs7Z0JBUEEsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSx3QkFBd0I7b0JBQ2xDLG91QkFBa0Q7b0JBQ2xELFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtpQkFDM0I7Ozt1QkFFRSxLQUFLOztJQUNSLGdDQUFDO0NBQUEsQ0FGOEMsaUJBQWlCOzs7Ozs7O0lDQXJCQSx5Q0FBaUI7SUFMNUQ7O0tBT0M7O2dCQVBBLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsbUJBQW1CO29CQUM3QixxTEFBNkM7b0JBQzdDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtpQkFDM0I7Ozt1QkFFRSxLQUFLOztJQUNSLDRCQUFDO0NBQUEsQ0FGMEMsaUJBQWlCOzs7Ozs7O0lDQXBCQSxzQ0FBaUI7SUFMekQ7O0tBT0M7O2dCQVBBLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixtNEJBQTBDO29CQUMxQyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFDUix5QkFBQztDQUFBLENBRnVDLGlCQUFpQjs7Ozs7OztJQ0FiQSwwQ0FBaUI7SUFMN0Q7O0tBT0M7O2dCQVBBLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5QiwydkJBQThDO29CQUM5QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFDUiw2QkFBQztDQUFBLENBRjJDLGlCQUFpQjs7Ozs7OztJQ0FwQkEsdUNBQWlCO0lBTDFEOztLQU9DOztnQkFQQSxTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGlCQUFpQjtvQkFDM0IsdVBBQTJDO29CQUMzQyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFDUiwwQkFBQztDQUFBLENBRndDLGlCQUFpQjs7Ozs7OztJQ0FqQkEsdUNBQWlCO0lBTDFEOztLQVdDO0lBSEMsc0JBQUksc0NBQUs7Ozs7UUFBVDtZQUNFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDaEU7OztPQUFBOztnQkFWRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGlCQUFpQjtvQkFDM0IsMFdBQTJDO29CQUMzQyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFLUiwwQkFBQztDQUFBLENBTndDLGlCQUFpQjs7Ozs7OztJQ0FoQkEsd0NBQWlCO0lBTDNEOztLQU9DOztnQkFQQSxTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIscUxBQTRDO29CQUM1QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFDUiwyQkFBQztDQUFBLENBRnlDLGlCQUFpQjs7Ozs7OztJQ0FkQSwyQ0FBaUI7SUFMOUQ7O0tBT0M7O2dCQVBBLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsc0JBQXNCO29CQUNoQyxnc0JBQWdEO29CQUNoRCxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFDUiw4QkFBQztDQUFBLENBRjRDLGlCQUFpQjs7Ozs7OztJQ0F0QkEsc0NBQWlCO0lBTHpEOztLQU9DOztnQkFQQSxTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjtvQkFDMUIscTFCQUEwQztvQkFDMUMsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO2lCQUMzQjs7O3VCQUVFLEtBQUs7O0lBQ1IseUJBQUM7Q0FBQSxDQUZ1QyxpQkFBaUI7Ozs7Ozs7SUNBYkEsMENBQWlCO0lBTDdEOztLQU9DOztnQkFQQSxTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIscWJBQThDO29CQUM5QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFDUiw2QkFBQztDQUFBLENBRjJDLGlCQUFpQjs7Ozs7O0FDWDdEO0lBd0JBO0tBZ0RDOzs7O0lBTlEsNEJBQU87OztJQUFkO1FBQ0UsT0FBTztZQUNMLFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO1NBQzNCLENBQUM7S0FDSDs7Z0JBL0NGLFFBQVEsU0FBQztvQkFDUixPQUFPLEVBQUU7d0JBQ1AsWUFBWTt3QkFDWixXQUFXO3dCQUNYLG1CQUFtQjt3QkFDbkIsd0JBQXdCO3dCQUN4QixxQkFBcUIsQ0FBQyxPQUFPLEVBQUU7cUJBQ2hDO29CQUNELFlBQVksRUFBRTt3QkFDWixhQUFhO3dCQUNiLGlCQUFpQjt3QkFFakIsc0JBQXNCO3dCQUN0Qix5QkFBeUI7d0JBQ3pCLHFCQUFxQjt3QkFDckIsa0JBQWtCO3dCQUNsQixzQkFBc0I7d0JBQ3RCLG1CQUFtQjt3QkFDbkIsbUJBQW1CO3dCQUNuQixvQkFBb0I7d0JBQ3BCLHVCQUF1Qjt3QkFDdkIsa0JBQWtCO3dCQUNsQixzQkFBc0I7cUJBQ3ZCO29CQUNELE9BQU8sRUFBRTt3QkFDUCxhQUFhO3dCQUNiLGlCQUFpQjt3QkFFakIsc0JBQXNCO3dCQUN0Qix5QkFBeUI7d0JBQ3pCLHFCQUFxQjt3QkFDckIsa0JBQWtCO3dCQUNsQixzQkFBc0I7d0JBQ3RCLG1CQUFtQjt3QkFDbkIsbUJBQW1CO3dCQUNuQixvQkFBb0I7d0JBQ3BCLHVCQUF1Qjt3QkFDdkIsa0JBQWtCO3dCQUNsQixzQkFBc0I7cUJBQ3ZCO2lCQUNGOztJQVFELDJCQUFDO0NBaEREOzs7Ozs7Ozs7Ozs7OzsifQ==