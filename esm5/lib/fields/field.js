/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FormBase } from '../form-base';
/**
 * @template T
 */
var /**
 * @template T
 */
Field = /** @class */ (function (_super) {
    tslib_1.__extends(Field, _super);
    function Field(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.hasData = true;
        _this.value = options.value;
        _this.label = options.label || '';
        _this.classes.push('ndf-field');
        _this.placeholder = options.placeholder || '';
        if (_this.placeholder.length > 0) {
            _this.classes.push('ndf-placeholder');
        }
        _this.derive = options.derive || null;
        return _this;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    Field.prototype.setForm = /**
     * @param {?} form
     * @return {?}
     */
    function (form) {
        _super.prototype.setForm.call(this, form);
        this.deriveValue();
    };
    /**
     * @param {?} value
     * @return {?}
     */
    Field.prototype.onChange = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.value = value;
        _super.prototype.onChange.call(this, this.prepareValue(value));
    };
    /**
     * @return {?}
     */
    Field.prototype.deriveValue = /**
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, _super.prototype.deriveValue.call(this).then(function (derived) {
                        if (!derived) {
                            if (_this.derive != null) {
                                return _this.derive(_this);
                            }
                            else if (_this.form.obj.hasOwnProperty(_this.name)) {
                                _this.value = _this.form.obj[_this.name];
                                return Promise.resolve(true);
                            }
                        }
                        return Promise.resolve(false);
                    })];
            });
        });
    };
    /**
     * @param {?} value
     * @return {?}
     */
    Field.prototype.prepareValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (value == null) {
            return '';
        }
        else {
            return value;
        }
    };
    return Field;
}(FormBase));
/**
 * @template T
 */
export { Field };
if (false) {
    /** @type {?} */
    Field.prototype.id;
    /** @type {?} */
    Field.prototype.hasData;
    /** @type {?} */
    Field.prototype.value;
    /** @type {?} */
    Field.prototype.label;
    /** @type {?} */
    Field.prototype.placeholder;
    /** @type {?} */
    Field.prototype.derive;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL2ZpZWxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQ0EsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGNBQWMsQ0FBQzs7OztBQUd4Qzs7OztJQUE4QixpQ0FBUTtJQVFwQyxlQUFZLE9BTU47UUFOTSx3QkFBQSxFQUFBLFlBTU47UUFOTixZQU9FLGtCQUFNLE9BQU8sQ0FBQyxTQVNmO1FBdEJELGFBQU8sR0FBRyxJQUFJLENBQUM7UUFjYixLQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFDM0IsS0FBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQztRQUNqQyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMvQixLQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFDO1FBQzdDLElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQy9CLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDdEM7UUFDRCxLQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDOztJQUN2QyxDQUFDOzs7OztJQUVELHVCQUFPOzs7O0lBQVAsVUFBUSxJQUFVO1FBQ2hCLGlCQUFNLE9BQU8sWUFBQyxJQUFJLENBQUMsQ0FBQztRQUNwQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCx3QkFBUTs7OztJQUFSLFVBQVMsS0FBSztRQUNaLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLGlCQUFNLFFBQVEsWUFBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDM0MsQ0FBQzs7OztJQUVLLDJCQUFXOzs7SUFBakI7Ozs7Z0JBQ0Usc0JBQU8saUJBQU0sV0FBVyxXQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTzt3QkFDckMsSUFBSSxDQUFDLE9BQU8sRUFBRTs0QkFDWixJQUFJLEtBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxFQUFFO2dDQUN2QixPQUFPLEtBQUksQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLENBQUM7NkJBQzFCO2lDQUFNLElBQUksS0FBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtnQ0FDbEQsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0NBQ3RDLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzs2QkFDOUI7eUJBQ0Y7d0JBQ0QsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNoQyxDQUFDLENBQUMsRUFBQzs7O0tBQ0o7Ozs7O0lBRUQsNEJBQVk7Ozs7SUFBWixVQUFhLEtBQVU7UUFDckIsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ2pCLE9BQU8sRUFBRSxDQUFDO1NBQ1g7YUFBTTtZQUNMLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7SUFDSCxDQUFDO0lBQ0gsWUFBQztBQUFELENBQUMsQUF6REQsQ0FBOEIsUUFBUSxHQXlEckM7Ozs7Ozs7SUF4REMsbUJBQVc7O0lBQ1gsd0JBQWU7O0lBQ2Ysc0JBQVM7O0lBQ1Qsc0JBQWM7O0lBQ2QsNEJBQW9COztJQUNwQix1QkFBOEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtIH0gZnJvbSAnLi4vZm9ybSc7XG5pbXBvcnQgeyBGb3JtQmFzZSB9IGZyb20gJy4uL2Zvcm0tYmFzZSc7XG5cblxuZXhwb3J0IGNsYXNzIEZpZWxkPFQ+IGV4dGVuZHMgRm9ybUJhc2Uge1xuICBpZDogc3RyaW5nO1xuICBoYXNEYXRhID0gdHJ1ZTtcbiAgdmFsdWU6IFQ7XG4gIGxhYmVsOiBzdHJpbmc7XG4gIHBsYWNlaG9sZGVyOiBzdHJpbmc7XG4gIGRlcml2ZTogKGZpZWxkOiBGaWVsZDxUPikgPT4gUHJvbWlzZTxib29sZWFuPjtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7XG4gICAgdmFsdWU/OiBULFxuICAgIGxhYmVsPzogc3RyaW5nLFxuICAgIGNsYXNzZXM/OiBzdHJpbmdbXSxcbiAgICBwbGFjZWhvbGRlcj86IHN0cmluZyxcbiAgICBkZXJpdmU/OiAoZmllbGQ6IEZpZWxkPFQ+KSA9PiBQcm9taXNlPGJvb2xlYW4+XG4gIH0gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMudmFsdWUgPSBvcHRpb25zLnZhbHVlO1xuICAgIHRoaXMubGFiZWwgPSBvcHRpb25zLmxhYmVsIHx8ICcnO1xuICAgIHRoaXMuY2xhc3Nlcy5wdXNoKCduZGYtZmllbGQnKTtcbiAgICB0aGlzLnBsYWNlaG9sZGVyID0gb3B0aW9ucy5wbGFjZWhvbGRlciB8fCAnJztcbiAgICBpZiAodGhpcy5wbGFjZWhvbGRlci5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLXBsYWNlaG9sZGVyJyk7XG4gICAgfVxuICAgIHRoaXMuZGVyaXZlID0gb3B0aW9ucy5kZXJpdmUgfHwgbnVsbDtcbiAgfVxuXG4gIHNldEZvcm0oZm9ybTogRm9ybSk6IHZvaWQge1xuICAgIHN1cGVyLnNldEZvcm0oZm9ybSk7XG4gICAgdGhpcy5kZXJpdmVWYWx1ZSgpO1xuICB9XG5cbiAgb25DaGFuZ2UodmFsdWUpIHtcbiAgICB0aGlzLnZhbHVlID0gdmFsdWU7XG4gICAgc3VwZXIub25DaGFuZ2UodGhpcy5wcmVwYXJlVmFsdWUodmFsdWUpKTtcbiAgfVxuXG4gIGFzeW5jIGRlcml2ZVZhbHVlKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgIHJldHVybiBzdXBlci5kZXJpdmVWYWx1ZSgpLnRoZW4oZGVyaXZlZCA9PiB7XG4gICAgICBpZiAoIWRlcml2ZWQpIHtcbiAgICAgICAgaWYgKHRoaXMuZGVyaXZlICE9IG51bGwpIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5kZXJpdmUodGhpcyk7XG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5mb3JtLm9iai5oYXNPd25Qcm9wZXJ0eSh0aGlzLm5hbWUpKSB7XG4gICAgICAgICAgdGhpcy52YWx1ZSA9IHRoaXMuZm9ybS5vYmpbdGhpcy5uYW1lXTtcbiAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHRydWUpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcbiAgICB9KTtcbiAgfVxuXG4gIHByZXBhcmVWYWx1ZSh2YWx1ZTogYW55KTogYW55IHtcbiAgICBpZiAodmFsdWUgPT0gbnVsbCkge1xuICAgICAgcmV0dXJuICcnO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gdmFsdWU7XG4gICAgfVxuICB9XG59XG4iXX0=