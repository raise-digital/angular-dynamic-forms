import { FormBase } from './form-base';
export declare class Form extends FormBase {
    obj: any;
    submit: (form: Form) => void;
    hasFields: boolean;
    isForm: boolean;
    constructor(options?: {
        name?: string;
        fields?: FormBase[];
        obj?: any;
        submit?: (form: Form) => void;
        classes?: string[];
    });
    readonly prefix: string;
    update(obj: any): void;
    fieldChange(key: string, value: any): void;
    onSubmit(): void;
}
