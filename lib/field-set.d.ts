import { FormBase } from './form-base';
import { Form } from './form';
export declare class FieldSet {
    private set;
    constructor(set: FormBase[]);
    setForm(form: Form): void;
    updateValues(): void;
    readonly fields: FormBase[];
    push(item: FormBase): void;
    insertBefore(item: FormBase, before: string, attr: string, field?: FormBase): void;
    insertBeforeName(item: FormBase, before: string, field?: FormBase): void;
    insertBeforeId(item: FormBase, before: string, field?: FormBase): void;
    insertAfter(item: FormBase, after: string, attr: string, field?: FormBase): void;
    insertAfterName(item: FormBase, after: string, _attr: string, field?: FormBase): void;
    insertAfterId(item: FormBase, after: string, _attr: string, field?: FormBase): void;
    remove(key: string, field: FormBase, attr: string): void;
    removeByName(name: string, field?: FormBase): void;
    removeById(id: string, field?: FormBase): void;
    field(key: string, attr: string): any;
    fieldByName(name: string): any;
    fieldById(id: string): any;
    fieldWithParent(key: string, attr: string): {
        field: any;
        parent: any;
    };
    fieldByNameWithParent(name: string): {
        field: any;
        parent: any;
    };
    fieldByIdWithParent(id: string): {
        field: any;
        parent: any;
    };
    resetValidation(): void;
}
