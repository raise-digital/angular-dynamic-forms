/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Field } from './field';
var TextField = /** @class */ (function (_super) {
    tslib_1.__extends(TextField, _super);
    function TextField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.type = 'text';
        _this.value = options['value'] || '';
        _this.classes.push('ndf-text');
        _this.inputType = options['inputType'] || '';
        return _this;
    }
    return TextField;
}(Field));
export { TextField };
if (false) {
    /** @type {?} */
    TextField.prototype.type;
    /** @type {?} */
    TextField.prototype.inputType;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1maWVsZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9maWVsZHMvdGV4dC1maWVsZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFaEM7SUFBK0IscUNBQWE7SUFJMUMsbUJBQVksT0FBZ0I7UUFBaEIsd0JBQUEsRUFBQSxZQUFnQjtRQUE1QixZQUNFLGtCQUFNLE9BQU8sQ0FBQyxTQUlmO1FBUkQsVUFBSSxHQUFHLE1BQU0sQ0FBQztRQUtaLEtBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNwQyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM5QixLQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7O0lBQzlDLENBQUM7SUFDSCxnQkFBQztBQUFELENBQUMsQUFWRCxDQUErQixLQUFLLEdBVW5DOzs7O0lBVEMseUJBQWM7O0lBQ2QsOEJBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRmllbGQgfSBmcm9tICcuL2ZpZWxkJztcblxuZXhwb3J0IGNsYXNzIFRleHRGaWVsZCBleHRlbmRzIEZpZWxkPHN0cmluZz4ge1xuICB0eXBlID0gJ3RleHQnO1xuICBpbnB1dFR5cGU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy52YWx1ZSA9IG9wdGlvbnNbJ3ZhbHVlJ10gfHwgJyc7XG4gICAgdGhpcy5jbGFzc2VzLnB1c2goJ25kZi10ZXh0Jyk7XG4gICAgdGhpcy5pbnB1dFR5cGUgPSBvcHRpb25zWydpbnB1dFR5cGUnXSB8fCAnJztcbiAgfVxufVxuIl19