import { FormItemComponent } from './form-item.component';
import { CheckboxSetField } from '../fields/checkbox-set-field';
export declare class CheckboxSetFieldComponent extends FormItemComponent {
    item: CheckboxSetField<any>;
}
