import { FormBase } from '../form-base';
export declare class ContentField extends FormBase {
    type: string;
    content: string;
    constructor(options?: {});
}
