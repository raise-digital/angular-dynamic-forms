/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { TextareaField } from '../fields/textarea-field';
import { FormService } from '../services/form.service';
var TextareaFieldComponent = /** @class */ (function (_super) {
    tslib_1.__extends(TextareaFieldComponent, _super);
    function TextareaFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TextareaFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-textarea-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <textarea [formControlName]=\"item.id\" [id]=\"item.id\" [value]=\"item.value\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" (blur)=\"onBlur($event)\"></textarea>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    TextareaFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return TextareaFieldComponent;
}(FormItemComponent));
export { TextareaFieldComponent };
if (false) {
    /** @type {?} */
    TextareaFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dGFyZWEtZmllbGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvdGV4dGFyZWEtZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFakQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUV2RDtJQUs0QyxrREFBaUI7SUFMN0Q7O0lBT0EsQ0FBQzs7Z0JBUEEsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLHFiQUE4QztvQkFDOUMsU0FBUyxFQUFFLENBQUUsV0FBVyxDQUFFO2lCQUMzQjs7O3VCQUVFLEtBQUs7O0lBQ1IsNkJBQUM7Q0FBQSxBQVBELENBSzRDLGlCQUFpQixHQUU1RDtTQUZZLHNCQUFzQjs7O0lBQ2pDLHNDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgRm9ybUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Zvcm0taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgVGV4dGFyZWFGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy90ZXh0YXJlYS1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi10ZXh0YXJlYS1maWVsZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi90ZXh0YXJlYS1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIFRleHRhcmVhRmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IFRleHRhcmVhRmllbGQ7XG59XG4iXX0=