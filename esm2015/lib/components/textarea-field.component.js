/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { TextareaField } from '../fields/textarea-field';
import { FormService } from '../services/form.service';
export class TextareaFieldComponent extends FormItemComponent {
}
TextareaFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ndf-textarea-field',
                template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <textarea [formControlName]=\"item.id\" [id]=\"item.id\" [value]=\"item.value\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" (blur)=\"onBlur($event)\"></textarea>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                providers: [FormService]
            }] }
];
TextareaFieldComponent.propDecorators = {
    item: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    TextareaFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dGFyZWEtZmllbGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvdGV4dGFyZWEtZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVqRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDekQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBT3ZELE1BQU0sT0FBTyxzQkFBdUIsU0FBUSxpQkFBaUI7OztZQUw1RCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIscWJBQThDO2dCQUM5QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7YUFDM0I7OzttQkFFRSxLQUFLOzs7O0lBQU4sc0NBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBUZXh0YXJlYUZpZWxkIH0gZnJvbSAnLi4vZmllbGRzL3RleHRhcmVhLWZpZWxkJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbmRmLXRleHRhcmVhLWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3RleHRhcmVhLWZpZWxkLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgVGV4dGFyZWFGaWVsZENvbXBvbmVudCBleHRlbmRzIEZvcm1JdGVtQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbTogVGV4dGFyZWFGaWVsZDtcbn1cbiJdfQ==