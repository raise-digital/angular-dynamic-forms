/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { TextField } from '../fields/text-field';
import { FormService } from '../services/form.service';
var TextFieldComponent = /** @class */ (function (_super) {
    tslib_1.__extends(TextFieldComponent, _super);
    function TextFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TextFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-text-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <label *ngIf=\"item.hasLabel && item.label\" [attr.for]=\"item.id\">{{ item.label }}</label>\n    <label *ngIf=\"!item.hasLabel\">\n        <div *ngIf=\"item.type == 'checkbox'\" class=\"box\"><fa *ngIf=\"item.value\" name=\"check\" size=\"lt\"></fa></div>\n        <input *ngIf=\"item.type == 'checkbox'\" type=\"checkbox\" [formControlName]=\"item.id\" [checked]=\"item.value\" (ngModelChange)=\"onChange($event)\" />\n        {{ item.label }}\n    </label>\n    <input [formControlName]=\"item.id\" [id]=\"item.id\" [type]=\"item.inputType\" [placeholder]=\"item.placeholder\" [value]=\"item.value\" [readonly]=\"item.readOnly\" (ngModelChange)=\"onChange($event)\" (blur)=\"onBlur($event)\" />\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    TextFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return TextFieldComponent;
}(FormItemComponent));
export { TextFieldComponent };
if (false) {
    /** @type {?} */
    TextFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1maWVsZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy90ZXh0LWZpZWxkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWpELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFdkQ7SUFLd0MsOENBQWlCO0lBTHpEOztJQU9BLENBQUM7O2dCQVBBLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixxMUJBQTBDO29CQUMxQyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFDUix5QkFBQztDQUFBLEFBUEQsQ0FLd0MsaUJBQWlCLEdBRXhEO1NBRlksa0JBQWtCOzs7SUFDN0Isa0NBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBUZXh0RmllbGQgfSBmcm9tICcuLi9maWVsZHMvdGV4dC1maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi10ZXh0LWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3RleHQtZmllbGQuY29tcG9uZW50Lmh0bWwnLFxuICBwcm92aWRlcnM6IFsgRm9ybVNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBUZXh0RmllbGRDb21wb25lbnQgZXh0ZW5kcyBGb3JtSXRlbUNvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGl0ZW06IFRleHRGaWVsZDtcbn1cbiJdfQ==