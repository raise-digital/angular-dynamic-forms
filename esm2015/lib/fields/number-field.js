/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { TextField } from './text-field';
export class NumberField extends TextField {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        super(options);
        this.value = options['value'] || 0;
        this.precision = (options['precision'] != null) ? options['precision'] : 2;
    }
    /**
     * @return {?}
     */
    deriveValue() {
        const _super = name => super[name];
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return _super("deriveValue").call(this).then(derived => {
                if (!derived) {
                    this.value = this.prepareValue(this.value);
                    return Promise.resolve(true);
                }
                return Promise.resolve(false);
            });
        });
    }
    /**
     * @return {?}
     */
    onBlur() {
        this.value = this.prepareValue(this.value);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    prepareValue(value) {
        /** @type {?} */
        let prepared;
        if (typeof value === 'number') {
            prepared = (/** @type {?} */ (value));
        }
        else if (typeof value === 'undefined') {
            prepared = 0;
        }
        else {
            prepared = parseFloat(value.toString().replace(/[^0-9.]/g, ''));
        }
        return prepared.toFixed(this.precision);
    }
}
if (false) {
    /** @type {?} */
    NumberField.prototype.precision;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtYmVyLWZpZWxkLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2ZpZWxkcy9udW1iZXItZmllbGQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBR3pDLE1BQU0sT0FBTyxXQUFZLFNBQVEsU0FBUzs7OztJQUd4QyxZQUFZLFVBQWMsRUFBRTtRQUMxQixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDZixJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDN0UsQ0FBQzs7OztJQUVLLFdBQVc7OztZQUNmLE9BQU8scUJBQWlCLFlBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN4QyxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUNaLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzNDLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDOUI7Z0JBQ0QsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztLQUFBOzs7O0lBRUQsTUFBTTtRQUNKLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsS0FBVTs7WUFDakIsUUFBZ0I7UUFDcEIsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDN0IsUUFBUSxHQUFHLG1CQUFBLEtBQUssRUFBVSxDQUFDO1NBQzVCO2FBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxXQUFXLEVBQUU7WUFDdkMsUUFBUSxHQUFHLENBQUMsQ0FBQztTQUNkO2FBQU07WUFDTCxRQUFRLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDakU7UUFDRCxPQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzFDLENBQUM7Q0FDRjs7O0lBakNDLGdDQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFRleHRGaWVsZCB9IGZyb20gJy4vdGV4dC1maWVsZCc7XG5cblxuZXhwb3J0IGNsYXNzIE51bWJlckZpZWxkIGV4dGVuZHMgVGV4dEZpZWxkIHtcbiAgcHJlY2lzaW9uOiBudW1iZXI7XG5cbiAgY29uc3RydWN0b3Iob3B0aW9uczoge30gPSB7fSkge1xuICAgIHN1cGVyKG9wdGlvbnMpO1xuICAgIHRoaXMudmFsdWUgPSBvcHRpb25zWyd2YWx1ZSddIHx8IDA7XG4gICAgdGhpcy5wcmVjaXNpb24gPSAob3B0aW9uc1sncHJlY2lzaW9uJ10gIT0gbnVsbCkgPyBvcHRpb25zWydwcmVjaXNpb24nXSA6IDI7XG4gIH1cblxuICBhc3luYyBkZXJpdmVWYWx1ZSgpOiBQcm9taXNlPGJvb2xlYW4+IHtcbiAgICByZXR1cm4gc3VwZXIuZGVyaXZlVmFsdWUoKS50aGVuKGRlcml2ZWQgPT4ge1xuICAgICAgaWYgKCFkZXJpdmVkKSB7XG4gICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLnByZXBhcmVWYWx1ZSh0aGlzLnZhbHVlKTtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSh0cnVlKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZmFsc2UpO1xuICAgIH0pO1xuICB9XG5cbiAgb25CbHVyKCk6IHZvaWQge1xuICAgIHRoaXMudmFsdWUgPSB0aGlzLnByZXBhcmVWYWx1ZSh0aGlzLnZhbHVlKTtcbiAgfVxuXG4gIHByZXBhcmVWYWx1ZSh2YWx1ZTogYW55KTogYW55IHtcbiAgICBsZXQgcHJlcGFyZWQ6IG51bWJlcjtcbiAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnbnVtYmVyJykge1xuICAgICAgcHJlcGFyZWQgPSB2YWx1ZSBhcyBudW1iZXI7XG4gICAgfSBlbHNlIGlmICh0eXBlb2YgdmFsdWUgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICBwcmVwYXJlZCA9IDA7XG4gICAgfSBlbHNlIHtcbiAgICAgIHByZXBhcmVkID0gcGFyc2VGbG9hdCh2YWx1ZS50b1N0cmluZygpLnJlcGxhY2UoL1teMC05Ll0vZywgJycpKTtcbiAgICB9XG4gICAgcmV0dXJuIHByZXBhcmVkLnRvRml4ZWQodGhpcy5wcmVjaXNpb24pO1xuICB9XG59XG4iXX0=