/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FieldSet } from './field-set';
export class FormBase {
    /**
     * @param {?=} options
     */
    constructor(options = {}) {
        this.isForm = false;
        this.hasData = false;
        this.hasFields = false;
        this.hasLabel = true;
        this.isAction = false;
        this.name = options.name || '';
        this.id = this.name;
        this.classes = options.classes || [];
        this.fields = new FieldSet(options.fields || []);
        this.readOnly = options.readOnly || false;
        this.change = options.change || null;
        this.blur = options.blur || null;
    }
    /**
     * @return {?}
     */
    getForm() {
        return this.form;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    setForm(form) {
        this.form = form;
        if (form.prefix) {
            this.id = `${form.prefix}${this.name}`;
        }
        if (!this.isForm) {
            this.fields.setForm(form);
        }
    }
    /**
     * @return {?}
     */
    deriveValue() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return Promise.resolve(false);
        });
    }
    /**
     * @return {?}
     */
    get control() {
        return null;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onChange(value) {
        /** @type {?} */
        let propagate = true;
        if (this.change != null) {
            propagate = this.change(this, value);
        }
        if (propagate) {
            this.form.fieldChange(this.name, value);
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onBlur(value) {
        if (this.blur != null) {
            this.blur(this, value);
        }
    }
    /**
     * @return {?}
     */
    onClick() {
    }
    /**
     * @param {?} message
     * @return {?}
     */
    setMessage(message) {
        this.message = message;
    }
    /**
     * @param {?} result
     * @return {?}
     */
    loadValidation(result) {
        this.resetValidation();
        if (result.messages != null && result.messages.length > 0) {
            this.setMessage(result.messages[0]);
        }
        for (const key in result.fields) {
            if (result.fields.hasOwnProperty(key)) {
                /** @type {?} */
                const field = this.fields.fieldByName(key);
                if (field != null) {
                    field.loadValidation(result.fields[key]);
                }
            }
        }
    }
    /**
     * @return {?}
     */
    resetValidation() {
        this.setMessage(null);
        if (this.fields) {
            this.fields.resetValidation();
        }
    }
}
if (false) {
    /** @type {?} */
    FormBase.prototype.name;
    /** @type {?} */
    FormBase.prototype.id;
    /** @type {?} */
    FormBase.prototype.type;
    /** @type {?} */
    FormBase.prototype.classes;
    /** @type {?} */
    FormBase.prototype.fields;
    /** @type {?} */
    FormBase.prototype.message;
    /** @type {?} */
    FormBase.prototype.readOnly;
    /** @type {?} */
    FormBase.prototype.change;
    /** @type {?} */
    FormBase.prototype.blur;
    /** @type {?} */
    FormBase.prototype.form;
    /** @type {?} */
    FormBase.prototype.isForm;
    /** @type {?} */
    FormBase.prototype.hasData;
    /** @type {?} */
    FormBase.prototype.hasFields;
    /** @type {?} */
    FormBase.prototype.hasLabel;
    /** @type {?} */
    FormBase.prototype.isAction;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1iYXNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHJhaXNlZGlnaXRhbC9uZy1keW5hbWljLWZvcm1zLyIsInNvdXJjZXMiOlsibGliL2Zvcm0tYmFzZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUNBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFNdkMsTUFBTSxPQUFPLFFBQVE7Ozs7SUFrQm5CLFlBQVksVUFPUixFQUFFO1FBYk4sV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNmLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFDaEIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixhQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFVZixJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxFQUFFLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDO1FBQzFDLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUM7UUFDckMsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQztJQUNuQyxDQUFDOzs7O0lBRUQsT0FBTztRQUNMLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNuQixDQUFDOzs7OztJQUVELE9BQU8sQ0FBQyxJQUFVO1FBQ2hCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNmLElBQUksQ0FBQyxFQUFFLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUN4QztRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzNCO0lBQ0gsQ0FBQzs7OztJQUVLLFdBQVc7O1lBQ2YsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hDLENBQUM7S0FBQTs7OztJQUVELElBQUksT0FBTztRQUNULE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBSzs7WUFDUixTQUFTLEdBQUcsSUFBSTtRQUNwQixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxFQUFFO1lBQ3ZCLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztTQUN0QztRQUNELElBQUksU0FBUyxFQUFFO1lBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztTQUN6QztJQUNILENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLEtBQUs7UUFDVixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQzs7OztJQUVELE9BQU87SUFFUCxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxPQUFlO1FBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0lBQ3pCLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLE1BQXdCO1FBQ3JDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixJQUFJLE1BQU0sQ0FBQyxRQUFRLElBQUksSUFBSSxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN6RCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNyQztRQUNELEtBQUssTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTtZQUMvQixJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFOztzQkFDL0IsS0FBSyxHQUFhLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQztnQkFDcEQsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO29CQUNqQixLQUFLLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDMUM7YUFDRjtTQUNGO0lBQ0gsQ0FBQzs7OztJQUVELGVBQWU7UUFDYixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RCLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNmLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDL0I7SUFDSCxDQUFDO0NBQ0Y7OztJQXJHQyx3QkFBYTs7SUFDYixzQkFBVzs7SUFDWCx3QkFBYTs7SUFDYiwyQkFBa0I7O0lBQ2xCLDBCQUFpQjs7SUFDakIsMkJBQWdCOztJQUNoQiw0QkFBa0I7O0lBQ2xCLDBCQUFpRDs7SUFDakQsd0JBQTRDOztJQUM1Qyx3QkFBcUI7O0lBRXJCLDBCQUFlOztJQUNmLDJCQUFnQjs7SUFDaEIsNkJBQWtCOztJQUNsQiw0QkFBZ0I7O0lBQ2hCLDRCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm0gfSBmcm9tICcuL2Zvcm0nO1xuaW1wb3J0IHsgRmllbGRTZXQgfSBmcm9tICcuL2ZpZWxkLXNldCc7XG5cbmltcG9ydCB7IFZhbGlkYXRpb25SZXN1bHQgfSBmcm9tICcuL3ZhbGlkYXRpb24tcmVzdWx0LmludGVyZmFjZSc7XG5cbmltcG9ydCB7IEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5leHBvcnQgY2xhc3MgRm9ybUJhc2Uge1xuICBuYW1lOiBzdHJpbmc7XG4gIGlkOiBzdHJpbmc7XG4gIHR5cGU6IHN0cmluZztcbiAgY2xhc3Nlczogc3RyaW5nW107XG4gIGZpZWxkczogRmllbGRTZXQ7XG4gIG1lc3NhZ2U6IHN0cmluZztcbiAgcmVhZE9ubHk6IGJvb2xlYW47XG4gIGNoYW5nZTogKGZpZWxkOiBGb3JtQmFzZSwgdmFsdWU6IGFueSkgPT4gYm9vbGVhbjtcbiAgYmx1cjogKGZpZWxkOiBGb3JtQmFzZSwgdmFsdWU6IGFueSkgPT4gdm9pZDtcbiAgcHJvdGVjdGVkIGZvcm06IEZvcm07XG5cbiAgaXNGb3JtID0gZmFsc2U7XG4gIGhhc0RhdGEgPSBmYWxzZTtcbiAgaGFzRmllbGRzID0gZmFsc2U7XG4gIGhhc0xhYmVsID0gdHJ1ZTtcbiAgaXNBY3Rpb24gPSBmYWxzZTtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7XG4gICAgbmFtZT86IHN0cmluZyxcbiAgICBjbGFzc2VzPzogc3RyaW5nW10sXG4gICAgZmllbGRzPzogRm9ybUJhc2VbXSxcbiAgICByZWFkT25seT86IGJvb2xlYW4sXG4gICAgY2hhbmdlPzogKGZpZWxkOiBGb3JtQmFzZSwgdmFsdWU6IGFueSkgPT4gYm9vbGVhbixcbiAgICBibHVyPzogKGZpZWxkOiBGb3JtQmFzZSwgdmFsdWU6IGFueSkgPT4gdm9pZFxuICB9ID0ge30pIHtcbiAgICB0aGlzLm5hbWUgPSBvcHRpb25zLm5hbWUgfHwgJyc7XG4gICAgdGhpcy5pZCA9IHRoaXMubmFtZTtcbiAgICB0aGlzLmNsYXNzZXMgPSBvcHRpb25zLmNsYXNzZXMgfHwgW107XG4gICAgdGhpcy5maWVsZHMgPSBuZXcgRmllbGRTZXQob3B0aW9ucy5maWVsZHMgfHwgW10pO1xuICAgIHRoaXMucmVhZE9ubHkgPSBvcHRpb25zLnJlYWRPbmx5IHx8IGZhbHNlO1xuICAgIHRoaXMuY2hhbmdlID0gb3B0aW9ucy5jaGFuZ2UgfHwgbnVsbDtcbiAgICB0aGlzLmJsdXIgPSBvcHRpb25zLmJsdXIgfHwgbnVsbDtcbiAgfVxuXG4gIGdldEZvcm0oKTogRm9ybSB7XG4gICAgcmV0dXJuIHRoaXMuZm9ybTtcbiAgfVxuXG4gIHNldEZvcm0oZm9ybTogRm9ybSk6IHZvaWQge1xuICAgIHRoaXMuZm9ybSA9IGZvcm07XG4gICAgaWYgKGZvcm0ucHJlZml4KSB7XG4gICAgICB0aGlzLmlkID0gYCR7Zm9ybS5wcmVmaXh9JHt0aGlzLm5hbWV9YDtcbiAgICB9XG4gICAgaWYgKCF0aGlzLmlzRm9ybSkge1xuICAgICAgdGhpcy5maWVsZHMuc2V0Rm9ybShmb3JtKTtcbiAgICB9XG4gIH1cblxuICBhc3luYyBkZXJpdmVWYWx1ZSgpOiBQcm9taXNlPGJvb2xlYW4+IHtcbiAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcbiAgfVxuXG4gIGdldCBjb250cm9sKCk6IEZvcm1Db250cm9sIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIG9uQ2hhbmdlKHZhbHVlKSB7XG4gICAgbGV0IHByb3BhZ2F0ZSA9IHRydWU7XG4gICAgaWYgKHRoaXMuY2hhbmdlICE9IG51bGwpIHtcbiAgICAgIHByb3BhZ2F0ZSA9IHRoaXMuY2hhbmdlKHRoaXMsIHZhbHVlKTtcbiAgICB9XG4gICAgaWYgKHByb3BhZ2F0ZSkge1xuICAgICAgdGhpcy5mb3JtLmZpZWxkQ2hhbmdlKHRoaXMubmFtZSwgdmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIG9uQmx1cih2YWx1ZSkge1xuICAgIGlmICh0aGlzLmJsdXIgIT0gbnVsbCkge1xuICAgICAgdGhpcy5ibHVyKHRoaXMsIHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBvbkNsaWNrKCkge1xuXG4gIH1cblxuICBzZXRNZXNzYWdlKG1lc3NhZ2U6IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMubWVzc2FnZSA9IG1lc3NhZ2U7XG4gIH1cblxuICBsb2FkVmFsaWRhdGlvbihyZXN1bHQ6IFZhbGlkYXRpb25SZXN1bHQpOiB2b2lkIHtcbiAgICB0aGlzLnJlc2V0VmFsaWRhdGlvbigpO1xuICAgIGlmIChyZXN1bHQubWVzc2FnZXMgIT0gbnVsbCAmJiByZXN1bHQubWVzc2FnZXMubGVuZ3RoID4gMCkge1xuICAgICAgdGhpcy5zZXRNZXNzYWdlKHJlc3VsdC5tZXNzYWdlc1swXSk7XG4gICAgfVxuICAgIGZvciAoY29uc3Qga2V5IGluIHJlc3VsdC5maWVsZHMpIHtcbiAgICAgIGlmIChyZXN1bHQuZmllbGRzLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgY29uc3QgZmllbGQ6IEZvcm1CYXNlID0gdGhpcy5maWVsZHMuZmllbGRCeU5hbWUoa2V5KTtcbiAgICAgICAgaWYgKGZpZWxkICE9IG51bGwpIHtcbiAgICAgICAgICBmaWVsZC5sb2FkVmFsaWRhdGlvbihyZXN1bHQuZmllbGRzW2tleV0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmVzZXRWYWxpZGF0aW9uKCkge1xuICAgIHRoaXMuc2V0TWVzc2FnZShudWxsKTtcbiAgICBpZiAodGhpcy5maWVsZHMpIHtcbiAgICAgIHRoaXMuZmllbGRzLnJlc2V0VmFsaWRhdGlvbigpO1xuICAgIH1cbiAgfVxufVxuIl19