import { FormItemComponent } from './form-item.component';
import { TextareaField } from '../fields/textarea-field';
export declare class TextareaFieldComponent extends FormItemComponent {
    item: TextareaField;
}
