/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FieldGroup } from './field-group';
import { HeaderField } from './header-field';
import { FormAction } from './form-action';
var MultiChildField = /** @class */ (function (_super) {
    tslib_1.__extends(MultiChildField, _super);
    function MultiChildField(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.child = false;
        _this.counter = 0;
        _this.subForm = options['subForm'] || null;
        _this.newObj = options['newObj'] || null;
        _this.addButtonText = options['addButtonText'] || 'Add New Line';
        _this.removeButtonText = options['removeButtonText'] || 'Remove Line';
        _this.child = options['child'] || false;
        _this.classes.push('ndf-multichild');
        if (options['title']) {
            _this.fields.push(new HeaderField({
                title: options['title']
            }));
            _this.fields.push(new FieldGroup({
                name: _this.name + "_controls",
                fields: [
                    new FormAction({
                        title: _this.addButtonText,
                        icon: 'plus',
                        click: function () {
                            if (_this.subForm !== null && _this.newObj !== null) {
                                /** @type {?} */
                                var obj = _this.newObj();
                                _this.list.push(obj);
                                _this.addChild(obj);
                            }
                        },
                        classes: ['ndf-add']
                    })
                ],
                classes: ['ndf-full-bordered', 'ndf-multichild-footer']
            }));
        }
        return _this;
    }
    Object.defineProperty(MultiChildField.prototype, "list", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.form.obj[this.name] == null) {
                this.form.obj[this.name] = [];
            }
            return ((/** @type {?} */ (this.form.obj[this.name])));
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} form
     * @return {?}
     */
    MultiChildField.prototype.setForm = /**
     * @param {?} form
     * @return {?}
     */
    function (form) {
        var _this = this;
        _super.prototype.setForm.call(this, form);
        if (this.subForm !== null) {
            this.list.forEach(function (obj) {
                _this.addChild(obj);
            });
        }
    };
    /**
     * @param {?} result
     * @return {?}
     */
    MultiChildField.prototype.loadValidation = /**
     * @param {?} result
     * @return {?}
     */
    function (result) {
        this.resetValidation();
        for (var key in result.fields) {
            if (result.fields.hasOwnProperty(key)) {
                /** @type {?} */
                var child = this.form.prefix + "line_" + key;
                /** @type {?} */
                var field = this.fields.fieldByName(child);
                if (field != null) {
                    field.loadValidation(result.fields[key]);
                }
            }
        }
    };
    /**
     * @param {?} obj
     * @return {?}
     */
    MultiChildField.prototype.addChild = /**
     * @param {?} obj
     * @return {?}
     */
    function (obj) {
        var _this = this;
        /** @type {?} */
        var group = this.form.prefix + "line_" + this.counter + "_group";
        this.fields.insertBeforeId(new FieldGroup({
            name: group,
            fields: [
                this.subForm(obj, this.form.prefix + "line_" + this.counter),
                new FormAction({
                    title: this.removeButtonText,
                    icon: 'trash',
                    click: function () {
                        _this.fields.removeById(group);
                        _this.list.splice(_this.list.indexOf(obj), 1);
                    },
                    classes: ['ndf-remove']
                })
            ],
            classes: ['ndf-full-bordered', 'ndf-multichild-row', (this.child) ? 'ndf-child' : 'ndf-parent']
        }), this.name + "_controls");
        this.counter++;
    };
    return MultiChildField;
}(FieldGroup));
export { MultiChildField };
if (false) {
    /** @type {?} */
    MultiChildField.prototype.type;
    /** @type {?} */
    MultiChildField.prototype.subForm;
    /** @type {?} */
    MultiChildField.prototype.newObj;
    /** @type {?} */
    MultiChildField.prototype.addButtonText;
    /** @type {?} */
    MultiChildField.prototype.removeButtonText;
    /** @type {?} */
    MultiChildField.prototype.child;
    /** @type {?} */
    MultiChildField.prototype.counter;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGktY2hpbGQtZmllbGQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcmFpc2VkaWdpdGFsL25nLWR5bmFtaWMtZm9ybXMvIiwic291cmNlcyI6WyJsaWIvZmllbGRzL211bHRpLWNoaWxkLWZpZWxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUkzQztJQUFxQywyQ0FBVTtJQVM3Qyx5QkFBWSxPQUFnQjtRQUFoQix3QkFBQSxFQUFBLFlBQWdCO1FBQTVCLFlBQ0Usa0JBQU0sT0FBTyxDQUFDLFNBOEJmO1FBbENELFdBQUssR0FBRyxLQUFLLENBQUM7UUFDTixhQUFPLEdBQUcsQ0FBQyxDQUFDO1FBSWxCLEtBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksQ0FBQztRQUMxQyxLQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLENBQUM7UUFDeEMsS0FBSSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLElBQUksY0FBYyxDQUFDO1FBQ2hFLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsa0JBQWtCLENBQUMsSUFBSSxhQUFhLENBQUM7UUFDckUsS0FBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSyxDQUFDO1FBQ3ZDLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDcEMsSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDcEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxXQUFXLENBQUM7Z0JBQy9CLEtBQUssRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDO2FBQ3hCLENBQUMsQ0FBQyxDQUFDO1lBQ0osS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxVQUFVLENBQUM7Z0JBQzlCLElBQUksRUFBSyxLQUFJLENBQUMsSUFBSSxjQUFXO2dCQUM3QixNQUFNLEVBQUU7b0JBQ04sSUFBSSxVQUFVLENBQUM7d0JBQ2IsS0FBSyxFQUFFLEtBQUksQ0FBQyxhQUFhO3dCQUN6QixJQUFJLEVBQUUsTUFBTTt3QkFDWixLQUFLLEVBQUU7NEJBQ0wsSUFBSSxLQUFJLENBQUMsT0FBTyxLQUFLLElBQUksSUFBSSxLQUFJLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRTs7b0NBQzNDLEdBQUcsR0FBUSxLQUFJLENBQUMsTUFBTSxFQUFFO2dDQUM5QixLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQ0FDcEIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQzs2QkFDcEI7d0JBQ0gsQ0FBQzt3QkFDRCxPQUFPLEVBQUcsQ0FBQyxTQUFTLENBQUM7cUJBQ3RCLENBQUM7aUJBQ0g7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsbUJBQW1CLEVBQUUsdUJBQXVCLENBQUM7YUFDeEQsQ0FBQyxDQUFDLENBQUM7U0FDTDs7SUFDSCxDQUFDO0lBRUQsc0JBQUksaUNBQUk7Ozs7UUFBUjtZQUNFLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRTtnQkFDcEMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUMvQjtZQUNELE9BQU8sQ0FBQyxtQkFBQSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQWMsQ0FBQyxDQUFDO1FBQ2xELENBQUM7OztPQUFBOzs7OztJQUVELGlDQUFPOzs7O0lBQVAsVUFBUSxJQUFVO1FBQWxCLGlCQU9DO1FBTkMsaUJBQU0sT0FBTyxZQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BCLElBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxJQUFJLEVBQUU7WUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFHO2dCQUNwQixLQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3JCLENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7OztJQUVELHdDQUFjOzs7O0lBQWQsVUFBZSxNQUF3QjtRQUNyQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsS0FBSyxJQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFFO1lBQy9CLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7O29CQUMvQixLQUFLLEdBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLGFBQVEsR0FBSzs7b0JBQ3hDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7Z0JBQzVDLElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtvQkFDakIsS0FBSyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQzFDO2FBQ0Y7U0FDRjtJQUNILENBQUM7Ozs7O0lBRU8sa0NBQVE7Ozs7SUFBaEIsVUFBaUIsR0FBUTtRQUF6QixpQkFxQkM7O1lBcEJPLEtBQUssR0FBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sYUFBUSxJQUFJLENBQUMsT0FBTyxXQUFRO1FBQzdELElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUN4QixJQUFJLFVBQVUsQ0FBQztZQUNiLElBQUksRUFBRSxLQUFLO1lBQ1gsTUFBTSxFQUFFO2dCQUNOLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxhQUFRLElBQUksQ0FBQyxPQUFTLENBQUM7Z0JBQzVELElBQUksVUFBVSxDQUFDO29CQUNiLEtBQUssRUFBRSxJQUFJLENBQUMsZ0JBQWdCO29CQUM1QixJQUFJLEVBQUUsT0FBTztvQkFDYixLQUFLLEVBQUU7d0JBQ0wsS0FBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzlCLEtBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUM5QyxDQUFDO29CQUNELE9BQU8sRUFBRyxDQUFDLFlBQVksQ0FBQztpQkFDekIsQ0FBQzthQUNIO1lBQ0QsT0FBTyxFQUFFLENBQUMsbUJBQW1CLEVBQUUsb0JBQW9CLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDO1NBQ2hHLENBQUMsRUFBSyxJQUFJLENBQUMsSUFBSSxjQUFXLENBQzVCLENBQUM7UUFDRixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDakIsQ0FBQztJQUNILHNCQUFDO0FBQUQsQ0FBQyxBQTdGRCxDQUFxQyxVQUFVLEdBNkY5Qzs7OztJQTVGQywrQkFBbUI7O0lBQ25CLGtDQUE0Qzs7SUFDNUMsaUNBQWtCOztJQUNsQix3Q0FBc0I7O0lBQ3RCLDJDQUF5Qjs7SUFDekIsZ0NBQWM7O0lBQ2Qsa0NBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRmllbGRHcm91cCB9IGZyb20gJy4vZmllbGQtZ3JvdXAnO1xuaW1wb3J0IHsgSGVhZGVyRmllbGQgfSBmcm9tICcuL2hlYWRlci1maWVsZCc7XG5pbXBvcnQgeyBGb3JtQWN0aW9uIH0gZnJvbSAnLi9mb3JtLWFjdGlvbic7XG5pbXBvcnQgeyBGb3JtIH0gZnJvbSAnLi4vZm9ybSc7XG5pbXBvcnQgeyBWYWxpZGF0aW9uUmVzdWx0IH0gZnJvbSAnLi4vdmFsaWRhdGlvbi1yZXN1bHQuaW50ZXJmYWNlJztcblxuZXhwb3J0IGNsYXNzIE11bHRpQ2hpbGRGaWVsZCBleHRlbmRzIEZpZWxkR3JvdXAge1xuICB0eXBlOiAnbXVsdGljaGlsZCc7XG4gIHN1YkZvcm06IChvYmo6IGFueSwgcHJlZml4OiBzdHJpbmcpID0+IEZvcm07XG4gIG5ld09iajogKCkgPT4gYW55O1xuICBhZGRCdXR0b25UZXh0OiBzdHJpbmc7XG4gIHJlbW92ZUJ1dHRvblRleHQ6IHN0cmluZztcbiAgY2hpbGQgPSBmYWxzZTtcbiAgcHJpdmF0ZSBjb3VudGVyID0gMDtcblxuICBjb25zdHJ1Y3RvcihvcHRpb25zOiB7fSA9IHt9KSB7XG4gICAgc3VwZXIob3B0aW9ucyk7XG4gICAgdGhpcy5zdWJGb3JtID0gb3B0aW9uc1snc3ViRm9ybSddIHx8IG51bGw7XG4gICAgdGhpcy5uZXdPYmogPSBvcHRpb25zWyduZXdPYmonXSB8fCBudWxsO1xuICAgIHRoaXMuYWRkQnV0dG9uVGV4dCA9IG9wdGlvbnNbJ2FkZEJ1dHRvblRleHQnXSB8fCAnQWRkIE5ldyBMaW5lJztcbiAgICB0aGlzLnJlbW92ZUJ1dHRvblRleHQgPSBvcHRpb25zWydyZW1vdmVCdXR0b25UZXh0J10gfHwgJ1JlbW92ZSBMaW5lJztcbiAgICB0aGlzLmNoaWxkID0gb3B0aW9uc1snY2hpbGQnXSB8fCBmYWxzZTtcbiAgICB0aGlzLmNsYXNzZXMucHVzaCgnbmRmLW11bHRpY2hpbGQnKTtcbiAgICBpZiAob3B0aW9uc1sndGl0bGUnXSkge1xuICAgICAgdGhpcy5maWVsZHMucHVzaChuZXcgSGVhZGVyRmllbGQoe1xuICAgICAgICB0aXRsZTogb3B0aW9uc1sndGl0bGUnXVxuICAgICAgfSkpO1xuICAgICAgdGhpcy5maWVsZHMucHVzaChuZXcgRmllbGRHcm91cCh7XG4gICAgICAgIG5hbWU6IGAke3RoaXMubmFtZX1fY29udHJvbHNgLFxuICAgICAgICBmaWVsZHM6IFtcbiAgICAgICAgICBuZXcgRm9ybUFjdGlvbih7XG4gICAgICAgICAgICB0aXRsZTogdGhpcy5hZGRCdXR0b25UZXh0LFxuICAgICAgICAgICAgaWNvbjogJ3BsdXMnLFxuICAgICAgICAgICAgY2xpY2s6ICgpID0+IHtcbiAgICAgICAgICAgICAgaWYgKHRoaXMuc3ViRm9ybSAhPT0gbnVsbCAmJiB0aGlzLm5ld09iaiAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIGNvbnN0IG9iajogYW55ID0gdGhpcy5uZXdPYmooKTtcbiAgICAgICAgICAgICAgICB0aGlzLmxpc3QucHVzaChvYmopO1xuICAgICAgICAgICAgICAgIHRoaXMuYWRkQ2hpbGQob2JqKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNsYXNzZXMgOiBbJ25kZi1hZGQnXVxuICAgICAgICAgIH0pXG4gICAgICAgIF0sXG4gICAgICAgIGNsYXNzZXM6IFsnbmRmLWZ1bGwtYm9yZGVyZWQnLCAnbmRmLW11bHRpY2hpbGQtZm9vdGVyJ11cbiAgICAgIH0pKTtcbiAgICB9XG4gIH1cblxuICBnZXQgbGlzdCgpOiBBcnJheTxhbnk+IHtcbiAgICBpZiAodGhpcy5mb3JtLm9ialt0aGlzLm5hbWVdID09IG51bGwpIHtcbiAgICAgIHRoaXMuZm9ybS5vYmpbdGhpcy5uYW1lXSA9IFtdO1xuICAgIH1cbiAgICByZXR1cm4gKHRoaXMuZm9ybS5vYmpbdGhpcy5uYW1lXSBhcyBBcnJheTxhbnk+KTtcbiAgfVxuXG4gIHNldEZvcm0oZm9ybTogRm9ybSk6IHZvaWQge1xuICAgIHN1cGVyLnNldEZvcm0oZm9ybSk7XG4gICAgaWYgKHRoaXMuc3ViRm9ybSAhPT0gbnVsbCkge1xuICAgICAgdGhpcy5saXN0LmZvckVhY2goKG9iaikgPT4ge1xuICAgICAgICB0aGlzLmFkZENoaWxkKG9iaik7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBsb2FkVmFsaWRhdGlvbihyZXN1bHQ6IFZhbGlkYXRpb25SZXN1bHQpOiB2b2lkIHtcbiAgICB0aGlzLnJlc2V0VmFsaWRhdGlvbigpO1xuICAgIGZvciAoY29uc3Qga2V5IGluIHJlc3VsdC5maWVsZHMpIHtcbiAgICAgIGlmIChyZXN1bHQuZmllbGRzLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgY29uc3QgY2hpbGQgPSBgJHt0aGlzLmZvcm0ucHJlZml4fWxpbmVfJHtrZXl9YDtcbiAgICAgICAgY29uc3QgZmllbGQgPSB0aGlzLmZpZWxkcy5maWVsZEJ5TmFtZShjaGlsZCk7XG4gICAgICAgIGlmIChmaWVsZCAhPSBudWxsKSB7XG4gICAgICAgICAgZmllbGQubG9hZFZhbGlkYXRpb24ocmVzdWx0LmZpZWxkc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgYWRkQ2hpbGQob2JqOiBhbnkpOiB2b2lkIHtcbiAgICBjb25zdCBncm91cCA9IGAke3RoaXMuZm9ybS5wcmVmaXh9bGluZV8ke3RoaXMuY291bnRlcn1fZ3JvdXBgO1xuICAgIHRoaXMuZmllbGRzLmluc2VydEJlZm9yZUlkKFxuICAgICAgbmV3IEZpZWxkR3JvdXAoe1xuICAgICAgICBuYW1lOiBncm91cCxcbiAgICAgICAgZmllbGRzOiBbXG4gICAgICAgICAgdGhpcy5zdWJGb3JtKG9iaiwgYCR7dGhpcy5mb3JtLnByZWZpeH1saW5lXyR7dGhpcy5jb3VudGVyfWApLFxuICAgICAgICAgIG5ldyBGb3JtQWN0aW9uKHtcbiAgICAgICAgICAgIHRpdGxlOiB0aGlzLnJlbW92ZUJ1dHRvblRleHQsXG4gICAgICAgICAgICBpY29uOiAndHJhc2gnLFxuICAgICAgICAgICAgY2xpY2s6ICgpID0+IHtcbiAgICAgICAgICAgICAgdGhpcy5maWVsZHMucmVtb3ZlQnlJZChncm91cCk7XG4gICAgICAgICAgICAgIHRoaXMubGlzdC5zcGxpY2UodGhpcy5saXN0LmluZGV4T2Yob2JqKSwgMSk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgY2xhc3NlcyA6IFsnbmRmLXJlbW92ZSddXG4gICAgICAgICAgfSlcbiAgICAgICAgXSxcbiAgICAgICAgY2xhc3NlczogWyduZGYtZnVsbC1ib3JkZXJlZCcsICduZGYtbXVsdGljaGlsZC1yb3cnLCAodGhpcy5jaGlsZCkgPyAnbmRmLWNoaWxkJyA6ICduZGYtcGFyZW50J11cbiAgICAgIH0pLCBgJHt0aGlzLm5hbWV9X2NvbnRyb2xzYFxuICAgICk7XG4gICAgdGhpcy5jb3VudGVyKys7XG4gIH1cbn1cbiJdfQ==