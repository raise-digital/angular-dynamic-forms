/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormItemComponent } from './form-item.component';
import { HeaderField } from '../fields/header-field';
import { FormService } from '../services/form.service';
var HeaderFieldComponent = /** @class */ (function (_super) {
    tslib_1.__extends(HeaderFieldComponent, _super);
    function HeaderFieldComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HeaderFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-header-field',
                    template: "<div [formGroup]=\"group\" [ngClass]=\"item.classes\">\n    <h3>{{ item.title }}</h3>\n    <p class=\"ndf-error\" *ngIf=\"item.message\">{{ item.message }}</p>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    HeaderFieldComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return HeaderFieldComponent;
}(FormItemComponent));
export { HeaderFieldComponent };
if (false) {
    /** @type {?} */
    HeaderFieldComponent.prototype.item;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLWZpZWxkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2hlYWRlci1maWVsZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVqRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDckQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBRXZEO0lBSzBDLGdEQUFpQjtJQUwzRDs7SUFPQSxDQUFDOztnQkFQQSxTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIscUxBQTRDO29CQUM1QyxTQUFTLEVBQUUsQ0FBRSxXQUFXLENBQUU7aUJBQzNCOzs7dUJBRUUsS0FBSzs7SUFDUiwyQkFBQztDQUFBLEFBUEQsQ0FLMEMsaUJBQWlCLEdBRTFEO1NBRlksb0JBQW9COzs7SUFDL0Isb0NBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vZm9ybS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBIZWFkZXJGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9oZWFkZXItZmllbGQnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZGYtaGVhZGVyLWZpZWxkJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2hlYWRlci1maWVsZC5jb21wb25lbnQuaHRtbCcsXG4gIHByb3ZpZGVyczogWyBGb3JtU2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIEhlYWRlckZpZWxkQ29tcG9uZW50IGV4dGVuZHMgRm9ybUl0ZW1Db21wb25lbnQge1xuICBASW5wdXQoKSBpdGVtOiBIZWFkZXJGaWVsZDtcbn1cbiJdfQ==