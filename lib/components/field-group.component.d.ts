import { FormItemComponent } from './form-item.component';
import { FieldGroup } from '../fields/field-group';
export declare class FieldGroupComponent extends FormItemComponent {
    item: FieldGroup;
}
