/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormBase } from '../form-base';
import { FormService } from '../services/form.service';
var FormItemComponent = /** @class */ (function () {
    function FormItemComponent(formService) {
        this.formService = formService;
    }
    /**
     * @return {?}
     */
    FormItemComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.item.hasData) {
            this.formService.registerField((/** @type {?} */ (this.item)));
        }
    };
    /**
     * @return {?}
     */
    FormItemComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.item.hasData) {
            this.formService.deregisterField((/** @type {?} */ (this.item)));
        }
    };
    Object.defineProperty(FormItemComponent.prototype, "group", {
        get: /**
         * @return {?}
         */
        function () {
            return this.formService.group;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} value
     * @return {?}
     */
    FormItemComponent.prototype.onChange = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.item.onChange(value);
    };
    /**
     * @param {?} value
     * @return {?}
     */
    FormItemComponent.prototype.onBlur = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.item.onBlur(value);
    };
    /**
     * @return {?}
     */
    FormItemComponent.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this.item.onClick();
    };
    FormItemComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ndf-form-item',
                    template: "<div [ngSwitch]=\"item.type\">\n    <ndf-checkbox-field *ngSwitchCase=\"'checkbox'\" [item]=\"item\"></ndf-checkbox-field>\n    <ndf-checkbox-set-field *ngSwitchCase=\"'checkboxset'\" [item]=\"item\"></ndf-checkbox-set-field>\n    <ndf-content-field *ngSwitchCase=\"'content'\" [item]=\"item\"></ndf-content-field>\n    <ndf-date-field *ngSwitchCase=\"'date'\" [item]=\"item\"></ndf-date-field>\n    <ndf-dropdown-field *ngSwitchCase=\"'dropdown'\" [item]=\"item\"></ndf-dropdown-field>\n    <ndf-field-group *ngSwitchCase=\"'group'\" [item]=\"item\"></ndf-field-group>\n    <ndf-field-group *ngSwitchCase=\"'multichild'\" [item]=\"item\"></ndf-field-group>\n    <ndf-form-action *ngSwitchCase=\"'action'\" [item]=\"item\"></ndf-form-action>\n    <ndf-header-field *ngSwitchCase=\"'header'\" [item]=\"item\"></ndf-header-field>\n    <ndf-option-set-field *ngSwitchCase=\"'optionset'\" [item]=\"item\"></ndf-option-set-field>\n    <ndf-text-field *ngSwitchCase=\"'text'\" [item]=\"item\"></ndf-text-field>\n    <ndf-textarea-field *ngSwitchCase=\"'textarea'\" [item]=\"item\"></ndf-textarea-field>\n</div>\n",
                    providers: [FormService]
                }] }
    ];
    /** @nocollapse */
    FormItemComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    FormItemComponent.propDecorators = {
        item: [{ type: Input }]
    };
    return FormItemComponent;
}());
export { FormItemComponent };
if (false) {
    /** @type {?} */
    FormItemComponent.prototype.item;
    /** @type {?} */
    FormItemComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1pdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0ByYWlzZWRpZ2l0YWwvbmctZHluYW1pYy1mb3Jtcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Zvcm0taXRlbS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUdwRSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBRXhDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUV2RDtJQVFFLDJCQUFvQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUFHLENBQUM7Ozs7SUFFaEQsb0NBQVE7OztJQUFSO1FBQ0UsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNyQixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxtQkFBQSxJQUFJLENBQUMsSUFBSSxFQUFjLENBQUMsQ0FBQztTQUN6RDtJQUNILENBQUM7Ozs7SUFFRCx1Q0FBVzs7O0lBQVg7UUFDRSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLG1CQUFBLElBQUksQ0FBQyxJQUFJLEVBQWMsQ0FBQyxDQUFDO1NBQzNEO0lBQ0gsQ0FBQztJQUVELHNCQUFJLG9DQUFLOzs7O1FBQVQ7WUFDRSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1FBQ2hDLENBQUM7OztPQUFBOzs7OztJQUVELG9DQUFROzs7O0lBQVIsVUFBUyxLQUFLO1FBQ1osSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDNUIsQ0FBQzs7Ozs7SUFFRCxrQ0FBTTs7OztJQUFOLFVBQU8sS0FBSztRQUNWLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzFCLENBQUM7Ozs7SUFFRCxtQ0FBTzs7O0lBQVA7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3RCLENBQUM7O2dCQXBDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGVBQWU7b0JBQ3pCLDhsQ0FBeUM7b0JBQ3pDLFNBQVMsRUFBRSxDQUFFLFdBQVcsQ0FBRTtpQkFDM0I7Ozs7Z0JBTlEsV0FBVzs7O3VCQVFqQixLQUFLOztJQStCUix3QkFBQztDQUFBLEFBckNELElBcUNDO1NBaENZLGlCQUFpQjs7O0lBQzVCLGlDQUF3Qjs7SUFFWix3Q0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUdyb3VwIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5pbXBvcnQgeyBGb3JtQmFzZSB9IGZyb20gJy4uL2Zvcm0tYmFzZSc7XG5pbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4uL2ZpZWxkcy9maWVsZCc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25kZi1mb3JtLWl0ZW0nLFxuICB0ZW1wbGF0ZVVybDogJy4vZm9ybS1pdGVtLmNvbXBvbmVudC5odG1sJyxcbiAgcHJvdmlkZXJzOiBbIEZvcm1TZXJ2aWNlIF1cbn0pXG5leHBvcnQgY2xhc3MgRm9ybUl0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIGl0ZW06IEZvcm1CYXNlO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlKSB7fVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGlmICh0aGlzLml0ZW0uaGFzRGF0YSkge1xuICAgICAgdGhpcy5mb3JtU2VydmljZS5yZWdpc3RlckZpZWxkKHRoaXMuaXRlbSBhcyBGaWVsZDxhbnk+KTtcbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgICBpZiAodGhpcy5pdGVtLmhhc0RhdGEpIHtcbiAgICAgIHRoaXMuZm9ybVNlcnZpY2UuZGVyZWdpc3RlckZpZWxkKHRoaXMuaXRlbSBhcyBGaWVsZDxhbnk+KTtcbiAgICB9XG4gIH1cblxuICBnZXQgZ3JvdXAoKTogRm9ybUdyb3VwIHtcbiAgICByZXR1cm4gdGhpcy5mb3JtU2VydmljZS5ncm91cDtcbiAgfVxuXG4gIG9uQ2hhbmdlKHZhbHVlKSB7XG4gICAgdGhpcy5pdGVtLm9uQ2hhbmdlKHZhbHVlKTtcbiAgfVxuXG4gIG9uQmx1cih2YWx1ZSkge1xuICAgIHRoaXMuaXRlbS5vbkJsdXIodmFsdWUpO1xuICB9XG5cbiAgb25DbGljaygpIHtcbiAgICB0aGlzLml0ZW0ub25DbGljaygpO1xuICB9XG59XG4iXX0=